---
title: "A Note on Peer Review"
date: 2021-06-24
draft: true
---

Every once in a while, the topic of peer review (and, usually, why it's Horrible&trade;) comes up on Hacker News, and I guess pretty much everywhere else. [Here](https://news.ycombinator.com/item?id=23280372) is a recent example. A typical quote begins:

> Peer review is completely broken nowadays [...]

Obviously, since I'm writing this note, I disagree. I do think that peer review is *misunderstood* --- sometimes (very unfortunately) by reviews and authors, often by the informed public, and essentially always by the media. Below, I'm going to try to lay out how I think of peer review in the age of arXiv and Twitter.

First, it's important to understand that science, like everything else, is massively heterogeneous. There are enormous differences in norms, habits, and so on even between physics and astrophysics, both very quantitative, computational, "give me plots not words" fields. There are also rather large differences between different institutions and different generations. I can't speak with any confidence to how things work outside of my corner (nuclear theory). Take everything below with that grain of salt.

TODO

I think it's a good idea, at this point, to think about the *origin* of journals (and peer review). TODO

What role does formal peer review play in this system? TODO

It's not clear if this situation is stable. Why not just neglect journal submissions altogether? There's a weak incentive to get your papers into journals: it looks weird, when applying for jobs (and funding), if you don't. Younger folks care less about this than older faculty, though, so it's possible that the incentive to shepherd papers through the review process will get weaker over time, diluting one of the few *formal* mechanicsms for accountability.

