---
title: "The Spectral Density Function of a Correlator"
date: 2021-10-14
tags: ['Physics']
---

The correlation function, at temperature \\(\beta^{-1}\\), of an operator \\(\mathcal O\\) is defined to be
$$
C(t) \equiv \langle \mathcal O(t) \mathcal O(0)\rangle
= \mathop{\mathrm{Tr}} e^{-\beta H} e^{i H t} \mathcal O e^{-i H t} \mathcal O
\text,
$$
where \\(H\\) is the Hamiltonian and we can safely ignore the normalization of \\(e^{-\beta H}\\) (because we'll only ever consider the one temperature).

This is often referred to specifically as the <em>Minkowski</em> or <em>real-time</em> correlation function (sometimes the retarded Green's function if you're feeling fancy), to contrast with the <em>Euclidean</em>/<em>imaginary-time</em> correlation function, which is much easier to extract in lattice calculations.
$$
C_E(\tau) \equiv \langle \mathcal O(\tau) \mathcal O(0)\rangle
= \mathop{\mathrm{Tr}} e^{-\beta H} e^{\tau H} \mathcal O e^{-\tau H} \mathcal O
$$
The Euclidean correlator, written this way, is defined only for \\(0 \le \tau \le \beta\\).

These two correlators are obviously connected by analytic continuation: \\(C_E(\tau) = C(-i \tau)\\). In fact they can both be obtained from a physically sensible underlying object termed the <em>spectral density</em> (or spectral function, or spectral density function to be safe).

First let's look at the imaginary-time correlator. I'll pretend we have a finite number of states all at different energies, since it doesn't change any of the physics and allows me to use a manageable notation. The correlator can be expanded into a double sum of states like so:
$$
C_E(\tau) = \sum_{E,E'} e^{-\beta E}e^{\tau E}e^{-\tau E'}  \big|\langle E| \mathcal O |E' \rangle \big|^2 = \sum_{E,\omega} e^{-\beta E} e^{-\tau\omega} \big|\langle E| \mathcal O |E+\omega \rangle \big|^2
$$

Now let's treat the real-time correlator in the same way. The expression is not very different, of course.
$$
C(t) = \sum_{E,\omega} e^{-\beta E} e^{-i t \omega} \big|\langle E| \mathcal O |E+\omega \rangle \big|^2
$$

From the similarity of the expressions for \\(C(t)\\) and \\(C_E(\tau)\\), we see that it is reasonable to pull out a function \\(f(\omega)\\), whose Fourier transform is the real-time correlator, and whose Laplace transform is the imaginary-time correlator.
$$
f(\omega) \equiv \sum_E e^{-\beta E} \big|\langle E |\mathcal O | E + \omega\rangle\big|^2
$$

The original Euclidean correlation function was symmetric under \\(\tau \rightarrow \beta-\tau\\). The symmetry manifests in \\(f(\omega)\\) as
$$
f(-\omega) = e^{-\beta\omega} f(\omega)\text.
$$

It's convenient not to work directly with \\(f(\omega)\\), but rather a symmetrized form of it defined only on \\(\mathbb R_+\\).
$$
\rho(\omega) =
\sum_E e^{-\beta E}\left[\big|\langle E | \mathcal O | E + \omega\rangle\big|^2 e^{-\beta\omega/2} + \big|\langle E | \mathcal O | E - \omega\rangle\big|^2 e^{\beta\omega/2}\right]
$$
In terms of \\(\rho(\omega)\\) or \\(f(\omega)\\), the correlation function is given by
$$
C(t) = \int_{-\infty}^\infty d\omega\\; e^{-it\omega} f(\omega)= \int_0^\infty d\omega\\; \cosh\Big(\frac{\beta\omega}{2} - i t \omega\Big) \rho(\omega)
$$
Note, of course, that \\(\rho(\omega)\\) is also a function of the temperature and chosen operator.

I haven't actually said what the spectral function is yet! That's because some people call \\(f(\cdot)\\) the spectral function, some people call \\(\rho(\cdot)\\) the spectral function, and there seem to be at least two other conventions in the literature that I can't quite work out. Broadly speaking, any object that looks like, and contains the same information as, \\(\rho(\cdot)\\), will be referred to as "the spectral function". The key point is that it contains the underlying information about the operator from which the two-point function can be reconstructed, either in real or imaginary time.
