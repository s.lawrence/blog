---
title: "Links for December 2022"
date: 2022-12-27
tags: ['Links']
---

Hanson [on Christmas](https://www.overcomingbias.com/2022/12/explaining-christmas.html).

Game of Life, [recursive](https://oimo.io/works/life/).

[On school closures](https://www.tabletmag.com/sections/news/articles/recess-responsibility).

Deveraux on the [withering](https://acoup.blog/2022/12/23/fireside-friday-december-23-2022-whither-history/) of academic history.

[On Oppenheimer](https://blog.nuclearsecrecy.com/2022/12/21/oppenheimer-vacated-but-not-vindicated/).

