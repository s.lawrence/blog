---
title: "Links for January 2021"
date: 2021-01-27
tags: ['Links']
---

Gil Kalai gives [a primer](https://gilkalai.wordpress.com/2020/12/29/the-argument-against-quantum-computers-a-very-short-introduction/) on his argument against the existence of quantum computers. My understanding is that a couple of decades ago, he got the sense that the role of "QC skeptic" was under-inhabited, and decided he'd take up the cause. I'm not really qualified to comment on the question of whether HQCA is possible with near-term devices, but I certainly agree that the role of "QC skeptic" is underplayed nowadays. Relative to the number of advocates, the problem is worse today, not better.

In a society with falling levels of trust, policies should [do what they say on the tin](https://www.slowboring.com/p/making-policy-for-a-low-trust-world). More generally, _legibility_ trumps transparency. If you make all decisions in public meetings, that's transparent. But if the meetings are held in Klingon, it's not legible.

[Differential dataflow](https://timelydataflow.github.io/differential-dataflow/introduction.html) is a Rust library for performing computations that automatically update when the input data updates.

Relatedly, [here's](https://fasterthanli.me/articles/a-half-hour-to-learn-rust) a wonderful Rust tutorial. This is my prefered format for a programming tutorial: rapid-fire examples. When reading most tutorials, I skim through (often skipping chapters) to get to the meat quickly.

[More on the great stagnation](https://elidourado.com/blog/notes-on-technology-2020s/) and prospects for the next decade of advancement.

Oh, and someone named Scott Something has [returned to blogging](https://astralcodexten.substack.com/).
