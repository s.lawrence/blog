---
title: "Lies That (Probably) Don't Matter"
date: 2020-12-19
---

Here are a few commonly repeated, casually believed statements (in or adjacent to physics), which are unambiguously false but extraordinarily useful. Most of the time, the fact that they're false doesn't matter at all; hence the "commonly repeated/extraordinarily useful" part. But once in a while, they might matter. At the very least, it's fun to keep them in mind.

**The universe has no privileged reference frame.** Yes, it does (and it's not Lorentz-invariant). Relatedly, [this](https://en.wikipedia.org/wiki/Cosmic_microwave_background#/media/File:WMAP_2010.png) is not a picture of the CMB. The [Cosmic Microwave Background](https://en.wikipedia.org/wiki/Cosmic_microwave_background) has an enormous dipole moment, because the Earth is at motion with respect to the universe's (locally) privileged reference frame. Photons on one side of the sky are mostly coming toward us, and so are higher energy than photons on the other side of the sky. That picture is a picture of the CMB with the dipole moment subtracted.

If the universe didn't have a privileged reference frame, then either there would be no CMB, or we'd be immediately ripped apart by infinitely blueshifted cosmic rays.  

Cosmology also breaks time-translation invariance, but that's much more obvious.

**Anything about the infinite volume limit.** Commonly in the theory of phase transitions, for instance, there's a step where the thermodynamic limit is taken. Similarly, computational complexity is founded on the nearly exclusive study of _asymptotic_ behavior. Of course these are only (very very good) approximations to reality. The conflict between computational complexity and physics (which [isn't just](https://arxiv.org/abs/1206.6559) about a finite universe) sometimes gets attention; I'm not aware of any reason to care about the conflict between thermodynamics and cosmology. Maybe at very late times, the number of particles within your event horizon is below any reasonably thermodynamic limit? Seems pretty uninteresting.

I suppose that black hole formation matters for thermodynamics too. Because a "small" system is only approximately thermodynamic, and black holes prevent systems from getting too large, there's some sort of fundamental limit on how accurate the thermodynamic approximation can be before a black hole forms. Again, probably consequence-free.

General relativity is generally a great killer of general beliefs. Energy conservation [gets mauled](https://www.preposterousuniverse.com/blog/2010/02/22/energy-is-not-conserved/), for instance.

**Church-Turing thesis.** Thanks to the finite volume/lifespan of the visible universe, most computable functions can't actually be computed.

Worrying about cosmology is getting boring. Even ignoring that, I should mention **the extended Church-Turing thesis.** (That is, the claim that Turing machines can efficiently perform any task the universe can efficiently perform.) Generally believed to be false now thanks to quantum computers, but still "mostly true" for natural problems. If you take anthropic arguments too seriously, this principle runs into some trouble there, too.

**The classification of phase transitions.** This is a complete mess, and can't really be summarized here. To start with, there is no "the" classification of phrase transitions, because there are several conflicting conventions --- go read the wikipedia article if you want to cry. More importantly: the point of a classification is to say "objects of type A all have properties A1, A2, and A3, whereas objects of type B have properties B1 and B2 instead". If you can do this, then the categories don't change much when you tweak the definition. With phase transitions, this just _cannot be done_. The most prominent example is the [BKT transition](https://en.wikipedia.org/wiki/Kosterlitz%E2%80%93Thouless_transition). It's common to say (and you may have heard in class) that divergent correlation lengths, and divergent susceptibilities, occur in the same systems. With the most common (and most insane) naming scheme, either property can be taken as the definition of "second-order transition". But, the BKT transition has a divergent correlation length without a divergent susceptibility; in fact, the partition function is smooth (but non-analytic).

Some other trivia about the difficulty of classifying transitions. Simple discontinuities (as opposed to smooth divergences) can occur in any derivative of the partition function: it needn't be a first derivative. First-order transitions need not have metastable phases ([possible example](https://arxiv.org/abs/2006.14545)). No type of phase transition is necessarily associated to an order parameter. Both sides of a first-order transition can be "in the same phase", in the sense that there's a smooth change of parameters to get from one side to the other, without crossing the transition.

A bit more niche, but the phrase **"low-energy Hilbert space"** [is problematic](/harmonic-oscillator-weirdness).

And, of course, I must mention [Tsirelson's problem](https://en.wikipedia.org/wiki/Tsirelson%27s_bound#Tsirelson's_problem), now apparently answered in the negative --- giving lie to pretty much everybody's understanding of quantum mechanics. But that's a topic for its own post...
