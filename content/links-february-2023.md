---
title: "Links for February 2023"
date: 2023-02-28
tags: ['Links']
---

Hanson on [modern bandits](https://www.overcomingbias.com/2023/02/why-is-everyone-so-boring.html).

[Cowen on the Twitter files](https://marginalrevolution.com/marginalrevolution/2023/02/why-was-i-bored-by-the-twitter-files.html). Straussian, although of course I'm not sure it was intended that way.

Between 1940 and 1962, somewhere between one-third and one-half of adult deaths among the Gebusi in New Guinea were due to homicide. From 1989 to 2017, there were no recorded homicides at all. Buckner [details](https://traditionsofconflict.substack.com/p/gebusi-homicide-and-the-cultural) the causes with a focus on economics and culture, and noting the absence of police presence among the Gebusi. See also the [original paper](https://hal.science/hal-03605911/document), which concludes in part:

> In rural areas of many developing nations, modern
> cultural development includes increasingly explicit demands for monetary
> compensation in cases of dispute, along with de-collectivized individuation of
> grievances.

Comments [from 2021](https://www.thedrive.com/the-war-zone/40054/adversary-drones-are-spying-on-the-u-s-and-the-pentagon-acts-like-theyre-ufos) on UFOs, in which Tyler Rogoway claims:

> it is clear that a very terrestrial adversary is toying with us in our own backyard using relatively simple technologies—drones and balloons—and making off with what could be the biggest intelligence haul of a generation.

I believe this is what's referred to as "calling it".

[Zvi's notes](https://thezvi.wordpress.com/2023/02/21/ai-1-sydney-and-bing/) on
Bing/Sidney are solidly hilarious. It's also worth skimming the whole thing
just to learn that yes, this is happening. My only comment is that after seeing
both ChatGPT and Sidney in action, I am now _less_ worried about doomsday-like
AGI scenarios. At least, I think there is a low probability that any serious
harm comes out of LLMs, even if scaling continues for the next decade. I am
also more worried than I used to be about the potential for missing good
things, from fear of (and eventually regulation of) AIs.

