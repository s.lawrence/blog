---
title: "Links for October 2022"
date: 2022-10-28
tags: ['Links']
---

[Data on (perceptions of) children](https://www.brookings.edu/blog/up-front/2020/10/07/americans-are-more-worried-about-their-sons-than-their-daughters/amp/), broken down by gender and political affiliation. Read plots; ignore text.

Data on [what squares grandmasters and computers move on](https://i.redd.it/kr09xbqth6w91.png). (Bd2 and Be2 are overrated!)

