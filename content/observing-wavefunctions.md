---
title: "Observing Wavefunctions"
date: 2020-10-27
tags: ['Physics']
---

At a conference a couple years ago, I was told this story of an argument that occured during a seminar. The speaker was listing measurable properties of a system (you know, "observables") and included the wavefunction in this list. Naturally, he was challenged by an audience member. There is no Hermitian operator whose measurement reveals the wavefunction, so of course it can't be considered an observable. The speaker countered with some form of "who cares? I can measure it". The two go several more rounds, with the questioner trying to get the speaker to admit that the wavefunction is "not an observable", the speaker trying to get the questioner to admit that it can (under appropriate circumstances) be measured, and absolutely no progress being made.

After a few rounds of this, the speaker made the mistake of again using the word "observable", instead of simply claiming "it can be measured". At this point, the questioner pulled out his phone, set it to record, and challenged his opponent, "say the wavefunction is an observable. Say it one more time, I dare you!" (#mycameraismyweapon?)

So, is the wavefunction an observable? Can it be measured? Are the two concepts the same? (They really ought to be!)

Suppose it's your birthday, and you receive a little quantum system in a box as a present (happy birthday!). There's no way for you to determine the wavefunction. You can measure the expectation values of a set of commuting Hermitian operators, but after those measurements, the wavefunction has changed in an unpredictable way. Moreover, the no cloning theorem prevents you from making a copy of the wavefunction before doing destructive measurements. It would be a _faux pas_ to go back to your friend and ask for another copy of the same system (hey, it was hand-made!), so you're just gonna have to live in ignorance.

Thus the wavefunction of the quantum system is not measurable. We already know it's not "an observable" (in the sense of there being no Hermitian operator blah blah blah), so the two concepts match. Good!

What was the speaker on about? Well, some friends are better than others. A less-devoted friend might just give you a store-bought quantum system. If you break it, you can always go to the store and get another one. If you trust the manufacturing process to be consistent, you can perform destructive measurements to your heart's content. This is called quantum tomography: as long as you have a large number of systems, which you know share the same wavefunction, you can determine that wavefunction to arbitrary precision by measuring all different expectation values.

To summarize: the wavefunction of a system is neither an observable nor a measurable quantity. However, if you have a black box that produces quantum systems in some reliable, reproducible way, then the wavefunction being produced _can_ be measured, and should be called an observable. The distinction to bear in mind is that although we talk about measuring "the wavefunction of the quantum system", what we're really measuring is a property of the machine producing the systems! The fact that there's no corresponding Hermitian operator acting on the quantum system, isn't concerning at all. The operator of interest --- the expectation value we're measuring when we do quantum tomography --- acts on the black box itself.

One last point. Suppose you have an infinite volume system, in the ground state for simplicity. Oh, let's also assume the system is gapped (no arbitrarily long-range correlations). Now, although this is "only one" quantum system, since it's in the infinite volume limit you can divide it into arbitrarily many, arbitrarily large, arbitrarily well-separated pieces. The wavefunction of the vacuum _is_ an observable, after all!
