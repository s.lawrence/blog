---
title: "Links for June 2022"
date: 2022-06-27
tags: ['Links']
---

[Hanson on selection biases](https://www.overcomingbias.com/2022/05/argument-selection-bias.html) for arguments. A closely related principle: hearing a well-thought-out, but ultimately unconvincing, argument for X can often give you more confidence in not-X.

[Trying to get](https://astralcodexten.substack.com/p/a-guide-to-asking-robots-to-design?s=r) a robot to correctly interpret your request. I'm far from the first to note this, but Asimov's concept of "robopsychology" is suddenly incredibly relevant.

[Zhao Yanjing](https://www.readingthechinadream.com/zhao-yanjing-on-chinas-real-estate-crisis.html) on housing in China. It hits some familiar notes: it seems that the same lessons are being learned in many countries. The piece also hits some exceedingly _unfamiliar_ notes (which may be unfamiliar for a good reason). Also, there's this lovely quote:

> There is a danger that external shocks such as the epidemic or deteriorating international relations will be used to explain the recent economic downturn.

Read wikipedia privately [using](https://spiralwiki.com/) fully homomorphic encryption. The link is a site demonstrating the ability of FHE to prevent a malicious server to know what information it sent you.

