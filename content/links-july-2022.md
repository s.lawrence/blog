---
title: "Links for July 2022"
date: 2022-07-27
tags: ['Links']
---

[Caplan on human smuggling](https://betonit.substack.com/p/human-smuggling-is-underrated). Note in particular that "smuggling is not a frowned-upon practice among migrant and refugee communities".

[Scaling laws for neural language models](https://arxiv.org/pdf/2001.08361.pdf).

Nate Silver and the 2016 election: a story in [two](https://goodreason.substack.com/p/nate-silvers-finest-hour-part-1-of) [parts](https://goodreason.substack.com/p/nate-silvers-finest-hour-part-2-of).

Majorana zero modes, [redux](https://arxiv.org/abs/2207.02472). A previous claim that Microsoft had observed Majorana zero modes was retracted, but now they're back! As noted in the abstract, this is "a prerequisite for experiments involving fusion and braiding of Majorana zero modes"---in other words, a possible path to usable quantum computers.

Balaji Srinivasan's new [book](https://thenetworkstate.com/) is available. I have skimmed it only briefly; Vitalik Buterin [writes](https://vitalik.ca/general/2022/07/13/networkstates.html) an excellent review.

