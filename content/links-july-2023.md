---
title: "Links for July 2023"
date: 2023-07-28
tags: ['Links']
---

Paul Graham has an [advice post](http://www.paulgraham.com/greatwork.html), which is quite good. Zvi said it's too long. That is incorrect. It _should_ be long.

Constructing invalid `String` objects [in Java](https://wouter.coekaerts.be/2023/breaking-string). Because Java string literals are interned, you can also force all copies of a certain string literal to be interned.

[Abusing DNS resolution](https://fingerprint.com/blog/apple-macos-mdns-brute-force/) to determine a user's first name.

