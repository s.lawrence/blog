---
title: "Notes on Quantum Error Correction"
date: 2021-07-22
tags: ['Physics']
---

I gave a set of two short lectures at Fermilab's QCIPU summer school. The subject was quantum error correction. I've uploaded the notes, targeted to undergraduates being introduced to quantum computing for the first time, [here](/edu/notes/QCIPU/errors.pdf).

Do not hesitate to email me to let me know how wrong the notes are.

