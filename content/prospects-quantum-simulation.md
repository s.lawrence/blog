---
title: "Prospects for Quantum Simulations (of Nuclear Physics)"
date: 2022-03-14
hidden: True
---

TODO intro

## Overview of quantum computation

TODO

## Hardware

TODO

## What are quantum computers good for, anyway?

TODO

## Physical targets

TODO

## Predictions

<div style="text-align: center">
<iframe src="//d3s0w6fek99l5b.cloudfront.net/s/1/questions/embed/3684/" width="550" height="260"></iframe>
</div>

## So what?

TODO

----

_Thanks to TODO_

