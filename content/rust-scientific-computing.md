---
title: "Thoughts on Rust for Scientific Computing"
date: 2021-03-10
draft: true
---

TODO intro

I should be clear what I mean when I say "scientific computing". I'm not thinking of the massive software engineering projects, spearheaded TODO

To get used to working with Rust, I TODO

## Task 1: Hydrodynamics

TODO

## Task 2: Lattice Field Theory

TODO

## Aesthetics

Aesthetics aren't everything. Fortran unquestionably has better aesthetics than C++ (which is in turn a big improvement on C, _in the context of scientific computing_), but the latter has managed --- slowly --- to replace the former in most places. But that's not to say they don't matter at all. In fact aesthetics matter more when the program is not the main artifact being produced in a project, as is often the case in scientific work.

Let's start with multi-dimensional arrays, without using any external crates.

```rust
fn main() {
    let arr: [[i32; 2]; 100] = [[0; 2]; 100];
    arr[82][1] = -2;
}
```

_Using_ arrays is fine, but the task of _creating_ a multidimensional array is disastrous. The underlying logic is common enough, shared by C, C++, and Python without `numpy`: the compiler doesn't see a multi-indexed object, but a one-indexed object in which each member itself takes one index. By itself, this is absolutely fine. The standard use of the array, `arr[i][j]` looks close enough to common notation that you can fool yourself into thinking the compiler is thinking the way you are. But whereas in C, the compiler complicit in this delusion when you're creating an array (`int arr[3][5]`), in Rust, the compiler forces you to acknowledge the way it thinks, every time you write a multi-dimensional array.

Oh, but it gets worse. Let's compare to some C++ code where we try to use a multidimensional `std::vector` (this is sometimes necessary, sadly).

```cxx
void main() {
    std::vector<std::vector<int>> arr(2, std::vector<int>(100));
    arr[1][82] = -2;
}
```

This is also pretty bad; the compiler isn't participating in our delusions at all! But _at least the dimensions appear in the right order_. When creating an array, it's clear that "the first index is small, the second one can be large". And that's true. But in Rust, the inner/outer indices are flipped when creating the array!

If you think the "right" way, both bits of code above make perfect sense. But if thinking about what the computer's doing isn't your main task, this little thing is surprisingly irritating.

## Crates

## All in all


