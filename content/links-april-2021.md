---
title: "Links for April 2021"
date: 2021-04-27
tags: ['Links']
---

Can the gender of a writer be reliably determined just by looking at the writing? With a large corpus and extensive study, the answer seems to be [no](https://lithub.com/have-italian-scholars-figured-out-the-identity-of-elena-ferrante/).

Lance Fortnow [chimes in](https://blog.computationalcomplexity.org/2021/04/quantum-stories.html) with his own stories of quantum computing hype.

[Here](https://benkrasnow.blogspot.com/2011/03/diy-scanning-electron-microscope.html) [are](https://dberard.com/home-built-stm/) [four](https://hackaday.com/2014/04/29/a-diy-atomic-force-microscope/) [constructions](https://www.youtube.com/watch?v=9TYlQ4urcg8) of modern microscopes --- of the types that often cost hundreds of thousands of dollars or more --- done cheaply and at home.

[Galen](https://astralcodexten.substack.com/p/your-book-review-on-the-natural-faculties), the first of the empiricists? Along with some early thought experiments! A book review well worth reading.

Robin Hanson on freedom of ["religion"](https://www.overcomingbias.com/2021/04/reviving-freedom-of-religion.html): perhaps returning to a more inclusive definition of what constitutes a religion --- and therefore receives some protected status --- would be wise. The best mechanism for defining a "religion" remains unclear. But shouldn't this problem sort itself out over time? "Religions", after all, should have an incentive to become religions (without quotes) to claim special-ish status under the law.

Tanner Greer [writes](https://scholars-stage.blogspot.com/2021/04/the-problem-of-new-right.html) about the New Right and the history of the nation. Greer's writing is, as always, worth reading in full, so I won't give a summary. Here's an excerpt/teaser instead:

> The libertarian streak of the median Trump voter is not refined or well-reasoned. It is more a product of instinct than intellect. It is the impulse that caused backcountrymen to cry for “elbow room” two centuries ago, causes modern Trumpists to yell “get off of my lawn!” today, and led both groups to embrace the slogan “don’t tread on me” when they felt like their way of life was under siege.

And one more for a laugh:

> European travelers regularly described Boston and environs as the only place in America where the lower orders seemed to understand their place.
