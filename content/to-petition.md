---
title: "To Petition the Government"
date: 2020-11-15
tags: ['Society']
---

> Congress shall make no law respecting ... the right of the people ... to petition the Government for a redress of grievances.

The _petition clause_ is probably the least-referenced part of the first amendment. Nowadays, petitions aren't taken so seriously, since "signing" an online petition (on the White House website, for instance) takes just a few seconds. It's hard to imagine an individual petitioning Congress and being paid any real attention. 'Twas not always so.

You can search old congressional documents [here](http://memory.loc.gov/ammem/hlawquery.html). If there are too many results, it just says "over 100", so I can't get statistics, other than to say "there sure were a lot of petitions". These things were read in Congress. There usually wasn't a long discussion, but they were often referred to committees for further discussion (and committee records are harder to find).

It's a weird feeling, reading those petitions. I started researching for [my last post](/magnetism-congress) because I was so taken aback by Morse's audacity in requesting aid directly from Congress. It clearly wasn't unusual at the time. Reading those petitions, one gets a sense of a very different relationship between government and the governed.

To give you a feel for this (but you really should [search yourself](http://memory.loc.gov/ammem/hlawquery.html)), I've clicked around to read a few at random. I have reproduced the first few below: I swear I have done no selection.

> _(January 2, 1810)_ Mr. Meigs presented the petition of Daniel Boone, an inhabitant of the territory of Louisiana, stating that he has spent a long life in exploring the wilds of North America; and that he hath, by his own personal exertions, been greatly instrumental in opening the road to civilization in the immense territories now attached to the United States, and in some instances matured into independent states; and praying a grant of some reasonable portion of land, within the territory of Louisiana, as a compensation for his services; and the petition was read.

Seriously, I swear that was the first thing I clicked on. Daniel Boone: "I'm pretty important, can I have money?"

The next day, we read:

> _(January 3, 1810)_ Resolved, That the petition of Daniel Boone, presented yesterday, be referred to a select committee

The committee reported back on the first of February, and we can read Boone's entire petition.

![Scanned image of the petition of Daniel Boone](/to-petition/boone.png)

Of course, some things never change. The next reference to Daniel Boone in the congressional journal is:

> _(January 17, 1812)_ Ordered, That the petition of Daniel Boone, presented the second of December, one thousand eight hundred and seven, be referred to the Committee on the Public Lands.

Yikes! Two more years later, Congress finally acted. The original petition can be seen (but perhaps not read) [here](https://www.archives.gov/legislative/features/boone).

The next reference to "petition" on which I randomly clicked (from just a few days later) concerned one "Daniel Boon". Skipping ahead a few months, on December 18, 1810, the Senate considered a petition by Philadelphia merchants concerned about how recent legislation may affect their property interests (in South Africa). On the same day, a petition from "the President and Directors of the Bank of the United States". Clearly, on this day, the Senate was entertaining the wishes of the high and mighty.

Next random click.

> _(January 25, 1811)_ Mr. Gregg presented the petition of Thomas Campbell, stating that he served as a captain in the Revolutionary army; that during the service he received several wounds; and praying relief, for reasons mentioned in the petition; which was read.

This is a common sort of petition; you'll note the broad similarities with Daniel Boone's.

Next I skipped ahead to 1818.

> _(January 15, 1818)_  Mr. Sanford presented the petition of Michael Hogan, of the city of New York, representing that early in the month of February, 1813, a valuable house belonging to him in the village of Utica, was taken possession of by a detachment of United States' troops ... praying redress for damages sustained...

Skipping back a few decades:

> _(November 13, 1792)_ The petition of Mary Kent, for the renewal of a loan office certificate, destroyed by accident, Was presented and read.

Searching for "petition" in these records alternates between triffling concerns and true affairs of state. The next three I saw were: a request for some land at a reasonable price (from nobody in particular), the State of Indiana requesting a land grant to allow them to improve a road from Chicago to [Vincennes](https://en.wikipedia.org/wiki/Vincennes,_Indiana), and this:

> _(March 20, 1834)_ Mr. Silsbee presented the petition of Zachariah Jellison, praying for a remission of the duty on a quantity of tobacco imported by him in 1832, from Cuba...

Some things never change.

But something _did_ change. Tanner Greer [frets](https://scholars-stage.blogspot.com/2020/10/we-were-builders-once-and-strong.html) that Americans today are more likely to complain to management than to try to fix something themselves. That could be true, but it seems that even complaining to management is a bit too much to handle these days. The picture I get from reading through the Congressional Record is of a relatively flat society, in which the distance between Congress and the average citizen is small, even in those pre-telegraph days of slow communication. Much flatter than society today.

I don't have any grand conclusion. Perhaps most of these petitioners (requesting duty-free cuban cigars) simply had an overblown sense of their own importance; after all, today there are plenty of cranks emailing everyone they can to announce that they've discovered why "Einstein was wrong" (it's always Einstein). It certainly isn't the case that Congress responded to these petitions particularly dilligently; both Boone and Morse had to wait nearly a decade.

Maybe this is just one more sign that American life has become too bureaucratized. Projects like [Emergent Ventures](https://www.mercatus.org/emergent-ventures) seek to counter that trend, and we need more of that.

Or this could be the result of a failure to imagine: a lost ability to look at the world, imagine it slightly better, and then try to make it so. If that's true, these stories aren't a rebuttal to Greer's concerns. They're both part of a larger pattern. If you were to start a committee to fix something, what would the committee fix? If you were to petition Congress, what would you ask for? Mr. Silsbee wanted Cuban tobacco; Boone wanted post-hoc payment for services rendered to the nation; Churchman wanted to explore the world; Morse wanted to connect it.

