---
title: "Links for January 2022"
date: 2022-01-26
tags: ['Links']
---

A periodic reminder: links are included in these lists because they are worth _reading_ (or worth skimming, or at least being aware of). That is not the same thing as being worth _believing_. These are not recommendations for links to click while in a hypnotic trance.

Behold, the [internet wall of shame](https://github.com/WebKit/WebKit/blob/main/Source/WebCore/page/Quirks.cpp). Alternatively, the set of websites important enough that the browser must accomodate the website, rather than the other way around.

[An account](https://endpts.com/inside-the-multibillion-dollar-silicon-valley-backed-effort-to-reimagine-how-the-world-funds-and-conducts-science/) of efforts to create new sources of science funding. (You have to create an account to read the article, but creating an account is free.)

From a few years ago, [Guzey](https://guzey.com/how-life-sciences-actually-work/) on funding, careers, incentives, and so on in the life sciences. Today, he works at [New Science](https://newscience.org/).

[Canada counts marijuana in GDP](https://marginalrevolution.com/marginalrevolution/2022/01/does-pot-contribute-to-gdp.html) but the U.S. does not. The Netherlands counts prostitution, although neither Canada nor the U.S. does. Arguably this means that the standard of living in the U.S. and Canada is being underestimated with respect to the Netherlands. Would the GDP more accurately reflect the standard of living if it included murder markets?

Old but good: [the Copenhagen interpretation of ethics](https://blog.jaibot.com/the-copenhagen-interpretation-of-ethics/). Note that the article says that you can be blamed for problems that you "observe or interact with", but most of the given examples (and all modern usage I've seen) fall under "interact with".

It is [easy](https://slimemoldtimemold.com/2022/01/11/reality-is-very-weird-and-you-need-to-be-prepared-for-that/) to miss simple stories. Alternatively, the space of simple, not-at-all weird realities is much larger than you think, and you should never expect that it has already been well explored. (No, I don't like the original title of the link.)

Military [thoughts](https://mwi.usma.edu/we-ignore-the-human-domain-at-our-own-peril/) on the "human domain". While reading, keep in mind the answer to the following question: "what does military action in the human domain look like?"  And here's a call for lowered epistemic standards:

> Because any measure of human cognitive activity is imperfect—no perfect system currently exists to map the dynamics of human behavior—the defense enterprise should stop trying to assess the human domain in concrete terms and become more comfortable with imperfection and uncertainty.

Followed a bit later by:

> Leaders should be empowered to accept high levels of risk when operating in the human domain to prevent analysis-paralysis and inaction.

[From](https://acoup.blog/2019/09/07/new-acquisitions-class-status-and-the-early-church/) Bret Devereaux:

> It is not an accident that the term for non-Christian – pagani – has a literal meaning of ‘villager’ or ‘rustic.’

A [more recent](https://acoup.blog/2022/01/14/collections-rome-decline-and-fall-part-i-words) post is more blunt:

> Indeed, the word paganus, increasingly used in this period to refer to the remaining non-Christian population, had a root-meaning of something like ‘country bumpkin,’ reflecting the degree to which for Roman elites and indeed many non-elites, the last fading vestiges of the old Greek and Roman religions were seen as out of touch.

Bret Devereaux [gives](https://acoup.blog/2022/01/21/fireside-friday-january-21-2022-on-public-scholarship/) perspectives on public scholarship, particularly in the humanities, particularly in history. Well worth a careful reading. ACOUP, as he points out, is an anomalously successful public engagement project, and so the article should be read as a _relatively_ authoritative perspective. No predictions are given for how public engagement might change the field: remember, all effects are non-zero, so this will either enhance or weaken the ability of the field to converge on true statements. Also, I will probably return to this quote in a future post:

> [..] many academic fields have frankly spent a lot of time making activism withdrawals from the bank of public support but almost no time making engagement deposits and now the accumulated savings of centuries are spent.

