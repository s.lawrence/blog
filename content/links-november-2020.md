---
title: "Links for November 2020"
date: 2020-11-26
tags: ['Links']
---

Andrew Gelman argues that (in thus-and-such narrow context) [experience is overrated](https://statmodeling.stat.columbia.edu/2020/11/02/as-a-forecaster-how-important-is-it-to-have-a-few-elections-under-your-belt/). The point of that post isn't that experience is overrated, though. At least, the meta-level point (the reason you should read it, maybe not the reason Gelman wrote it) is an extraordinary demonstration of humility.

Robin Hanson [interprets](https://www.overcomingbias.com/2020/11/prestige-is-mob-enforced-dominance.html) prestige as mob-enforced dominance (whereas usually, dominance is enforced by the person doing the dominating).

"America's laziest and dumbest judge" has passed away. [RIP Judge Richard Neely](https://marginalrevolution.com/marginalrevolution/2020/11/judge-richard-neely-rip.html).

[Some](https://www.agglomerations.tech/cracks-in-the-great-stagnation/) [claims](https://marginalrevolution.com/marginalrevolution/2020/11/is-the-great-stagnation-over.html) that the Great Stagnation is coming to an end. I can't claim to be impressed. The first tweet cited focuses largely on technologies that were invented _during_ the supposed Great Stagnation!
