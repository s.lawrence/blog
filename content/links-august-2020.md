---
title: "Links for August 2020"
date: 2020-08-29
tags: ['Links']
---

NIST plans to release an initial standard for post-quantum cryptography in 2022, after having [moved to the selection round](https://www.nist.gov/news-events/news/2020/07/nists-post-quantum-cryptography-program-enters-selection-round) last month. There are 15 algorithms under consideration at this point.

[Competetive equilibrium](https://en.wikipedia.org/wiki/Competitive_equilibrium) purports to predict the final price in certain markets.  For a still smaller set of market designs, this model is experimentally validated, with that experiment being [reproduced](https://www.nature.com/articles/s41562-020-0916-8) thousands of times over ([press version](https://phys.org/news/2020-08-famous-economics-thousands.html)). The paper notes (smugly) that CE assumes hardly anything about the behavioral characteristics of participants in the market, and indeed experimental results suggest that no such assumptions are necessary. Some parts of the world are slightly less dismal. The article is paywalled, but not the data or code. Surely there's an opportunity for a service that auto-generates key plots from the paper?

Prevelance of dementia [appears to have fallen](https://n.neurology.org/content/95/5/e519) rather dramatically, over the last decade or two. That's for the US and Europe; the study notes that similar declines are not seen in Japan, China, or Nigeria.

The ["Rapid Deployment Vaccine Collaborative"](https://radvac.org/) provides instructions for a DIY vaccine. This has no direct practical import, of course --- there's no chance that enough people make use of this vaccine to make a difference to herd immunity of statistics. It's an interesting read for other reasons. For instance, it appears that the task of designing a _plausible_ vaccine is really not so difficult.

If making your own vaccine isn't your thing, how [listening to satellite transmissions](https://nyan-sat.com/)? Will anybody stream the received data online?

There is [no insect apocalypse](https://www.nature.com/articles/s41559-020-1269-4) in the United States; see also the [press release](https://news.uga.edu/insect-apocalypse-not-happening-in-us/). Again, the article is paywalled but the data and analysis code are not. The paper is based entirely on publicly available data, in this case [NSF's Long-Term Ecological Research](https://lternet.edu/using-lter-data/) site data.

Grant Sanderson gives [an introduction to the monster group](https://www.youtube.com/watch?v=mH0oCDa74tE). The video gives no information about the monster group, but is otherwise perfect. (I really shouldn't hold that against him, but I watched the whole thing waiting with baited breath for his perfect explanation of how to construct the group, or something, and now I'm bitter.)

Amazon [hops on the quantum computing bandwagon](https://aws.amazon.com/braket/). As far as I can tell this service just provides a semi-unified interface to other public quantum computing services. Somewhat related, here's [Woit's summary of NSF's funding priorities](https://www.math.columbia.edu/~woit/wordpress/?p=11961).
