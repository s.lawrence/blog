---
title: "Links for July 2021"
date: 2021-07-29
tags: ['Links']
---

An extensive [reading list](http://matt-clancy.com/economics-of-innovation-detailed-reading-list/) on the economics of innovation.

[Hayek](https://cdn.mises.org/Intellectuals%20and%20Socialism_4.pdf) on intellectuals, and indeed on the social construction of science. Relatedly, [Musa al-Gharbi](https://heterodoxacademy.org/blog/seizing-means-knowledge-production/) on a particular modern aspect of this dynamic.

Weights for a network of comparable performance to GPT-3 [have been released](https://github.com/kingoflolz/mesh-transformer-jax).

The rust compiler [is a SAT solver](https://niedzejkob.p4.team/rust-np/), although not a particularly fast one.

Some [evidence](https://www.pnas.org/node/992474.abstract) that society is become more depressed-ish over time. See the Hacker News [discussion](https://news.ycombinator.com/item?id=27952176) for appropriate skepticism, but I don't think anything mentioned there invalidates the conclusion that there's a trend in written English towards more distorted phrasing and thinking. See also Scott Alexander: "[He who has ears to hear, let him listen.](https://astralcodexten.substack.com/p/book-review-crazy-like-us)".

From Balaji Srinivasan, [cryptocurrency as economic education](https://twitter.com/balajis/status/1419563043122339840).

