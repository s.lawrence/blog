---
title: "Links for July 2020"
date: 2020-07-23
tags: ['Links']
---

[A brief immunology tutorial](https://twitter.com/VirusesImmunity/status/1285944893085491204) from Akiko Iwasaki et al.; see also the accompanying [video](https://www.youtube.com/watch?v=jeN8v5I5VNA).

Scott Aaronson [reviews](https://www.scottaaronson.com/papers/bb.pdf) the study of the busy beaver function. I'm pleased to see that he considers Radó's shift function to be "by far [more] natural" than the ones function. More seriously, the article should be accessible to anyone familiar with the definition of a Turing machine.

I probably won't write about [GPT-3](https://arxiv.org/abs/2005.14165) independently, but reasonable arguments [for](https://marginalrevolution.com/marginalrevolution/2020/07/the-case-for-gpt-3.html) and [against](https://marginalrevolution.com/marginalrevolution/2020/07/the-case-against-the-import-of-gpt-3.html) the import of GPT-3 are found on Marginal Revolution. The simplest way to play with it is to play the [AI dungeon](https://play.aidungeon.io/). The field of "AI" (as distinct from the ostensibly less-ambitious "machine learning") was perhaps once the most overhyped field in academia. Today (despite [plenty](https://twitter.com/ESYudkowsky/status/1285333002252247040) [of](https://twitter.com/mckaywrigley/status/1285827683776004096) [hype](https://twitter.com/sharifshameem/status/1282676454690451457)), I would not rank it in the top three.

[There is no automation revolution](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3496364) (from late last year). Of course Robin Hanson has been beating this drum for a while now, as have others, but it bears repeating.

Paul Romer's [statement](https://paulromer.net/statement-for-house-budget-comittee/) for the House budget committee advocates for a few changes in the structure of U.S. science funding. Among the suggestions are the idea of granting funding directly to students. Senior scientists, presumably, would be forced to compete with each other for attention from students. Romer doesn't mention it, but presumably that scheme would be a substantial improvement for climate issues. The statement mentions that the reforms best for scientific progress won't necessarily be enthusiastically greeted by entrenched (scientific) interests. Only tangentially related, but also of interest: Romer [discusses](https://paulromer.net/what-went-wrong/) a hypothetical cost-benefit analysis of economics as a field.
