---
title: "Is the Great Stagnation just a narrative?"
date: 2020-12-15
tags: ['Society']
---

The Great Stagnation smells a lot like a purely psychological phenomenon --- a matter of narrative rather than a matter of data. For instance, as I mentioned [last month](/links-november-2020), a lot of the recent progress heralded as "the end of stagnation" actually occured _during_ the supposed stagnation. The recent optimism (or triumphalism?) doesn't appear to be supported by much actual change, [or any actual data](https://applieddivinitystudies.com/stagnation/).

Of course, that doesn't mean that it's not coming to an end. If you think of the great stagnation as an economic fact, rooted in data, then we need to wait 5-10 years for new data and careful analysis to come in, before deciding. If you believe "the great stagnation was purely a matter of narrative", then the fact that Tyler Cowen has [declared](https://marginalrevolution.com/marginalrevolution/2020/12/why-did-the-great-stagnation-end.html) [it](https://marginalrevolution.com/marginalrevolution/2020/12/how-should-the-possible-end-of-the-great-stagnation-influence-your-media-diet.html) [over](https://marginalrevolution.com/marginalrevolution/2020/12/thats-it-for-this-week.html) is decisive evidence that it is, in fact, over.

Can "pure narrative" still have real (presumably negative) effects? Sure... probably --- although those effects are by no means guaranteed to be what the narrative says they are. A narrative of stagnation might have bigger effects (and/or causes, what's the difference anyway?) in politics than in innovation, for instance.
