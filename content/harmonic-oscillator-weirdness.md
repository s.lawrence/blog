---
title: "Harmonic Oscillator Weirdness"
date: 2020-12-17
tags: ['Physics']
---

The phrase "low-energy Hilbert space" is massively deceptive.

Some background: working with infinite-dimensional vector spaces is hard. We like to truncate them, to obtain a finite-dimensional space, so that operators can be treated as matrices on a computer. The standard truncation is to take some well-understood Hamiltonian, diagonalize it, and consider only the (vector space spanned by the) lowest \\(N\\) eigenstates. As \\(N\\) is increased, the approximation is improved, and the properties of the true (infinite-dimensional) system are recovered in the limit.

Physically, the idea is that high-energy physics can be ignored when you know that the energy of your system is, well, not that high. Since we took only the low-energy states, it seems natural to call the resulting vector space the "low-energy Hilbert space".

So far, seems fine. Now consider the (not normalized) state of the harmonic oscillator (hamiltonian \\(H = x^2 + p^2 - 1\\)):
$$
\Psi(x) = \frac{1}{1 + x^2}
\text.
$$
This is, of course, just the glorious [Cauchy-Lorentz distribution](https://en.wikipedia.org/wiki/Cauchy_distribution). The expectation value \\(\langle x^2\rangle\\) is some finite number. Same for \\(\langle p^2\rangle\\). Therefore, the energy of this state is finite!  The expectation value \\(\langle x^4 \rangle\\), though, is infinite:
$$
\langle x^4\rangle =
\int dx\\; \Psi(x)^\* x^4 \Psi(x) =
\int dx\\; \frac{x^4}{x^4 + O(x^2)}
"=" \infty
\text.
$$

But wait! The low-energy Hilbert space (for any finite cutoff) is spanned by states with finite \\(\langle x^4\rangle\\) (and indeed finite expectations of every polynomial in \\(x\\) and \\(p\\)). And here we have a low-energy state with an infinite expectation value. What gives?

Well, the state \\(\Psi\\) is not in any low-energy Hilbert space. It has amplitudes (decaying algebraically) for every excited state of the harmonic oscillator. For certain observables (like \\(x^4\\)), its physics is dominated by those high-lying states. It can be approximated arbitrarily well within the cutoff, but some qualitative features can never be recovered.

This isn't just about divergent integrals, either. If \\(|E\rangle\\) is the harmonic oscillator eigenstate with energy \\(E\\), then by considering the state \\(|0\rangle + \epsilon^2|\frac 1 \epsilon\rangle\\), we see that there are arbitrarily low-energy states that don't lie within any low-energy Hilbert space.

In short: the "low-energy Hilbert space" does not contain all (or most) low-energy states. Conversely, the space of low-energy states is not a vector space, since it fails to be closed under linear combination. Moreover, if you ignore the fact that it's not a vector space, it appears to be infinite-dimensional; more evidence that it really looks nothing like the space constructed from a truncation of the eigenbasis.

Of course, this is not actually about the harmonic oscillator. The same logic should apply to every quantum system, including field theories.
