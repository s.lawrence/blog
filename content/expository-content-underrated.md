---
title: "Expository content is underrated"
date: 2022-02-21
tags: []
---

I am increasingly of the view that expository (dare I say pedagogical? no) content
is severely underrated. When I say that, I mean two things:

1. The writers (or more generally, creators) of quality exposition do not
   receive as much prestige as they should.

2. Potential expository writers underestimate the amount of credit they _would_
   receive, even taking the first point into account.

Both are true at many levels, but particularly so at the "top", or at any
frontier of knowledge. My direct experience is mostly in physics research, but
I've begun to see the same pattern elsewhere, enough to suspect that it's a
general phenomenon, particularly occurring anywhere novelty is valued. (More on
that connection at the end.)

It's pretty remarkable for both to be true. If true, the consequences are
pretty clear. Everybody should spend more time both producing (fixing point 2)
and recommending (1) expository articles. (Presumably funding agencies should
tweak their behavior, as well as hiring committees, and so on.)

That leaves us with: why should anyone believe the two claims above are true?
Certainly research papers outnumber expository papers by an order of magnitude
or two. The average expository paper is read more (that's the point!), while
being somewhat less likely to be cited, at least per read.

Perhaps relatedly, the median research paper is of far lower quality than the
median expository work. I expect that a world where more people are producing
exposition will have a lower quality median and a higher quality maximum. Since
it's usually fairly cheap to select high-quality papers, there's no major cost
to lowering the median, and of course there's a large benefit to improving the
maximum.

Finally, it's notable that the best way to learn something _at the frontier_
is, by a large margin, to find the inventor and talk. This isn't true for more
mundane things like calculus, where the inventors are dead and would do a
terrible job explaining anyway. For these lower-level topics, enough time and
effort has been put into constructing high-quality explanations that it's
pretty feasible to learn from a nice video or set of lecture notes (or both).

Leaving academia and moving to software, let's consider
[Rust](https://www.rust-lang.org/) libraries. There is [certainly no
shortage](https://www.boringcactus.com/2020/08/21/survey-of-rust-gui-libraries.html)
of Rust GUI libraries. But tutorials for using these libraries are scarce. To
take a particular example, consider [iced](https://github.com/iced-rs/iced):
one year ago, there [were
none](https://www.reddit.com/r/learnrust/comments/mc83am/iced_gui_tutorial_or_guidelines_needed/).
A similar situation exists across the other most commonly used libraries.

Or, libraries for parser combinators. There are
[a](https://crates.io/crates/pom) [few](https://crates.io/crates/nom)
[options](https://crates.io/crates/combine), so again, no shortage of actual
libraries. But explanations of how to use these are surprisingly scarce. Such articles are, in some sense, redundant. Expository work always is. If you
want to know how to use `combine`, just learn about
[`parsec`](http://book.realworldhaskell.org/read/using-parsec.html). But that's
more work --- _vastly_ more for anybody not familiar with functional
programming. Reducing time-to-learn is valuable.

I don't think I'm alone in noting that expository content is underrated: see
for instance [Grant Sanderson](https://www.3blue1brown.com/blog/some1). But I
want to emphasize here the value of expository content being produced _at or
near the frontier_; in other words, for researchers. I badly wish there was more of it.

---

Finally, a **note about novelty**. It is completely reasonable to read the
above and object:

> But this is little more than a call to devalue novel contributions relative
> to educational work! The purpose of _research_ is _novel_ work, the purpose
> of _education_ is _pedagogical_ work, and the desire to confuse the two is
> just a manifestation of a deeper desire to redirect funding to people who, to
> be frank, find the former out of reach.

Such requests have been made before, and amount to mere value judgements about
what type of contribution (or, perhaps, what type of contributor?) is more
important. Such judgements are not my intention here.

(There's an easy reply, along the lines of "oh, you're overestimating how novel
most research really is". This is simultaneously true, irrelevant ---
suppressing a distribution also suppresses the tail --- and an incredibly lazy
jab at an entire area of human endeavor.)

No, I claim that our hypothetical objector is underestimating the novelty of
high-quality expository work. Finding efficient explanations is difficult.
Exposing information to new audiences is difficult. Drawing connections between
old results and new trends is difficult. And these aren't difficult in the
sense of "boring work that takes a lot of time" --- these are tasks that have a
lot of intellectual content.

Knowledge isn't just a list of propositions that are true, or a list of
programs that have been written. The meat of knowledge is just as much in the
connections between the facts, and that's the stuff that good explanations are
meant to reveal. The
[alchemists](https://slatestarcodex.com/2017/11/09/ars-longa-vita-brevis/),
measured by man-hours, spend nearly all of their time on those explanations,
not the ostensible frontier.

