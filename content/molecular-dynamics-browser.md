---
title: "Molecular Dynamics In Your Browser!"
date: 2021-09-25
tags: ['Physics']
---

Click [here](/md). A few points follow, from least to most technical.

You can click and drag the mouse to push particles around. The "brush" is deliberately wide and weak, to keep the simulation as close to the hydrodynamic regime as possible.

The parameter \\(n\\) controls the dimensionless density of particles; that is, what fraction of the screen is shaded white. The result is that changing \\(R\\) (the size of a particle) zooms in or out without changing the underlying physics.

Enabling the hydrodynamic overlay will highlight regions where particles have a net vertical velocity. You can use this to watch shear waves. Shear waves will decay more quickly if you make things sparse.

The average velocity of the particles is fixed to be zero. You can think of this as the camera panning to keep pace with the average velocity.

The underlying technologies (aside from the obvious) are WebGL and WebAssembly. The source code to the WASM binary blob is available [here](/md/wmd.c).

