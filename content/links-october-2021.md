---
title: "Links for October 2021"
date: 2021-10-27
tags: ['Links']
---

Vitalik Buterin [discusses](https://vitalik.ca/general/2021/09/26/limits.html) finance, the political theory behind cryptoeconomics, and collusion.

[Advice on chess improvement](https://mbuffett.com/posts/chess_improvement_thoughts/). I would interpret this post as a list of things to try.

[Orwell on nationalism](https://www.orwellfoundation.com/the-orwell-foundation/orwell/essays-and-other-works/notes-on-nationalism/). See also [Paul Graham](http://www.paulgraham.com/identity.html) and [Bryan Caplan](https://www.econlib.org/archives/2014/10/dear_identity_p.html), but Orwell's article is descriptive more than normative, and has correspondingly more meat. Some striking claims are included, for instance:

> After the fall of France, the French pacifists, faced by a real choice which their English colleagues have not had to make, mostly went over to the Nazis[...]

There's something rather astonishing about Orwell's writing, maybe more noticeable in this text than in any other. There is no hint of the Straussian; no indication that there was any group he feared to displease.

[Lectures on geometric complexity theory](https://www.youtube.com/playlist?list=PLM24JbgCBLfvCd-55sfjrCqehsX5hp8cl).

This video is deeply offensive --- perhaps moreso with context:
<iframe width="599" height="337" src="https://www.youtube.com/embed/-Rp7UPbhErE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
