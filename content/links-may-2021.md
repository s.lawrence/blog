---
title: "Links for May 2021"
date: 2021-05-27
tags: ['Links']
---

Scott Alexander is posting book reviews from his book review contest (long delayed by, um, things). I've linked to [one](/links-april-2021) already. [Here](https://astralcodexten.substack.com/p/your-book-review-double-fold#sdfootnote1sym) is another, on the trend of replacing books with microfilm. A large part of the motivation for this replacement was the fragility of books and the durability of microfilm. The review is not too long and definitely worth reading, but your guess about the punchline is correct.

[Still more](https://blog.computationalcomplexity.org/2021/05/why-do-countries-and-companies-invest.html) --- this time from Bill Gasarch --- on the quantum computing bubble. The question is "why would a company/country invest their own money in quantum computing". Claims of the form "it's not their money, they're spending someone else's money" don't really answer the question, they just push it back a level.

An [animated guide](https://ciechanow.ski/internal-combustion-engine/) to internal combustion engines.

[Websites can](https://fingerprintjs.com/blog/external-protocol-flooding/) determine which applications you have installed (among the set of applications that define custom URL handlers) and use this to identify you. Browsers are likely to fix this pretty soon, but... the claims on that site seem a bit strong. Most applications don't define their own scheme handlers, and most people I know have almost exactly the same set of applications installed. This is still a vulnerability, but no evidence is presented that it provides "a unique array of bits associated with a visitor’s identity".

Can we change the probability that our universe is a simulation by running our own simulations of other universes? Should we be worried about the similarity between a story and a (low fidelity) simulation of another universe? The record for the most bizarre premise for a book review has been [set](https://astralcodexten.substack.com/p/book-review-arabian-nights), and is not likely to be overturned any time soon.
