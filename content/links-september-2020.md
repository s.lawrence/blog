---
title: "Links for September 2020"
date: 2020-09-26
tags: ['Links']
---

[A thread](https://twitter.com/DiffusePrioR/status/1303646695398535168) on statistical issues in historical persistence studies. The [original paper](https://economics.yale.edu/sites/default/files/understanding_persistence_ada-ns.pdf) is just a bit long. Perhaps the most interesting part of the thread is the last tweet: "Lastly, it is important to note that this paper is not an attack on the quality of scholarship of any study. These papers have their own robustness exercises and multiple specifications, so the Kelly findings are not evidence that any specific paper is discredited."

Scott Alexander [updates](https://slatestarcodex.com/2020/09/11/update-on-my-situation/) us on the future of SSC.

Alvaro de Menard is [not pleased](https://fantasticanachronism.com/2020/09/11/whats-wrong-with-social-science-and-how-to-fix-it/) with the state of the social science literature. "Incentives matter" features prominently, of course. The predominance of atheoretical papers is briefly mentioned. I believe the absence of _quantitative_ theory in particular is still an underrated problem. The start of the article is the most interesting: replication markets are good at spotting bad papers, so why do academics (both authors and citers) seem to be unable to do so? Menard seems to miss the possibility that academics have a preternatural talent for self-deception. Moreover, there is often a culture of _normative credulity_: expressing anything more than the most mild skepticism is a social faux pas.

Phosphine is [detected](https://www.nature.com/articles/s41550-020-1174-4) in the Venusian atmosphere, and considered evidence for life on the grounds that phosphine is difficult to create, but a common product of microbial life. It seems like, once upon a time, we would have had a week or two of breathless news articles about aliens. The front page of NYT the day after the announcement was [this](https://static01.nyt.com/images/2020/09/15/nytfrontpage/scan.jpg). (The ad at the bottom is delicious.)

Robin Hanson [observes](https://www.overcomingbias.com/2020/09/specialized-innovation-is-easier.html) that specialization aids innovation, and argues that this ought to subsume other explanations for why innovation happens where it does. See also [this recent paper](https://www.nber.org/papers/w27787), arguing that innovation is slowed by a "burden of knowledge" --- a plausible mechanism by which specialization may aid innovation.

Different fields can have varying amounts of prestige relative to one another. This holds true for subfields of mathematics, for instance, and those prestige differences [can be quantified](https://arxiv.org/abs/2008.13244). The paper makes no mention of having checked for a correlation with gender/racial composition.

QuICS, the quantum information group at UMD, posts some of their seminars [on youtube](https://www.youtube.com/c/QuICS).

Robin Hanson [speculates](https://www.overcomingbias.com/2020/09/the-world-forager-elite.html) on worldwide homogeneity in policy, especially regulation. If you fantasize about any version of [archipelago](https://slatestarcodex.com/2014/06/07/archipelago-and-atomic-communitarianism/), Robin's angsty tone will make sense. There's definitely been a great deal of convergence in culture; on the other hand, didn't policy responses to COVID-19 differ quite a bit?

The Xinjiang data project has created [a map](https://xjdp.aspi.org.au/map/?cultural=none&mosque=none) of detention facilities --- handy if you're scouting for locations to shoot a live-action remake of an animated classic. Note that (according to the website) the project is based on publicly available data.

Max Roser [narrates](https://ourworldindata.org/its-not-just-about-child-mortality-life-expectancy-improved-at-all-ages) a plot of life expectancy, for different age groups, over time. His punchline is that life expectancy gains haven't just come from decreases in infant and childhood mortality. Looking closely, the story is a bit more complicated. From 1850-1900, there's pretty much no improvement for anyone over the age of 10. Until 1950, there's very little improvement for age groups over 50. It's only in the last quarter of the 20th century that we really see improvements for the oldest age bracket.
