---
title: "Poor Phrasing as a Shibboleth"
date: 2020-11-11
tags: ['Society']
---

## I.

> [Science is a social construct.](https://www.salon.com/2019/12/22/why-science-is-a-social-construct/)

What the hell does that mean? I'm admittedly biased, but the mood affiliation clearly isn't good!

Kareem Carr [explains this well](https://twitter.com/kareem_carr/status/1309132237569482752). The idea is that there is nothing in the act of writing a paper (I mean actually writing it, not doing the work behind it), nor in the act of giving a talk, nor in the act of reading a paper, that reveals truth. A talk filled with lies sounds much the same as a talk filled with revelations about black holes; a paper can be formatted with two columns and a pretty heading no matter how many errors lie in the equations; a press release from an esteemed institution carries gravitas no matter how preposterous the claims.

If scientific consensus is to reflect nature's truth, there must be some mechanism for the truth to "leak" into the process of writing papers, giving talks, and bragging about accomplishments. There must, in other words, be some causal path from "X is true" to "scientist asserts that X is true". In experimental sciences, that causal path comes from the ability to check assertions against experiments. Those checks are complicated and often prone to multiple interpretations, but that's okay: they provide a sort of systematic bias towards the truth. As long as that bias outweighs all other biases, the random noise of prestige and envy and all the rest will average out, and you'll asymptote to the truth.

Critically, the meaning beneath "science is a social construct" is _not_ that there is no objective truth, it's that our determination of objective truth will only work if social structures are correctly callibrated. I said "as long as that bias outweighs all other biases...". That's an easy condition to violate! Imagine, for instance, that there was no public funding for medical research; all trials are funded by pharmaceutical companies. You see the problem, I'm sure. Or think about economics, where a large fraction of papers have policy conclusions (and therefore, as any economist will tell you, incentives abound to distort the results).

Physicists are very lucky in this regard: incentives to distort the truth are relatively small, and rarely anticorrelated (as opposed to uncorrelated) with the truth. Simultaneously, quantitative data is much easier to obtain.

## II.

> [The Dollar Value Of Human Life](https://www.npr.org/2020/04/23/843310123/how-government-agencies-determine-the-dollar-value-of-human-life)

Suppose you're running the government, and you calculate that with an additional expenditure of ten million dollars, a single human life can be saved. (Okay, okay, really we should talk about [QALYs](https://en.wikipedia.org/wiki/Quality-adjusted_life_year), but for simplicity...) If you fund a poorly run hospital that spends one billion dollars per life saved, that will come across as negligent. You could have saved 100 times as many lives for that money! Or consider the flip side: if there's an opportunity to save lives at a cost of only one thousand dollars per life, you should spend that money! _Right now_, and without hesitation.

This is what is meant by the value of human life. It's a quantification of the principle that we should be trying to maximize the number of lives saved, and the way to do that is to first fix all the "easy" deaths, and then work our way up to the more difficult cases.

One reason this is controversial is that we all have this intuition that no amount of money ought to be spared to save a (well, my) life. But of course, "money" here is being used mostly as a quantitative triage mechanism. If 100 people are dying of something trivial, like diarrhea, and one is fighting a nearly incurable cancer, you first make sure to give the 100 people some water and electrolytes, and _then_ worry about the hard case. The underlying principle is not particularly objectionable.

For the other source of controversy, consider the following: it's much cheaper to save lives in poorer countries. Let's pick on Haiti, [the most malnourished country in the world](https://www.worldatlas.com/articles/most-malnourished-countries-in-the-world.html). Saving the life of a starving Haitian kid is much easier than saving the life of some north American kid with leukemia.

Now phrase that statement in terms of "the value of human life". Say it out loud. See the problem?

## III.

These are not isolated instances of poor phrasing. I've chosen two of the least politically controversial ones; you ought to be able (with a moment's thought) to fill in plenty more.

A quick search will reveal plenty of outrage about either of the above ideas, usually without much sanity. Either could be rephrased in a way that avoids this outrage, without too much trouble. Carr's "science is socially constructed" is already a massive improvement over "science is a social construct", although I'm sure it's possible to do better. (Perhaps "scientific institutions often posess systemic biases"? I'm not good with pithy phrasing.) Referring to the "cost" of a human life similarly avoids nasty connotations, while providing even better intuition for what we're really talking about.

Why do such poor phrasings persist? They're clearly maladaptive for society at large. They seem, however, to be quite helpful at a smaller scale. It's hard for many people to say "science is a social construct" with a straight face; _doing_ so indicates you are part of a certain intellectual tradition. The same (for a very different intellectual tradition) holds for assertions about the dollar value of human life. These poorly-phrased tropes are shibboleths: they reliably distinguish the in from the out.

One thing worth noting about the poor phrasings is the political calibration. "Science is a social construct" appeals primarily to lefty, social-science types. But the actual statement, along the lines of "the structure of scientific institutions can bias findings", ought to be quite popular with, for instance, those on the right who are climate skeptics (or social-science skeptics). This is why I claim that the _content_ of the statement is not a shibboleth, but the _phrasing_ is. Similarly, the assertion "saving black lives is cheaper than saving white lives"... you get the idea.

Of course many facially absurd assertions can be rephrased in saner terms. I chose the above examples to be relatively non-political, and therefore less controversial. The more controversial ones serve as even more extreme shibboleths for (what I am forced by the metaphor to term) political faiths.
