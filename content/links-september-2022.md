---
title: "Links for September 2022"
date: 2022-09-27
tags: ['Links']
---

[Persistence!](https://marginalrevolution.com/marginalrevolution/2022/09/the-cream-rises-to-the-top.html).

[Eleven words](https://www.reddit.com/r/TheMotte/comments/w5jyyd/eleven_magic_words/).

[Book review](https://brettongoods.substack.com/p/gambling-on-development-review): Gambling on Development.

Zvi [creates](https://thezvi.wordpress.com/2022/09/25/announcing-balsa-research/) a new MX record, among other things.

And, of course, there's ongoing [chess drama](https://twitter.com/MagnusCarlsen/status/1574482694406565888). From what I saw, the initial reaction to Magnus's implicit accusations was overwhelmingly negative. That's not because the chess world doesn't like to [ban candidates](https://worldchess.com/news/all/gm-sergey-karjakin-is-banned-for-6-months-for-real/).

