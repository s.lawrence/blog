---
title: "Nimzowitsch Quotes"
date: 2021-04-10
tags: ['Chess']
draft: true
---

"It is easy to bring a passed pawn into the world. It is a much more difficult thing to provide for his future."

After giving up control of the center: "Was happiness no happiness because it endured for just a short time? One cannot always be happy."
