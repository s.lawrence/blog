---
title: "Links for April 2023"
date: 2023-04-30
tags: ['Links']
---

It's time for a reminder: I put links here because they are enlightening to
read, or at least skim. There is an impressive gap between "enlightening to
read" and "good to believe".

I also leave off links that are in the news too much. Lots of AI stuff keeps
being written, and I'll link to just about none of it.

[Vesuvius challenge](https://scrollprize.org/).

[Armin Rosen](https://www.tabletmag.com/sections/arts-letters/articles/ivy-league-exodus) on race and the Ivy League. Interesting in so many dimensions.

Claim from IBM: [4k qubits by 2025](https://www.ibm.com/quantum/roadmap).
Similar claims can be found from the other makers of quantum chips.

