---
title: "Links for May 2022"
date: 2022-05-27
tags: ['Links']
---

[Treat thinkers as a portfolio](https://threadreaderapp.com/thread/1519560150159298562.html)---that is, don't look for a single optimum, but instead try to have a broad spread. (Don't read too much into the "portfolio" metaphor!)

In apparent opposition to cold utilitarianism: ["short chain justice"](https://www.readingthechinadream.com/sun-liping-on-the-woman-with-eight-children.html). It's not clear to me that this is in _actual_ opposition. A good utilitarian might be willing to say "taxation is theft, and therefore unjust", while also believing that a world with taxation is better than one without, and therefore favoring the unjust thing.

From New Science (with Guzey as executive directory), [yet another article](https://newscience.org/nih/) on the pitfalls of NIH funding structure. This one is based on interviews with a large number of researchers at all career stages, which is valuable, and contains some questionable logical leaps, which is less valuable. Worth reading, if only to see how the biosciences compare to your field. And it starts off with quotes like this:

> Every person that I interviewed was granted anonymity. Despite that promise, quite a few said, during the interview, that they would be concerned about their jobs or ability to get a grant from the NIH in the future if they were publicly attached to a criticism of the NIH. One interviewee referred to a “fortress mentality” within the organization.

The [obvious](https://apxhard.substack.com/p/do-not-lament-the-collapse-of-the?s=w) about institutions and the crypto revolution. Worth reading nevertheless.

NVIDIA [lurches](https://developer.nvidia.com/blog/nvidia-releases-open-source-gpu-kernel-modules/) toward open source. Perhaps AMD will lurch towards a GPU compute stack that can actually be compiled, in response!

[Cons of julia](https://yuri.is/not-julia/).

A very high-level [overview](https://marginalrevolution.com/wp-content/uploads/2022/05/Cryptoeconomics-Modern-Principles.pdf) of cryptocurrencies and related fields. (I tend to think "cryptocurrencies" is a deceptively narrow term, since one usually means to include stuff like NFTs, DeFi, smart contracts...)

The most interesting [forecast question](https://www.metaculus.com/questions/10965/us-compute-capacity-restrictions-before-2050/) of the month: will the U.S. enforce compute capacity restrictions before 2050? A related question: if so, will it be an intensely partisan issue?

Numeric markets on Manifold are not ready for prime time. Even [important](https://manifold.markets/ZviMowshowitz/what-percentage-of-baby-formula-pro) ones get no attention. (All buys but one are mine---I think that the one is Zvi.)

[Why](https://richardhanania.substack.com/p/why-do-i-hate-pronouns-more-than) Richard Hanania dislikes academics.

