---
title: "Links for August 2022"
date: 2022-08-27
tags: ['Links']
---

PredictIt [loses](https://www.cftc.gov/csl/22-08/download) its special status from the CFTC; will most likely be forced to shut down. This seems like it will do real harm to the epistemological environment around politics and elections. To the extent that you're at all worried about election security and misinformation (rather than "election security" and "misinformation"), the regulation of prediction markets does harm.

DJB [is suspicious](http://blog.cr.yp.to/20220805-nsa.html) of the PQC selection process at NIST, and has filed a FOIA lawsuit.

Robin Hanson on [STEM progress and moral "progress"](https://www.overcomingbias.com/2022/08/moral-progress-is-not-like-stem-progress.html). The corollary is that STEM progress _is_ moral progress, while moral research is not moral progress.

ACOUP [writes](https://acoup.blog/2022/08/26/collections-why-no-roman-industrial-revolution/) a progress studies piece, sort of. As he says, "the key takeaway here is just how contingent the industrial revolution was". I'm less convinced than I should be, I think. An inevitable event (thousands of different paths cause X) can appear contingent ("look how unusual and contingent the path to X was!"). It's difficult to distinguish the two without being able to consider counterfactuals.

White House announces debt forgiveness. Not only is this a year of jubilee, but [all years](https://marginalrevolution.com/marginalrevolution/2022/08/the-student-loan-giveaway-its-much-bigger-than-you-think.html) will be years of jubilee. "If you can't say anything nice, say nothing at all"; nevertheless it seems appropriate for taxes to be raised specifically on people with college degrees. We could determine the taxation rate by polling graduates about what _they_ think the correct taxation rate is... but that would just be cruel.

