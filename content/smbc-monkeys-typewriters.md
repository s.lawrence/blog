---
title: "Monkeys, Typewriters, and Black Holes"
date: 2021-12-19
tags: ['Physics']
---

Here's a recent comic from SMBC:

<a href="https://www.smbc-comics.com/comic/monkeys"><img src="https://www.smbc-comics.com/comics/1639846048-20211218.png" class="display-small" /></a>

But obviously we don't need an infinite number of monkeys to eventually create the works of Shakespeare. Let's take the [Japanese macaque](https://en.wikipedia.org/wiki/Japanese_macaque) --- they weigh about 10 kilograms each and live for around 30 years. A reasonably diligent macaque, working 2000 hours each year and typing at 50 words per minute, will bang out just about \\(10^9\\) characters in its lifetime.

According to the Folger Library's [Shakespeare FAQ](https://www.folger.edu/shakespeare-faq), the collected works of Shakespeare total nearly \\(10^6\\) words --- but I think that's not the right number to look at. I'm not going to require that all the collected works of Shakespeare be delivered in order, with no interspersed rubbish. Therefore, what we really care about is the longest play, Hamlet, which has around \\(3\times 10^5\\) words. (By the time Hamlet has been typed up by chance, all the shorter works will already have been produced.) Being a bit generous to the monkeys, let's call that \\(10^6\\) characters.

How many characters do we need to type up Hamlet by chance? Very roughly, we need \\(30^{10^6}\\) chances, and we'll pretend that character typed corresponds to an independent chance to get it right. At this point you see that it's probably sufficient to calculate things to the right "order of magnitude of orders of magnitude". We'll call it \\(10^{10^6}\\) monkey-lives.

The nice thing about working with orders of magnitude of orders of magnitude is that _units don't matter_! \\(10^{10^6}\\) characters is \\(10^{10^6}\\) monkey-lives, and having that many monkeys weighs \\(10^{10^6}\\) pounds. Or kilograms. Whatever.

The radius of a black hole of that mass would be \\(10^{10^6}\\) meters. Or light-years. So that's how much space we need in order to have that many monkeys without them collapsing into a black hole.

The size of the observable universe is \\(10^{10}\\) in some units. So, the conclusion is unchanged: you can't have monkeys produce the works of Shakespeare without collapsing the universe.

