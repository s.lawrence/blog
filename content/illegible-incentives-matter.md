---
title: "Illegible Incentives Matter"
date: 2021-02-06
tags: ['Society']
---

A central motto of modern economics is that "[incentives matter](https://www.econlib.org/library/Columns/y2006/Robertsincentives.html)". What people do is influenced, quite strongly, by how they'll be rewarded or punished for their actions.

> Incentives matter in a house, <br/>
> Incentives matter to a mouse, <br/>
> Incentives matter, here and there, <br/>
> Incentives matter anywhere! <br/>

The main consequence of this view is that, if you want to change people's behavior, you want to change their incentives. Similarly, if you're making policy, you need to be aware of what incentives are being set up, lest you incentivize undesirable behavior.

It's easy to view this as a statement about individual psychology. In that context, this claim is _testable_: do individuals, when offered some money (or presented with some non-monetary incentive, but that's harder to arrange) become more likely to do the thing asked? As a testable hypothesis, it [doesn't hold up so well](https://doi.org/10.1016/bs.pbr.2016.06.010). Of course, you have to take into account the full incentive structure. If you want someone to wash your car, you can offer them \\(20, but that's not the only incentive involved. If they do it, then _everyone will know_ that they're the sort of low-status person willing to perform menial labor for a mere \\)20. This incentive looks different for different people --- for John, it's a good advertisement for his fledgling car-washing business, but for Dr. Sally Smith, who just got her Ph.D. in exolinguistics, it would be rather mortifying.

Still, it seems that incentives are far from the be-all and end-all of individual behavior. Does this mean we should pay less attention to them in understanding societal behavior?

In Libertistan, there are no laws against dumping toxic sludge into rivers. Also, for some reason, there are a lot of toxic sludge factories along the big river that runs through the capital city. Shipping toxic sludge out to be safely disposed of (in some other country where it isn't our problem) is expensive; dumping it into the river is free.

The other weird thing about Libertistan, though, is that the people who live there aren't like us. We (you and I, dear reader) are greedy capitalists. If _we_ were running a sludge factory in Libertistan, you can bet we'd dump it into the river and never lose a wink of sleep. But the people of Libertistan are good, virtuous people. They would never dream of polluting the commons! The river will surely remain clean.

At least, 99% of the people of Libertistan are so virtuous. A tiny fraction are more like us. Perhaps this is of no concern --- only a very small amount of the sludge will be "taken care of" by these morally stunted individuals.

There are 100 sludge factories along that river. The first year, exactly one of them immorally (but totally legally) dumps into the river. The others get together and fund cleanup efforts, and the river remains clean. The second year, a recession hits, and 20 of the factories go bankrupt. The one that was saving money on waste disposal, though, is in strong financial shape. The owner is moved by the plight of the workers of those 20 bankrupt factories, and buys all those factories out.

Now 21 of the factories are run by "the 1%". The third year, the river is in precisely the state you'd expect.

This is a pretty general pattern. You, personally, can ignore the incentives; but then, you will no longer be in charge of deciding how society uses resources. Societal outcomes, for the most part, don't depend on what the majority of _people_ think. What matters is what the majority of _power_ thinks --- and power follows the incentives, even if people don't. (If you define the word "incentive" carefully, that statement is tautological.)

Scott Alexander [discusses one manifestation of this](https://astralcodexten.substack.com/p/webmd-and-the-tragedy-of-legible) particularly in the context of health policy. Official health advice (from any of many sources) is sub-par because there are pretty strong incentives not to be par. Sure, there are loads of people willing to ignore those incentives, but then those people will never have enough influence to be "official".

## Not in my utopia!

Communistan (I'm sorry, I'm not as good at this [as some other people](https://slatestarcodex.com/2020/03/30/legal-systems-very-different-from-ours-because-i-just-made-them-up/).) wants to avoid the tragedy of Libertistan's river. Communistan doesn't have a currency, or even trade. Every month, the 100 Virtuous Bureaucrats (that's the official title) collect all resources in the country, and vote on where they are best allocated --- the tiebreak is given by the Chief Bureaucrat, Hermes Comrade. Aware of the potential for corrupting incentives, the bureaucrats have decreed that any citizen who defies the central allocation scheme will be imprisoned for life. No incentive can overcome that threat, so the prisons remain empty.

The bureaucrats naturally do not see completely eye-to-eye regarding the allocation of resources. This is why voting is necessary. Most of the bureaucrats are virtuous (as per the title), and would never dream of abusing their power to enrich themselves. Five are not so virtuous, and routinely vote to expand their own front yards. Fortunately, material wealth does not affect one's voting power in the Virtuous Congress, and so the body as a whole remains uncorrupted.

Communistan is considering borowing to finance a ballpark and a shoe factory. This means increasing the already high national debt, so neither proposal is very popular. The ballpark has the support of 20 bureaucrats; the shoe factory has the support of 35 different bureaucrats. The first month, neither motion passes. A large school is financed with the support of 52 bureaucrats.

There are always many motions that do not pass, but these two are special because of what happens the next month. The 55 bureaucrats involved strike a deal: they will _all_ vote yes on _both_ measures. Both measures pass. After this surprise, a few deficit hawks become concerned and withdraw their support for building schools. No new schools are financed this month.

The third month, proponents of the school system (and every other part of the budget) realize that in order to get their priorities financed, they're going to have to form similar coalitions. Formally, which items are budget priorities is still decided by vote. In reality, allocations are now entirely decided by back-room deals.

This is all just to make the point that Libertistan's woes have nothing to do with money, or capitalism, or insufficient appreciation for natural beauty. Likewise Communistan's struggles aren't due to the nature of her government, nor the weird ideologies taught at her most prominent school (Princevard). The issue is _power_. In Libertistan, power is determined by monetary resources. In Communistan, power is obtained through flattery and the construction of political alliances.

Once you've defined a utopia free of the notion of power, then you may be free of the influence of incentives. Good luck.

## Incentives and cultural evolution

You, virtuous reader, would never be corrupted by incentives. In fact, you're so concerned about avoiding corruption that you studiously avoid knowledge of what your incentives actually are. When you vote, you do so without calculating whether the ballot measure will help or harm you. You pursue your art without thought of monetary reward. You make sure to remain ignorant of what your romantic partner desires. And so on.

Instead of incentives, you've picked a few (rather famous) individuals you particularly admire. Imitate _them_, you think, and a virtuous life is sure to result.

I'm sure you see the problem. How did your role models get to the place where you had heard of them? By following those incentives of which you're so carefully unaware. Oops! Ignorance of incentives doesn't have so strong a protective effect as one might think.

In fact, this principle goes deeper. An incentive can be unknown to _everyone_, that is, completely illegible, yet still exert a powerful force on society.

This is just a minor variant of an idea at the core of the "Californian school" of cultural evolution. (If you have access, I like [this review](https://doi.org/10.1016/j.shpsc.2016.12.005). [Scott Alexander's book review](https://slatestarcodex.com/2019/06/04/book-review-the-secret-of-our-success/) is also a good introduction.) People naturally copy the behavior of those who are successful. This copying behavior doesn't require anyone to understand _why_ success is associated to certain habits. You can copy a behavior without knowing the history or logic.

The famous example is Manioc. An incentive exists to prepare Manioc properly (a labor-intensive process). If you neglect the preparation, you'll be slowly poisoned over the course of the next many years. Because the poisoning is delayed, this is an _illegible_ incentive. Nonetheless, people who've been preparing their Manioc correctly for the last few years will, on average, be visibly better-off than those who have not, and more people will copy them. The incentive influences behavior at a large scale without any individual necessarily being aware of its influence.

Even the copying behavior needn't be conscious (and I suspect usually isn't). Think about fashion: some people pay a lot of intention to fashion trends, but a lot more just go out and buy something that "looks good". What defines "looks good"? The people buying don't know, but I'll bet it has something to do with all those pictures of happy, healthy, wealthy models on the walls of The Gap.

## Your incentives and mine

Cultural evolution is not something that "happens to other people". I mean, it does, but it also happens here and now. It's driven in part by incentives created by nature (don't eat raw Manioc!), but equally driven by artificial incentives, even when those incentives are illegible.

This is cool until you think about it, at which point it's scary. Everybody knows that academia has been corrupted by a bad incentive: "publish or perish". Nobody knows if this is the only, or even the dominant, bad incentive in academia. Somehow that seems unlikely to me.

It's hard to name candidates for illegible incentives, of course, because the fact that I can recognize it as an incentive implies that it's legible enough. The closest I can come is to name incentives that I'm not totally sure exist, or that others might disagree with:

* Normative credulity. Praise others' work even when it's of very obviously low quality --- this ensures that they do the same for you.

* Relatedly, always cite as many people as possible. Leave nobody out, even if you didn't actually read them. I've heard various sensible-sounding excuses for this practice, but of course reciprocity establishes an incentive.

* Mentor as many students as possible, and try to get them into good positions. Senior professors (at least in math and physics) are judged in large part on who their students were; those who we look up to, therefore, will often be those with many successful students.

Not all incentives, as you can see, are bad --- although whether an incentive is good or bad can itself be controversial.

I'm assuming I've missed a bunch of incentives. In particular, I'm guessing I've missed the incentives that most directly affect my behavior. That's the bit that makes this story scary: it's one thing to knowingly take a bribe, but quite another to unknowingly be doing the Dark Lord's bidding.
