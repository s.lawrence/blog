---
title: "Henry V's Fallacy"
date: 2020-09-05
---

> Westmoreland: _Oh, that we now had here but one ten thousand of those men in England that do no work today._

> Henry: _... No, my fair cousin. If we are marked to die, we are enough to do our country loss; and if to live, the fewer men, the greater share of honor... I pray thee wish not one man more._

The King's statement here is not merely rhetoric (although [his entire speech](http://shakespeare.mit.edu/henryv/henryv.4.3.html) may be the best _fictional_ [motivational speech](https://www.youtube.com/watch?v=A-yZNMWFqvM) in the English language). It's a quantifiable claim of fact. Henry is claiming that the expected utility \\(\langle u\rangle\\) of the [coming battle](https://en.wikipedia.org/wiki/Battle_of_Agincourt), as measured by an individual soldier in his army, would be lowered by increasing the number of soldiers \\(N\\).
$\\( \frac{\mathrm d \langle u \rangle}{\mathrm d N} < 0\\)$

He presents a simple argument. The battle may be won or lost, depending chiefly on many unknown factors beyond the control or knowledge of the English. The expected utility can be written as an integral over all possible values \\(x\\) of those unknown factors (leaving the measure implicit for brevity). The value of \\(x\\) describes everything about the world, _except_ of course the number of english soldiers \\(N\\).
$\\( \langle u \rangle = \int \mathrm d x \\; u(x) \\)$

The utility depends primarily on whether the battle is won or lost. As a slight approximation, let's say the utility of defeat is uniformly \\(u_d\\), and that of victory is uniformly \\(u_v\\). The set of unknown possiblities can be split (assuming a deterministic universe) into those that result in victory and those that result in defeat.
$\\( \langle u \rangle = \int_{\mathrm{loss}} \mathrm d x \\; u_d + \int_{\mathrm{win}} \mathrm d x\\; u_v\\)$

So far, so good. Next Henry notes (correctly, if we ignore [traitors](https://www.youtube.com/watch?v=YK4K7CV_PGY)) that, for a fixed outcome, the average soldier will be no sadder if fewer soldiers fought in the battle. The analysis is simple: either the outcome is a loss, in which case the soldier does not particularly care, or the outcome is a victory, and a victory is sweeter for having been fought against greater odds. 
$\\(\frac{\mathrm d u_{v,d}}{\mathrm d N} \le 0\\)$

The King concludes, Solomon-like, that the derivative of the expected value must negative.
$$
\frac{\mathrm d \langle u \rangle}{\mathrm d N} =
\frac{\mathrm d}{\mathrm d N}\int_{\mathrm{loss}} \mathrm d x \\; u_d +
\frac{\mathrm d}{\mathrm d N} \int_{\mathrm{win}} \mathrm d x\\; u_v =
\int_{\mathrm{loss}} \mathrm d x \\; \frac{\mathrm d u_d}{\mathrm d N} +
\int_{\mathrm{win}} \mathrm d x\\; \frac{\mathrm d u_v}{\mathrm d N}
\le 0
$$

The King has made a classic, perhaps un-Solomon-like mistake: he neglected to account for the dependence of the domain of the integrals on \\(N\\). By changing the number of soldiers and holding everything else fixed, the battle could be taken from a loss to a win --- presumably a dramatic improvement in utility!

As far as I know, the mistake of "neglecting the dependence of the domain of integration" has no [specific](https://en.wikipedia.org/wiki/Freshman%27s_dream) [name](https://en.wikipedia.org/wiki/Sophomore%27s_dream). It's certainly rhetorically useful, though.

You see how much better textual analysis in high school english could have been? For my next post, I'll be sanctimoniously and anachronistically problematizing the Earl of Westmoreland's cavalier attitude towards the unemployment rate in his country.

