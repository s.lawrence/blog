---
title: "Links for March 2023"
date: 2023-03-30
tags: ['Links']
---

Reading James C. Scott in [China's mountains](https://danwang.co/2022-letter/).

Also [from China](https://www.readingthechinadream.com/zhao-yanjing-on-high-tech-tit-for-tat.html) on tech-related trade restrictions (between China and the U.S.):

> We can readily predict that as China's policy of attracting talent grows
 stronger and as those who return to China continue to succeed, American
 suspicions of the Chinese who remain in the U.S. will increase, meaning
 that the space for these Chinese to continue to advance in the U.S. 
will narrow, which will only encourage more Chinese to return to 
China. Once the patents prohibited by the U.S. can be reapplied for in
 China, the Samuel Slaters and Zhang Rujings who master this technical 
know-how will flock to China.  This will reverse the talent flow, 
leaving the U.S. with a shortage of talent, and the outcome of the 
U.S.-China technology war will be clear.

A [report](https://reason.com/2023/03/22/this-just-in-conspiracy-theorists-not-quite-as-kooky-as-previously-reported/) from "the second International Conspiracy Theory Symposium". _Relative to the relevant opposition_, conspiracy theorists are looking quite good these days. 

Somewhat relatedly, here's [Musa al-Gharbi, writing in 2019](https://musaalgharbi.com/2019/08/27/unearned-smugness-partisan-diploma-divide/), on the vaunted "diploma divide". He ends with:

> It may be emotionally satisfying for academics and intellectuals to disparage or patronize the less educated and their political allegiances, but this condescension is unearned: the political leanings of highly-educated or intelligent people tend not to be any more rational or informed than anyone else’s. Putting on a pretense of superiority is likely to blow up in our faces.

As Rick Perry would say, "oops!"

[In which](https://twitter.com/peakcooper/status/1639716822680236032) GPT-4 provides medical advice. Of course this sort of thing was possible before, and has been in principle possible for a long time---but now it has a friendly face!

Again somewhat related, on the topic of AI safety, it looks possible that [all](https://thezvi.wordpress.com/2023/03/30/on-the-fli-open-letter/) [hell](https://astralcodexten.substack.com/p/mr-tries-the-safe-uncertainty-fallacy) [is](https://twitter.com/mattparlmer/status/1641230149663203330) [about](https://marginalrevolution.com/marginalrevolution/2023/03/thursday-assorted-links-398.html) [to](https://futureoflife.org/open-letter/pause-giant-ai-experiments/) [break](https://manifold.markets/au_/will-flis-pause-giant-ai-experiment) [loose](https://manifold.markets/MatthewBarnett/will-any-top-ai-lab-commit-to-a-mor). I don't think I've read anything on this topic that makes me happy, either in the "things are going well" sense or simply because "at least there are some people behaving reasonably". Cowen's comparison to the early days of COVID is apt, of course, but it's not an encouraging observation!

