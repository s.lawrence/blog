---
title: "Links for February 2022"
date: 2022-02-26
tags: ['Links']
---

[A thread](https://twitter.com/The_OtherET/status/1487851988981362694) on epidemics "crowding out" other respiratory diseases.

Freddie deBoer on [harassment of public figures](https://freddiedeboer.substack.com/p/this-is-the-world-in-which-we-live). A question worth considering: suppose you have some vice. Clearly, the best thing would be to rid yourself of the vice, but that's somewhat unrealistic (and not what Freddie requests). Given that constraint, is it better to lie about your vice --- thus making yourself a hypocrite while encouraging others to do better --- or to "own" it?

[A good reason to write](http://www.paulgraham.com/words.html).

Hanson [asks](https://www.overcomingbias.com/2022/02/can-we-tame-political-minds.html): as the market yolks self-interested minds, what structures can harness political minds to divert their energies towards societal good?

A recurring theme nowadays: ["The world of flesh and blood does not work like the world of memes and tweets."](https://scholars-stage.org/thoughts-on-shitpost-diplomacy/). See also [simulacra levels](https://www.lesswrong.com/posts/Z5wF8mdonsM2AuGgt/negative-feedback-and-simulacra), and [Zvi's comments](https://thezvi.wordpress.com/2020/06/15/simulacra-and-covid-19/).

Also quotable: ["The true revolutionary impulse [is] of construction rather than destruction"](https://lateralthinkingtechnology.wordpress.com/2020/08/16/the-revolution-will-not-be-trending-hong-kong-social-media-and-the-failure-of-attention-politics/).

