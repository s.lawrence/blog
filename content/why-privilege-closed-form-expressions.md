---
title: "Why privilege closed-form expressions?"
date: 2020-09-25
---

If I say, "let \\(n\\) be the smallest integer expressible as a sum of two positive integer cubes in two distinct ways", I have unique identified [an integer](https://en.wikipedia.org/wiki/Taxicab_number). Likewise, if I say "let \\(r\\) be the ratio between the masses of the lightest vector and lightest scalar in \\(SU(3)\\) Yang-Mills", I have (under [a reasonable conjecture](https://en.wikipedia.org/wiki/Yang%E2%80%93Mills_existence_and_mass_gap)) uniquely identified a positive real number. If you ask me for a positive real number, and I reply "\\(r\\), where \\(r\\) is the ratio...", you will be rightly annoyed with me. You wanted a reply more like "1.86".

But what if I reply with "\\((5\pi + 1)/9\\)"? It's not an immediately useful expression: you have to do a bit of work even to decide if it's greater than or less than \\(2\\). But at least it's a "closed-form expression". Aren't I nice?

Closed-form expressions are exciting. We can calculate the ratio of masses \\(r\\) above to a few digits of precision; we can get critical exponents of [some](https://en.wikipedia.org/wiki/Ising_critical_exponents) [CFTs](https://en.wikipedia.org/wiki/Percolation_critical_exponents) quite precisely. But even ten digits is not as desirable as a closed-form expression. What gives?

First, the notion of a _closed-form_ expression has no universally accepted, rigorous definition. It certainly excludes stuff like implicit definitions (\\(x = 1 + 1/x\\), is not a closed-form expression). It definitely includes \\((5\pi+1)/9\\), which consists only of well-known quantities and a few simple arithmetic operations. Integration is more controversial: most people seem to hold that integration is not allowed, although I think low-dimensional integrals ought to be. Exponentiation is generally allowed; more exotic functions (the Bessels, say) are often allowed. (I'm comfortable including such functions only because they can be written as low-dimensional integrals.)

Roots of polynomials are a good example. Nobody would consider "the largest-magnitude root of \\(P(x)\\)" to be a closed-form expression. If \\(P\\) is quadratic, cubic, or quartic, there are of course closed-form expressions available for \\(x\\) in terms of nothing more complicated than radicals. Even for a quintic polynomial, though, a closed-form expression is available if you're willing to admit Jacobi theta functions --- and that's perfectly satisfactory.

There's a qualitative difference between (most) closed-form expressions and other types of specifications: closed-form expressions are algorithms. I don't mean that algorithms exist for evaluating closed-form expressions, but rather that most closed-form expressions are, quite literally, the algorithms themselves. The expression \\((5\pi + 1)/9\\) specifies an algorithm: start with \\(5\\), multiply by \\(\pi\\), add \\(1\\), finally divide by \\(9\\). Low-dimensional integrals (of sufficiently smooth functions, I guess) specify approximation algorithms: the Riemann integral is defined as the output of an algorithm.

Expressions we'd acknowledge as closed-form are generally also _efficient_ algorithms. Here's a pathological example:
$$
f(N) =
\int \mathrm d^{N}\\!x\\;
e^{-x^2}
$$
Since this is an integral, as we've discussed, it constitutes an algorithm for evaluating \\(f(x)\\). Alas, the algorithm requires time exponential in one of the parameters! This is not so useful.

So there is, after all, a nice unifying principle here. Closed-form expressions are those that describe efficient (polynomial in all relevant parameters) algorithms. And of course, that's a moral lesson as well as a definition: for nearly all purposes, an efficient algorithm is as good as (and as hard to find as) a clean-looking expression.
