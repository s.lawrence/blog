---
title: One-paragraph book reviews
date: 2024-02-25
tags: ['Books']
---

Bryan Caplan [started it](https://www.betonit.ai/p/one-paragraph-book-reviews) (and seems to have dropped it), and I think it's a good idea, so here goes.

C. S. Lewis, _That Hideous Strength_. Probably my favorite Lewis book, at least for adults. Like _Narnia_, clearly has an evangelic thrust towards Lewis's particular flavor of Christianity. (This will be much more obvious if you've read e.g. _Screwtape Letters_ or _Mere Christianity_.) Reading it today it has an entertaining reactionary feel: a distrust of scientists and scientism, of large institutions, of anything much beyond family, friends, and individual duty.

Neil Gorsuch, _A Republic, If You Can Keep It_. I've often found that I like people less after reading their memoirs/autobiographies. Happily that was not the case here. This is a collection of speeches, essays, and excerpts from opinions. About two-thirds of the book is what might be covered in a decent civics course; the remaining third of the book is legal ethics. On the latter topic, Gorsuch's speeches (understandably) emphasize the conduct of judges and trial lawyers. I can't find good statistics on what law school graduates end up doing, but my sense is that this is something around 20% (at most) of lawyers. A great deal of societal power is surely wielded by the remaining 80%, but less examined.

John J. Mearsheimer, _The Tragedy of Great Power Politics_. An argument for [offensive realism](https://en.wikipedia.org/wiki/Offensive_realism). This is the first book I've read on international relations, so I'm entirely unable to pass judgement on the claims made. The final part of the book (written 2014) is dedicated to predictions for relations between the U.S. (and east Asian allies) and China, conditioned on China's economic rise continuing more or less unabated. Offensive realism, at least as defined by Mearsheimer, claims that `hot' conflict between China and the U.S. is considerably more likely than it was between the U.S. and U.S.S.R. during the cold war. Mearsheimer clears two minimal bars with this book: first, to an uninformed reader, his arguments are convincing; second, his predictions made in 2014 have not yet been obviously falsified.

