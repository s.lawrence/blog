---
title: "Links for October 2020"
date: 2020-10-25
tags: ['Links']
---

How willing are scientists to change the direction of their research, in exchange for money? From NIH data, [Kyle Myers concludes](https://pubs.aeaweb.org/doi/pdfplus/10.1257/app.20180518) that the switching costs are large. ([Here](https://eadf8162-9717-419f-88f5-93956f82ef0f.filesusr.com/ugd/73b908_6cc9f6d247fe4d408f27286f50b8c34f.pdf) is an ungated draft.) Of course, "science" is heterogeneous. Are these large switching costs primarily due to the purchase of new equipment, or the need to re-train in a new field? Anecdotal evidence suggests that switching costs are much lower in, say, mathematics or theoretical physics. I looked for an answer, but all I found was George Borjas [complaining about immigration](https://www.nber.org/system/files/working_papers/w18614/w18614.pdf).

The [MIP\* = RE](https://arxiv.org/abs/2001.04383) proof (which I hope one day to understand well enough to write about) had a bug, now fixed. One of the authors [writes about the experience](https://mycqstate.wordpress.com/2020/09/29/it-happens-to-everyonebut-its-not-fun/).

A room temperature superconductor has been constructed, by [using very high pressures](https://www.sciencenews.org/article/physics-first-room-temperature-superconductor-discovery).

The House of Representatives [published](https://judiciary.house.gov/uploadedfiles/competition_in_digital_markets.pdf) a report on antitrust issues regarding the big technology companies. [Matt Stoller argues](https://mattstoller.substack.com/p/congress-gets-ready-to-smash-big) that this reflects a larger shift towards strong anti-monopoly sentiment, which is now obvious to everybody. I believe this, because Tyler Cowen has, for a little over a year now, been [singing the praises](https://www.amazon.com/Big-Business-Letter-American-Anti-Hero/dp/1250110548) of large, monopoly-like businesses.

Closely related: [does concentration reduce labor's share of income?](https://itif.org/publications/2020/10/13/monopoly-myths-concentration-eroding-labors-share-national-income) See also [Cowen's comment](https://marginalrevolution.com/marginalrevolution/2020/10/is-concentration-eroding-labors-share-of-national-income.html).

Tom Lehrer [has released](https://tomlehrersongs.com/) the lyrics of his songs into the public domain. The big news here is that he hadn't bothered to do that previously, either out of sloth or sheer disregard for the notion of copyright law.

The correspondence between git repositories and Github repositories is not one-to-one. If you fork a repository on Github, Github doesn't keep the two repositories entirely separate. There have been mild security concerns related to this before. Most recently: the RIAA submitted a DMCA takedown notice to Github for `youtube-dl`. Github complied, of course, and posted the takedown notice to the [`dmca`](https://github.com/github/dmca) repository, as usual. An enterprising fellow forks the `dmca` repository, and adds the source code to `youtube-dl` to his fork. As a result, the source code that the RIAA wanted taken down is, as I write this, visible through Github's repository of take-down notices. Discussion [on Hacker News](https://news.ycombinator.com/item?id=24882921).
