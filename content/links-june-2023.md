---
title: "Links for June 2023"
date: 2023-06-30
tags: ['Links']
---


[Here](http://library.sciencemadness.org/lanl1_a/lib-www/la-pubs/00329010.pdf) is the famous paper on atmospheric ignition after a nuclear explosion. Not often mentioned (e.g. by [EY](https://www.lesswrong.com/posts/f3W7QbLBA2B7hk84y/la-602-vs-rhic-review)): the article is dated 1946.

From RtCD, on [psychological health in China](https://www.readingthechinadream.com/blog/back-from-china). Very American (and later articles posted too).

