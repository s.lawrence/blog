---
title: "Trivia From Medema's \"Economics Book\""
date: 2021-05-08
tags: ['Books']
---

_The Economics Book_ is [Steven Medema's](https://sites.duke.edu/sgmedema/) collection of 500 important accomplishments in economics, from 700 B.C.E. up through 2009. The trivia below I came across while reading this, although many are not actually in the book. The trivia are mostly in chronological order, just like the book is; however, I've relegated a few notes (chiefly criticisms) to the end.

----

The Catholics were famous for having a rather moralizing attitude towards commerce and finance, but Thomas Aquinas's _Summa Theologica_ apparently takes a rather modern view of trade (not interest). Among other things, he argued that market prices are automatically fair (after all, there's somebody willing to pay), and condoned what today is called price gouging.

Ibn Khaldun's _Al-Muqaddimah_ not only presages Adam Smith (division of labor, harmful monopolies, and so on), but contains a story of economic cycles eerily reminiscent of Acemoglu and Robinson's _Why Nations Fail_. In Medema's words:

> As standards of living increased, both the population and the ruling dynasty would become complacent, losing the drive that spurred the original economic expansion. The government would levy oppressive taxes...

In the late 18th and early 19th centuries, the UK had specific laws against the destruction of machines, including (between 1812 and 1814) the death penalty. More than 50 Luddites were executed around this time.

The phrase "dismal science" originates in Thomas Carlyle's complaint that economics had nothing to say in support of slavery.

For some reason, all of this talk about "gold standard" had me convinced that gold doesn't experience inflation in practice. In fact, price indices were invented and used in the 18th and 19th centuries to measure the fall in gold's purchasing power.

Gossen's second law holds that if I try to optimize my utility, then "the last dollar spent on clothing generates the same utility increase as the last dollar spent on food". This is simple enough to prove, and note that it's reminiscent of the idea of triage. Thinking this way gives rise to the notion of the [value of life](/poor-phrasing-shibboleth).

You know how every pop economist (including your uncle at thanksgiving) believes that there are ~50-year-long cyclic phenomena driving society? The most recently-in-the-news case of this is Turchin's claim that we should see peak violence this year (or was it last?). It looks like this meme got started with Nikolai Kondratiev in the early 1920s. Note that if you have \\(L\\) data points, it's really easy to think you see a wave-like phenomenon with period around \\(L/2\\) or \\(L/3\\). If data collection and retention improves, and our historical time horizons get longer, I expect cyclic theories with longer periods to come into vogue.

The idea of a "multiplier effect" --- the government spends one dollar and the economy grows by three --- originated in the 1930s with Richard Kuhn. Of course, the size of the multiplier must depend on your time horizon. I suppose the multiplier of the _average_ dollar, across the entire economy, can be determined from the rate of GDP growth.

An allocation of resources is *Pareto efficient* if there's no reallocation under which nobody is worse off (and at least one person is better). A *Pareto improvement* is such a reallocation. A *Kaldor-Hicks improvement* is a reallocation under which those who are worse off could in principle be compensated by those who have improved, to yield a Pareto improvement --- note that the compensation does not need to actually occur. An allocation is *Kaldor-Hicks efficient*, of course, if no such improvement is possible.

In an English auction --- the "going once, going twice" kind --- the early bids you make should be well below your true price. After all, if your competitor values the good at &euro;10, and you value it at &euro;60, you don't want to end up paying six times as much as you have to! So, the price slowly increases, until the other fellow drops out. The *Vickrey auction* is a beautiful mechanism that takes this into account. Each contestant submits a single sealed bit, and the person who submits the highest bid wins, but pays the second-highest bid instead. In this system, everybody is incentivized to give a truthful bid! The two mechanisms have, however, the same expected outcome: the contestant with the highest valuation wins, paying the second-highest valuation.

Sen's "[liberal paradox](https://en.wikipedia.org/wiki/Liberal_paradox)" arises when individuals have a preference for the rights of other individuals to not be respected. In such a situation, no social planner (or other mechanism) can simultaneously respect individual rights and achieve Pareto efficiency.

----

Writing about economics for a popular audience tends to be terrible. This surprises nobody. A typically horrid example is the Wikipedia article on "[balance of trade](https://en.wikipedia.org/wiki/Balance_of_trade)". As I write, Wikipedia has a paragraph on Armenia's trade deficit claiming:

> The reason for the trade deficit is that Armenia's foreign trade is limited by its landlocked location and border disputes with Turkey and Azerbaijan, to the west and east respectively.

This, of course, doesn't make a whole lot of sense, and _certainly_ does not describe a mechanism. Moreover, a quick search reveals there are plenty of landlocked countries that run a trade surplus. I can't be certain, but I'm guessing that the writers of that article don't know what they're talking about.

Actually, that paragraph might be one of the least-bad parts of the article. Much of that article is at pains to make clear that trade deficits are not a bad thing. I'm perfectly willing to believe that, except the article falls squarely into "doth protest too much" territory, with clear signs of motivations beyond "propagating knowledge".

It would help if the article didn't immediately contradict itself. Take the paragraph beginning

> The notion that bilateral trade deficits are bad in and of themselves is overwhelmingly rejected by trade experts and economists.

After citing five articles that are more about Trump than trade, the article proceeds to say

> According to the IMF trade deficits can cause a balance of payments problem, which can affect foreign exchange shortages and hurt countries. On the other hand, Joseph Stiglitz points out that countries running surpluses exert a "negative externality" on trading partners, and pose a threat to global prosperity, far more than those in deficit.

These statements certainly sound like they're saying "countries running a surplus are harming countries running a deficit", which is the protectionist line the article is trying to argue against. Note also that "on the other hand" seems to imply Stiglitz's statement is in contradiction to the IMF, when it's really not.

I mention this mess to make clear that, although I'm about to complain at some length about _The Economics Book_, I was nevertheless pleasantly surprised by the quality and accuracy of Medema's writing! The constraints on such a book are strong: each article is merely one page long, and must be accessible to a general audience without horrifically misrepresenting the concept. In general, Medema seems to have done surprisingly well.

----

Now for the (mild) kvetching about Medema's book.

Obviously, any time a vaguely mathematical subject comes up, the book falls flat. For example, on Jevons's price index, we read

> Jevons calculated the ratios of the prices between the base year and the year in question for each product. He then took the geometric mean, which better accounts for the potentially vast differences in prices across different goods [...]

This is not really an accurate description of why one might take the geometric mean here. First, the issue isn't "vast differences in prices", but rather "vast differences in price _ratios_", and realizing that we're working with ratios is what gives the intuition that the geometric mean might be appropriate. Moreover, the claim that the geometric mean is "better" when working with "potentially vast differences" is flat-out false, and this is why it's not used today. If the price of any one good drops to zero, then in the geometric mean (i.e. the arithmetic mean on a log scale) that change will dominate everything else.

As another example, the description of the marginal revolution strikes me as missing the point. Medema says that it "highlighted the influence that even [infinitesimal] changes in incentives will have". That makes it sound like a case of "small changes can have big effects", which is definitely not what all the fuss was about. (See Gossen's second law, mentioned above, for an example of marginal thinking.)

Plenty of similar examples exist, which I won't bother to list. These sorts of misunderstandings are to be expected. Less forgivable is the description of Hayek's information argument. It starts off good:

> [...] Hayek made the case that government planners couldn't possibly possess all the information about the demands for, quality of, and availability of goods that is conveyed by the pricing system. Resources would inevitably be misallocated [...]

Notice that Hayek's argument is _not_ about the difficulty of optimization problems. An optimization problem is difficult whether the work is parallelized or not, so in a world of perfect information, there's no computation-based argument that a central planner must fail. Hayek's argument is that we do not live in a world of perfect information! The task of collecting accurate information (when people have many incentives to lie) is what is hard.

So, after explaining Hayek's argument both accurately and succinctly, Medema concludes the page with

> [...] advances in computation continue to suggest, for some, that efficient socialist planning is very much within the realm of possibility

thereby undoing the good work he'd done in the previous sentences.
