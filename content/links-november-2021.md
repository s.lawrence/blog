---
title: "Links for November 2021"
date: 2021-11-27
tags: ['Links']
---

Bidirectional unicode features can be used (e.g. in [rust](https://blog.rust-lang.org/2021/11/01/cve-2021-42574.html)) to produce code that apparently says one thing, but compiles to another.

[More](https://richardhanania.substack.com/p/liberals-read-conservatives-watch) from Hannania. I think the way to read him is to ignore all of the conclusions. He does seem to have some talent for picking out interesting, important, and underrated facts.

[Claims](https://b0ac9a84-177d-4e54-a19e-df525dfaf051.filesusr.com/ugd/87e482_9eb44ae506c446f9a0094db9a82bc610.pdf) about politics and technology. From the abstract:

> How has mobile internet affected political polarization in the United States?
> Using Gallup Daily Poll data covering 1,765,114 individuals in 31,499 ZIP codes
> between 2008 and 2017, I show that, after gaining access to 3G internet, Democratic
> voters became more liberal in their political views and increased their support for
> Democratic congressional candidates and policy priorities, while Republican voters
> shifted in the opposite direction. This increase in polarization largely did not take
> place among social media users. Instead, following the arrival of 3G, active internet
> and social media users from both parties became more pro-Democratic, whereas
> less-active users became more pro-Republican.

[Urbanicity and mental health](https://www.nature.com/articles/s41562-021-01204-7) --- the correlation goes in the expected direction.
