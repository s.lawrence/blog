---
title: "Emergent Hypocrisy"
date: 2022-03-02
tags: ['Society']
---

There's an interesting phenomenon where many individually honest people come
together and form a hypocritical group. Take two issues: for instance, support
for disruptive protest, and support for international free trade. Considering
those two issues, our group might be made of:

* Those who oppose free trade, and who think that blocking bridges is an acceptable protest
* Those who oppose tariffs, and who think the blocking bridges is an acceptable protest
* Those who oppose free trade, and think the government should forcefully clear bridges of protesters
* Those who oppose tariffs, and who think the government should forcefully clear bridges of protesters

This is a simplification of real life. In real life, plenty of people are open
hypocrites, who believe bridges should be forcefully cleared only of the
"wrong" protestors. Nevertheless, we're going to look at groups consisting only
of the honest and consistent. In particular, we'll focus on the first
category---the yellow faction---who oppose free trade and support disruptive
protests, and the last category---the green faction---who oppose tariffs and
support police breaking up protests by force if necessary.

One month, President Cain announces a free trade agreement with
Ubekibekibekistanstan; predictably, protests break out. The next month, the
U.S. withdraws and institutes tariffs instead. Again, disruptive protests
appear. How do our hypothetical factions respond to these events?

After the first, pro-tariff protests, the yellow faction speaks out in
unambiguous support. Many join the protesters; some suggest strikes; Twitter is
lit up for days. "You're either with us or against us." Folks in the green
faction are similarly united, except in condemnation rather than support, and
advocate for the national guard to be called in to clear the protests out. Your
Facebook feed is filled with videos of poorly behaved protestors. "Silence is
violence."

Now come the anti-tariff protests. Both factions find themselves in an awkward
spot. From the green faction, we hear mumbled condemnation: "we agree with your
cause _of course_, but your methods are not productive". In the market of
social media outrage-bait, these articles don't sell very well. Meanwhile, when
asked, everybody in the yellow faction says that they support the right to
protest---but when the police come through and clear the streets, the
objections are somewhat muted. Certainly they won't join their fellow citizens
in the streets! "Now is not the time." In both factions, most choose to simply
stay silent, rather than risk annoying their friends or betraying their
consciences.

And what happens when someone calls out the hypocrisy? Responding to the
accusation is easy. Every individual person can truthfully point out they've
been consistent, either in supporting or opposing disruptive protest. What's to
complain about? "If the volume of my support has changed, perhaps it's because
I'm busier this week. Besides, I can't be expected to fight every battle. This
protest isn't the most important thing happening right now."

---

Many variations on the above story are possible. Common to all is some form of
selective silence. Even people who successfully stick to their principles find
themselves caring about some incidents more than about others. Some "strike
close to home", others are "complicated". Individually understandable,
collectively corrupted.

So how can one avoid contributing to group-level hypocrisy? The simplest
approach is to say nothing, to join no group. This seems doomed to fail:
eventually, there'll be an incident that hits just the right combination of
hot-button issues for you, and you'll be compelled to speak out.

At the other extreme: speak out on every issue, no matter how minor or
inconsequential. But of course, you don't hear about every issue. Your local
social graph is biased, and now your speech carries their biases with it. Nor
do you have enough time and energy to guarantee that on every issue, your voice
carries the same volume.

A more moderate course, though, might work. A sharp, clear, reasonably
objective criterion for when to speak out, and when to not. Some people have a
single issue, which they pursue with a fanatical single-mindedness, working
hard to seek out _every_ relevant incident. (For an example, see the [free
speech section](https://jonathanturley.org/tag/free-speech/) at Jonathon
Turley's blog.) Another option, more workable for those with broader interests
and less time, is to comment on those issues that directly affect us, or people
sufficiently nearby in the social graph. This has other advantages too, for
instance reducing the frequency with which I must comment on things I know
nothing about. It's also, in some sense, more "natural". It removes power from
the great movements that sweep over a whole nation, and works to reduce debates
to local, concrete issues.

_See also [Freddie deBoer](https://freddiedeboer.substack.com/p/this-is-the-world-in-which-we-live) on a long-standing instance of this phenomenon._
