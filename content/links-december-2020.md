---
title: "Links for December 2020"
date: 2020-12-26
tags: ['Links']
---

Over-incarceration [as a market failure](https://marginalrevolution.com/marginalrevolution/2020/11/misaligned-incentives-for-incarceration-in-the-united-states.html). It seems like much more could be written on this perspective.

Deepmind [declares victory](https://deepmind.com/blog/article/alphafold-a-solution-to-a-50-year-old-grand-challenge-in-biology)! Mohammed AlQuraishi [outlines the method](https://moalquraishi.wordpress.com/2020/12/08/alphafold2-casp14-it-feels-like-ones-child-has-left-home), and discusses the implications for protein structure prediction and related fields. And [here](https://www.youtube.com/watch?v=B9PL__gVxLI) is a video about the result from a machine learning researcher.

Gowers [issues a challenge](https://xenaproject.wordpress.com/2020/12/05/liquid-tensor-experiment/): create a computer-checkable proof of a foundational result in "condensed mathematics". The challenge is issued as a result of his own fear that his proof is flawed (after spending about a year working on it).

Socrates [as the bad guy](https://diagonalargument.com/2020/12/08/socrates-bad-guy/).

Bryan Caplan [doesn't think much](https://www.econlib.org/call-it-sour-grapes/) of elite universities.

[Another claim](https://www.scottaaronson.com/blog/?p=5159) of quantum supremacy appears! As with all these early claims, a key question is whether or not a classical computer could perform the same computation. In this case, Gil Kalai [thinks so](https://arxiv.org/abs/1409.3093).

The [essence of economics](https://statmodeling.stat.columbia.edu/2020/12/17/if/), in a parody of he-who-must-not-be-named. Nearly half applies to physics without modification.

Microsoft's report on [the SolarWinds hack](https://www.microsoft.com/security/blog/2020/12/18/analyzing-solorigate-the-compromised-dll-file-that-started-a-sophisticated-cyberattack-and-how-microsoft-defender-helps-protect/) is worth a read. One can draw many parallels between public health (particularly pandemic preparedness) and cyber security. One way in which they're not similar: we have a pretty good idea what "the worst" pandemic looks like. We have no idea what a worst-case cybersec incident looks like.
