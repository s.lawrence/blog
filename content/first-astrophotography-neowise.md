---
title: "First Astrophotography: Neowise"
date: 2020-07-20T21:13:44-04:00
---

The comet NEOWISE (properly, [C/2020 F3 (NEOWISE)](https://en.wikipedia.org/wiki/C/2020_F3_(NEOWISE))) provided an opportunity to finally try out astrophotography with a reasonably nice camera.  The comet was magnitude 3.3 at this point, and visible to me only if I looked slightly to the side.  If you look closely, you can clearly see that I need an actual tripod.
<img src="/neowise2020/neowise1.jpg" class="display" />

By the time we'd found a good location and I'd figured out how to work my camera, the comet was already setting into the lights of Strasburg (at least, I _think_ that's Strasburg).
<img src="/neowise2020/neowise2.jpg" class="display" />

Once the comet set, things were somewhat more relaxed (and the crowds of other watchers began to abate). The only summertime constellation I'm really familiar with is the Big Dipper, and I managed to get quite a nice photo of that.
<img src="/neowise2020/dipper.jpg" class="display" />

In trying to take a photo with less light pollution from the city, I got the Little Dipper visible as well.
<img src="/neowise2020/dippers.jpg" class="display" />

None of these images have been digitally retouched, of course, because I couldn't figure out how to get GIMP to do anything useful. To the extent that digitally retouched photos are dishonest, I am honest only in proportion to my sloth and incompetence.

