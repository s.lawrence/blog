---
title: "Links for March 2022"
date: 2022-03-27
tags: ['Links']
---

Very few _interesting_ things happened in the past month. [Here](https://thezvi.wordpress.com/2022/03/10/ukraine-post-2-options/) is Zvi on some of the less interesting stuff.

Information warfare in 2003: [an American perspective](https://irp.fas.org/doddir/army/fm3-05-301.pdf).

"[Whoever created this Rube-Goldberg machine was not as smart as they imagine.](https://github.com/ytdl-org/youtube-dl/issues/29326#issuecomment-894619419)"

[A game](https://oec.world/en/tradle/). Great concept, the execution is slightly flawed---don't click for details, because it may spoil the answer. [Semantle](https://semantle.novalis.org/) is also quite good.

A [conversation](https://betonit.blog/2022/03/18/spreadsheets-letters-from-a-quant/) between Bryan Caplan and Applied Divinity Studies on rigor in social sciences. The existence of quantitative analysis can matter, even when nobody checks it, as long as it's possible _in principle_ for it to be checked.

[An argument against decoupling research from education](https://www.nber.org/papers/w29853).

France's [moratorium](https://www.science.org/content/article/france-issues-moratorium-prion-research-after-fatal-brain-disease-strikes-two-lab) on prion research. (Apparently, research has since resumed.)

