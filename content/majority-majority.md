---
title: "The Majority are a Majority"
date: 2022-02-11
tags: ['Society']
---

Most people are in the majority.

This really shouldn't need to be said. It's one of those things that's obviously, unquestionably true, while its consequences are routinely ignored.

## I

In that room where exactly one person is a liar, two people describe themselves to you. Alice claims "4 out of 5 of my friends voted for Politicus Maximus, as did I". Says Bob, "Nearly all of my countrymen oppose Politicus Maximus; only I am bold enough to support him".

Bob is lying. Certainly? No. But very probably.

## II

In 1997, 27% of the U.S. supported same-sex marriage. Twenty-five years later, that sits at 70%. Ditto, roughly speaking, for legalization of marijuana. [Planck's principle](https://en.wikipedia.org/wiki/Planck%27s_principle), clearly, does not apply: half the population of the U.S. has not been replaced in that time (and plenty of newcomers sit on "opposed"). Some people have changed their minds. Most of those people started in opposition when that was the majority position, and then became supporters when support lay between 40% and 60%.

> _You served him well, when serving was safe._

Much the same story is true of any case where the public's mind changed on a reasonably short (sub-generational) timescale. It will always appear that the median voter is rather spineless --- no matter how said voter behaves!

## III

<blockquote class="poem">
<pre>
You at the barricade listen to this
No one is coming to help you to fight
You're on your own
You have no friends
Give up your guns - or die!
</pre>
</blockquote>

Sure, we could be the only 100 people (out of 35 million in France) who agree with our cause, but that's vanishingly unlikely. Much more plausible: the others are not coming to help because they don't believe that _other_ others are coming to help us fight. It's nothing but a giant collective action problem.

Or maybe it isn't! Just as likely, similar struggles continue throughout (breaking the French metaphor) the evil empire, and we just don't know about it because the dictator controls all the broadcast towers. Imperial forces have, after all, a strong incentive to make you believe that you're truly alone, so why trust them?

When fighting power, you're never as alone as you fear. If you believe you're in the 1%, it's far more likely (nearly ten times as likely) that you're in the 10%.

<blockquote class="poem">
<pre>
Your comrades chase e'en now the fliers,
      And, but for you, possess the field.
</pre>
</blockquote>

## IV

Which is good news! Unless you care about your immortal soul. 

You woke up. You found the invader. And now you're one of a very few brave resistance fighters: 5% of your countrymen on a good day. You'd love to fight honorably, to spare civilians who dissent, to give fair trials to suspected spies, to protect your prisoners of war. But hard facts trump soft hearts, and the brutal truth of the matter is that you're fighting for your survival. Yes, your methods now are brutal, but without them your 5% is certain to be wiped out in the face of the vast unity of evil you confront.

But, as you know, you're not in the 5%. Whatever your position in the world looks like to you, you must take into account the simple evidence that you are in your group --- and therefore your group is unlikely to be particularly small. Could you be in the 15%? The 30%? Just how many are your compatriots, and how few the oppressor? How, exactly, will you know when your compatriots become a true majority?

> _Are we the baddies?_

And how many of your compatriots will care?

## V

Thus far we have considered the majority of _people_. But there are other types of majorities. A typical dollar in the stock market may believe that it is part of a contrarian investment, but of course, statistically, it isn't. In fact, it can never be the case that a majority of invested money is meaningfully contrarian. Again, this is a statement about definitions: if you wish, an "accounting identity" (or "accounting inequality", strictly speaking). We don't get to dream of a kinder, gentler world. Words mean what they mean.

In the same way, it can never be the case that a majority of opinion pieces are running contrary to "the narrative" (and you should be suspicious of any opinion piece that tells you that it is). Nor can a majority of power, suitably measured, be on the side of the powerless.

