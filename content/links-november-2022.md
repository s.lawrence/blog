---
title: "Links for November 2022"
date: 2022-11-27
tags: ['Links']
---

Sven Bachmann: [On the classification of quantum phases of matter](https://www.youtube.com/watch?v=d5MTFCj8NSk).

There was a conference on academic freedom. I particularly recommend the [panel](https://www.youtube.com/watch?v=sG6hXR8UbMk) with Niall Ferguson, John Cochrane, and Tyler Cowen. A recurrent theme (obvious in retrospect, maybe?) is that academics outsourcing administrative duties has made things much worse. The good news is that this can plausibly be reversed!

