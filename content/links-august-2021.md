---
title: "Links for August 2021"
date: 2021-08-24
tags: ['Links']
---

Introducing the quantum mechanical [bootstrap](https://arxiv.org/pdf/2108.08757.pdf).

[On the decoupling of rhetoric and reality.](https://www.thepullrequest.com/p/we-are-no-longer-a-serious-people) (The inflamatory bits are not the interesting bits.)

[Results](https://arxiv.org/abs/2103.01256) on creative hot streaks.

More [fraud](https://www.buzzfeednews.com/article/stephaniemlee/dan-ariely-honesty-study-retraction). The irony is juicy.

