---
title: "Links for September 2021"
date: 2021-09-26
tags: ['Links']
---

[The obvious](https://unherd.com/2021/08/how-not-to-talk-to-a-science-denier/).
I suspect there's a general principle: Alice will never convince Bob unless
(Bob thinks) there's a chance that Bob will somehow convince Alice. This makes
all advice addressing "how to convince people" kind of pointless.

[The long road](https://blog.computationalcomplexity.org/2021/08/the-long-road.html) to a faculty post.

Lance Fortnow [on the rise of machine learning](https://blog.computationalcomplexity.org/2021/09/the-death-of-expertise.html) and its relation to expertise:

> There's a more personal issue--people spend their entire careers creating
> their expertise in some area, and that expertise is often a source of pride
> and a source of income. If someone comes along and tells you that expertise
> is no longer needed, or even worse irrelevant or that it gets in the way, you
> might feel protective, and under guise of our expertise tear down the
> learning algorithm. 

A [website](https://forecastadvisor.com/) for comparing different sources of
weather forecasts, depending on where you live. For most places (including
where I live), NWS seems to be solidly middle-of-the-pack.

[Tom Ricks on U.S. military leadership](https://www.youtube.com/watch?v=OZbhIr04B5g). Is there any reason to believe that military leadership is fundamentally different from leadership of any other large part of the government?

[Various studies](https://gregmankiw.blogspot.com/2021/09/follow-up-references.html), reported by Mankiw, on the effect of increased tax rates in Europe as compared with the U.S.

