---
title: "Links for January 2023"
date: 2023-01-27
tags: ['Links']
---

[Musings](https://www.readingthechinadream.com/sun-liping-whats-wrong-with-our-experts.html) from China on expert failure.

There is much qualitative evidence for "democratic backsliding" around the world, but quantitative evidence is harder to come by, [even when you're looking](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4327307):

> The simplest explanation is that recent declines in average democracy scores are driven by changes in coder bias. 

Cyanide, famously, smells like almonds. Doesn't that seem strange? Well, it
turns out [it's not true](https://www.youtube.com/watch?v=WYagO-nup6c); or at
least, if you say to yourself "cyanide smells like almonds", you are probably
making your mental model of the world worse.

