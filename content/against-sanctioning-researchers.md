---
title: "Against Sanctioning Researchers"
date: 2022-03-03
tags: ['Society']
---

I generally don't write---at least explicitly---about current events. This one
is close enough to me that I think I [should](/emergent-hypocrisy).

As [reported](https://www.nature.com/articles/d41586-022-00601-w) by Nature:

> “My former student lives in Germany and we still collaborate. She was
> informed by her superiors that any contact with Russian scientists would be
> strongly discouraged,” says Mikhail Gelfand, a co-organizer of the Russian
> scientists’ letter and a biology lecturer at the Skoltech Center of Life
> Sciences in Moscow. 

> [...]

> More than 130 people have signed an open letter to the European Commission
> and member states of the European Union calling for an urgent suspension of
> all funding to, and international collaboration with, Russian institutions.
> [...] A European Commission spokesperson says that it has received the letter
> and that “nothing is off the table”. 

> [...]

> Restrictions on Russian scientists must be all-encompassing, they say: “We
> urge that researchers with an affiliation of such institutions not be
> admitted to international grant teams, not be invited to international
> conferences, and not be published in leading international scientific
> journals.”

And in a similar vein, two members of Congress
[suggest](https://www.sfchronicle.com/politics/article/Swalwell-doubles-down-on-controversial-idea-of-16948110.php)
[kicking](https://twitter.com/RubenGallego/status/1497341029921349632) Russian
students out of the U.S.

Many related measures have been proposed, and a variety of wildly different
ideas are discussed in that Nature article. I am strongly opposed to the
specific idea that individual researchers, by dint of being "Russian" (however
defined), should be excluded from meaningful engagement with the international
community. Primarily, this treats people not as individuals, but as
representatives (and perhaps property) of a nation and a government. It is not
often that the opportunity comes to resist that impulse in a meaningful way,
but the opportunity is here and we ought to seize it.

Nor is it reasonable or wise to require individuals to condemn Russia before
they may be admitted back to the broader community. The standard concerns are
cliché but true: compelled speech, besides being morally questionable, has the
totally unsurprising habit of being disingenuous. People living in repressive
societies, or with family and friends not yet out, reasonably fear unreasonable
repercussions for their speech. And, of course, treating people differently
based on their place of birth is both morally outrageous and idiotic.

More specific to today's circumstances, nationalistic passions tend to [carry a
religious quality](https://www.youtube.com/watch?v=61DiWi00d2w).  Directly
attacking religious views has a long history of being ineffective. In the
humdrum of small-scale American life, religious differences haven't been
annihilated, but instead _made irrelevant_. Religion is subordinate to a
hundred more salient differences, like football and the correct way to eat a
steak. These hot-button issues serve to remind us of what we share, and we're
all better off for it.

Many in academia today see research as a conduit for, and sometimes source of,
political power. Funding agencies and hiring committees ask how research has
been used to further various social causes. Bret Devereaux
[writes](https://acoup.blog/2022/01/21/fireside-friday-january-21-2022-on-public-scholarship/)
of "activism withdrawals from the bank of public support". I think this comes
from an understandable impulse to use what power we might have for good. In
practice, this makes scientific research subservient to the great political
movements that sweep across the country and now across the globe, disabling an
important institution that ought to be damping them instead.

Broader, institution-level sanctions already inflicted may be expected to have
[harsh](https://www.nature.com/articles/d41586-019-02795-y)
[effects](https://www.science.org/content/article/science-iran-languishes-after-trump-re-imposes-sanctions)
on the Russian scientific community. This is in keeping with the purpose of
those sanctions: Russian science will bear those costs just like the rest of
Russian society. I leave questions regarding the morality or usefulness of
those sorts of measures to others.

Lastly, remember that the future is uncertain. It is not historically
unprecedented for a nationalistic resistance movement, fighting Russian
imperialism, funded by the west, to have its reputation tarnished and
eventually be deemed a moral abomination. The calculus above continues to hold.
No matter the reputation of Ukraine's government in one decade, sanctions
against individual Ukrainian scientists will be worth opposing.  Individuals
ought not be held accountable for the bad behavior of political groups with
which they are loosely associated, and international ties should never be
broken in service of a misguided urge to
[purify](https://www.youtube.com/watch?v=-WmDszVxti0).

_Variously related links from [Tyler Cowen](https://marginalrevolution.com/marginalrevolution/2022/03/anti-russia-sentiment-is-the-new-mccarthyism.html) and [Terence Tao](https://terrytao.wordpress.com/2022/03/02/resources-for-displaced-mathematicians/). And thanks to an anonymous friend for reading a draft._
