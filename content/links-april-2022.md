---
title: "Links for April 2022"
date: 2022-04-25
tags: ['Links']
---

Things happen in the world. [Zvi](https://thezvi.wordpress.com/) does a good job of covering them. Obviously there's no point in mentioning any of that in a monthly links post.

[A claim](https://www.nber.org/papers/w29932): increase in substance abuse accounts for between one-tenth and one-quarter of the pandemic-era decline in labor force participation rates.

Paul Graham's [musings](http://www.paulgraham.com/heresy.html) on heresy. Mostly flagellating a deceased equine---but of course if you're not a veterinarian you might not have noticed.

A [glossary](https://acoup.blog/2022/03/25/miscellanea-a-very-short-glossary-of-military-terminology/) of military jargon.

[On the incompetence of ethnic parties](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4043185). You get what you optimize for!

