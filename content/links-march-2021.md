---
title: "Links for March 2021"
date: 2021-03-27
tags: ['Links']
---

[Here](https://thevaluable.dev/vim-advanced/) is the third part of a vim tutorial, for advanced users.

Exercise your bullshit detector! Courtesy of Robert Jervis, [a list of 78-ish](https://statmodeling.stat.columbia.edu/wp-content/uploads/2020/05/Fallacies.pdf) fallacies, largely centered around international relations and, to a lesser extent, politics. These aren't chess puzzles: as far as I can tell, there's really no guarantee of one clean answer. Nevertheless, a good read. You can, of course, get much the same effect by reading the news, but the fallacies here are largely in the past and therefore less irritating.

["Facial recognition technology can reveal political orientation,"](https://www.nature.com/articles/s41598-020-79310-1) scream the headlines! Go look at figure 3; facial recognition (in this case referring to an algorithm that extracts a grab-bag of humanly incomprehensible characteristics from a face) does about half again as well as an algorithm would if it just looked at: facial hair, glasses, position, and emotion. The most interesting thing here, frankly, is that stuff like "facial expression'' is such a strong predictor --- culture matters! A close competitor for "most interesting": humans are _terrible_ at guessing political orientation, [getting it right only 55% of the time](https://journals.sagepub.com/doi/10.1177/1088868312461308), as compared with about 65% for the amalgamation of position, glasses, hair, and emotion.

Rust code [can](https://blog.yossarian.net/2021/03/16/totally_safe_transmute-line-by-line) perform unsafe operations wihout using an `unsafe` block. Once you see it, it's obvious: just open the `/proc/self/mem` file and edit the memory manually! On the one hand, this is some combination of obvious, pointless, and idiotic. On the other, it's an important reminder of the fact that modern software is largely too complicated for any security/corretness model to be, y'know, secure. Or correct.

A [fable of opportunity cost](https://www.econlib.org/library/Columns/y2006/Mungeropportunitycost.html). Long-winded (in the tradition of Faulkner's Fable, I suppose), but readable. I'm not sure this is the best story to illustrate the point, which as nearly as I understand it, is: "Losing a penny is the same as failing to gain a penny. First derivatives of your objective function ought to be continuous everywhere!" Still, it's worth reading and thinking about, hence the link.

Quantum computing optimism [recedes](https://www.nature.com/articles/d41586-021-00612-z) by a small amount. See also Scott Aaronson on [ethics](https://www.scottaaronson.com/blog/?p=5387).

And finally, a laugh:
<div style="text-align: center"><iframe style="width:32em; height: 18em" src="https://www.youtube.com/embed/cE4tEkUqPrk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
