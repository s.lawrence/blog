---
title: "My Marginal Hour"
date: 2023-06-13
---

A couple months ago, I broke my wrist ([triquetral](https://en.wikipedia.org/wiki/Triquetral_bone) fracture). I wouldn't recommend it.

After a couple weeks, it became clear to me that, combining the actual incident, time spent in urgent care and with other doctors, the effort of taking care of the healing bone correctly, and reduced productivity from stress and all these distractions, I had probably lost the equivalent of 40 hours of productive work time. A week of work, killed off by a moment of carelessness while biking. As I said: not recommended!

Now, that's a rather strange (perhaps sad), thing to say, right? To me, it's suggestive of some sort of messed up priorities. The cost of a broken bone might be measured in medical bills, or a lower quality of time with loved ones, or actual pain. Is it weird to think first of the 40 lost hours of work?

Well, on reflection, no. You see, I have a pretty cushy job. This manifests in a few different ways, but a major one is that it's _flexible_. As a result, if something unexpected comes up that requires an investment of time, that time comes out of work. After breaking my wrist, the amount of time I spent with friends didn't change, nor did the amount of time I spent with my wife, or eating, sleeping, or playing video games. The most flexible part of my life is work, so that's where the hours and effort came from.

In short: my marginal hour is a work hour. When something happens that costs hours, that cost is correctly measured in work hours.

This reminds me a bit of an observation---I think made by Hayek in _Road to Serfdom_---regarding how not-desperately-poor people perceive money. If you're reasonably well-off, the marginal dollar isn't spent on much-needed calories, nor on shelter. It's spent on something relatively trivial, like buying a somewhat nicer TV, or a bigger car than necessary, or room service while on vacation. This contrasts with your median dollar, which is likely spent on something much more important. But, by definition, when you're consciously making a decision about money, you're thinking about the marginal dollar. So people tend to associate money with trivial goods.

All this causes me to doubt, slightly, popular narratives about people who "make their work their identity" (and "striver culture" and other related things). This is at best a half-formed thought, but it seems clear that with a straightforward, inflexible, 9-to-5 job, no matter how much you care about it, "work" is not going to be an appropriate measure of a cost or benefit in your life. For other types of jobs, all the slack is in the work, and any life event is about work, because that's the thing that changes. So for a fixed amount of caring-about-work, you can get a wide range of talking-about-work, just by changing the nature of the job.

