---
title: "Alex Wellerstein's Restricted Data"
date: 2023-09-10
tags: ['Books']
---

[<img src="/restricted-data/cover.webp" style="width: 90pt" />](https://alexwellerstein.com/writing/books/restricted-data/)

This book is a history of one corner of American politics: the debate, now approaching a century old, over which bits of nuclear information ought to be protected, and how. As politically oriented books go, it's remarkable for being level-headed. Nuclear secrecy is a naturally emotional topic, particularly in the American context, where views on free expression and  distrust of government were strong enough to compete with Cold War fears of annihilation. If Wellerstein didn't repeatedly point this fact out, one might read _Restricted Data_ and be unaware what a politically contentious topic the thing is.

An unfortunate aspect of this being a _political_ history is that it's disappointingly sparse on hard details. Of course there's only minimal discussion of anything technical---that would be tedious, and has been covered elsewhere anyway. But Wellerstein gives fewer concrete details about what the implementation of a secrecy regime looked like on the ground than I would have liked. A great deal of space is dedicated to the deliberations of the high-ranking officials responsible for writing and interpreting the law. There are several good stories about the intersection of the classification system with people outside the system, some of whom voluntarily chose to obey its requests, and others who played the role of activists, attempting to tear it all down. But the gritty implementation details are largely missing, so a lot of the discussion feels a bit hollow.

Except for that, the book is exceedingly thorough beginning in 1943 and extending nearly through the end of the cold war. After that, everything is understandably very sparse. Wellerstein states that he has no clearance and is under no obligation to keep secrets, but it's impossible to write a book that's well grounded in facts when it's clear that an important plurality of those facts are not yet known. So the post-Cold War era, and particularly events after around 2010, are discussed quite briefly.

Wellerstein suggests (I think partly as a rhetorical flourish) that Western nuclear secrecy essentially began with Szilard's attempts to convince other scientists not to publish on fission and its applications. Not long after, Szilard became an advocate for relaxing nuclear security, eventually being considered by General Groves to be "a malcontent", arguing that "secrecy was pointless". More generally, many of the physicists who were most prominent in the creation of a system of nuclear secrecy pushed for extensive liberalization after the war, and largely failed: this included Oppenheimer and eventually Teller. A common trope with respect to secrecy---and one that gets truer with each decade---is that "the genie cannot be put back in the bottle". This is typically invoked as an argument for erring on the side of conservatism. It's interesting to note that at an institutional level, it's the secrecy itself that has turned out to be irreversible.

Alex Wellerstein, by the way, also has an [excellent blog](https://blog.nuclearsecrecy.com/), which I've linked to previously.

