---
title: "Links for December 2021"
date: 2021-12-27
tags: ['Links']
---

SQLite [adds](https://www.sqlite.org/releaselog/3_37_0.html) (optional) type checking!

Jackson Crawford [on leaving the university classroom](https://www.youtube.com/watch?v=-x9vRBgiDG0).

Read [this post](https://marginalrevolution.com/marginalrevolution/2021/12/why-the-new-pollution-literature-is-credible.html), and don't think about air pollution, or the environment. What are good standards for when to trust "the literature"?

[On _Stubborn Attachments_](https://applieddivinitystudies.com/exegesis/) as the moral foundation for "Progress Studies".

Markus Strasser [writes](https://markusstrasser.org/extracting-knowledge-from-literature/) about his experience trying to extract (in a programmatic way) information from biomed papers. Various strong claims are included, culminating in:

> I had to wrap my head around the fact that close to nothing of what makes science actually work is published as text on the web.


[Restrospectively obvious](https://nav.al/failure): groups have trouble admitting failure, and for-profit groups receive better real-world feedback than not-for-profits.

[An opinionated review](https://journals.sagepub.com/doi/10.1177/2631787720929704) of the field of "bullshitology". A lovely story! How seriously should a data- and equation-free paper be taken? I can't help but agree with some parts of it, though:

> Pseudo-theorizing occurs when the external trappings of theorizing (such as technical experts and scientific language) is present but substantive processes of theorization are absent. One way pseudo-theorizing happens is when experts with apparently legitimate credentials are mobilized to vouch for empty and misleading ideas. This gives bullshitting a sheen of technicality, precision and rationality.

[Has the placebo effect been getting stronger](https://journals.lww.com/pain/Abstract/2015/12000/Increasing_placebo_responses_over_time_in_U_S_.27.aspx)? But only in the states! Well, the bottom of [this](https://atis.substack.com/p/all-placebos-are-not-created-equal) blog post has the key plot with evidence that that's true.

