---
title: "A Sudden Change of Environment"
date: 2022-02-27
---

It is well known that lions do not fare well in the Arctic. This is not a design flaw. Lions are correctly adapted to life in sub-Saharan Africa, which is, among other things, somewhat warmer than the Arctic. If lions were able to survive in the Arctic, that would mean they were incorrectly adapted to the place where they actually live. That _would_ be a design flaw.

As with lions, so with you and me. We have various heuristics that we apply to make sense of our local "information environment", allowing us to somewhat reliably discern facts without independently verifying every rumor that comes along. These heuristics are adapated: they are appropriate to when and where we are, and not necessarily to other contexts. A small sampling of commonly used heuristics:

* People who care about their reputations [don't deliberately lie](https://astralcodexten.substack.com/p/bounded-distrust), and generally make an effort to verify information before repeating it.

* _Most_ people, in any category, are reluctant to knowingly lie.

* Change comes slowly.

* Quotes --- things placed in quotation marks --- tend to be accurate.

* People who are emotional about a topic are closer to the subject, likely know more, and therefore may be _more_ trustworthy.

* In the presence of contradictory information, exercise caution.

And most importantly:

* If sources I know from experience to be trustworthy make some claim, that claim is true.

In the long term, war leads to good epistemic habits: those with bad habits, lose. Short term, there is no such guarantee. Moreover, war is generally associated with a sudden change in environment. Even for those entirely removed from combat, in a different country, in a different hemisphere, a war brings with it a large amount of rapidly changing information, always from motivated sources, often emotionally charged for the listener.  Habits developed and heuristics trained in peacetime environments cannot be expected to perform well in the new environment.

That's bad news, of course, but there's worse yet to come. The local information environment consists mainly of other people, each with their own set of now-maladapted heuristics. That previously trustworthy source may simply no longer be trustworthy, because he is trusting a third source who is no longer trustworthy, because she is trusting...

It's no surprise that, in the midst of a war (or any other fast-moving, emotionally charged event), it can be difficult to tell what is true, what is misleading, what is false, what is a deliberate lie. Standard heuristics, like "beware of contradictory information", already warn you to be uncertain. However, one mustn't forget that _those standard heuristics are miscalibrated_. Absent reliable heuristics for evaluating incoming information, it is generally appropriate to become more uncertain than the heuristics would suggest. Thus, even with all the alarm bells being set off by obvious contradictions and transparently motivated sources, it's still far too easy to overestimate your knowledge.

In the context of a war, you can't trust other people, you can't trust the sources you trust, and you can't trust your own judgement about who and what to trust.

