---
title: "Links for February 2021"
date: 2021-02-27
tags: ['Links']
---

A water treatment plant in Florida is [attacked](https://www.wtsp.com/article/news/local/pinellascounty/pinellas-oldsmar-water-system-computer-intrustion/67-512b2bab-9f94-44d7-841e-5169fdb0a0bd) --- attacker raises sodium hydroxide level in an apparent attempt to poison people. I'm pretty sure "thanks to a vigilant operator" is one of the ten scariest phrases in the English language. We haven't really explored the long tail of cybersecurity risk: be unsurprised by unwelcome surprises in the next decade.

Andrew Gelman [comments](https://statmodeling.stat.columbia.edu/2021/02/08/my-thoughts-on-whats-wrong-with-social-science-and-how-to-fix-it-reflections-after-reading-2578-papers/) on Alvaro de Menard's [review](https://fantasticanachronism.com/2020/09/11/whats-wrong-with-social-science-and-how-to-fix-it/) (previously posted [here](/links-september-2020)) of the state of the social science literature.

Here's a [10 terapixel image](https://viewer.legacysurvey.org/) of the night sky. I don't think I can reliably tell galaxies from the dimmer stars.

The New York Times finally gets around to publishing [their piece](https://www.deccanherald.com/business/technology/why-slate-star-codex-is-silicon-valley-s-safe-space-950727.html) on SSC and the rationalists; the word "rancid" comes to mind, among others. (I include this link because it seems culturally important, not because it's worth your time to read.) Here's [Scott's reply](https://astralcodexten.substack.com/p/statement-on-new-york-times-article); if you read anything on this, read that first and last. Variously irate commentary is provided by [Scott Sumner](https://www.themoneyillusion.com/understanding-middlebrow/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+Themoneyillusion+%28TheMoneyIllusion%29), [Jason Crawford](https://jasoncrawford.org/guide-to-scott-alexander-and-slate-star-codex), [Matt Yglesias](https://www.slowboring.com/p/slate-star-codex), [Noah Smith](https://noahpinion.substack.com/p/silicon-valley-isnt-full-of-fascists), and [Scott Aaronson](https://www.scottaaronson.com/blog/?p=5310) (with a [follow-up](https://www.scottaaronson.com/blog/?p=5330)), among others. Finally, Tanner Greer [comments](https://scholars-stage.blogspot.com/2021/02/the-framers-and-framed-notes-on-slate.html): "Mortals know that when Olympians feud, it is never Olympians who die."

Rob Rhinehart on the [evil octopus](https://www.robrhinehart.com/the-new-york-times/) --- if you're irate about what happened, this is the piece that Scott Alexander thinks you should read. The title of this article is "The New York Times", which I think is how we know it's not really about the NYT. They're just one of the arms of the Kraken. As Leonard Cohen [points out](https://unsongbook.com/chapter-71-but-for-another-gives-its-ease/), the Kraken is _everything_.

When nothing determines status but status itself --- when there are no objective outside measures of the quality of one's work --- social dynamics [become](https://scholars-stage.blogspot.com/2021/02/why-writers-and-think-tankers-feud-so.html) much more toxic. Greer's explanation is based on a link between personal uncertainty and personal insecurity. I think the story goes beyond that. An [illegible incentive](/illegible-incentives-matter) can drive a group to behave in a certain way without any individual being aware of what's happening; in "a world where one's reputation rests on little more than reputation itself", there's a pretty strong illegible incentive to fight tooth-and-nail for reputation.

[Vitalik Buterin on prediction markets](https://vitalik.ca/general/2021/02/18/election.html) --- how modern ones can fail, and lessons for futarchy.

Also by Vitalik, here's a nice [introduction to zk-SNARKs](https://vitalik.ca/general/2021/01/26/snarks.html).

