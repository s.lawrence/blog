---
title: "Links for August 2023"
date: 2023-08-31
tags: ['Links']
---

New substack and a good interview [regarding PEPFAR](https://www.statecraft.pub/p/saving-twenty-million-lives). Is this replicable today? Is this a story of executive dysfunction or executive triumph?

Wellerstein discusses the [non-destruction of Kyoto](https://blog.nuclearsecrecy.com/2023/07/24/henry-stimson-didnt-go-to-kyoto-on-his-honeymoon/) in WWII. A lot of effort to deal with a glib just-so story.

