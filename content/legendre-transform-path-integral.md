---
title: "The Legendre Transform and the Path Integral"
date: 2021-02-23
tags: ['Physics']
---

This is yet another post on something I really should have learned long ago, but somehow never quite grasped. Or, in this case, never learned at all.

I have a Hamiltonian --- let's say \\(H = p^2 + V(x)\\) for simplicity's sake --- and I'd like to get a Lagrangian for the same system. Classically, this is done through the Legendre transform. Focus on the \\(p\\)-dependence of \\(H(p;x)\\) for fixed \\(x\\). Define \\(h(p,\dot x;x) = p \dot x - H(p;x)\\), and the Legendre transform of \\(H\\) is given by the maximum value attained by \\(h\\):
$$
L(x,\dot x) = \max_{p} h(p,\dot x;x)
\text.
$$
Of course, that maximum value can be found by requiring that \\(0 = \frac{\partial h}{\partial p}\\). As a result, the Lagrangian can be defined more simply by simply saying \\(L = p \dot x - H\\), where \\(p\\) is defined such that \\(\dot x = \frac{\partial H}{\partial p}\\).

In quantum mechanics, there's another way to go from the Hamiltonian to a Lagrangian. This is what we do when we derive the path integral. For the same Hamiltonian as above, we can expand the propagator like so:
$$
\langle x_f | e^{-i H t} | x_i \rangle =
\int d x_n\cdots d x_1\\;
\langle x_{f} | e^{-i H \Delta t} | x_n\rangle
\cdots
\langle x_{1} | e^{-i H \Delta t} | x_i\rangle
$$
where \\(\Delta t = t / n\\). When \\(n\\) is large, the operator \\(e^{-i H \Delta t}\\) is close to the identity, and easily approximated:
$$
\langle x' | e^{-i H \Delta t} | x\rangle =
e^{i \big(\frac 1 {2 \Delta t} (x'-x)^2 - V(x)\big)} + O(\Delta t^2)
\text.
$$
Hey, look, there's a naive discretization of the Lagrangian up in the exponential! Putting it all together and treating \\(x(t)\\) as a continuous function (at the very least, the discrete values can be interpolated to construct a piecewise smooth function), we obtain the usual path integral for the propagator. The object in the exponential is by definition the action, or the integral of the Lagrangian.
$$
\langle x_f | e^{-i H t} | x_i \rangle =
\int \mathcal D x(t)\\; e^{i \int dt\\; L(x,\dot x)}
$$

These two procedures give the same answer, at least in "reasonable" cases, so they must be related. How? In particular, the second procedure should be hiding a Legendre transform somewhere. (I'm particularly interested in this question because I have no trouble keeping track of the second procedure, whereas the first is a source of perpetual bafflement.)

Well, let's look at the quantum case more carefully. In order to approximate the matrix element of \\(e^{-i H \Delta t}\\), one usually expands it with the Suzuki-Trotter decomposition:
$$
\langle x' | e^{-i H \Delta t} | x\rangle =
\int d p\\; \langle x' | e^{-i p^2 \Delta t/2} | p\rangle \langle p | e^{-i V(x)} | x\rangle =
\int d p\\; e^{i p (x'-x)} e^{-i p^2 \Delta t/2 - i V(x)}
\text.
$$
Rather than integrating out the momentum, let's keep it around for a bit and see what happens. Plugging this formula into the expression for the full propagator, and again treating \\(p\\) and \\(x\\) as continuous functions, we find
$$
\langle x_f | e^{-i H t} | x_i \rangle =
\int \mathcal D x(t)\\; \mathcal D p(t)\\;
e^{i p \dot x - H(p,x)}\text.
$$

We're halfway there! The exponential is just the function \\(h(p,\dot x;x)\\) that we use in the definition of the Legendre transform. The last bit is to realize that we want the classical limit, that is, the \\(\hbar \rightarrow 0\\) limit. Rewriting the above expression with \\(\hbar\\) in place:
$$
\langle x_f | e^{-i H t / \hbar} | x_i \rangle =
\int \mathcal D x(t)\\; \mathcal D p(t)\\;
e^{i (p \dot x - H(p,x))/\hbar}\text.
$$

_Now_ we can consider integrating out the momentum. This integral is dominated by the region of stationary phase; that is, where \\(\frac{\partial h}{\partial p} = 0\\). So, the Legendre transform reappears in the classical limit of the path integral (just as Lagrange's equations of motion do).

[Here](https://physics.stackexchange.com/questions/200216/is-there-a-mathematical-relationship-between-legendre-conjugates-and-fourier-con) is a related StackExchange post for further reading. It doesn't quite go all the way (at least along the direction I'm interested in), but most of the important bits are there. There's also a hint of this story in section 9.1 of Peskin.

