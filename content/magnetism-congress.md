---
title: "Magnetism and Congress"
date: 2020-11-14
tags: ['Society']
---

## I.

In 1842, Morse pressed Congress into allowing him to demonstrate his telegraph by sending a message from one congressional conference room to another. As a result, he was given about a million dollars (inflation-adjusted):

> [_(March 3, 1843)_](http://memory.loc.gov/cgi-bin/ampage?collId=llsl&fileName=005/llsl005.db&recNum=655) Be it enacted... That the sum of thirty thousand dollars be... appropriated... for testing the capacity and usefulness of the system of electro-magnetic telegraphs invented by Samuel F. B. Morse... The Secretary of the Treasury [is] authorized to pay... Morse, and the persons employed under him, such sums of money as he may deem to be a fair compensation...

Today, this seems like an astonishingly implausible story --- a single person pesters Congress for a massive sum of money, and _gets it_.

This was not Morse's first demonstration to Congress, though. It took him five years to secure funding.

In 1837, the Secretary of the Treasury (Levi Woodbury) was instructed by the House to investigate the feasibility of constructing a system of telegraphs for the United States. At the time, "telegraph" generally meant "optical telegraph"; the relays in such a system were humans, who watched one signal and then passed it on. When Woodbury called for proposals, though, Morse replied with his electromagnetic telegraph. This was the beginning of his engagement with Congress.

In 1838 and 1839, both the House and the Senate considered bills to support the development of Morse's magnetic telegraph. In the Senate, the issue was whether to allow duty-free importation of construction materials; the House considered a bill for testing the practicality of constructing such a system (presumably with an eye towards directly financing it). Morse demonstrated the system, in 1838, to Congress and the President. Nevertheless, neither bill was ever voted on.

In 1842, Morse convinced Congress to allow him to demonstrate again. I can't find any claim that Morse's second demonstration was noticeably more impressive, but in 1843, Congress finally passed an appropriate for Morse and his colleagues.

Morse reported in 1844 that the line from the District to Baltimore was complete. Side note: "What hath God wrought" was not, in fact, Morse's first transmission. The first public transmission, according to wikipedia, was "A patient waiter is no loser". The famous "What hath God wrought" was the opening line for the D.C.-Baltimore line.

The Congressional Journal over the next decade is filled with references to petitions and bills for funding expansion of the telegraph system across the country. Congress wasn't just pleased with the telegraph, though. Morse's demonstration --- and the fact that he delivered on his promises --- had made him personally popular. In 1845, the House Committee on Public Buildings and Grounds was instructed to ask Morse

> if, in his opinion, he can invent or adopt a more expeditious plan of taking the yeas and nays in this hall...

He seems to have declined, as the House didn't vote electronically [until 1973](https://history.house.gov/Exhibitions-and-Publications/Electronic-Technology/House-Technology/).

In 1845, Congress approved an expansion of the telegraph system to New York, and began referring to "Professor Morse" instead of "Samuel F. B. Morse". In the subsequent years, telegraph lines were often built alongside railroads, funded by Congress in the same bill. By 1870 the magnetic telegraph reached California.

An astonishing journey, beginning with the audacity of an ordinary man to write to Congress for money.

## II.

Or not. To start with, Morse was no ordinary man. He was a consumate self-promoter, intensely concerned with his own reputation, and quite politically active.

Wikipedia discusses his political activites [at some length](https://en.wikipedia.org/wiki/Samuel_Morse). He published a political tract (explaining how the Catholics were conspiring to take over the country) in 1835. He ran for mayor of New York (to fight the "deep Catholic state", I assume) in 1836. Note the timeline: neither a prelude nor an afterthought, these activites were essentially contemporary with his creation of the telegraph.

After the telegraph was well established, in the 1850s, Morse turned his attention to the question of slavery, comparing it favorably to employment, parenting, and government.

In 1871, H.R. 285 was passed, allowing a statue of Morse to be erected on government land. Morse died in 1872, and a memorial service was held in Congress. I'm assuming this service primarily focused on the telegraph, rather than his paintings ([not bad](https://en.wikipedia.org/wiki/Samuel_Morse#/media/File:Samuel_Finley_Breeze_Morse_001.jpg), actually) or his support for slavey (none too popular with Congress during reconstruction, I guess).

## III.

Henry Hall Sherwood was a physician in New York. In the 1830s, the electromagnetic nature of the nervous system was still a relatively new discovery, and Sherwood seems to have gotten a bit over-excited. His [books](https://collections.nlm.nih.gov/?f%5Bdrep2.authorAggregate%5D%5B%5D=Sherwood%2C+H.+H.+%28Henry+Hall%29) include case studies; for the most part, he seems to have waved magnets near his patients. He claims this cured them of various ailments. I'd laugh, but there's an accupuncture clinic a couple blocks from my office. I'm afraid to find out whether this was classified an "essential service" during the lockdown.

Electromagnetism (or as it was often known then, "magnetism" --- even when referring to phenomena we would call electric) was all the rage. A major challenge of the age was to understand the structure of the magnetic field of the Earth, although that's slightly modern terminology --- it was often referred to as "the variation of the compass". This would be helpful, among other things, for navigation at sea: you can easily imagine that a precise model might allow the measurement of longitude. Sherwood, being an expert on the magnetic nature of the nervous system, decided to lend a hand to this closely related problem. Overall, this went about as well as you'd expect.

Like his better-known contemporary, Sherwood was bold enough to make requests directly of Congress.  He petitioned the House, claiming to have invented an instrument (the _geometer_) for navigation purely from magnetic observations, and

> praying the aid of the Government of the United States in the publication of a work to explain the discoveries...

among other things. It was proposed that he give the House a demonstration of his techniques, in June of 1838. This was voted down, and the matter dropped entirely.

That same month, Sherwood also petitioned the Senate; his petition was presented by [Nathaniel Tallmadge](https://en.wikipedia.org/wiki/Nathaniel_P._Tallmadge) of New York. Like the House, on June 21st, the Senate considered and declined the possiblity of a demonstration. Sherwood's petition was referred to the Committee on Naval Affairs, which submitted a report, presented again by Tallmadge, to the Senate on July 3rd. 

Here it gets interesting. That report caused something of a stir in the scientific community. No less than [Joseph Henry](https://en.wikipedia.org/wiki/Joseph_Henry), soon to become the first Secretary of the Smithsonian Institution, published a denunciation of the report. You can read his article [here](https://siarchives.si.edu/sites/default/files/pdfs/JHPVol/JHPP_V4_P75-79_Transcript.pdf), which was written in protest

> against the plan of discussing such subjects in Congress before proper means have been taken to determine their true character.

In other words, against Congressional meddling and speculation. Most of Henry's complaint is taken up by a somewhat detailed critique (occasionally, mockery) of Sherwood's theories. The above quote somewhat masks Henry's true opinion. A few paragraphs later he writes:

> we do not believe that there is a person of any scientific reputation in our country, who has paid attention to this subject, who will not immediately say that the whole affair is perfectly peurile...

Despite his earlier words, I don't get the impression the Henry would object to Congressional support for a speculative endeavor: just not an entirely futile endeavor. After Henry's rebuttal was published, the Senate seems to have forgotten about Sherwood's petition for a while.

Sherwood again requested financial aid from Congress in mid-February, 1839. Another report was delivered by Tallmadge from his committee, but I can find no details on what it said. It must not have been positive: the last reference to H. H. Sherwood in the papers of Congress is on March 2, 1839, when the Committee on Naval Affairs was given permission, by the Senate, to drop the matter.

## IV.

It is sometimes argued that Congress's support of Morse's telegraph was not the really the first case in which the new government funded science: for a sufficiently broad definition of "science", that honor belongs to the Lewis and Clark expedition nearly 40 years prior. But just as Morse's was not the first time the government _considered_ funding science, the 1803 expedition was not the first time the U.S. government _considered_ funding an expedition.

John Churchman was [a surveyor and mapmaker](https://bostonraremaps.com/inventory/john-churchman-magnetic-atlas/), who in the 1780s became preoccupied with the task of measuring the longitude of a ship at sea via magnetic observations (much like Sherwood!). Churchman, however, was not a complete crank. Like Morse, he was no stranger to make requests of governments, having (for instance) [written](https://founders.archives.gov/documents/Jefferson/01-11-02-0378) to Thomas Jefferson in 1787 to request that a paper of his be presented to the Royal Academy of Sciences at Paris. (That puts things in perspective. There's no excuse for not sending cold emails!)

In 1791, Churchman made a request of Congress: "praying the patronage of Government to enable him to undertake a voyage" to investigate the magnetic fields near the geomagnetic north pole. Of course, his _motivation_ for this voyage was to confirm a theory of the behavior of magnetic field lines which is now known to be incorrect. In brief, the behavior of magnetic field lines is not as predictable as he assumed, and therefore not as useful for navigation beyond distinguishing north from south.

Nevertheless, exploratory voyages based on incorrect assumptions were commonplace and often productive, and Congress considered the matter serious --- more seriously, in fact, than the Sherwood business. The House held [a lengthy discussion](http://memory.loc.gov/cgi-bin/ampage?collId=llac&fileName=003/llac003.db&recNum=153) on 6 Jan 1792 to consider the petition.

The representatives (at least those who participate in the debate) come across as reasonably knowledgable about the subject. Halley's (yes, that Halley) theory of a hollow Earth is mentioned and mocked, as a way of raising Churchman's relative status.

> [N]o theory, except Mr. C[hurchman]'s, has been offered to the world, which solves so many phenomena of the variation of the needle... 
> The United States need not be ashamed to encourage [Churchman]; the British Parliament encouraged voyages to ascertain the truth of Halley's theory...

[Murray](https://en.wikipedia.org/wiki/William_Vans_Murray) had the last word:

> This was a question on which he was incompetent to decide, but he [felt it was in] the great interest of science... to commit the matter to a committee...
> Let those gentlemen who wish to have an opportunity of gratifying a laudable curiosity be indulged. When a man of science comes here with supposed discoveries... we owe it to the subject, to ourselves, and to human nature, to give his propositions fair play and mature consideration.

I suspect this is the first defense of "pure" science, "science for science's sake", or "basic research" as it would later be known, in the U.S. government. Murray continues

> We... ought to take warning from the disgrace of other nations whom history has held up for their premature rejection of enterprises and schemes of science. Columbus himself... a philosophical vagabond...

This positive view was not unanimous. Early in the discussion it was pointed out that, if the treasury "was full and flowing", there were surely "other expeditions" more worthy. This is a timeless objection: there's a bit of physics folklore that the superconducting supercollider was sunk when a physicists objected that other subfields were more deserving of the funding. According to the lore, those subfields didn't receive any funding either. Another representative pointed out that this was an old topic, extensively studied in Europe, and it was unreasonable to be hopeful of success after all those failures "by the ablest navigators and philosophers".

The matter was referred to a select committee, which [reported favorably](http://memory.loc.gov/cgi-bin/ampage?collId=llhj&fileName=001/llhj001.db&recNum=517&itemLink=D?hlaw:9:./temp/~ammem_VJSP::%230010518&linkText=1), finding

> That the said Churchman ... has found a number of observations, made in different parts of the world, the confirm his hypothesis ... and [has been] applauded for his ingenuity by several learned societies in Europe...

The House was interested exclusively in the practical consequences of discoveries concerning "the laws of magnetism", which included (according the same committee)

> adjusting and preventing disputes respecting ... boundaries ... of land, and of correcting many inaccuracies in geographical charts... 

This effort was a near miss: Churchman's petition never made it out of the House. Later in the year, Churchman [wrote](https://founders.archives.gov/documents/Washington/05-11-02-0038) to none less than George Washington (really --- there's no excuse for not sending cold emails), declaring his intention to seek support in Europe, and requesting that Washington furnish him with introductions to appropriate officials. It seems Churchman met with no success there, either. Today he is not considered noteworthy.


