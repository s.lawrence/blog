---
title: "Links for June 2021"
date: 2021-06-28
tags: ['Links']
---

Certain big decisions are [linked](https://marginalrevolution.com/marginalrevolution/2021/06/do-individuals-make-more-rational-decisions-when-the-stakes-are-higher.html) to questions of identity --- in this case political identity. Fear is not the only mind-killer.

[Lessons from Fast Grants](https://future.a16z.com/what-we-learned-doing-fast-grants/). 

[On the growth of modern liberalism and the administrative state](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3864614): "Because administrative agencies guaranteed a central policymaking role for credentialed urban professionals, liberals could support farmers and industrial workers against big business while no longer fearing the rising power of their coalition partners." I link this without endorsement, since one should be massively skeptical of all such facile narratives.

Tanner Greer [discusses](https://scholars-stage.org/historians-the-slaves-of-fashion/) the influence of political fashions on historical writing.

[In which](https://marginalrevolution.com/marginalrevolution/2021/06/new-york-fact-of-the-day-3.html) new housing construction is blocked on the grounds that it might (in general, not a specific tower) disproportionately impact minority residents. Well, I agree that it would disproportionately impact minorities! [Related](https://belonging.berkeley.edu/roots-structural-racism).

Playing chess increases calorie consumption [only very slightly](https://skeptics.stackexchange.com/questions/7527/does-playing-chess-burn-as-many-calories-as-running/7568#7568).

[Report on UFOs](https://www.dni.gov/files/ODNI/documents/assessments/Prelimary-Assessment-UAP-20210625.pdf), linked for completeness only.
