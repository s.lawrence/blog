---
title: Circumscribe expertise (and maybe talk to the police)
date: 2022-06-19
tags: ['Society']
---

There's a standard piece of internet wisdom: don't talk to the police. The canonical video link, I think, is [this one](https://www.youtube.com/watch?v=d-7o9xYp7eE). It's a lecture by a law professor and former criminal defense attorney, alongside a police officer. The gist of it is usually summarized as "even if you believe you haven't done anything wrong, never talk to the police---it can only harm".

Now, a random internet stranger claiming "you should never talk to the police---they're out to get you, doncha know!" is not very believable. Such a claim might be seen as conspiratorial, or even sovereign-citizen-adjacent. So it's important for the believability of this claim (and the propagation of this meme) that it's usually accompanied by the above link, and that the speakers in that link are apparently relevant experts. It's difficult to plausibly doubt them when they say, for instance, that talking to the police without a lawyer can be a risky act even for someone who is in reality innocent.

The ultimate claim, though, is not a simple factual statement about a long tail of risk when talking to the police. The claim that this video is generally used to support is a _normative_ one: you _should_ not talk to the police. Translating the central factual claim into a normative claim is a tricky task, and it serves as a nice little case study on the relevance of "relevant" experts.

The pope announces that speaking to the police is commanded by God---you will be richly rewarded in the afterlife. Now should you talk to the police? Maybe you're not so interested in the afterlife. Okay, your psychologist (named "Fraud") tells you that police represent father figures, and you need to talk to them in order to overcome your inexplicable fear of rockets and sausages.

Those examples are deliberately silly. Fine: late one Tuesday night, the sheriff comes to your door.

> "Excuse me. I'm investigating a series of crimes. Sunday, the fellow on the
> corner was shot and killed. Yesterday, Ms. Watserneym next door was
> shot---she's doing okay in the hospital, though. We believe the suspect lives
> nearby. Have you seen anything suspicious?"

If, unbeknownst to the police, you just last night recorded a video of a
shadowy figure hiding a package nearby, should you tell them? Sure, they might
look at your phone and bust you for movie piracy. On the other hand, _you're
about to die_.

In the median case, though, you haven't seen anything. What to do then? Your incentives, properly understood, still point towards "make the police as efficient as possible", even at some short-term cost to your legal position. The sheriff is going to have to go interview everyone on your block, and decide if they seem suspicious. Neglecting, as you should, considerations of your own legal liability, you should do _whatever it takes_ to convince the sheriff that his time is better spent elsewhere. (That way, he might spend his time elsewhere, increasing the odds of success.) If he asks "may I search your house", the correct answer is "absolutely". Remember, _you have hours left to live_.

In short: sometimes, you should talk to the police simply because it may help them do their job better, and _police doing their job well has positive externalities_.

Back to generalities about expertise. Law professors and police officers are not experts in quantifying externalities (more's the pity). So now we see that deciding what you _should_ do when questioned by the police in fact involves a slightly larger set of experts: one must at least include a certain flavor of economists. But is that all? Should we add a priest? A psychologist? An anthropologist? This is not a question to be answered. Stating a set of "relevant" experts is itself a factual statement about the world, and it's a factual statement for which there is no particularly relevant expert.

