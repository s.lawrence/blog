---
title: "Links: Negativity Bias Redux"
date: 2021-09-12
tags: ['Links']
---

From [four years ago](https://www.theguardian.com/science/head-quarters/2017/may/26/negativity-bias-why-conservatives-are-more-swayed-by-threats-than-liberals):

> However, the more conservative respondents were significantly more likely to believe the hazard messages than the more liberal ones.

This is an article primarily about a single study, but a [whole](https://journals.sagepub.com/doi/pdf/10.1177/0956797617692108) [host](https://www.cambridge.org/core/journals/behavioral-and-brain-sciences/article/differences-in-negativity-bias-underlie-variations-in-political-ideology/72A29464D2FD037B03F7485616929560) of [studies](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5793824/#nsx133-B28) came out around that time, often with the idea that the difference was fundamentally biological. For example:

> We found that economic conservatism predicted greater connectivity between the BNST and a cluster of voxels in the left amygdala during threat vs safety. These results suggest that increased amygdala–BNST connectivity during threat may be a key neural correlate of the enhanced negativity bias found in conservatism.

Somewhat related, and worth reading in any case: Yglesias discusses more modern negativity bias [here](https://www.slowboring.com/p/media-negativity).

This is probably a good time to clarify that what I link to --- particularly in a post tagged as this one is --- is not necessarily what I endorse. The targets of the links are interesting to read. I do not mean that the writing is interesting (or good), nor that the content is interesting (or true). I mean exactly what I say: they are interesting _to read_.

