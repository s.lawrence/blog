---
title: "Links for May 2023"
date: 2023-05-31
tags: ['Links']
---

Scott Alexander on [sexuality
statistics](https://astralcodexten.substack.com/p/raise-your-threshold-for-accusing).
It's one of those articles that, never mind the actual subject matter, is also a
good read for practice understanding statistics. Oh, and that [reminds me](https://web.archive.org/web/20150110230807/https://qntm.org/gay).

From Tablet, [on disinformation](https://www.tabletmag.com/sections/news/articles/guide-understanding-hoax-century-thirteen-ways-looking-disinformation).

Yassine Meshkout [argues](https://ymeskhout.substack.com/p/what-are-the-limits-of-the-weak-man) that it's okay (in fact, important) to attack the weaker form of an argument, if that's the form that's spreading easily. 

