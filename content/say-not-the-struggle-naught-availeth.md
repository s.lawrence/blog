---
title: "Say Not the Struggle Nought Availeth"
date: 2021-05-11
tags: ['Society']
---

> [...] I was describing the materials of which our spurious aristocracy is composed. You gentlemen belong to the big-bellied, little-brained, numskull aristocracy. How dare you hiss me, you contemptible set of platter-faced, amphibious politicians?

So spoke Feargus O'Connor, a prominent leader of the Chartist movement in the UK, circa 1840. This was a fairly unusual rant for him, as he didn't actually call for violence, nor deride those opposed to violence as "moral philosophers". Another prominent Chartist leader, James Bronterre O'Brien, idolized Robespierre. (O'Brien was eventually kicked out of the movement for being too peaceable.)

Chartism grew out of the economic difficulties faced by the UK's working poor in the wake of the industrial revolution. At the core of their concerns was the extent to which working men in England had no say in how their government was run. Closely linked were the need for higher wages and the desire to end the domination of the Church of England. This last point must have appealed strongly to Arthur Hugh Clough, a poet who in 1848 quit his post at Oriel College rather than be required to teach religious doctrines he opposed.

These were noble causes, but Chartism was a large social movement, with many leaders and hundreds of thousands of followers. Chartists sometimes called for higher wages, sometimes for a ground-up redesign of society, and sometimes for the "utter" destruction of "every vestige of Priestcraft". It depended on who was speaking (and who was listening). The powers that were felt understandably threatened.

In 1848, revolution in France overthrew the monarchy, and other less-successful revolts broke out over Europe. In the wake of this rash of instability, the Chartists organized to submit a petition to the Houses of Parliament on 10 April. Fearing violence, authorities in London requested that the petition be submitted quietly, by a small number of representatives. Instead, O'Connor led a crowd of 200,000 people into London. Some called it a peaceful demonstration, others a show of force.

In any event, the demonstration was a failure. The police did not allow the mob to cross the Thames to reach parliament, instead forcing O'Connor to present the petition himself. Londoners were surprised at the poor showing of the Chartists: O'Connor claimed there were 200,000 marchers, but in reality there were around one-tenth that number. O'Connor claimed that the petition held well over five million signatures, but when parliament inspected, they found that only a fraction were genuine. (The queen, supposedly, had signed seventeen times.)

In the wake of this humiliation, the Chartist movement collapsed. A small hardcore band, led by William Cuffay, decided that their only hope was the violent overthrow of the government. On the 16th of August, around 5000 of his followers gathered in London to attempt this... and then dispersed, once it became clear that the police knew of their plans. The next day, the police arrested several of them (including Cuffay, who was sent to Australia) and confiscated various weapons: swords, pikes, and "the leg of a chair loaded with lead, and a number of nails driven in at the extremity". Parliament formally rejected the petition soon after.

Arthur Hugh Clough heard the news of the failed Chartist revolt while in Italy. He wrote his most famous poem the following year:

<blockquote class="poem">
<pre>
Say not the struggle nought availeth,
     The labour and the wounds are vain,
The enemy faints not, nor faileth,
     And as things have been they remain.
</pre>
<pre>
If hopes were dupes, fears may be liars;
     It may be, in yon smoke concealed,
Your comrades chase e'en now the fliers,
     And, but for you, possess the field.
</pre>
<pre>
For while the tired waves, vainly breaking
     Seem here no painful inch to gain,
Far back through creeks and inlets making,
     Comes silent, flooding in, the main.
</pre>
<pre>
And not by eastern windows only,
     When daylight comes, comes in the light,
In front the sun climbs slow, how slowly,
     But westward, look, the land is bright.
</pre>
</blockquote>

The optimistic tone of Clough's poem contrasted rather strongly with affairs on the ground. O'Connor may have had syphilis; at any rate his behavior became exceedingly erratic after 1848. This may have contributed to the rapid decline of Chartism. The final national convention of the Chartist movement was held in 1858, with little attendance. Clough himself died shortly after, in 1861.


----

The Chartists, like many mass movements, had a broad and sometimes ill-defined agenda. The core of their goals deduced from their name: they were named after the "People's Charter of 1838", a drafted act of parliament which specified six reforms in British government. They were:

1. Voting rights for all men over 21 years of age
2. Secret ballots
3. Abolish the requirement that Members of Parliament own property.
4. Pay MPs, to allow the poor to be elected.
5. Constituencies of equal size
6. Annual parliamentary elections

At the time of Clough's death, the Chartist movement had been defeated --- but the property qualification had already been abolished by an act of 1858. In 1867, the size of the electorate was [tripled](https://en.wikipedia.org/wiki/Reform_Act_1867#Enfranchisements). The first secret ballot was held [in 1872](https://en.wikipedia.org/wiki/Ballot_Act_1872). The notion of equally populated constituencies was enshrined in law [in 1885](https://en.wikipedia.org/wiki/Redistribution_of_Seats_Act_1885). MPs began to be payed in 1911, and elections were required every five years at the same time. Finally, [in 1928](https://en.wikipedia.org/wiki/Representation_of_the_People_%28Equal_Franchise%29_Act_1928) all persons, men and women, 21 years of age or older, earned the right to vote.


<blockquote class="poem" style="text-align: center; font-style: italic">
In front the sun climbs slow, how slowly,<br>
But westward, look, the land is bright.
</blockquote>


----


