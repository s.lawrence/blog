---
title: "Evolutionary Pressure on Weddings"
date: 2021-09-14
---

When you hold a wedding, it's likely to be influenced by weddings you've
attended in the past. Mostly this has obvious boring consequences; nice
features of weddings you've been to (open bar) might be copied, horrid features
avoided. Slightly more subtle, but still unsurprising, is something like
anchoring bias. The weddings you have attended in the past define a "typical"
wedding, and good or bad, any new ceremony is likely to be heavily based on
that model.

In summary: good features propagate, bad features die out, and culture
persists.

One feature of a wedding, though, is a little different: its size. More people
attend larger weddings. If, on a given day, ten weddings are held, of which
half have ten guests and half have two hundred, then twenty times as many
people will have attended a large two-hundred-person wedding, even though only
half the weddings that day were so large.

This has consequences for the idea of a "typical" wedding. Former guests, when
planning their own ceremonies, will look to the set of weddings they've
attended to determine what is typical. But that set is inherently biased
towards larger weddings. As a result, when guests try to design their own
weddings based on what is typical, there's pressure for the size of weddings to
grow over time. Of course, this growth is not without bound, because there are
practical reasons why a wedding can be too large. But we'd expect more weddings
to "err" on the side of being too large than too small.

After a quick rephrasing, this is the same as the [friendship
paradox](https://en.wikipedia.org/wiki/Friendship_paradox). Most weddings
you've attended have had more guests than yours had (or will).

Finally, note that there are other reasons why weddings might grow (a large
wedding is a nice signal of wealth/status, for instance), but those are all
dependent on external societal factors. The evolutionary pressure on size is
independent of culture, laws, or customs.

