---
title: "Where Is the Cone?"
date: 2020-08-22
draft: true
---

<!-- ## The usual story: Kepler's first law and conic sections -->

Kepler's first law, governing the motion of planets about the Sun, is usually quoted as
> A planet orbits in an ellipse, with the Sun at one of the two foci.

As written, the law governs only the motion of planets in <em>bound</em> orbits; that is, planets which will never escape the Sun's gravitational influence, but instead repeat the same path over and over again. Of course this is not the most general type of trajectory in the Sun's gravitational field. In fact there are three kinds of trajectories:
- _Bound_ trajectories (orbits), which take the shape of an ellipse.
- _Unbound_ trajectories, which take the shape of a hyperbola.
- _Escape_ trajectories, which take the shape of a parabola. These sit at the margin between bound and unbound trajectories.

At this point, we see a bit of a coincidence. The ellipse, hyperbola, and parabola are also the three types of conic sections. We can rephrase Kepler's law as follows:

> The trajectory of an object about the Sun is a conic section. The type of conic section depends on whether the object is bound (ellipse), unbound (hyperbola), or marginally bound (parabola).

Geometrically, a conic section is the intersection of a cone (half-cone, if you like) and a plane. Why should the classification of orbits correspond so closely to the classification of such intersections? Given an orbit, **where are the cone and the plane**?

The plane is obvious: it can only be the orbital plane, defined by \\(\vec L \cdot \vec x = 0\\). The cone is more problematic. Before trying to identify it, let's count.

Specifying a single orbit requires eight parameters. There are three each for the position of the sun, the position of the orbiting body, and the momentum of the orbiting body --- but these nine parameters specify both the orbit and the position of the orbiting body. Losing the information about the position of the orbiting body reduces us to eight parameters.

What about specifying a cone and a plane? A plane is defined by three parameters: two for the orientation, and one for the amount of translation orthogonal to the plane. A cone has one parameter specifying the angle of aperture, three givign the position of the apex, and two giving the orientation. Thus there are a total of nine parameters specifying the cone and the plane.

This is sad news. Because the space of cones and planes is nine-dimensional, one higher dimension than the space of orbits, we can see an orbit will not be associated to a unique cone. The question, "_Where is the cone?_" has no natural answer.

We're not done, though. All we've learned is that the reason orbits correspond to conic sections is not "there's a cone and a plane in space, which intersect".


<!--
The cone is defined by the location of the apex, the orientation, and the angle of aperture; because there are so many unitless parameters, simple arguments based on dimensions won't be enough to find the cone.

Before moving on, it's worth noting that there are 8 parameters specifying a conic section (two to specify the plane TODO WRONG, three to position the cone, two to orient the cone, and one to specify the aperture). The behavior of a particle orbiting the sun is determined by 9 parameters (position and velocity of the particle, and position of the sun) --- the difference of one is because the conic section does not know about time.

It's usually convenient to specify an orbit simply by listing the conserved quantities. After all, the existence of a complete set of conserved quantities is what makes the two-body problem analytically solvable. The two-body system (with zero total momentum) has 8 independent constants of motion. These are often represented by specifying the center of mass (three components), the energy of the orbit \\( E \\), the angular momentum (\\(\vec L\\), three components) and the [Laplace-Runge-Lenz vector](https://en.wikipedia.org/wiki/Laplace%E2%80%93Runge%E2%80%93Lenz_vector) (\\(\vec A\\), three components).
-->

<!--
## A boring answer

The simplest way to account for the correspondence between orbits and conic sections is rather unsatisfying. Conic sections (in cartesian coordinates) are solutions to quadratic equations in two variables:
$$ y^2 + a y + b x^2 + c x + d = 0 \text. $$

We will now show that equations for gravitational trajectories must have this same form. The gravitational acceleration of a body at the point \\((x,y)\\) is given by an inverse square law, so that the trajectory is a solution to the coupled second-order differential equations
$$\ddot x = \frac{G M}{x^2 + y^2} \frac{x}{\sqrt{x^2 + y^2}}\text{ and }\ddot y = \frac{G M}{x^2 + y^2} \frac{y}{\sqrt{x^2+y^2}}\text.$$
Note that this system has more information than the quadratic equation above, which says nothing about the velocity of the trajectory. Therefore we can discard some information, and note that any solution to this system is also a solution of
$$y \ddot x = x \ddot y \text,$$
TODO are these equations right, and does this imply the solution is quadratic?

This explains the coincidence between gravitational trajectories and conic sections, but without giving any physical insight. As we will see, we can in fact identify a cone and a plane, which intersect to form a trajectory.

## Conservation laws

Energy and angular momentum are both conserved along a gravitational trajectory. This gives a simpler derivation of the fact that gravitational trajectories are described by quadratic equations. Denoting the energy (per unit mass) of the trajectory \\(\epsilon\\), and the angular momentum \\( l \\), the conserved quantities are
$$ \epsilon = \frac{1}{2}\left(\dot x^2 + \dot y^2\right) - \frac{G M}{\sqrt{x^2 + y^2}} \text{ and } l = x \dot y - y \dot x\text. $$

TODO
-->
