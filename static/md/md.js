/*
 * Molecular dynamics in the browser.
 * Copyright 2021  Scott Lawrence
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 */

/*
 * TODO:
 *
 *  - touch input
 *  - use VAO
 *
 */

function showMenu() {
	let menu = document.querySelector("#menu");
	if (menu.style.display == "block")
		menu.style.display = "none";
	else
		menu.style.display = "block";
}

function load() {
	WebAssembly.instantiateStreaming(fetch('wmd.wasm'), {}).then(r => {
		window.wasm = r.instance.exports;
		window.wasm_buffer = r.instance.exports.memory.buffer;
		window.wasm_pos_offset = r.instance.exports.pos.value;

		let MAX_N = wasm.get_MAX_N();
		let MAX_L = wasm.get_MAX_L();
		let OVERLAY_L = wasm.get_OVERLAY_L();
		let possz = 2*MAX_N;
		window.wasm_pos = new Float32Array(wasm_buffer, wasm_pos_offset, possz);
		let overlay_sz = OVERLAY_L*OVERLAY_L;
		window.overlay_position = new Float32Array(wasm_buffer, wasm.overlay_position.value, overlay_sz*6*2);
		window.overlay_color = new Float32Array(wasm_buffer, wasm.overlay_color.value, overlay_sz*6*4);

		document.querySelector('#n').oninput = start;
		document.querySelector('#R').oninput = start;
		document.querySelector('#T').oninput = function () {
			wasm.set_T(document.querySelector('#T').value);
		};
		start();

		const canvas = document.querySelector("#glCanvas");

		function get_xy(ev) {
			let aspect = canvas.width / canvas.height;
			let x = 2 * ev.x / canvas.height - aspect;
			let y = -2 * ev.y / canvas.height + 1;
			return [x,y]
		}

		canvas.onmousedown = (ev) => {
			if (ev.buttons != 1) return;
			let xy = get_xy(ev);
			r.instance.exports.mouse_down(xy[0],xy[1]);
		};

		canvas.onmousemove = (ev) => {
			if (ev.buttons != 1) return;
			let xy = get_xy(ev);
			r.instance.exports.mouse_move(xy[0],xy[1]);
		};
	});
}

var paused = false;

const particleVS = `
attribute vec2 vp;
attribute vec2 pos;

uniform mat2 uAM;
uniform mat2 uSM;

void main() {
	gl_Position = vec4(uAM * (uSM * vp + pos), 1., 1.);
}
`;

const particleFS = `
uniform lowp vec4 uColor;
void main() {
	gl_FragColor = vec4(uColor[0], uColor[1], uColor[2], 1.0);
}
`;

const hydroVS = `
attribute vec2 pos;
attribute vec4 col;
varying vec4 vCol;

uniform mat2 uAM;

void main() {
	gl_Position = vec4(uAM * pos, 0.5, 1.);
	vCol = col;
}
`;

const hydroFS = `
varying lowp vec4 vCol;
void main() {
	gl_FragColor = vCol;
}
`;

function start() {
	if (window.last_request)
		cancelAnimationFrame(last_request);

	const canvas = document.querySelector("#glCanvas");
	const gl = canvas.getContext("webgl2", {antialias: true});
	canvas.height = gl.canvas.clientHeight;
	canvas.width = gl.canvas.clientWidth;
	let aspect = canvas.width / canvas.height;

	let size = document.querySelector("#R").value;
	let vol = 4 * aspect / (Math.PI * size * size) / 2.5;
	let Nparticles = Math.round(document.querySelector("#n").value * vol);
	let MAX_N = wasm.get_MAX_N();
	if (Nparticles > MAX_N) Nparticles = MAX_N;
	document.querySelector('#nparticles').innerHTML = Nparticles;
	let temperature = document.querySelector("#T").value;

	window.onresize = function () {
		canvas.height = gl.canvas.clientHeight;
		canvas.width = gl.canvas.clientWidth;
		aspect = canvas.width / canvas.height;
		wasm.set_aspect(aspect);
		gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
	};
	window.onresize();

	/* Shaders */
	gl.loadShader = function (type, src) {
		const shader = gl.createShader(type);
		gl.shaderSource(shader, src);
		gl.compileShader(shader);
		return shader;
	};

	const particleVertexShader = gl.loadShader(gl.VERTEX_SHADER, particleVS);
	const particleFragmentShader = gl.loadShader(gl.FRAGMENT_SHADER, particleFS);
	const particleShaderProgram = gl.createProgram();
	gl.attachShader(particleShaderProgram, particleVertexShader);
	gl.attachShader(particleShaderProgram, particleFragmentShader);
	gl.linkProgram(particleShaderProgram);

	const vpPos = gl.getAttribLocation(particleShaderProgram, 'vp');
	const posPos = gl.getAttribLocation(particleShaderProgram, 'pos');
	const uAM = gl.getUniformLocation(particleShaderProgram, 'uAM');
	const uSM = gl.getUniformLocation(particleShaderProgram, 'uSM');
	const uColor = gl.getUniformLocation(particleShaderProgram, 'uColor');

	const hydroVertexShader = gl.loadShader(gl.VERTEX_SHADER, hydroVS);
	const hydroFragmentShader = gl.loadShader(gl.FRAGMENT_SHADER, hydroFS);
	const hydroShaderProgram = gl.createProgram();
	gl.attachShader(hydroShaderProgram, hydroVertexShader);
	gl.attachShader(hydroShaderProgram, hydroFragmentShader);
	gl.linkProgram(hydroShaderProgram);

	const hydro_pos = gl.getAttribLocation(hydroShaderProgram, 'pos');
	const hydro_col = gl.getAttribLocation(hydroShaderProgram, 'col');
	const hydro_uAM = gl.getUniformLocation(hydroShaderProgram, 'uAM');

	gl.enable(gl.BLEND);
	//gl.enable(gl.DEPTH_TEST);
	//gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
	gl.blendFunc(gl.SRC_ALPHA, gl.DST_ALPHA);
	gl.clearColor(0., 0.0, 0.0, 1.);

	/* Arrays */
	const particleBuffer = gl.createBuffer();
	const particleResolution = 50;
	const particleGeometry = new Float32Array((particleResolution + 1)*2);
	particleGeometry[0*3+0] = 0.;
	particleGeometry[0*3+1] = 0.;
	for (var i = 0; i < particleResolution; i++) {
		theta = 2*Math.PI * i / (particleResolution-2);
		thetap = 2*Math.PI * (i+1) / (particleResolution-2);
		particleGeometry[(i+1)*2+0] = Math.cos(theta);
		particleGeometry[(i+1)*2+1] = Math.sin(theta);
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, particleBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, particleGeometry, gl.STATIC_DRAW);

	const positionBuffer = gl.createBuffer();
	const overlayBuffer = gl.createBuffer();
	const colorBuffer = gl.createBuffer();

	/* Initialize WASM */
	wasm.init(Math.round(1e6*Math.random()), aspect, Nparticles, size, temperature);

	let OVERLAY_L = wasm.get_OVERLAY_L();

	/* Physics update */
	const update = function (dt, now) {
		wasm.update(dt/1000, now);
	};

	/* Rendering */
	var last_ms = 0;
	let frames_this_second = 0;
	const render = function (ms) {
		frames_this_second++;
		// Update FPS
		if (last_ms % 1000 > ms % 1000) {
			document.querySelector('#fps').innerHTML = frames_this_second;
			frames_this_second = 0;
		}

		// Don't update too much at once.
		if (ms - last_ms > 100) last_ms = ms-100;
		if (paused) {
			last_ms = ms;
			requestAnimationFrame(render);
			return;
		}
		update(ms - last_ms, ms);
		window.last_request = requestAnimationFrame(render);
		last_ms = ms;

		gl.clear(gl.COLOR_BUFFER_BIT);

		// Prepare to draw particles.
		gl.useProgram(particleShaderProgram);
		gl.uniformMatrix2fv(uAM, false, [1/aspect,0,0,1.0]);
		gl.uniformMatrix2fv(uSM, false, [size,0,0,size]);

		gl.bindBuffer(gl.ARRAY_BUFFER, particleBuffer);
		gl.vertexAttribPointer(vpPos, 2, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(vpPos);

		// Copy positions in.
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, wasm_pos, gl.STREAM_DRAW);
		gl.vertexAttribPointer(posPos, 2, gl.FLOAT, false, 0, 0);
		gl.vertexAttribDivisor(posPos, 1);
		gl.enableVertexAttribArray(posPos);

		// Draw particles
		gl.uniform4f(uColor, 1., 1., 1., 0.)
		gl.drawArraysInstanced(gl.TRIANGLE_FAN, 0, particleResolution, Nparticles);

		let overlay = document.querySelector('input[name="overlay"]:checked').value;
		if (overlay == "none") return;
		if (overlay == "velocity")
			wasm.overlay_velocity();
		else if (overlay == "temperature")
			wasm.overlay_temperature();

		// Display overlay
		gl.useProgram(hydroShaderProgram);
		gl.uniformMatrix2fv(hydro_uAM, false, [1/aspect,0,0,1.0]);
		
		gl.bindBuffer(gl.ARRAY_BUFFER, overlayBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, overlay_position, gl.STREAM_DRAW);
		gl.vertexAttribPointer(hydro_pos, 2, gl.FLOAT, false, 0, 0);
		gl.vertexAttribDivisor(hydro_pos, 0);
		gl.enableVertexAttribArray(hydro_pos);

		gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, overlay_color, gl.STREAM_DRAW);
		gl.vertexAttribPointer(hydro_col, 4, gl.FLOAT, false, 0, 0);
		gl.vertexAttribDivisor(hydro_col, 0);
		gl.enableVertexAttribArray(hydro_col);

		gl.drawArrays(gl.TRIANGLES, 0, OVERLAY_L*OVERLAY_L*6);
	}
	window.last_request = requestAnimationFrame(render);
}

