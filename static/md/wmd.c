#define MAX_N (1 << 17)
#define MAX_L 400
#define OVERLAY_L 20
#define M 10

int get_MAX_N() { return MAX_N; }
int get_MAX_L() { return MAX_L; }
int get_OVERLAY_L() { return OVERLAY_L; }

float pos[2*MAX_N];
float vel[2*MAX_N];

/* Fundamental parameters */
float aspect;
int N;
float R;
float T;

/* Grid */
int Lx, Ly;
float Dx, Dy;
float Ox, Oy;
int grid[MAX_L][MAX_L][M];

/* Overlay */
float density[OVERLAY_L][OVERLAY_L];
float momentum[OVERLAY_L][OVERLAY_L][2];
float color[OVERLAY_L][OVERLAY_L][4];
float overlay_position[OVERLAY_L][OVERLAY_L][6][2];
float overlay_color[OVERLAY_L][OVERLAY_L][6][4];

#define RAND_MAX (1 << 30)
#define RAND_A 1103515245
#define RAND_C 12345
long long prng_state;

int rand() {
	return prng_state = (RAND_A*prng_state + RAND_C + RAND_MAX) % RAND_MAX;
}

float randf() {
	return ((float)rand())/RAND_MAX;
}

float dispx(int i, int j) {
	float r = pos[2*i+0] - pos[2*j+0];
	if (r > aspect) r -= 2*aspect;
	if (r < -aspect) r += 2*aspect;
	return r;
}

float dispy(int i, int j) {
	float r = pos[2*i+1] - pos[2*j+1];
	if (r > 1) r -= 2.;
	if (r < -1) r += 2.;
	return r;
}

float dist(int i, int j) {
	float d0 = dispx(i,j);
	float d1 = dispy(i,j);
	return __builtin_sqrt(d0*d0 + d1*d1);
}

void set_aspect(float aspect_) {
	aspect = aspect_;
}

void zero_sum_vel() {
	float vel0 = 0, vel1 = 0;
	for (int i = 0; i < N; i++) {
		vel0 += vel[2*i+0];
		vel1 += vel[2*i+1];
	}
	for (int i = 0; i < N; i++) {
		vel[2*i+0] -= vel0/N;
		vel[2*i+1] -= vel1/N;
	}
}

void set_T(float T_) {
	T = T_;
}

void init(int seed, float aspect_, int N_, float R_, float T_) {
	prng_state = seed%RAND_MAX;

	aspect = aspect_;
	N = N_;
	R = R_;
	T = T_;

	Lx = (int)(aspect/R);
	Ly = (int)(1/R);
	if (Lx < 1) Lx = 1;
	if (Lx > MAX_L) Lx = MAX_L;
	if (Ly < 1) Ly = 1;
	if (Ly > MAX_L) Ly = MAX_L;

	Dx = 2.*aspect / Lx;
	Dy = 2. / Ly;

	Ox = 2.*aspect / OVERLAY_L;
	Oy = 2. / OVERLAY_L;

	// Initialize particles.
	for (int i = 0; i < N; i++) {
		pos[2*i+0] = aspect * (randf()*2-1);
		pos[2*i+1] = randf()*2-1;

		for (int k = 0; k < 2; k++)
			vel[2*i+k] = (randf()*2 - 1) * R;
	}
	zero_sum_vel();

	// Initialize overlay.
	for (int x = 0; x < OVERLAY_L; x++) for (int y = 0; y < OVERLAY_L; y++) {
		float xc = -aspect + x*Ox;
		float yc = -1 + y*Oy;
		overlay_position[x][y][0][0] = xc;
		overlay_position[x][y][0][1] = yc;
		overlay_position[x][y][1][0] = xc+Ox;
		overlay_position[x][y][1][1] = yc;
		overlay_position[x][y][2][0] = xc+Ox;
		overlay_position[x][y][2][1] = yc+Oy;
		overlay_position[x][y][3][0] = xc;
		overlay_position[x][y][3][1] = yc;
		overlay_position[x][y][4][0] = xc+Ox;
		overlay_position[x][y][4][1] = yc+Oy;
		overlay_position[x][y][5][0] = xc;
		overlay_position[x][y][5][1] = yc+Oy;

		color[x][y][0] = 0.;
		color[x][y][1] = 0.;
		color[x][y][2] = 0.;
		color[x][y][3] = 0.;
	}
}

void update_collision(int i, int j) {
	if (i == j) return;
	if (dist(i,j) > 2*R) return;
	float d0 = dispx(i,j);
	float d1 = dispy(i,j);
	float dnorm = __builtin_sqrt(d0*d0+d1*d1);
	float dir0 = d0/dnorm;
	float dir1 = d1/dnorm;
	float v0 = vel[2*i+0] - vel[2*j+0];
	float v1 = vel[2*i+1] - vel[2*j+1];
	float parallel = dir0*v0 + dir1*v1;
	if (parallel > 0) return;
	vel[2*i+0] -= parallel*dir0;
	vel[2*i+1] -= parallel*dir1;
	vel[2*j+0] += parallel*dir0;
	vel[2*j+1] += parallel*dir1;
}

void update_particle(int i) {
	float xf = pos[2*i+0];
	float yf = pos[2*i+1];
	int x = (int)((xf+aspect)/Dx);
	int y = (int)((yf+1.)/Dy);
	int rx = 1 + (int)(2*R/Dx);
	int ry = 1 + (int)(2*R/Dy);
	for (int dx = -rx; dx <= rx; dx++)
		for (int dy = -ry; dy <= ry; dy++) {
			int xp = (x+dx+Lx)%Lx;
			int yp = (y+dy+Ly)%Ly;
			for (int j = 0; grid[xp][yp][j] != -1 && j < M; j++)
				update_collision(i, grid[xp][yp][j]);
		}
}

void update_grid(float dt, float now) {
	for (int i = 0; i < N; i++) {
		// Move
		pos[2*i] += T*dt*vel[2*i];
		pos[2*i+1] += T*dt*vel[2*i+1];

		// Boundary
		if (pos[2*i+0] > aspect) pos[2*i+0] -= 2.*aspect;
		if (pos[2*i+0] < -aspect) pos[2*i+0] += 2.*aspect;
		if (pos[2*i+1] > 1.) pos[2*i+1] -= 2.;
		if (pos[2*i+1] < -1.) pos[2*i+1] += 2.;
	}

	// Place everything in the grid.
	for (int x = 0; x < Lx; x++)
		for (int y = 0; y < Ly; y++)
			for (int m = 0; m < M; m++)
				grid[x][y][m] = -1;
	for (int i = 0; i < N; i++) {
		float xf = pos[2*i+0];
		float yf = pos[2*i+1];
		int x = (int)((xf+aspect)/Dx);
		int y = (int)((yf+1.)/Dy);
		int j = 0;
		while (grid[x][y][j] != -1 && j < M) j++;
		grid[x][y][j] = i;
	}

	// Perform collisions.
	for (int x = 0; x < Lx; x++)
		for (int y = 0; y < Ly; y++)
			for (int i = 0; grid[x][y][i] != -1 && i < M; i++)
				update_particle(grid[x][y][i]);
}

void update(float dt, float now) {
	update_grid(dt, now);
}

float last_x, last_y;

void mouse_down(float mx, float my) {
	last_x = mx;
	last_y = my;
}

void mouse_move(float mx, float my) {
	for (int i = 0; i < N; i++) {
		float x = pos[2*i+0];
		float y = pos[2*i+1];
		float dx = x - mx;
		if (dx > aspect) dx -= 2*aspect;
		if (dx < -aspect) dx += 2*aspect;
		float dy = y - my;
		if (dy > 1) dy -= 2;
		if (dy < -1) dy += 2;
		float dist = __builtin_sqrt(dx*dx+dy*dy);
		vel[2*i+0] += R / (2*dist*dist + 1) * (mx - last_x);
		vel[2*i+1] += R / (2*dist*dist + 1) * (my - last_y);
	}
	last_x = mx;
	last_y = my;
	zero_sum_vel();
}

void copy_colors() {
	for (int x = 0; x < OVERLAY_L; x++) for (int y = 0; y < OVERLAY_L; y++) {
		int xp = (x+1)%OVERLAY_L;
		int yp = (y+1)%OVERLAY_L;
		for (int k = 0; k < 4; k++) {
			overlay_color[x][y][0][k] = color[x][y][k];
			overlay_color[x][y][1][k] = color[xp][y][k];
			overlay_color[x][y][2][k] = color[xp][yp][k];
			overlay_color[x][y][3][k] = color[x][y][k];
			overlay_color[x][y][4][k] = color[xp][yp][k];
			overlay_color[x][y][5][k] = color[x][yp][k];
		}
	}
}

void overlay_temperature() {
	for (int x = 0; x < OVERLAY_L; x++) for (int y = 0; y < OVERLAY_L; y++) {
		color[x][y][0] = 0.;
		color[x][y][1] = 0.;
		color[x][y][2] = 0.;
		color[x][y][3] = 1.;
	}
	copy_colors();
}

void overlay_velocity() {
	for (int x = 0; x < OVERLAY_L; x++) for (int y = 0; y < OVERLAY_L; y++) {
		density[x][y] = 1.;
		momentum[x][y][0] = 0.;
		momentum[x][y][1] = 0.;
	}
	for (int i = 0; i < N; i++) {
		float xf = pos[2*i+0];
		float yf = pos[2*i+1];
		int x = (int)((xf+aspect)/Ox);
		int y = (int)((yf+1.)/Oy);
		density[x][y] += 1.;
		momentum[x][y][0] += vel[2*i+0];
		momentum[x][y][1] += vel[2*i+1];
	}
	for (int x = 0; x < OVERLAY_L; x++) for (int y = 0; y < OVERLAY_L; y++) {
		float p = momentum[x][y][1];
#define MC 0.3
		if (p > MC) p = MC;
		if (p < -MC) p = -MC;
		color[x][y][0] = (MC+p)*.05 + .95*color[x][y][0];
		color[x][y][1] = (MC-p)*.05 + .95*color[x][y][1];
		color[x][y][2] = (MC-p)*.05 + .95*color[x][y][2];
		color[x][y][3] = 1.;
	}
	copy_colors();
}
