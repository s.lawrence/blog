
const vsSource = `
attribute vec4 vp;
//uniform mat4 uMVM;
//uniform mat4 uPM;

void main() {
	//gl_Position = uPM * uMVM * vp;
	gl_Position = vp;
}
`;

const fsSource = `
void main() {
	gl_FragColor = vec4(1., 1., 1., 1.);
	//gl_FragColor = vec4(0.5, 0.5, 1., 1.);
}
`;


function start() {
	const canvas = document.querySelector("#glCanvas");
	canvas.height = window.innerHeight;
	canvas.width = window.innerWidth * 0.7;
	const gl = canvas.getContext("webgl", {antialias: true});
	if (gl == null) {
		alert("WebGL not supported. :'(");
		return;
	}

	window.onresize = function () {
		canvas.height = window.innerHeight;
		canvas.width = window.innerWidth * 0.7;
	}

	gl.loadShader = function (type, src) {
		const shader = gl.createShader(type);
		gl.shaderSource(shader, src);
		gl.compileShader(shader);
		return shader;
	}

	/* Initialize shaders */
	const vertexShader = gl.loadShader(gl.VERTEX_SHADER, vsSource);
	const fragmentShader = gl.loadShader(gl.FRAGMENT_SHADER, fsSource);
	const shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);
	gl.useProgram(shaderProgram);
	const vpPos = gl.getAttribLocation(shaderProgram, 'vp');
	
	gl.clearColor(0., 0., 0., 1.);
	gl.clearDepth(1.0);

	const len = 800;
	const x = new Float32Array(len);
	const u = new Float32Array(len);
	const u_ = new Float32Array(len);
	const up = new Float32Array(len);
	const upp = new Float32Array(len);
	for (var i = 0; i < len; i++) {
		x[i] = 2*i/len - 1;
		//u[i] = Math.cos(2 * Math.PI * x[i] * 1);
		u[i] = Math.exp(-x[i]*x[i] / 0.01) + 0.4*Math.exp(-(x[i]-0.4)*(x[i] - 0.4) / 0.01)
	}
	const dx = x[1] - x[0];

	const step = function (dt) {
		dt = dt / 1000;

		// Calculate derivatives.
		up[0] = (u[1] - u[len-1]) / dx;
		upp[0] = (u[1] + u[len-1] - 2*u[0]) / dx / dx;
		up[len-1] = (u[0] - u[len-2]) / dx;
		upp[len-1] = (u[0] + u[len-2] - 2*u[len-1]) / dx / dx;
		for (var i = 1; i < len-1; i++) {
			up[i] = (u[i+1] - u[i-1]) / (2*dx)
			upp[i] = (u[i+1] + u[i-1] - 2*u[i]) / (dx*dx)
		}

		// Evolve.
		for (var i = 0; i < len; i++) {
			//u_[i] = u[i] + up[i] * dt / 40;
			u_[i] = u[i] + upp[i] * dt / 1000;
		}

		// Copy to destination.
		for (var i = 0; i < len; i++) {
			u[i] = u_[i];
		}
		return upp[0];
	}

	var last_ms = 0.;
	const nsteps = 100;

	const positionBuffer = gl.createBuffer();
	const render = function (ms) {
		requestAnimationFrame(render);
		var diff = ms - last_ms;
		if (diff > 100)
			diff = 100;

		for (var i = 0; i < nsteps; i++)
			step(diff/nsteps);
		last_ms = ms;
		gl.clear(gl.COLOR_BUFFER_BIT);

		/* Set up buffers */
		const positions = new Float32Array(2*len);
		for (var i = 0; i < len; i++) {
			positions[2*i] = x[i];
			positions[2*i+1] = u[i];
		}
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, positions, gl.DYNAMIC_DRAW);

		gl.vertexAttribPointer(vpPos, 2, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(vpPos);
		gl.drawArrays(gl.LINE_STRIP, 0, len);

	}
	requestAnimationFrame(render);
}

