<!DOCTYPE html>
<meta charset="UTF-8">
<title>Notes on Bosonization</title>
<script async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
<link rel="stylesheet" href="notes.css">
<script>
window.MathJax = {
  tex: {
    tags: 'ams'
  }
};
</script>
<h1>Notes on Bosonization</h1>
<p>These notes are based on <a href="https://arxiv.org/abs/1711.00515">Chen, Kapulstin, Redicevic</a> and <a href="https://arxiv.org/abs/1807.07081">Chen, Kapustin</a>. Those authors work in terms of Majorana modes, rather than creation and annihilation operators. At a single site, the majorana modes \(\gamma\) and \(\gamma'\) are defined in terms of the creation and annihilation operators by
\begin{equation}
\gamma = c^\dagger + c
\;\text{ and }\;
\gamma' = i (c^\dagger - c)
\end{equation}
With multiple sites, there are two majorana modes at each site. The operators \(\gamma\) and \(\gamma'\) are hermitian, square to the identity, and anticommute.

Two bilinears are sufficient to generate all even fermionic operators. In the notation of Chen et al., these are
\begin{equation}\label{bilinears}
(-1)^{F_f} = -i \gamma_f \gamma_f'
\;\text{ and }\;
S_e = i \gamma_{L(e)} \gamma'_{R(e)}
\end{equation}
where the index \(f\) denotes a fermion site, and \(R(e)\) and \(L(e)\) denote two adjacent fermion sites connected by edge \(e\). For example, note that
\begin{equation}
(-1)^{F_{L(e)}}
S_e
(-1)^{F_{R(e)}}
= -i \gamma'_{L(e)} \gamma_{R(e)}
\text.
\end{equation}
The combination of this identity with (\ref{bilinears}) allows the standard fermionic hopping term to be constructed:
\begin{equation}
c^\dagger_a c_b + c^\dagger_b c_a
=
\frac 1 2
\left(
i \gamma_{L(e)} \gamma'_{R(e)}
-i \gamma'_{L(e)} \gamma_{R(e)}
\right)
\text.
\end{equation}
Finally, the number operator is given by
\begin{equation}
c_f^\dagger c_f = \frac 1 2 (1 + i \gamma_f \gamma'_f)
\text.
\end{equation}

<h2>Bosonization in two dimensions</h2>
<p>Draw a square lattice. The fermions live in the faces. Place \(\mathbb Z_2\) degrees of freedom on the edges, associated with pauli operators \(X_e,Y_e,Z_e\).

<img src="lattice.png" style="width:50%"/>

<p>To bosonize, we want to obtain a set of operators on the quantum computer with the same algebra as the even part of the grassmann algebra discussed above. That algebra is characterized by (I won't try to show that these relations are complete):
<ul>
	<li>All \((-1)^{F_f}\) operators are mutually commuting.</li>
	<li>\(\{(-1)^{F_f},S_e\} = 0\) if \(e \subset f\); otherwise they commute.</li>
	<li>Similarly, \(S_e\) and \(S_{e'}\) commute, unless they originate from the same point and point east and south, or north and west.</li>
	<li>What happens when you move a particle in a circle?
		\begin{equation}
		\gamma_a \gamma'_b \gamma_b \gamma'_c \gamma_c \gamma'_d \gamma_d \gamma'_a
		=
		(-1)^{F_a}
		(-1)^{F_b}
		(-1)^{F_c}
		(-1)^{F_d}
		\end{equation}
		Or, in terms of the swap operators,
		\begin{equation}
		S_{58} S_{56} S_{25} S_{45}
		=
		(-1)^{F_a} (-1)^{F_c}
		\end{equation}
	</li>
</ul>

<p>Define the following operators, which will correspond to \((-1)^{F_f}\) and \(S_e\), respectively:
\begin{equation}
W_f = \prod_{e\subset f} Z_e
\;\text{ and }\;
U_{56} = X_{56} Z_{25}
\;\text{ and }\;
U_{58} = X_{58} Z_{45}
\end{equation}
with all other \(U_e\) operators defined by translation.

<h3>Drawing pictures</h3>
<p>Chains of paulis are a pain to read and write. Instead of
\[
X_{12} X_{25} X_{56} Z_{25} Z_{58}
=
-
Z_{25} Z_{58}
X_{12} X_{25} X_{56}
\]
it helps to draw pictures like this:
<img src="pauli-demo.svg" style="width:80%" />

<h3>Commutators</h3>
<p>It's immediately obvious that all \(W_f\) operators are mutually commuting.

<img src="commutators.svg" style="width: 90%"/>

<h3>One more constraint</h3>
<p>The last constraint can be written more succinctly as
\begin{equation}
(-1)^{F_a} (-1)^{F_c} S_{58} S_{56} S_{25} S_{45}
=
1
\end{equation}
This leads to a constraint on the bosonic operators that must be satisfied in order to reproduce the fermionic algebra:
\begin{equation}\label{gauge}
W_{NE(v)}\prod_{e \in v} X_e
= 1
\text.
\end{equation}
Can this constraint be satisfied? Certainly it defines a particular slice of Hilbert space. Moreover, this constraint commutes with the generators \(W_f\) and \(S_e\) of the algebra. That's sufficient.

<h3>"Gauge theory"?</h3><p>Well, there's a constraint on the Hilbert space, which is preserved by all physical operators. But, it's not the case that gauge transformations are hyper-local, nor do Wilson lines seem to transform in any particularly nice way.

<h3>Coupling to \(SU(3)\)</h3>
<p>Note that the lattice of \(\mathbb Z_2\) degrees of freedom does not coincide with the normal lattice, along which \(SU(3)\) fields live.

<p>This scheme (along with the higher-dimensional generalizations) is not directly compatible with <a href="https://arxiv.org/abs/1903.08807">Lamm, Lawrence, Yamauchi</a>; that method requires odd fermionic operators to be implemented, instead of just the even part of the algebra. Therefore, <em style="color:red">implementing the gauge-invariant hopping operator</em> is apparently an open problem.


<h2>Bosonization in three dimensions</h2>
<p>The generalization to three dimensions is fairly straightforward; however, the bosonic degrees of freedom now live on faces of a cubic lattice, with the fermionic degrees of freedom each surrounded by \(6\) faces.

<p>Each face \(f\) is associated to a \(\mathbb Z_2\) degree of freedom. The fermionic operator on a cube \(t\), \((-1)^{F_t}\) maps to \(W_t\), which is defined as the product
\begin{equation}
W_t = \prod_{f\subset t} Z_f
\text.
\end{equation}

<p>Fermionic hopping operators now implement a hop across a face, rather than an edge. The operator \(S_f\) is mapped to \(U_f\), which is a product of \(X_f\) and two \(Z\) operators. The choice of \(Z\) operators is defined, in part, by performing a stereographic projection of the 3d-lattice onto a plane.

<p>After all is said and done, the gauge-invariance constraint is
\begin{equation}
1
=
X_1 X_2 X_3 X_4
Z_1 Z_4 Z_5 Z_6 Z_7 Z_8 Z_9 Z_{10}
\text.
\end{equation}
<img src="lattice3d.png" style="width:40%" />
<p>Indeed, the word "elegant" appears nowhere in those papers. <a href="https://arxiv.org/abs/1908.10453">Banks</a> may provide a cleaner picture.

<h3>Gauge theory?</h3>
<p>This is referred to as a "2-form gauge theory". In this framing, it is &mdash; but the \(\mathbb Z_2\) degrees of freedom could just as well be said to live on edges of the dual lattice. Moreover, the law of gauge transformations is again not the natural one (gauge transformations are not hyperlocal).

