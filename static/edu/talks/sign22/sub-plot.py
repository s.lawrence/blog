#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np

with open('sub.dat') as f:
    dat = [[complex(x) for x in l.split()] for l in f.readlines()]
dat = np.array(dat).real

plt.figure(figsize=(5.5,3.6))
plt.errorbar(dat[:,0]/.35, dat[:,1], dat[:,2], fmt='ro', label='Raw')
plt.errorbar(dat[:,0]/.35, dat[:,5], dat[:,6], fmt='bo', label='Subtracted')
plt.xlabel('$\\mu / m_F$')
plt.ylabel('$\\langle\\sigma\\rangle$')
plt.legend(loc='best')
plt.tight_layout()
plt.savefig('sub-6-sign.png')

plt.figure(figsize=(5.5,3.6))
plt.errorbar(dat[:,0]/.35 - 0.03, dat[:,3], dat[:,4], fmt='ro', label='Raw')
plt.errorbar(dat[:,0]/.35 + 0.03, dat[:,7], dat[:,8], fmt='bo', label='Subtracted')
plt.xlabel('$\\mu / m_F$')
plt.ylabel('$\\langle n\\rangle$')
plt.legend(loc='best')
plt.tight_layout()
plt.savefig('sub-6-n.png')

