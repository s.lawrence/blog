\documentclass[11pt,letterpaper]{article}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{xcolor}
\newcommand{\todo}{\colorbox{pink}{\textsc{Todo}}}
\begin{document}
\title{Large-$N$ thermodynamics}
\author{Scott Lawrence}
\maketitle
\section{Quantum mechanics}
\[
H = \frac 1 2 \sum_i \left( p_i^2 + m^2 x_i^2\right)
+ \frac\lambda N \left(\sum_i x_i^2\right)^2
\]
\subsection{Mean-field ground state}
Consider only a single degree of freedom $x_1$. Disregarding effects that are $O(\frac 1 N)$, this oscillator is governed by a Hamiltonian
\[
H = \frac 1 2 p^2 + \frac {m^2}{2} x^2 + 2 \lambda \langle \tilde x^2 \rangle x^2
\text,
\]
where consistency requires that $\langle x^2 \rangle = \langle \tilde x^2 \rangle$. Using the expression for $\langle \tilde x^2 \rangle$ obtained in the appendix, this results in the following gap equation:
\[
\tilde m^2 = m^2 + 2 \lambda \frac 1 {\tilde m}
\text.
\]
An interesting limit to consider is where the bare mass vanishes. Here we find $\tilde m = \left(2 \lambda\right)^{1/3}$, which is at least the correct dimensional scaling.
\subsection{Finite temperature}
Now we take the same approach, but for finite temperature. The expectation value of $\langle x^2 \rangle$, at temperature $T$ and with mass term $\frac{m^2}{2} x^2$, is determined in the appendix:
\[
\langle x^2\rangle
=
\frac 1 {m^2}\langle H \rangle
=
\frac 1 {2m}
+ \frac 1 m \frac{1}{1 - e^{-\beta m}}
\text.
\]
The effective mass of a single degree of freedom is the same as it was when we were discussing the ground state, namely
\[
\tilde m^2 = m^2 + 4 \lambda \langle \tilde x^2 \rangle
\text.
\]
However we now have a different expression for $\langle \tilde x^2 \rangle$. The gap equation at arbitrary temperature is
\[
\tilde m^2 = m^2 + 4 \lambda \left(
\frac 1 {2\tilde m}
+ \frac 1 {\tilde m} \frac{1}{e^{\beta \tilde m} - 1}
\right)
\text.
\]
Again we might consider the case of vanishing bare mass, where the gap equation simplies a bit but not as much as in the zero-temperature case:
\[
\tilde m^3 = 2 \lambda + \frac{4 \lambda }{e^{\beta\tilde m} - 1}
\text.
\]
One more interesting limit can be taken. At high temperature (and with or without a non-zero $m$), the gap equation reads
\[
\tilde m = \left(4 T \lambda\right)^{1/4}
\text.
\]
Let us compute the energy of this high-temperature state. Note that we cannot simply take $N \langle H_1 \rangle$ with $H_1$ the one-body Hamiltonian, as this double-counts the interaction term. The \emph{state} is the finite-temperature state of that Hamiltonian, but evaluating the \emph{energy} is slightly more involved.

Using expressions from the appendix, we see
\begin{align*}
\langle p^2 \rangle &=
\frac {\tilde m} 2
+ \frac{\tilde m}{e^{\beta \tilde m} - 1}
 \\
\text{and }
\langle x^2 \rangle &=
\frac 1 {\tilde m} \left(\frac 1 2
+ \frac{1}{e^{\beta \tilde m} - 1}\right)
\text.
\end{align*}
So evaluating $\langle H \rangle$ (and disregarding fluctuations and other $\frac 1 N$-suppressed effects), we obtain the equation of state
\[
\frac 1 N
E(T)
=
\underbrace{\frac{\tilde m}{4} + \frac{\tilde m}{2 e^{\beta \tilde m} - 2}}_{\frac 1 2 \langle p^2 \rangle}
+
\underbrace{
\frac {m^2} {2 \tilde m} \left(\frac 1 2
+ \frac{1}{e^{\beta \tilde m} - 1}\right)
}_{\frac {m^2} 2 \langle x^2 \rangle}
+
\underbrace{
\frac \lambda {\tilde m^2} \left(\frac 1 2
+ \frac{1}{e^{\beta \tilde m} - 1}\right)^2
}_{\lambda \langle x^2 \rangle \langle x^2 \rangle}
\text.
\]
In the field theory case Paul looks at the large-coupling limit, where $\frac \lambda T = \hat \lambda$ is the dimensionless coupling constant. The equivalent thing for us is to look at the high-temperature limit:
\[
\frac 1 N E(T)
=
\frac 1 2 \frac{T \tilde m}{\tilde m}
+ \frac{m^2}{2} \frac{T}{\tilde m^2}
+
\lambda \frac{T^2}{\tilde m^4}
\]
The second term is subleading, being proportional to $\sqrt T$, and therefore there is no dependence on the original mass $m$ (as one expects). Expanding all $\tilde m = (4 T \lambda)^{\frac 1 4}$ in what remains yields:
\[
\frac 1 N E(T)
=
\frac 1 2 T + \frac{\lambda T^2}{4 T \lambda}
= \frac T 2 + \frac T 4
= \frac 3 4 T
\]

Note that the high-temperature equation of state for a single oscillator (with gap given above) is
\[
E_1(T) = T + O(T^{-1})
\text.
\]
So we have $E = \frac {3N}{4} E_1$.

\section{Quantum field theory}
\appendix
\section{Harmonic oscillator calculations}
\[
H = \frac {m^2} 2 x^2 + \frac 1 2 p^2
\]
The symmetry between $x$ and $p$ gives
\[
m^2 \langle x^2 \rangle = \langle p^2 \rangle = \langle H \rangle
\text.
\]
Since the spectrum is known to be $m (n + \frac 1 2)$, we immediately know these quadratic expectation values. In particularly we have
\[
\langle x^2 \rangle = \frac{1}{2 m^2}
\]
in the ground state.

Alternatively, we could compute these expectation values in the ground state by noticing that
\[
\psi(x) = e^{-m x^2 / 2}
\]
is an eigenfunction of this Hamiltonian with eigenvalue given by
\[
\hat H \psi(x)
=
\left(\frac {m^2}{2} x^2
- \frac 1 2 \frac{d^2}{dx^2} \right) e^{-m x^2 / 2}
=
\frac m 2 \psi(x)
\text,
\]
and integrating expectation values directly.

Energies and expectation values at finite temperature can be obtained directly by summing over the states. Using the relation $m^2 \langle x^2 \rangle \langle H \rangle$ above, we will only need to compute energies. The partition function at temperature $T$ is
\[
Z(T) = \sum_{k=0}^\infty e^{-E_k / T}
= \sum_k e^{-\frac {m}{2T} - k \frac m T}
= \frac{e^{-\frac m {2T}}}{1 - e^{-\frac m T}}\text.
\]
The expectation value of the energy is most easily extracted by differentiating with respect to the inverse temperature:
\[
\langle H \rangle
=
- \frac 1 Z \frac{\partial}{\partial \beta}
\frac{e^{-\frac 1 2 m \beta}}{1 - e^{-m \beta}}
= 
\frac m 2
+ \frac{m}{e^{\beta m} - 1}
\text.
\]
As usual we have $\langle p^2 \rangle = m^2 \langle x^2 \rangle = \langle H \rangle$, giving the quadratic expectation values:
\begin{align*}
\langle p^2 \rangle &= \frac m 2
+ \frac{m}{e^{\beta m} - 1}
\\
\text{and }\langle x^2 \rangle &=
\frac 1 m \left(\frac 1 2
+ \frac{1}{e^{\beta m} - 1}\right)
\text.
\end{align*}
\end{document}
