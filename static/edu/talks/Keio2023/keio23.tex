\documentclass[]{beamer}

\usepackage{CJKutf8}

\usepackage{amsmath}
\usepackage{slashed}
\usepackage{tikz}
\usepackage{multicol}
\usepackage{comment}
\usepackage{mathtools}
\usepackage{cancel}

\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{textcomp}

\usepackage[force]{feynmp-auto}

%\setlength{\parskip}{1em}

% Colors
\definecolor{red}{rgb}{0.8,0,0}
\definecolor{green}{rgb}{0.0,0.5,0}
\definecolor{blue}{rgb}{0.2,0.2,1.0}

% Math declarations
\let\Im\relax
\let\Re\relax
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Re}{Re}
\renewcommand{\d}{\mathrm d}

% Maintenance macros
\newcommand{\todo}{\colorbox{pink}{\textsc{Todo}}}

\usetheme[sectionpage=none]{metropolis}
\usecolortheme{seahorse}

\setbeamertemplate{footline}[frame number]

\setbeamercolor{progress bar}{fg=gray,bg=black}

\begin{document}

\title{Aspects of Large-$N_f$ Quantum Field Theories}
\author{\textbf{Scott Lawrence}\\
\small{\texttt{arXiv:2206.04765} with Romatschke}\\
\small{\texttt{arXiv:2303.01470} with Peterson, Romatschke, Weller}}
\institute{\Large\begin{CJK}{UTF8}{min}慶應義塾大学\end{CJK}}
\date{20 October 2023}
\bgroup
\setbeamertemplate{footline}{}
\begin{frame}[noframenumbering]
	\titlepage
	\begin{tikzpicture}[overlay, remember picture]
		\node[anchor=center] at (7.5,3.5) {\includegraphics[height=0.3in]{cuboulder}};
		\node[anchor=center] at (7.5,2.5) {\includegraphics[height=0.3in]{lanl}};
		\node[anchor=center] at (7.5,1.5) {\includegraphics[height=0.3in]{nnsa}};
		\node[anchor=east] at (11.8,9.8) {\scriptsize LA-UR-23-31920};
	\end{tikzpicture}
\end{frame}
\egroup

\begin{frame}{Some nonperturbative puzzles}
\color{blue}
	How does field theory give rise to hydrodynamics?

	What are the transport properties of \texttt{\$FIELD\_THEORY}?

\color{red}
	What happens near a Landau pole?

	Is there a UV-completion of $\phi^4$ theory in $4$ spacetime dimensions?
\end{frame}

\begin{frame}{Field theories at large $N_f$}
	\[\color{blue}
		S = \int d^d x \, \frac 1 2 \sum_i^N \left(\partial \phi_i\right)^2 
		+ \frac{\lambda}{N} \left(\sum_i^N \phi_i^2\right)^2
	\]
	These are ``nonperturbatively'' solvable systems (easily at LO, with effort at NLO).

	Here ``nonperturbative'' means all orders in $\lambda$ have been summed.

	{\color{red}However, the (``nonperturbative''!) S-matrix is nearly the identity.}

	{\color{green}Nevertheless renormalization and thermodynamics are not those of a free theory.

	Transport properties are also non-trivial.}
	\[\color{blue}
		S = \int d^d x\, \sum_f^N \bar\psi_f \partial \psi_f + \frac{g^2}{N} \left(\sum_f^N \bar\psi_f \psi_f\right)^2
	\]
\end{frame}

\begin{frame}{Outline}
	\begin{itemize}
		\item Calculations at large $N_f$
			\begin{itemize}
				\item Path integral
				\item Diagrammatics
				\item Hamiltonian
			\end{itemize}
		\item Thermodynamics and transport
			\begin{itemize}
				\item Thermodynamics of scalar field theory
				\item Coupling to gravitational radiation in the Fermi gas
				\item Summary of other results
			\end{itemize}
		\item Renormalization and negative couplings
			\begin{itemize}
				\item $\beta$-function of the $O(N)$ model
				\item Lefschetz thimble regularization of the $\lambda < 0$ regime
			\end{itemize}
		\item {\color{red}Wild speculation}
	\end{itemize}
\end{frame}

\begin{frame}{The $1/N_f$ expansion}
	\begin{columns}
		\begin{column}{0.49\textwidth}
			\[\scriptsize
				Z = \int \mathcal D \phi \mathcal D \sigma\,
				e^{-\int \phi \frac{m^2 - \partial^2}{2} \phi + \phi^4 + \frac{N}{16 \lambda}\sigma^2}
			\]
			\[\scriptsize
				= \int \mathcal D \phi \mathcal D \sigma\,
				e^{-\int \frac{N}{16 \lambda}\sigma^2 + \phi\frac{m^2 + i \sigma - \partial^2}{2}\phi}\]
				\[\scriptsize\color{blue}
				= \int \mathcal D \sigma e^{-N \int \frac{1}{16\lambda}\sigma^2 - \frac 1 2 \tr \log \frac{m^2 + i \sigma - \partial^2}{2}}
			\]
			Now: {\color{green}saddle-point expansion}

			Expand in powers of $\sigma - \sigma_0$: $\sigma^3$ and higher are perturbations.

			{\footnotesize (This can be made very explicit by rescaling $\sigma$ so that the quadratic term is $\sigma^2$ rather than $N \sigma^2$.)}
		\end{column}
		\vrule{}
		\begin{column}{0.49\textwidth}

			{\color{blue}Introduce an infinitely massive particle to mediate the interaction:}
	\[
		\begin{gathered}
	\begin{fmffile}{fmf-interaction-orig}
		\fmfframe(10,10)(10,10){
		\begin{fmfgraph*}(50,40)
			\fmfleft{i1,i2}
			\fmfright{o1,o2}
			\fmf{plain}{i1,a}
			\fmf{plain}{i2,a}
			\fmf{plain}{o1,a}
			\fmf{plain}{o2,a}
		\end{fmfgraph*}
	}
	\end{fmffile}\end{gathered}
			\Longrightarrow
		\begin{gathered}
	\begin{fmffile}{fmf-interaction-split}
		\fmfframe(10,10)(10,10){
		\begin{fmfgraph*}(50,40)
			\fmfleft{i1,i2}
			\fmfright{o1,o2}
			\fmf{plain}{i1,a}
			\fmf{plain}{i2,a}
			\fmf{dashes}{a,b}
			\fmf{plain}{o1,b}
			\fmf{plain}{o2,b}
		\end{fmfgraph*}
	}
	\end{fmffile}\end{gathered}
\]
			Every $\phi$ loop comes with a factor of $N$; every $\sigma$ propagator comes with a factor of $N^{-1}$.


		\end{column}
	\end{columns}
	\[\scriptsize
		\tr \log \frac{m^2 - \partial^2 + i\sigma}{2} \sim
		\cdots + i \tr \left(m^2-\partial^2 + i \sigma_0\right)^{-3} (\sigma-\sigma_0)^3 + \cdots
	\]
\end{frame}

\begin{frame}{Scattering at large $N_f$}
	\begin{columns}
		\begin{column}{0.49\textwidth}
	\[\color{red}
		\left[
		\begin{gathered}
	\begin{fmffile}{fmf-scattering1}
		\fmfframe(10,10)(10,10){
		\begin{fmfgraph*}(50,40)
			\fmfleft{i1,i2}
			\fmfright{o1,o2}
			\fmflabel{a}{i1}
			\fmflabel{b}{i2}
			\fmflabel{a}{o1}
			\fmflabel{b}{o2}
			\fmf{plain,f=red}{i1,a}
			\fmf{plain,f=red}{o1,a}
			\fmf{dashes,f=red}{a,b}
			\fmf{plain,f=red}{b,i2}
			\fmf{plain,f=red}{b,o2}
		\end{fmfgraph*}
	}
	\end{fmffile}\end{gathered}\right]^2
	\sim
	N^{-2}
\]
		\end{column}
			\begin{column}{0.49\textwidth}
				\[\color{blue}
				\sum_b
				\left[
				\begin{gathered}
	\begin{fmffile}{fmf-scattering2}
		\fmfframe(8,8)(8,8){
		\begin{fmfgraph*}(50,40)
			\fmfleft{i1,i2}
			\fmfright{o1,o2}
			\fmflabel{a}{i1}
			\fmflabel{a}{i2}
			\fmflabel{b}{o1}
			\fmflabel{b}{o2}
			\fmf{plain,f=blue}{i1,a}
			\fmf{plain,f=blue}{i2,a}
			\fmf{dashes,f=blue}{a,b}
			\fmf{plain,f=blue}{b,o1}
			\fmf{plain,f=blue}{b,o2}
		\end{fmfgraph*}}
	\end{fmffile}
				\end{gathered}
					\right]^2
				\sim N^{-1}
\]
			\end{column}
		\end{columns}
			\vspace{1em}

			\centerline{\color{green}Same-species cross section is $O(N^{-1})$; all others are $O(N^{-2})$.}
			Other diagrams at the same order:
			\vspace{0.5em}
			\[
				\begin{gathered}
	\begin{fmffile}{fmf-scattering3}
		\begin{fmfgraph*}(80,30)
			\fmfleft{i1,i2}
			\fmfright{o1,o2}
			\fmflabel{a}{i1}
			\fmflabel{a}{i2}
			\fmflabel{b}{o1}
			\fmflabel{b}{o2}
			\fmf{plain}{i1,a}
			\fmf{plain}{i2,a}
			\fmf{plain}{b,o1}
			\fmf{plain}{b,o2}
			\fmf{dashes}{a,c}
			\fmf{plain,left=1.0,tension=0.5}{c,d}
			\fmf{plain,right=1.0,tension=0.5}{c,d}
			\fmf{dashes}{d,b}
		\end{fmfgraph*}
	\end{fmffile}
				\end{gathered}
				\hspace{2em}+\hspace{2em}
			\begin{gathered}
	\begin{fmffile}{fmf-scattering4}
		\begin{fmfgraph*}(90,30)
			\fmfleft{i1,i2}
			\fmfright{o1,o2}
			\fmflabel{a}{i1}
			\fmflabel{a}{i2}
			\fmflabel{b}{o1}
			\fmflabel{b}{o2}
			\fmf{plain}{i1,a}
			\fmf{plain}{i2,a}
			\fmf{plain}{b,o1}
			\fmf{plain}{b,o2}
			\fmf{dashes}{a,c}
			\fmf{plain,left=1.0,tension=0.5}{c,d}
			\fmf{plain,right=1.0,tension=0.5}{c,d}
			\fmf{dashes}{d,e}
			\fmf{plain,left=1.0,tension=0.5}{e,f}
			\fmf{plain,right=1.0,tension=0.5}{e,f}
			\fmf{dashes}{f,b}
		\end{fmfgraph*}
	\end{fmffile}
			\end{gathered} \hspace{2em}+ \cdots
\]
{\small (With appropriately resummed propagator.)}
\end{frame}

\begin{frame}{Hamiltonian formalism}
	The large-$N_f$ limit is often called {\color{blue}``mean-field''}:
	\[
		H = \frac 1 2 \sum_i \left(p_i^2 + \frac{m^2}{2} x_i^2\right)
		+ {\color{blue}\frac\lambda N \left(\sum_i x_i^2\right)^2}
	\]
	Consider the effective Hamiltonian for a single degree of freedom $\tilde x$. In the large-$N$ limit, $\frac 1 N \sum x^2 = \langle x^2\rangle$, with no fluctuations.
	\[
		H_1 = \frac 1 2 \tilde p^2 + \frac{m^2}{2} \tilde x^2 + \color{blue}2 \lambda \langle x^2\rangle \tilde x^2
	\]
	It's a harmonic oscillator with $\tilde m^2 = m^2 + 4 \lambda \langle \tilde x^2\rangle$.

	Requiring consistency, solve for $\langle \tilde x^2 \rangle$ and learn $\tilde m$:
	\[
		\langle \tilde x^2 \rangle
		= \frac 1 2 \sqrt{m^2 + 4 \lambda \langle \tilde x^2\rangle}
	\]
\end{frame}

\begin{frame}{Thermodynamics at large $N_f$ (QM)}
The same idea holds, only now the expectation value $\langle \tilde x^2 \rangle$ is:
\[
\langle \tilde x^2 \rangle = 
\frac 1 {m^2}\langle H \rangle
=
\frac 1 {2m}
+ \frac 1 m \frac{1}{e^{\beta m} - 1}
\]
\color{blue} Gap equation:
\[
\tilde m^2 = m^2 + 4 \lambda \left(
\frac 1 {2\tilde m}
+ \frac 1 {\tilde m} \frac{1}{e^{\beta \tilde m} - 1}
\right)
\]
\begin{columns}
\begin{column}{0.49\textwidth}
\color{red}
\[T \rightarrow 0\]
\[
\tilde m = \left(2 \lambda \right)^{1/3}
\]
\end{column}
\begin{column}{0.49\textwidth}
\color{green}
\[T \rightarrow \infty\]
\[
\tilde m = \left(4 T \lambda \right)^{1/4}
\]
\end{column}
\end{columns}
\color{black}
We can calculate the high-temperature equation of state as well:
\[
\frac 1 N E(T) = \frac {3 T }{4} = \frac 3 4 E_1(T)
\]
\centerline{\color{red}A free gas with $\frac {3 N}{4}$ effective degrees of freedom.}
\end{frame}

\begin{frame}{Thermodynamics at large $N_f$ (QFT)\footnote{Results from \texttt{1904.09995}}}
~{\color{blue}In $2+1$ dimensions} the saddle-point condition (`gap equation') is:
\[
i \sigma^* = -4 \lambda \left[
\frac{i \sigma^*}{4\pi}
+ \frac {T}{2\pi} \log \left(1 - e^{- \frac{i z^*}{T}}\right)
\right]
\]

\begin{columns}
\begin{column}{0.49\textwidth}
	\centerline{\includegraphics[width=2in]{eta-s}}
\end{column}
\begin{column}{0.49\textwidth}
In the limit of strong coupling (and tuned $m \rightarrow 0$), we get
\color{green}
\[
s(T) = \frac 4 5 s_{\mathrm{free}}(T)
\]
\end{column}
\end{columns}

\centerline{A free theory of $\frac {4N}{5}$ effective species.}
\end{frame}

\begin{frame}{Hydrodynamics}
Finite-temperature quantum matter ({\color{red}hard to simulate!}), when ``zoomed out'', is described by \emph{classical} hydrodynamics ({\color{green}``easy''}).

Navier-Stokes:
\[
\rho \frac{\d u_i}{\d t} + \partial_i p = {\color{blue}\eta} \left(\frac 1 3 \partial_i \partial_j u_j + \partial_j^2 u_i \right) + {\color{blue}\zeta}\cdots
\]
More systematic (and relativistic): $\partial_\mu T^{\mu\nu} = 0$, expand $T^{\mu\nu}$ in $\nabla$:
\[
T^{\mu\nu} = T^{\mu\nu}_{(0)} - \underbrace{2 {\color{blue}\eta} \nabla^{\langle \mu} u^{\nu \rangle} - {\color{blue}\zeta}\Delta^{\mu\nu}\nabla^\perp_\lambda u^\lambda}_{T^{\mu\nu}_{(1)}}
+ 
\underbrace{{\color{blue}\kappa}\left[R^{\langle i j \rangle} - 2 R^{t \langle i j \rangle t}\right]
 + \cdots}_{T^{\mu\nu}_{(2)}} + \cdots
\]
\centerline{\textbf{Gradient expansion}: long distances, long times}
\centerline{\textbf{Transport coefficients}: LECs through which quantum effects can appear}
\end{frame}

\begin{frame}{Low-order transport}
\color{green}
\textbf{Sound waves}
\[
\int dx\, \sin kx\, \langle T^{00}(x,t) T^{00}(0,0) \rangle
\sim \exp\left[i c_s k t - \left(\frac{\zeta + \frac{2 (d-1)}{d}\eta}{\epsilon + P}\right)k^2 t\right]
\]
\color{blue}
\textbf{Shear waves}
\[
\int dx\, \sin kx\, \langle T^{01}(x,t) T^{01}(0,0) \rangle
\sim e^{-\frac{\eta}{\epsilon + P} k^2 t}
\]
\color{red}
\textbf{``Shear channel''}
\[
\langle T^{12}(\omega,k) T^{12}(\omega,k) \rangle
= P - i \eta\omega + O(\omega^2) + O(k)
\]
% Show exponential decays
\end{frame}

\begin{frame}{What about the lattice?}
	\centerline{\textbf{\color{red}Out-of-equilibrium physics is largely inaccessible.}}
	A typical observable is $\langle \mathcal O(t) \mathcal O(0)\rangle$. To put this on the lattice:
	\[
		\langle \mathcal O(t) \mathcal O(0)\rangle
		= \frac 1 Z \Tr e^{-\beta H} e^{i H t} \mathcal O e^{-i H t} \mathcal O
		= \frac 1 Z \int \mathcal D \phi\; e^{-S_E + \color{red}i S_+ - i S_-}
	\]
	The {\color{red}real-time} portions of the {\color{blue} Schwinger-Keldysh contour} introduce a sign problem.

	There are various attempts to extract transport by fitting a spectral function to lattice data:
	\centerline{\includegraphics[width=3in]{meyer}}

	\centerline{\textbf{\color{red}Finite fermion density (heavy-ion collision) is out of the question.}}
\end{frame}

\begin{frame}{Thermodynamic ``transport''}
Not all LECs in the hydrodynamic expansion are specific to out-of-equilibrium physics.
\begin{itemize}
\item Pressure
\item Gravitational wave-to-matter coupling ($\kappa$)
\end{itemize}
These appear when the spacetime metric undergoes a time-independent perturbation.

Equivalently, these are detectable from fluctuations in thermodynamic equilibrium.
\end{frame}

\begin{frame}{Gravitational wave-to-matter coupling of unitary Fermi gas\footnote{\texttt{arXiv:2206.04765}; SL, Romatschke}}
\footnotesize
\[
T^{ij} \supset \kappa \left[R^{\langle i j \rangle} - 2 R^{t \langle i j \rangle t}\right]
\]
{\color{blue}Thermodynamic transport can be seen from \emph{Euclidean} correlators:}
\[
\kappa = \left.\frac{\partial^2}{\partial k^2} \langle T^{12}T^{12}\rangle(\omega = 0, k)\right|_{k=0}
\]
{\color{blue}For nonrelativistic fermions, the stress-energy tensor is:}
\tiny
\[
T^{12}
=\frac{1}{4m}\left[\partial_1 \Psi^\dagger \sigma_z \partial_2 \Psi + \partial_2 \Psi^\dagger \sigma_z \partial_1 \Psi
- \partial_1 \partial_2 \Psi^\dagger \sigma_z \Psi- \Psi^\dagger \sigma_z \partial_1 \partial_2 \Psi
  \right]-\frac{i s}{4m}\partial_k \Sigma_k
\]
\[
\text{where }\Sigma_3=\Psi \sigma_x (\partial_1-i\partial_2) \Psi+\Psi^\dagger \sigma_x (\partial_1+i \partial_2) \Psi^\dagger
\]
\footnotesize
{\color{blue}Evaluated at the saddle point:}
\tiny
\[\scriptscriptstyle
C(k) =
-\frac{2}{m^2} \int\! \frac{d^4 p}{(2\pi)^4} \frac{{\bf p}_1^2 {\bf p}_2^2 \left[(\epsilon_{\bf p}-\mu)(\epsilon_{\bf k+p}-\mu)-\omega^2-\Delta^2\right]}{\left[(\epsilon_{\bf p}-\mu)^2+\omega^2+\Delta^2\right]\left[(\epsilon_{\bf p+k}-\mu)^2+\omega^2+\Delta^2\right]}+
\frac{s^2 {\bf k}^2}{2 m^2}\int\!\frac{d^4 p}{(2\pi)^4}  {\bf p}_1^2{\rm tr} \left[\sigma_x G(\omega,{\bf p})\sigma_x  G(-\omega,-{\bf p})\right]
\]
\small
{\color{blue}Integrating:}
\[
\lim_{a_s \rightarrow -\infty} \kappa = \frac{(2 m \mu)^{\frac{3}{2}}}{3\pi^2 m} \frac{1}{\xi^{\frac{3}{2}}}\frac N {12} = \frac{n}{12m}
\]
\end{frame}

\begin{frame}{Other transport results}
\centerline{\color{blue}For the $O(N)$ (bosonic) model}
\centerline{
\includegraphics[width=2in]{eta-s}
}
From \texttt{2104.06435:} $\frac{\eta}{s}$ has a minimum at $N \times 0.42(1)$.

The rest of the transport coefficients are computed in \texttt{2208.10502}.
\centerline{\color{red}$SU(N)$ fermionic results on the way}
\end{frame}

\begin{frame}{Renormalization at large $N_f$ (in $3+1$ dimensions)}
\vspace{-10pt}
\[
S = \int d^4 x \, \frac 1 2 \left(\partial \phi\right)^2 + \frac{m^2}{2} \phi^2 + \frac\lambda N \left(\phi^2\right)^2
\]
	\[
		\begin{gathered}
	\begin{fmffile}{fmf-divergence-mass}
		\fmfframe(10,10)(10,10){
		\begin{fmfgraph*}(50,40)
			\fmfleft{i}
			\fmfright{o}
			\fmftop{c}
			\fmf{plain}{i,a}
			\fmf{dashes,tension=0.2}{a,b}
			\fmf{plain,tension=0.1,right}{b,c,b}
			\fmf{plain}{a,o}
		\end{fmfgraph*}
	}
	\end{fmffile}\end{gathered}
+
\begin{gathered}
	\begin{fmffile}{fmf-divergence-mass2}
		\fmfframe(10,10)(10,10){
		\begin{fmfgraph*}(50,40)
			\fmfleft{i}
			\fmfright{o}
			\fmf{plain}{i,a}
			\fmf{plain,tension=0.3}{a,b}
			\fmf{dashes,left=1.0,tension=0.3}{a,b}
			\fmf{plain}{b,o}
		\end{fmfgraph*}
	}
	\end{fmffile}\end{gathered}
\hspace{0.2in}
		\begin{gathered}
	\begin{fmffile}{fmf-divergence-coupling}
		\fmfframe(10,10)(10,10){
		\begin{fmfgraph*}(50,40)
			\fmfleft{i1,i2}
			\fmfright{o1,o2}
			\fmf{plain}{i1,a}
			\fmf{plain}{i2,a}
			\fmf{dashes}{a,b}
			\fmf{plain,left=1,tension=0.3}{b,c}
			\fmf{plain,right=1,tension=0.3}{b,c}
			\fmf{dashes}{c,d}
			\fmf{plain}{o1,d}
			\fmf{plain}{o2,d}
		\end{fmfgraph*}
	}
	\end{fmffile}\end{gathered}
+
		\begin{gathered}
	\begin{fmffile}{fmf-divergence-coupling2}
		\fmfframe(10,10)(10,10){
		\begin{fmfgraph*}(50,40)
			\fmfleft{i1,i2}
			\fmfright{o1,o2}
			\fmf{plain}{i1,a}
			\fmf{plain}{i2,d}
			\fmf{dashes}{a,b}
			\fmf{plain,left=1,tension=0.3}{b,c}
			\fmf{plain,right=1,tension=0.3}{b,c}
			\fmf{dashes}{c,d}
			\fmf{plain}{o1,a}
			\fmf{plain}{o2,d}
		\end{fmfgraph*}
	}
	\end{fmffile}\end{gathered}
\]
\begin{columns}
\begin{column}{0.49\textwidth}
At leading order in $N^{-1}$ (but to all orders in $\lambda$):
\[
\lambda_R(\mu) = \frac{1}{\frac {1}{\lambda_0} + \frac{1}{(2\pi)^2} \log \frac{\Lambda^2}{\mu^2}}
\]
\color{green}
So we have correlation functions defined at all energy scales\footnote{From Moshe, Zinn-Justin (2003).}.
\end{column}
\begin{column}{0.49\textwidth}
\centerline{
\includegraphics[width=2.5in]{renorm}
}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{A tale of two limits}
	Compare two facts concerning $3+1$-dimensional $\phi^4$ theory:
	\begin{enumerate}
		\item Scalar triviality: there is no such UV-complete interacting theory. (Theorem\footnote{See Aizenman, Duminil-Copin for $N=1$ case.} for $N=1$; widely believed for $Nx\ge >2$).
		\item The large-N limit is well defined up to all energies.
	\end{enumerate}
	\hrule{}
	{\color{green}The ordering of limits changed!}
	
	Schematically, $\lim_{N\rightarrow\infty} \lim_{\Lambda\rightarrow\infty} \ne \lim_{\Lambda\rightarrow\infty} \lim_{N\rightarrow\infty}$.

	The large-$N$ theory can physically relevant only if (crudely) $N \gg \sqrt{\frac{\hbar c^5}{G}} \mathrm{GeV}^{-1} \approx 10^{19}$. \color{red}Unlikely.
\end{frame}

\begin{frame}{Quantum mechanics at negative coupling}
	\[
		H = \frac 1 2 p^2 - g x^4 \text{\hspace{1em} with }g \in \mathbb R_+
	\]
This Hamiltonian is unbounded below. Still we can make sense of it in a few different ways.

\vspace{1em}
\begin{columns}
\begin{column}{0.49\textwidth}
\centerline{\color{red}\textbf{Analytic continuation}}
$Z(\lambda,T)$ depends only on $\lambda T^{1/3}$:
\[
Z_{\mathrm{a.c.}}(-g,T)
=
\sum_k e^{-E_i e^{i \pi / 3}/ T}
\]

This results in a ``non-unitary'' theory. $Z$ is complex; its real part is negative.
\end{column}
\begin{column}{0.49\textwidth}
\centerline{\color{blue}\textbf{$PT$ symmetry}}
Quantize not on $x \in \mathbb R$, but on a different contour, respecting the symmetry:
\[
x \xrightarrow{\mathrm{PT}} -x \text{ and }
i \xrightarrow{\mathrm{PT}} -i
\]
\centerline{\color{green}This yields unitary time evolution!}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{}
\includegraphics[width=4in]{contours}
\end{frame}

\begin{frame}{Lefschetz thimbles\footnote{For introduction, see Witten, ``Analytic continuation of Chern-Simons''}}
Each thimble extends from a saddle point $z_*$ (where $\partial S = 0$), and consists of the union of solutions to $\frac{dz}{dt} = \frac{\partial}{\partial z}\Re S$, obeying
\[
\lim_{t\rightarrow -\infty} z(t) = z_*
\text.
\]
\begin{columns}
\begin{column}{0.4\textwidth}
\includegraphics[width=2in]{thimbles}
\end{column}
\begin{column}{0.59\textwidth}
Two major theorems:
\begin{enumerate}
\item Each thimble is a valid integration contour ($\int e^{-S}$ converges).
\item The thimbles form a basis for the space of all valid integration contours.
\end{enumerate}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Thimble regularization of the unstable theory}
        \centerline{\color{green}\textbf{Any combination of thimbles obeys the Schwinger-Dyson equations.}}
\[
\langle f \partial S \rangle = \langle \partial S \rangle
\]
We only need to find a combination of thimbles that preserves unitarity.

{\color{red}Analytic continuation} consists of ``following'' the original thimble decomposition as the coupling is rotated through the complex plane. \textbf{\color{red}Not unitary!}

Performing the path integral on a different integration contour does yield a unitary theory in QM.
\end{frame}

\begin{frame}{A bounce/instanton\footnote{See \texttt{2303.01470} (SL, Romatschke, Peterson, Weller) for lengthy details.}}
In QM, we can numerically diagonalize the PT-symmetric theory, and compute the analytically continued Hermitian theory.

\begin{columns}
\begin{column}{0.44\textwidth}
Precise agreement at high temperatures (equivalent to small $\lambda$ or small $\hbar$). The difference grows:
\[
Z_{PT}(T) - Z_{H}(T) \sim e^{-1 / \hbar}
\]
\end{column}
\begin{column}{0.55\textwidth}
\centerline{
\includegraphics[width=2.5in]{fig2l}
}
\end{column}
\end{columns}

This is a ``bounce'' (closely related to instantons). The origin is clear from the semiclasical expansion:
\[
Z \sim \underbrace{e^{-S_0/\hbar}}_{\text{one thimble}} + \underbrace{e^{-S_1/\hbar}}_{\text{another thimble}} + \cdots
\]

\end{frame}

\begin{frame}{Field theory at negative coupling?\footnote{See Weller: \texttt{2310.02516}}}
All that remains is to select a contour.

Define
\[f(\chi) = \theta(\chi) e^{i \pi / 4}+ \theta(-\chi) e^{-i \pi / 4}\]
and split the parameterizing field up into Fourier modes, with $\chi_0$ being the mean field and $\chi'$ representing all others.
\begin{align*}
\phi_0 &= f(\chi_0) \chi_0\\
\phi' &= f(\chi_0) \chi'
\end{align*}
\hrule{}
Note that we have abandoned any Hamiltonian picture. No Hilbert space has been defined.
\end{frame}

\begin{frame}{Scalar triviality}
	\centerline{\color{red}\textit{``There is no continuum theory of interacting scalar fields in $d=4$.''}}
	\hrule{}
	A more precise statement\footnote{For $d>4$, see Aizenmann '82 and Fr\"olich '81 for more robust triviality proofs.} is: correlation lengths in certain\footnote{Simon-Griffiths class, see SG '73. These field theories can be reduced to Ising models} lattice field theories cannot be made arbitrarily long without making large-range correlations Gaussian.

	This has been proven by Aizenman and Duminil-Copin for polynomial scalar interactions at $N=1$ and $d=4$. Likely extends to $N > 1$.

	{\color{green}Not covered: things that are not probability distributions over field configurations.}
\end{frame}

\begin{frame}{Related reading}
	On transport:
	\begin{itemize}
		\item Scalar: \texttt{arXiv:2104.06435},\texttt{arXiv:2208.10502}
		\item Fermionic calculations coming soon?
	\end{itemize}

	On PT symmetry:
	\begin{itemize}
		\item Quantum mechanics: \texttt{arXiv:physics/9712001}
		\item A useful duality: \texttt{arXiv:quant-ph/0601188}
		\item Field theory: \texttt{arXiv:2103.14864}, \texttt{arXiv:2103.14864}
	\end{itemize}

	On negative coupling:
	\begin{itemize}
		\item Romatschke: \texttt{arXiv:2305.05678}, \texttt{arXiv:2310.03815}
		\item Weller: \texttt{arXiv:2310.02516}
	\end{itemize}
\end{frame}

\end{document}
