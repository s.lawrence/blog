<!DOCTYPE html>
<html>
  <head>
    <title>
      Convexity and Quantum Physics
    </title>
    <link rel="stylesheet" href="present.css" />
    <link rel="stylesheet" media="print" href="print.css" />
    <script src='present.js'></script>
    <script src='vis.js'></script>
    <script async src='/mathjax/tex-svg-full.js'></script>
    <!--<script src="plotly-2.9.0.min.js"></script>-->
  </head>
  <body>
    <noscript>
      <div style="font-size: 30pt;">Javascript really is required, sorry</div>
    </noscript>

    <!-- TITLE -->
    <header id="title">
      <h1>Convexity and Quantum Physics</h1>
      <div>
        <img src="title.svg" style="height: 40vh; text-align: center;"/>
        <img src="cern.jpeg" style="width: 36vh"/>
      </div>
      <div class="author">Scott Lawrence</div>
      <div>May 9, 2022</div>
      <div>
        <img src="boulder.png" style="width: 35vh; padding: 0 1em" />
        <img src="FNAL.png" style="width: 35vh; padding: 0 1em" />
      </div>
      <div></div>
    </header>

    <section id="intro-qm">
      <header><h1>Quantum Mechanics</h1></header>
      <div class="flex-horiz">
        <img src="spin.svg" />

        <img src="cern.jpeg" style="width: 30vh"/>
      </div>
      <div class="flex-horiz">
        <img src="cryostat.jpg" style="width: 38vh; padding: 2vh"/>
        <img src="neutron-star.jpg" style="width: 38vh; padding: 2vh"/>
      </div>
      <footer></footer>
    </section>

    <section id="axioms">
      <header><h1>Ingredients of Quantum Mechanics</h1></header>
      <ul>
        <li>Vector space of states (\(|\psi\rangle\))</li>
        <li><span style="color:blue"><b>Positive-semidefinite</b> inner product: \(\langle \psi | \psi \rangle \ge 0\)</span></li>
        <li>Hermitian observables</li>
        <li>Hamiltonian (Hermitian generator of time evolution)</li>
      \[
      U(t) = e^{-i H t}
      \]
        <li><span style="color:green"><b>Ground state</b> \(|\Omega\rangle\) minimizing \(\langle \Omega | H| \Omega\rangle\)</span></li>
      </ul>
      <hr>
      <div class="flex-horiz">
        <span><b>Questions we can ask</b></span>
      </div>
      <ul>
        <li><b>Ground state properties</b></li>
        <li>Thermal properties</li>
        <li>Time-dependent behavior (near or far from equilibrium)</li>
      </ul>
      <footer></footer>
    </section>

    <section id="harmonic-oscillator">
      <header><h1>Example: Harmonic Oscillator</h1></header>
      \[
      \hat H = \frac{1}{2} \hat x^2 + \frac{1}{2} \hat p^2
      \]
      <p>Remember: \({\color{green}\langle \Psi|\color{blue}|\Psi\rangle}\ge 0\)
      <p>Of course \({\color{green}\langle 0 | x \color{blue}x | 0 \rangle} \ge 0\) and \({\color{green}\langle 0 | p \color{blue}p | 0 \rangle} \ge 0\). So: \(\langle \hat H \rangle \ge 0\).
      <hr>
      <p><b>We can do better</b>, by using the uncertainty principle: \([x,p] = i\hbar\).
      <div class="flex-horiz">
        <div style="border-right: 1px solid black; padding: 5vh">
          <p>Take \(|\Psi\rangle = (\hat x+i\hat p)|0\rangle\).
          \[
          \langle  (\hat x-i\hat p) (\hat x+i\hat p) \rangle
          =
          \langle \hat x^2 \rangle
          +i\langle[\hat x,\hat p]\rangle
          +\langle \hat p^2 \rangle
          \ge 0
          \]
        </div>
        <div style="padding: 5vh; color: green">
          <p>This is the classic proof that \(\langle \hat H \rangle \ge \frac \hbar 2\) for the harmonic oscillator.
        </div>
      </div>
      <footer></footer>
    </section>

    <section id="one-spin">
      <header><h1>Example: A Spin</h1></header>
      <div class="flex-horiz">
      \[
      H = - \mu \sigma_x
      \]
        <img src="spin.svg" />
      </div>
      <p>Define \(|\Psi\rangle = (1 + \sigma_x)|0\rangle\)
      \[
      \langle \Psi | \Psi\rangle \ge 0
      \Longrightarrow
      \langle 2 + 2\sigma_x \rangle \ge 0
      \Longrightarrow
      \langle \sigma_x \rangle \ge -1
      \]
      <p>So \(\langle H\rangle \ge -\mu\) (as expected!)
      <div style="font-weight: bold; color:red; text-align: center">
        <p>Both bounds happen to be <em>tight</em>. This won't always happen.</p>
      </div>
      <footer></footer>
    </section>

    <section id="positive-semidefinite">
      <header><h1>Being More Systematic</h1></header>
      <p style="text-align:center; color:blue">Let's move from states to operators!</p>
      \[\color{green}
      \langle \mathcal O^\dagger \mathcal O \rangle \ge 0
      \]
      <p>Consider a basis of operators \(\mathcal O_1,\ldots\mathcal O_N\). We can build a matrix
      \[
      M = \left(\begin{matrix}
      \langle \mathcal O_1^\dagger \mathcal O_1 \rangle & \langle \mathcal O_1^\dagger \mathcal O_2\rangle & \cdots \\
      \langle \mathcal O_2^\dagger \mathcal O_1 \rangle & \langle \mathcal O_2^\dagger \mathcal O_2\rangle & \cdots \\
      \vdots & \vdots & \ddots
      \end{matrix}\right)
      \]
      <div style="text-align:center; color:green; font-weight: bold"><p>The matrix \(M\) is positive semi-definite!</p></div>
      <p><em>Proof</em>: for any vector \(v\), consider the operator \(\mathcal O = \sum_i v_i \mathcal O_i\).
      \[
      v^\dagger M v = \langle (\vec v \cdot \mathcal {\vec O})^\dagger (\vec v \cdot \mathcal {\vec O}) \rangle = \langle \mathcal O^\dagger \mathcal O \rangle \ge 0
      \]
      <div style="text-align:center; color:blue"><p>With a complete set of operators, this sums up the <b>entire</b> positivity axiom.</p></div>
      <footer></footer>
    </section>

    <section id="semidefinite-programs">
      <header><h1>Semidefinite Programs</h1></header>
      <div style="text-align: center">This is a specific case of a <b>semi-definite program</b>.</div>
      <p>Let \(C\) be an \(N \times N\) matrix. We wish to find a positive semi-definite matrix \(X\)
      \[
      \text{minimizing }\;\mathrm{Tr}\;CX
      \]
      subject to linear constraints on the matrix elements of \(X\).
      <hr>
      <p>In our context, \(C\) specifies the Hamiltonian, and the linear constraints follow from the commutation relations.

      <p style="text-align:center"><em>Example</em>: \(\langle xp\rangle - \langle px\rangle = i\hbar\)
      <footer></footer>
    </section>

    <section id="one-spin-again">
      <header><h1>One Spin, Again</h1></header>
      <p>Here's a basis of operators: \(\{1, \sigma_x, \sigma_y, \sigma_z\}\). (In fact this basis is <b>complete</b>.)</p>
      \[\color{green}
      \left(\begin{matrix}
      1 & \langle \sigma_x \rangle  & \langle \sigma_y \rangle & \langle \sigma_z \rangle\\
      \langle \sigma_x \rangle & 1 & i \langle \sigma_z \rangle & -i \langle \sigma_y \rangle\\
      \langle \sigma_y \rangle & -i \langle \sigma_z \rangle & 1 & i\langle \sigma_x \rangle\\
      \langle \sigma_z \rangle & i\langle \sigma_y \rangle & -i \langle \sigma_x \rangle & 1
      \end{matrix}\right)\succeq 0
      \]
      <p>Note that <em>any</em> assignment of expectation values obeying the above constraint corresponds to some density matrix \(\rho\).
      <p>For \(H = -\mu \sigma_x\), only the upper-left \(2\times 2\) minor is needed.
      \[
      \left|\begin{matrix}
      1 & \langle \sigma_x \rangle\\
      \langle \sigma_x \rangle & 1
      \end{matrix}\right|\ge 0
      \Longrightarrow
      1 - \langle \sigma_x \rangle^2 \ge 0
      \]
      <footer></footer>
    </section>

    <section id="visualizing">
      <header><h1>Visualizing The One-Spin SDP</h1></header>
      <div class="flex-horiz">
        \[
        \left(\begin{matrix}
        1 & \langle \sigma_x \rangle\\
        \langle \sigma_x \rangle & 1
        \end{matrix}\right)\succeq 0
        \]
        <img src="vis.png" style="width: 80vh" />
      <!--
      <div id="spin-vis" style="width: 40vh; height: 20vh"></div>
      -->
      </div>
      <script>
      </script>
      <footer></footer>
    </section>

    <section id="ho-again">
      <header><h1>Harmonic Oscillator, Again</h1></header>
      <p>We can't use a complete basis. Let's start with \(\{1,x,p\}\).
      \[\color{green}
      \left(\begin{matrix}
      1 & \langle x\rangle & \langle p \rangle\\
      \langle x \rangle & \langle x^2 \rangle & \langle xp \rangle\\
      \langle p \rangle & \langle xp \rangle + i\hbar & \langle p^2 \rangle
      \end{matrix}\right)\succeq 0
      \]
      <p>This is just one small part of the "full" SDP, but it represents several important constraints!
      <p>Using the lower \(2\times 2\) minor:
      \[
      \left|\begin{matrix}
      \langle x^2 \rangle & \langle xp \rangle\\
      \langle xp \rangle + i\hbar & \langle p^2 \rangle
      \end{matrix}\right|
      =
      \langle x^2 \rangle \langle p^2\rangle - \langle xp\rangle^2 - i \hbar \langle xp \rangle
      \ge 0
      \Longrightarrow
      \langle x^2\rangle \langle p^2\rangle \ge \hbar
      \]
      <p style="color: blue; font-weight: bold; text-align: center">Adding more operators can only make the bound stricter.
      <footer></footer>
    </section>    <section id="sum-of-squares">
      <header><h1>Noncommutative Sum-of-Squares</h1></header>
      <div class="flex-horiz">
        <div>
        \[ H = -\mu \sigma_x = \color{green}\frac 1 2 (1+\sigma_x)(1+\sigma_x) - 1\]
        <p>From which we see that \(\color{blue}\langle H \rangle \ge -1\)
        </div>
        <div style="height: 25vh; border: 1px solid black"></div>
        <div>
        \[ H = \frac 1 2 x^2 + \frac 1 2 p^2 = \color{green}a^\dagger a + \frac 1 2\]
        <p>Similarly, \(\color{blue}\langle H \rangle \ge \frac 1 2\)
        </div>
      </div>
      <hr>
      <p>In general, if we can write
      \[
      H = C + \mathcal O_1^\dagger \mathcal O_1 + \mathcal O_2^\dagger \mathcal O_2 + \cdots
      \]
      <p>then the bound \(\color{blue}\langle H \rangle \ge C\) follows immediately.
      <div class="flex-horiz">
        <span style="font-weight: bold; color: #885500">
          This is equivalent to the semi-definite program formulation.
        </span>
      </div>
      <footer></footer>
    </section>

    <section id="recap">
      <header><h1>Summary</h1></header>
      <ul>
        <li>The space of permitted expectation values is <em>convex</em></li>
        <li>The ground state sits at the boundary (the lowest possible value of \(\langle H \rangle\))</li>
        <li>Any sum-of-squares representation yields a rigorous lower bound</li>
        <li>Equivalently, solving a truncated SDP yields a rigorous lower bound</li>
      </ul>
      <hr>
      <div class="flex-horiz"><span style="font-weight: bold">Coming up:</span></div>
      <p style="color: blue">Quantum information
      <p style="color: green">Conformal bootstrap: a history lesson
      <p style="color: red">Computational applications (matrix models, lattice field theories)
      <footer></footer>
    </section>

    <section id="TODO">
      <header><h1>Intermission: Quantum Information</h1></header>
      <p style="color:blue"><b>Setup:</b> Alice samples a random number \(i \in 1\ldots N\), with probability \(p_i\).  Alice sends a quantum state \(\rho_i\) to Bob. Bob must make a measurement and guess \(i\).</p>
      <p style="font-weight: bold; text-align: center">How well can Bob do?</p>
      <hr>
      <p>Bob will measure onto some set of projectors \(\{E_j\}\), and reply \(j\) with probability
      \[
      P_j(|\psi\rangle) = \mathrm{Tr}\; E_j \rho_i
      \]
      We want to maximize the success probability, that is
      \[
      \sum_i p_i \mathrm{Tr}\; \rho_i E_i
      \]
      subject to
      \[
      E_i \succeq 0 \text{ and } \sum_i E_i = 1
      \]
      <p style="font-weight:bold; color:blue; text-align: center">Surprise! It's an SDP!</p>
      <footer>For more examples see: Siddhu and Tayur, <tt>arXiv:2112.08276</tt></footer>
    </section>

    <section id="npa-hierarchy">
      <header><h1>Intermission: The NPA Hierarchy</h1></header>
      <p>Alice and Bob each have access to one half of a quantum system. Alice can measure operators \(E_\alpha\), and Bob \(E_\beta\). A <em>quantum correlation</em> is:
      \[
      P_{\alpha\beta} = \mathrm{Tr}\;E_\alpha E_\beta \rho
      \]
      <p style="text-align: center; color: blue">Which probabilities \(P_{\alpha\beta}\) are achievable quantum correlations?
      <p style="font-size:90%"><b>Answer:</b> \(\Gamma_{ij} = \mathrm{Tr}\; S_i^\dagger S_j \rho \succeq 0\). So, we require that such a \(\Gamma\) exists, compatible with the given probabilities (a linear condition).
      \[\color{green}
      \mathcal S_1 = \{E_\alpha\} \cup \{E_\beta\}
      \;\text{ ; }\;
      \mathcal S_2 = \{E_\mu E_\nu\}
      \;\text{ ; }\;
      \cdots
      \]
      <hr>
      <div class="flex-horiz">
        <div style="padding: 0 2vh">
      <p><b>Tsirelson's problem:</b> can every quantum correlation be approximated by a system of finite Hilbert space dimension?
      <p style="text-align: center; color: red; font-weight: bold; font-size:120%">No (!)
        </div>
        <img src="mipre.png" style="width: 40vh"/>
      </div>
      <footer>See Navascues, Pironio, Acin (<tt>arXiv:0803.4290</tt>); Ji et al. (<tt>arXiv:2001.04383</tt>)</footer>
    </section>

    <section id="efficient">
      <header><h1>Why Does Convexity Matter?</h1></header>
      <div class="flex-horiz">
        <div>
          <p style="color:red; text-align: center">Finding a global minimum is <em>hard</em>, in general.</p>
          <div style="text-align: center"><img src="minimizing.svg" width="70%" /></div>
        </div>
        <div>
          <p style="color: green" style="text-align: center">For convex functions, it's much easier</p>
          <div style="text-align: center"><img src="convex.svg" width="70%" /></div>
        </div>
      </div>
      <hr>
      <p>Current tools for solving SDPs: SDP-A, <b>MOSEK</b>, SDPB
      <p style="text-align:center; color:red">There is certainly room for improvement in this space.</p>
      <footer></footer>
    </section>

    <section id="history">
      <header><h1>A Bit of History</h1></header>
      <div class="flex-horiz" style="padding-right:1em; padding-left: 1em">
        <div>
          <p>The original dream: construct an S-matrix purely from symmetries and consistency conditions.
          <p>Circumvents the need for a sensible definition of field theory.
          <p>Fell out of favor as QCD became understood.
        </div>
        <img src="chew.jpg" width="25%"/>
      </div>
      <p style="text-align: center; color: green">\(\sim 40\) years pass...</p>
      <p><b>Conformal bootstrap:</b> modern numerical techniques allow the bootstrap program to strongly constrain the space of conformal field theories, purely from symmetries and consistency.
      <p style="color:red">Now "bootstrap" and "semi-definite programming" are somewhat synonymous.
      <footer></footer>
    </section>

    <section id="cfts">
      <header><h1>Conformal Field Theories</h1></header>
      <p>A conformal field theory is:
      <ul>
        <li>Lorentz invariant</li>
        <li>Unitary</li>
        <li>Scale invariant (conjecturally implies conformal invariance)</li>
        <li style="color: red">Characterized by <em>critical exponents</em>
      </ul>
      <hr>
      <p style="text-align: center; color: blue; font-weight: bold">Why do we care?</p>
      <p>At asymptotically high or low energies, any field theory approaches a conformal fixed point.
      <p>Also, lamppost effect: we study these because they're (relatively) easy.
      <footer></footer>
    </section>

    <section id="conformal-bootstrap">
      <header><h1>Conformal Bootstrap</h1></header>
      <div class="flex-horiz">
      <ul>
        <li>Conformal invariance</li>
        <li>Crossing symmetry</li>
        <li><b>Unitarity</b> \(\Rightarrow\) Radially quantized states have positive norm</li>
        <li>Internal symmetries (\(\mathbb Z_2\))</li>
      </ul>
      <img src="crossing.png" style="width: 70vh"/>
      </div>
      <div style="text-align: center"><img src="conformal-bootstrap.png" style="width:75vh"/></div>
      <footer>See Kos et al: <tt>arXiv:1603.04436</tt></footer>
    </section>

    <section id="finding-eigenvalues">
      <header><h1>Finding Eigenvalues</h1></header>
      <p>To study individual eigenstates, we can impose:
      \[
      \langle H \mathcal O \rangle = E \langle \mathcal O \rangle
      \]
      <p>For the anharmonic oscillator, this yields constraints like:
      <div class="flex-horiz">
        <img src="region.png" style="width: 70vh"/>
      </div>
      <p style="text-align: center; color:red"><b>The allowed region is not convex!</b></p>
      <footer></footer>
    </section>

    <section id="matrix-models">
      <header><h1>Matrix Models</h1></header>
      <p>Let \(X,P\) be \(N\times N\) Hermitian matrices of operators, obeying
      \[
      [P_{ij},X_{kl}] = -i \delta_{ik} \delta{jl}
      \]
      <p>Some Hamiltonians
      \[
      \color{green}
      H_{\mathrm{easy}} = \mathrm{Tr}\; P^2 + \mathrm{Tr}\;X^2 + \frac{g}{N}\mathrm{Tr}\;X^4
      \]
      \[\color{red}
      H_{\mathrm{harder}} = \mathrm{Tr}\; P_X^2 + \mathrm{Tr}\; P_Y^2 + m^2 \mathrm{Tr}\;(X^2 + Y^2) - \mathrm{Tr}\;g^2[X,Y]^2
      \]
      <hr>
      <p>Matrix models are quantum mechanical and consistent.
      <p><b>Conjecture</b>: Larger matrix models (e.g. BFSS) exhibit emergent spacetime.
      <footer>See Han, Hartnoll, Kruthoff. Phys. Rev. Lett. 125, 041601.</footer>
    </section>

    <section id="sdp-matrix-models">
      <header><h1>Bootstrapping Matrix Models</h1></header>
      \[
      \color{green}
      H_{\mathrm{easy}} = \mathrm{Tr}\; P^2 + \mathrm{Tr}\;X^2 + \frac{g}{N}\mathrm{Tr}\;X^4
      \]
      <div class="flex-horiz">
      \[
      \langle \mathrm{Tr} \left(\begin{matrix}
      1 & X^2 & 0 & 0\\
      X^2 & X^4 & 0 & 0\\
      0 & 0 & X^2 & XP\\
      0 & 0 & PX & P^2\\
      \end{matrix}\right)\rangle
      \succeq 0
      \]
        <img src="han.png" width="45%" />
      </div>
      <div style="font-size:80%">
      \[\color{red}
      H_{\mathrm{harder}} = \mathrm{Tr}\; P_X^2 + \mathrm{Tr}\; P_Y^2 + m^2 \mathrm{Tr}\;(X^2 + Y^2) - \mathrm{Tr}\;g^2[X,Y]^2
      \]
      <div class="flex-horiz">
        <img src="han2.png" width="37%" />
      </div>
      </div>
      <footer>From Han, Hartnoll, Kruthoff. Phys. Rev. Lett. 125, 041601.</footer>
    </section>

    <section id="lattice-field-theory">
      <header><h1>Lattice Field Theory</h1></header>
      <div class="flex-horiz">
        <img src="lattice-points.svg" style="width: auto;height:20vh;"/>
      </div>
      <p> Each lattice site has a degree of freedom with Hilbert space \(\mathcal H_1\). The whole system has Hilbert space
      $$
      \mathcal H = \mathcal H_1 \otimes \mathcal H_1 \otimes \cdots
      $$
      <p>Some Hamiltonian \(H\) couples the different lattice sites. For a spin system, we might have
      $$
      H = \sum_{\langle i j\rangle} \sigma_z(i) \sigma_z(j) + \sum_i \sigma_x(i)
      $$
      <p style="color: blue">When correlations are large, the lattice structure is irrelevant, hence "field theory".
      <footer></footer>
    </section>

    <section id="bootstrap-lattice">
      <header><h1>The Bootstrap on the Lattice</h1></header>
      \[
      H = \sum_x \left[\frac 1 2 \pi(x)^2 + \frac 1 2 (\phi(x) - \phi(x-1))^2 + \frac{m^2}{2} \phi(x)^2 + \lambda \phi(x)^4\right]
      \]
      <div class="flex-horiz" style="padding: 2vh">
        <div>
        <p>With \(m = 0.2\)</p>
        <p>Operators:
        <ul>
          <li>\(1,\pi(0)\)</li>
            <li>\(\phi(0),\phi(1),\ldots,\phi(L)\)</li>
            <li>\(\phi(0)^2,\phi(0)^3,\ldots,\phi(0)^N\)</li>
        </ul>
        </div>
        <img src="scalar-energy.png" width="45%"/>
      </div>
      <footer>See S.L., <tt>arXiv:2111.13007</tt></footer>
    </section>

    <section id="finite-density">
      <header><h1>A Hard Problem: Finite Density Fermions</h1></header>
      <div class="flex-horiz" style="margin: 2vh">
        <div>
          <p>Monte Carlo (i.e. lattice QCD) methods are powerful, but fail for relativistic theories at finite fermion (proton - antiproton) density.
          <p>First noted in 1990. Not well-understood in general.
          <p>Makes it difficult to study:
          <ul>
            <li>Superconductors (and other strongly interacting electrons)</li>
            <li>Large nuclei</li>
            <li><b>Dense nuclear matter (neutron stars)</b></li>
          </ul>
        </div>
        <div style="border-left: 1px solid #222; padding-left: 1vh">
          <div class="flex-horiz"><img src="neutron-star.jpg" style="width: 25vh; padding: 2vh"/></div>
          <div class="flex-horiz"><img src="mr.jpg" style="width: 60vh"/></div>
        </div>
      </div>
      <footer></footer>
    </section>

    <section id="fermion-bootstrap">
      <header><h1>Bootstrapping Fermions</h1></header>
      <p>The lattice Thirring model (staggered fermions):
      <div style="font-size:70%">
      \[
      H = \sum_x (-1)^x m \chi^\dagger(x) \chi(x) + {\color{red} \mu \chi^\dagger(x) \chi(x)} + (-1)^x\frac{\chi^\dagger(x) \chi(x+1) + \mathrm{h.c.}}{2}
      +g^2 \chi(x) \chi^\dagger(x)\chi^\dagger(x+1)\chi(x+1)
      \]
      \[
      \text{ where }\;
      \{\chi(x),\chi(y)\} = 0
      \text{ and }
      \{\chi(x)^\dagger,\chi(y)\} = \delta_{xy}
      \]
      </div>
      <p>At finite chemical potential, lattice Monte Carlo exhibits the <span style="color:red">sign problem</span>.
      <div class="flex-horiz">
        <!--<img src="fermion-bootstrap.png" width="40%"/>-->
        <img src="scan-6.png" width="40%"/>
        <ul>
          <li>Operators: all \(\chi(x)\) and \(\chi^\dagger(x)\)
          <li>\(L = 6\)
          <li>\(m = 0.5\)
          <li>\(g^2 = 1.0\)
        </ul>
      </div>
      <p style="text-align: center; color: green; font-weight: bold;">No evidence of a sign problem</p>
      <footer></footer>
    </section>

    <section id="open-problems">
      <header><h1>Open Problems</h1></header>
      <div class="flex-horiz">
        <div>
          <div style="text-align:center"><b>Direct extensions</b></div>
        <ul style="padding-left: 2em">
          <li>QM fermions</li>
          <li>Continuum limit</li>
          <li>Finite temperature</li>
          <li>Time-separated correlation functions</li>
        </ul>
        </div>
      <div style="height: 70vh; border: 1px solid black"></div>
      <ul style="padding-left: 2em">
        <li style="color:red"><b>Better tools</b>. Faster, easier to use.</li>
        <li style="color:blue">Understand scaling:
          <ul>
            <li>For QM bootstrap, getting \(d\) digits takes polynomial time, empirically. Why?</li>
            <li>For field theories, it's unknown</li>
            <li>Why is the conformal bootstrap so successful?</li>
            <li>How do sign problems manifest?</li>
          </ul>
        <li style="color:green">Find other worthy targets</li>
      </ul>
      </div>
      <footer></footer>
    </section>

    <!-- CLOSING -->
    <section id="closing">
      <header></header>
      <div style="text-align: center;font-size: 5vh;">FIN</div>
      <footer></footer>
    </section>

    <div id="present-control">
      <div id="position"><span id="pos-cur"></span>/<span id="pos-len"></span></div>
      <a href='javascript:prev()'>&lt;</a>
      <a href='javascript:next()'>&gt;</a>
    </div>
  </body>
</html>
