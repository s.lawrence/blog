#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

with open('scan-6.dat') as f:
    dat = np.array([[float(x) for x in l.split()] for l in f.readlines()])

dat0 = dat[dat[:,0]==0,:]
dat2 = dat[dat[:,0]==2,:]

fig = plt.figure(figsize=(5,3))
ax_H, ax_N = fig.subplots(1,2)

ax_H.plot(dat0[:,1], dat0[:,2], ls=':', label='Bootstrap')
#ax_H.plot(dat2[:,1], dat2[:,2], ls='--')
ax_H.plot(dat2[:,1], dat2[:,4], ls='-', label='Exact')
ax_H.set_xlabel('$\\mu$')
ax_H.set_ylabel('$\\langle H \\rangle$')
ax_H.legend(loc='upper right')

ax_N.plot(dat0[:,1], dat0[:,3], ls=':', label='Bootstrap')
#ax_N.plot(dat2[:,1], dat2[:,3], ls='--')
ax_N.plot(dat2[:,1], dat2[:,5], ls='-', label='Exact')
ax_N.set_xlabel('$\\mu$')
ax_N.set_ylabel('$\\langle N \\rangle$')

fig.tight_layout()
fig.savefig('scan-6.png', dpi=300, transparent=True)
