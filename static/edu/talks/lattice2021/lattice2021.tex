\documentclass[]{beamer}

\usepackage{amsmath}
\usepackage{slashed}
\usepackage{tikz}
\usepackage{multicol}
\usepackage{comment}
\usepackage{mathtools}
\usepackage{cancel}

\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{textcomp}

%\setlength{\parskip}{1em}

% Colors
\definecolor{red}{rgb}{0.8,0,0}
\definecolor{green}{rgb}{0.0,0.5,0}
\definecolor{blue}{rgb}{0.2,0.2,1.0}

% Math declarations
\let\Im\relax
\let\Re\relax
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Re}{Re}
\renewcommand{\d}{\mathrm d}

% Maintenance macros
\newcommand{\todo}{\colorbox{red}{TODO}}

\usetheme[sectionpage=none]{metropolis}
\usecolortheme{seahorse}

\setbeamertemplate{footline}[frame number]

\setbeamercolor{progress bar}{fg=gray,bg=black}

\begin{document}

\title{Real-Time Dynamics At Large $N$}
\author{\textbf{Scott Lawrence}}
\institute{Lattice 2021}
%\institute{University of Colorado, Boulder}
%\institute{\includegraphics[width=2.5in]{umd}}
\date{27 July 2021}

\bgroup
\setbeamertemplate{footline}{}
\begin{frame}[noframenumbering]
	\titlepage
	\begin{tikzpicture}[overlay, remember picture]
		\node[anchor=center] at (8,3.6) {\includegraphics[height=1.7in]{sk}};
		\node[anchor=center] at (2.5,2.0) {\includegraphics[height=0.3in]{cuboulder}};
	\end{tikzpicture}
\end{frame}
\egroup

\begin{frame}{The Real-Time Sign Problem}
\begin{minipage}{0.47\textwidth}
\centerline{\color{blue}\textbf{Schwinger-Keldysh}}
\vspace{-1em}
\[
Z = \Tr e^{-\beta H} e^{i H T} e^{-i H T}
\]
The usual construction yields:
\[
S = \int (\partial \phi)^2 + V(\phi) \; dt
\]
with the integral taken over the \emph{Schwinger-Keldysh contour}:
\centerline{\includegraphics[width=1in]{sk}}
\end{minipage}\pause
\hfil \vrule \hfil
\begin{minipage}{0.47\textwidth}
\centerline{\color{red}\textbf{Sign Problem}}
In the ``real-time directions'', the path integral looks like:
\[
\int dx\;e^{i V(x)}
\]
With $\Re S = \mathrm{const}$, the sign problem is maximally bad.
\begin{itemize}
\item For $\mathbb R$-valued fields, $\langle \sigma\rangle = 0$.
\item For a compact field space, merely ``maximally bad''.
\end{itemize}
The sign problem depends only on the number of lattice sites.
\end{minipage}
\end{frame}

\begin{frame}{Two Models}
\begin{minipage}{0.45\textwidth}
\centerline{\color{blue}\textbf{Scalars}}
With $\phi \in \mathbb R^N$:
\[
S = \int \frac {(\partial \phi)^2}2 + \frac{m^2}{2} \phi^2
+ \frac\lambda N (\phi^2)^2
\]
\end{minipage}
\pause\hfil\vrule\hfil
\begin{minipage}{0.45\textwidth}
\centerline{\color{green}\textbf{Fermions}}
With $N$ spinors $\psi$:
\[
S = \int \bar\psi (\slashed\partial +m)\psi + \frac{g^2}{2N} \left(\bar \psi \psi\right)^2
\]
(Thirring, at $N=1$, $d=2$.)
\end{minipage}
\vspace{1em}

The scalar theory is UV-complete for $d < 4$. There's a critical point at some finite $\lambda = \lambda_c$, flowing to the $O(N)$ model.

\pause
In both cases, the coupling has been introduced to make the $N\rightarrow\infty$ limit sensible.

\vspace{1em}
\centerline{\color{red}At large $N$, the theory is free.}
\end{frame}

\begin{frame}{Prior Work}
\centerline{Real-time dynamics (especially transport) extensively studied at Large $N$}
\begin{itemize}
\item Aarts, Resco. \texttt{hep-ph/0503161}. (Fermions)
\item Lang, Kaiser, Weise. \texttt{1506.02459}. (NJL)
\item Dasgupta, Emelin, Gale, Richard. \texttt{1611.07998}. (Gauge)
\item Romatschke. \texttt{1905.09290}. (Scalar)
\item Romatschke. \texttt{2104.06435}. (Scalar)
\end{itemize}
\centerline{\color{red}Analytic calculations are often error-prone.}
\end{frame}

\begin{frame}{Large-$N$ Expansion}
Introducing an auxiliary field $\zeta$:
\[
S = N \left[\int \frac{\zeta^2}{16 \lambda} + \phi (\partial^2 + m^2 + i \zeta) \phi\right]
\]
We can integrate out the original field $\phi$:
\[
S_N = N \left[\int \frac{\zeta^2}{16\lambda} + \frac 1 2 \log \det M(\zeta)\right]
\]
So the path integral has the form:
\[
Z = \int\mathcal D \zeta\; e^{- N S_1(\zeta)}
\]
\centerline{\color{green}The large-$N$ expansion is a saddle-point expansion!}
\pause
For fermions:
\[
S_N = N \left[\frac{1}{2g^2} \int \zeta^2 - \log\det D(\zeta)\right]
\]
\end{frame}

\begin{frame}{Large-$N$ Expectation Values}
\[
Z = \int\mathcal D \zeta\; e^{- N S_1(\zeta)}
\]
Change variables to ``zoom in'' on the saddle point.
\[
Z = \int\mathcal D\sigma\;
e^{-\frac 1 2 A_{xy} \sigma_x \sigma_y \color{blue}- \frac{1}{6 \sqrt{N}} B_{xyz}\sigma_x\sigma_y\sigma_z - \frac 1 {24N} C_{wxyz}\sigma_w\sigma_x\sigma_y\sigma_z - O(N^{-3/2})}
\]
\pause
Taylor expansion of expectation value:
\begin{align*}
\langle \mathcal O\rangle
&= \frac
{\int \mathcal D \sigma\; e^{-\frac 1 2 A_{xy} \sigma_x\sigma_y}
\mathcal O(\sigma)
\left(
1 - \frac{1}{24N} C_{wxyz}\sigma_w\sigma_x\sigma_y\sigma_z
\right)}
{\int \mathcal D \sigma\; e^{-\frac 1 2 A_{xy} \sigma_x\sigma_y}
\left(
1 - \frac{1}{24N} C_{wxyz}\sigma_w\sigma_x\sigma_y\sigma_z
\right)}
\\
&\sim
\langle\mathcal O\rangle_{\zeta_c}
+ \frac{1}{24 N}\left[
\langle C_{wxyz}\sigma_w\sigma_x\sigma_y\sigma_z(1-\mathcal O)\rangle_{\zeta_c}
\right]
\end{align*}
\centerline{\small\color{red}(Typically $\mathcal O$ will need to be expanded as well.)}
\end{frame}

\begin{frame}{A Thimble}
A Lefschetz thimble extends from the saddle point $\zeta = \zeta_c$.
\[
\frac{i \zeta_c(T)}{16 \lambda} = \langle \phi^2(T)\rangle_{\zeta_c(\cdot)}
\]
\begin{center}
\includegraphics[width=3in]{saddle}
\end{center}
\centerline{\color{green}Much reduced sign problem even on the tangent plane.}
\end{frame}

\begin{frame}{Convergence and Cauchy's Integral Theorem}
\begin{columns}
\begin{column}{0.49\textwidth}
\color{green}
\centering\textbf{Fermions}
\[
Z = \int \mathcal D \zeta \underbrace{e^{-\int \zeta^2} \det D[\zeta]}_{\text{Holomorphic}}
\]
\end{column}
\begin{column}{0.49\textwidth}
\color{red}
\centering \textbf{Bosons}
\[
Z = \int \mathcal D \zeta \underbrace{e^{-\int \zeta^2} \frac{1}{\sqrt{\det M[\zeta]}}}_{\text{Singular}}
\]
\end{column}
\end{columns}
\vspace{0.6in}

\textbf{Bosons:} Integrating over the tangent plane {\color{red}will not} converge (at least to the right answer).

\textbf{Fermions:} Integrating over the tangent plane is fine.
\end{frame}

\begin{frame}{Two Contours}
\begin{center}
\includegraphics[width=3.6in]{sk-contours}
\end{center}

The ``L'' contour has a degenerate critical point; the ``S'' contour is therefore more convenient.

\color{red}Still more complicated contours are required for OTOCs.
\end{frame}

\begin{frame}{Validation}
\centering\includegraphics[width=4in]{verification}
\centerline{At $\hat m = 0.2$, $\hat \lambda = 0.1$, $\hat \beta = 6$}
\end{frame}

\begin{comment}
\begin{frame}{Tangent Plane Sign Problem (Fermions)}
\todo
\end{frame}
\end{comment}

\begin{frame}{Near-Equilibrium Bosons (Leading Order)}
\includegraphics[width=\linewidth]{mechanics-lo}
\end{frame}

\begin{frame}{Near-Equilibrium Bosons (NLO)}
\includegraphics[width=\linewidth]{mechanics-nlo}
\end{frame}

\begin{comment}
\begin{frame}{Near-Equilibrium Bosons ($1+1$)}
\todo
\end{frame}
\end{comment}

\begin{comment}
\begin{frame}{Near-Equilibrium Fermions}
\end{frame}
\end{comment}

\begin{comment}
\begin{frame}{Non-Equilibrium}
\end{frame}
\end{comment}

\begin{frame}{The Future}
\begin{itemize}
\item Non-equilibrium (scattering)
\item Transport coefficients (requires extreme timescales)
\item OTOCs / quantum chaos
\end{itemize}
The \emph{spectral form factor} is easily computed:
\[
Z(\beta) \rightarrow Z(\beta + i t)
\]
\vspace{3em}
\centerline{All code available: \texttt{https://gitlab.com/s.lawrence/saddle}}
\end{frame}

\begin{comment}

% Disable frame numbering.
\setbeamertemplate{footline}{}
\makeatletter
\beamer@noframenumberingtrue
\undef\beamer@noframenumberingfalse
\def\beamer@noframenumberingfalse{}
\makeatother

\bgroup
\setbeamercolor{background canvas}{bg=black}
\setbeamertemplate{navigation symbols}{}
\begin{frame}[plain]
\end{frame}
\egroup

\begin{frame}
	\frametitle{Outline}
	%\begin{multicols}{2}
		\tableofcontents
		%\end{multicols}
\end{frame}

\end{comment}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extra slides

\end{document}
