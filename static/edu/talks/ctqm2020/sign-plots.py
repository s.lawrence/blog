#!/usr/bin/env python

from pylab import *

x = linspace(-3,3,100)

figure(figsize=(4.0,2.5), dpi=300)
plot(x, exp(-x**2))
tight_layout()
savefig('signfree.png', transparent=True)

figure(figsize=(4.0,2.5), dpi=300)
plot(x, exp(-x**2)*cos(10*x)*exp(10))
tight_layout()
savefig('signfull.png', transparent=True)
