Quantum Simulations of (Gauge) Field Theories

Quantum computers provide a unique way of computing real-time correlators from
first principles, a task not yet achievable on classical computers due to the
sign problem. In this talk I discuss algorithms for obtaining various real-time
observables on a quantum computer in field theory, with an emphasis on SU(3)
gauge theory, relevant for nuclear physics. Many of these algorithms have
requirements that are entirely unrealistic in the near- and medium-term. As a
result, substantial efforts have gone to reducing the computational cost one
must pay in order to extract useful QCD observables. I outline some of these
efforts, and argue that determining transport coefficients of hot Yang-Mills
may be feasible sooner than most other calculations.
