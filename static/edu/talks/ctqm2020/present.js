"use strict";

// Selector that lists slides.
const screenSelector = 'body > header, body > section';

function setScreen(screen) {
	// Close the current screen.
	if (window.current) {
		if (window.current.dataset.cleanup) {
			eval(window.current.dataset.cleanup);
		}
	}
	
	location.hash = screen.id;
	window.current = screen;
	document.querySelector('#pos-len').innerHTML = screens.length;
	document.querySelector('#pos-cur').innerHTML = screens.indexOf(current)+1;

	if (window.current.dataset.init) eval(window.current.dataset.init);
}

window.onload = function () {
	window.screens = [];
	document.querySelectorAll(screenSelector).forEach(function(s) {
		screens.push(s);
	});
	if (location.hash) {
		// TODO check that this exists
		setScreen(document.querySelector(location.hash));
	} else {
		setScreen(screens[0]);
	}

	window.onkeydown = function (ev) {
		if (ev.key == ' ' || ev.key == 'ArrowRight') {
			next();
		}
		if (ev.key == 'ArrowLeft' || ev.key == 'Backspace') {
			prev();
		}
	}
}

function next() {
	var idx = screens.indexOf(current);
	if (idx < screens.length-1) setScreen(screens[idx+1]);
}

function prev() {
	var idx = screens.indexOf(current);
	if (idx > 0) setScreen(screens[idx-1]);
}
