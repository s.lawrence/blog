<!DOCTYPE html>
<html>
  <head>
    <title>
      Quantum Simulation of Gauge Theories
    </title>
    <link rel="stylesheet" href="present.css" />
    <script src='present.js'></script>
    <script async src='/mathjax/tex-svg-full.js'></script>
  </head>
  <body>
    <noscript>
      <div style="font-size: 30pt;">Javascript really is required, sorry</div>
    </noscript>

    <!-- TITLE -->
    <header id="title">
      <h1>Quantum Simulation of Gauge Theories</h1>
      <div><img src="dream.svg" style="width: 30vh; text-align: center;"/></div>
      <div class="author">Scott Lawrence</div>
      <div style="font-size:3.0vh;">
        in collaboration with Andrei Alexandru, Paulo Bedaque, Siddhartha Harmalkar, <br/>Hersh Kumar, <span style="color:green">Henry Lamm</span>, Neill Warrington, Yukari Yamauchi
      </div>
      <div></div>
      <div style="font-size: 4vh; color:blue">"The best model of a cat is another cat"
        <div style='font-size:80%; width: 80%; text-align: right; font-family: cursive;'>
          Norbert Wiener
        </div>
      </div>
    </header>

    <!-- MOTIVATION -->
    <section id="problems">
      <header><h1>Neutron stars and little bangs</h1></header>
      <div class="flex-horiz">
        <img src="neutron-star.jpg" style="width: 40vh;"/>
        <div style="font-size: 1.5vh; text-align:right;"><img src="hic.png" style="width: 70vh;"/><br/>
          Thomas McCauley/CERN
        </div>
      </div>
      $$
      \mathscr L_{\mathrm{\color{red}Q\color{green}C\color{blue}D}} = - \frac 1 4 F_{\mu\nu} F^{\mu\nu}
      + \bar \psi \left(i \gamma^\mu D_\mu - m\right)\psi
      $$
      <footer></footer>
    </section>

    <section id="sign-where">
      <header><h1>What calculations have a sign problem?</h1></header>
      <p><strong>Calculations that have sign problems:</strong>
      <ul style="color: red">
        <li>Real-time response (as opposed to thermal equilibrium)</li>
        <li>Hubbard model away from half-filling</li>
        <li>Finite density of relativistic fermions</li>
      </ul>
      <p style="color: blue">Closely related, but mild: determining the mass of a proton
      <div></div>
      <p><strong>Calculations without sign problems:</strong>
      <ul style="color: green">
        <li>Zero-fermion-density equation of state</li>
        <li>Some masses (pion)</li>
      </ul>
      <footer></footer>
    </section>

    <section id="overview">
      <header><h1>Overview</h1></header>
      <div style="text-align: center; font-size: 90%; color:blue">
        The best model of a quantum system is another quantum system.
        <div style='font-size:90%; width: 90%; text-align: right; font-family: cursive;'>
          Norbert Wiener, almost
        </div>
      </div>
      <div style="text-align: center">
        <strong style="color:green">A quantum computer is a quantum system evolved in real-time.</strong>
      </div>
      <p>Set up an analogy between the quantum computer and the system to be simulated, and treat the computer like a (perfectly controlled) laboratory.
      <ul>
        <li>Some quantum algorithms</li>
        <li>Hamiltonian lattice gauge theory</li>
        <li>Quantum simulations of field theory</li>
        <li>Studying a proton</li>
        <li>Mitigating noise</li>
      </ul>
      <footer></footer>
    </section>

    <!-- QUANTUM COMPUTING -->
    <section id="qc-qubit">
      <header><h1>From Bit to Qubit</h1></header>
      $$
      |0\rangle,|1\rangle,\ldots,\frac{1}{\sqrt{2}}\left(|0\rangle+|1\rangle\right)
      $$
      <div class="flex-horiz">
        <img src="H1.svg" style="width: 20vh"/>
        <img src="Bloch_sphere.svg" style="width: 24vh"/>
      </div>
      <p style="text-align: center">This is a spin from QM.</p>
      <div class="flex-horiz">
        <img src="spin.svg" style="width: 14vh"/>
      </div>
      <footer></footer>
    </section>

    <section id="qc-qubits">
      <header><h1>A Quantum Computer</h1></header>
      <div class="flex-horiz">
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
      </div>
      <p>What's the Hilbert space?
      $$
      \mathcal H = \mathcal H_1 \otimes \cdots \otimes \mathcal H_1
      \;\text{ where }
      \mathcal H_1 = \mathrm{span}\{|0\rangle,|1\rangle\}
      $$
      <p>In other words, superpositions of all possible bitstrings.
      $$
      \mathcal H = \mathrm{span}\{|00\cdots\rangle,|10\cdots\rangle,|01\cdots\rangle,\cdots\}
      $$
      <div class="flex-horiz">
        <div style="text-align: center">
          $$
          \left({\color{blue}|0\rangle + i|1\rangle}\right)\left({\color{green}|0\rangle+|1\rangle}\right)
          = |{\color{blue}0\color{green}0}\rangle + |{\color{blue}0\color{green}1}\rangle + i|{\color{blue}1\color{green}0}\rangle + i|{\color{blue}1\color{green}1}\rangle
          $$
          Not entangled
        </div>
        <div style="text-align: center">
          $$
          |{\color{blue}0}{\color{green}0}\rangle + |{\color{blue}1}{\color{green}1}\rangle
          $$
          Entangled
        </div>
      </div>
      <footer></footer>
    </section>

    <section id="qc-gates">
      <header><h1>Gates</h1></header>
      <p>All operations must be unitary (conservation of probability). On one qubit:
      <div class="flex-horiz">
        <div style="text-align: center;">
          $$ T = \left(
          \begin{matrix}
          1 &amp; 0\\
          0 &amp; e^{i \pi / 4}\\
          \end{matrix}
          \right)
          $$
          <!--<img src="gate-t.png" style="width: 10vh;" />-->
        </div>
        <div style="text-align: center;">
          $$ H = \frac 1 {\sqrt{2}}\left(
          \begin{matrix}
          1 &amp; 1\\
          1 &amp; -1\\
          \end{matrix}
          \right)
          $$
          <!--<img src="gate-h.png" style="width: 10vh;" />-->
        </div>
      </div>
      <p>On two qubits, the controlled-not operation:
      <div class="flex-horiz">
        $$
        CX = \left(
        \begin{matrix}
        1 &amp; 0 &amp; 0 &amp; 0\\
        0 &amp; 1 &amp; 0 &amp; 0\\
        0 &amp; 0 &amp; 0 &amp; 1\\
        0 &amp; 0 &amp; 1 &amp; 0\\
        \end{matrix}
        \right)
        \left(
        \begin{matrix}
        |00\rangle\\
        |01\rangle\\
        |10\rangle\\
        |11\rangle\\
        \end{matrix}
        \right)
        $$
        <div style="text-align: center">
          <!--<img src="gate-cx.png" style="width: 10vh;" />-->
        </div>
      </div>
      <p>These fundamental gates are sufficient to construct any unitary we want.
      <footer></footer>
    </section>

    <section id="qc-measurement">
      <header><h1>Measurement</h1></header>
      <p>In principle, we measure any Hermitian operator. In practice, we measure \(\sigma_z\) acting on each qubit.
      $$\Psi = \alpha|0\rangle + \beta|1\rangle$$
      <p>Each measurement yields \(1\) or \(0\). Probability of \(1\) is \(\langle\Psi|1\rangle\langle 1|\Psi\rangle = |\beta|^2\).
      <p>Thus, we <span style="color:blue">require many measurements</span> for a precise result. This is "shot noise".
      <p>Hermitian operators are exactly those which may appear as terms in the Hamiltonian.
      <footer></footer>
    </section>

    <section id="qc-exist">
      <header><h1>State of the art</h1></header>
      <div class="flex-horiz">
        <div style="margin: 1vh">
          <img src="bristlecone.png" style="width: 40vh;"/><br/>
          <img src="cryostat.jpg" style="width: 40vh;"/>
        </div>
        <div style="margin: 1vh">
          <p>Current best: \(\sim 50\) qubits.
          <p>Each qubit can undergo \(\lesssim 10\) operations before decohering.
          <p><strong style="color:green">Are large processors worth it?</strong><br/>
          <div style="text-align: center"><img src="tang.png" style="width: 70vh;"/></div>
        </div>
      </div>
      <footer></footer>
    </section>

    <section id="qc-classical">
      <header><h1>What is a classical computer?</h1></header>
      <div style="text-align: center;">A quantum computer constantly being measured</div>
      <div style="text-align: center;">\(|01\rangle\) is okay; \(\left[|10\rangle + |01\rangle\right]\) gets destroyed</div>
      <p>This restricts the set of possible operations, as well. We only have <strong>permutations</strong>:
      <div class="flex-horiz"><span></span>
        <span style="color: green">
      \[\left(
      \begin{matrix}
      1&amp; 0 &amp; 0 &amp; 0\\
      0&amp; 1 &amp; 0 &amp; 0\\
      0&amp; 0 &amp; 0 &amp; 1\\
      0&amp; 0 &amp; 1 &amp; 0\\
      \end{matrix}
      \right)
      \]</span>
      <span> and not </span>
      <span style="color: red">
      \[
      \left(
      \begin{matrix}
      -i &amp; 0\\
      0 &amp; i\\
      \end{matrix}
      \right)
      \]
      </span>
      <span></span></div>
      <footer></footer>
    </section>

    <section id="qc-classical-algorithms">
      <header><h1>Classical algorithms are quantum algorithms</h1></header>
      <div class="flex-horiz"><strong style="color:blue">Any classical circuit can be re-labelled as a quantum circuit.</strong></div>
      <div class="flex-horiz">
        <div>
          <p>Example: two-bit adder
          $$
          \begin{align}
          |00\rangle |00\rangle &amp;\rightarrow |00\rangle |00\rangle \\
          |01\rangle |00\rangle &amp;\rightarrow |01\rangle |01\rangle \\
          |10\rangle |00\rangle &amp;\rightarrow |10\rangle |01\rangle \\
          |11\rangle |00\rangle &amp;\rightarrow |11\rangle |10\rangle \\
          \end{align}
          $$
        </div>
        <img src="two-bit-adder.png" style="width: 40vh;" />
      </div>
      <p>In general, given a classical circuit for \(f(x)\), we can obtain
      $$
      |x\rangle|0\rangle \rightarrow |x\rangle |f(x)\rangle
      $$
      <footer></footer>
    </section>

    <section id="trotter">
      <header><h1>Suzuki-Trotter decomposition</h1></header>
      $$ H = {\color{blue}A} + {\color{green}B} $$
      <p> Assume we can evolve under \(A\) and \(B\). How to get evolution under \(H\)?
      $$
      e^{-i H \epsilon} \approx {\color{blue}e^{-i A \epsilon}} {\color{green}e^{-i B \epsilon}}
      $$
      <p>So, time-evolve by rapidly alternating between two Hamiltonians
      $$
      e^{-i H t} \approx \left({\color{blue}e^{-i A \epsilon}} {\color{green}e^{-i B \epsilon}}\right)^{t / \epsilon}
      $$
      <footer></footer>
    </section>

    <!--
    <section id="qc-inverting">
      <header><h1>Inverting circuits</h1></header>
      <footer></footer>
    </section>

    <section id="qc-control">
      <header><h1>Controlled unitaries</h1></header>
      <footer></footer>
    </section>
    -->

    <!-- SIMULATIONS -->
    <!--
    <section id="sim-intro">
      <header><h1>Simulating quantum mechanics</h1></header>
      <footer></footer>
    </section>

    <section id="sim-box">
      <header><h1>Particle in a box</h1></header>
      <footer></footer>
    </section>

    <section id="sim-anderson">
      <header><h1>Anderson localization</h1></header>
      <footer></footer>
    </section>

    <section id="sim-potential">
      <header><h1>Random potential</h1></header>
      <footer>
        Andrei Alexandru, Paulo F. Bedaque, Scott Lawrence.
        <em>Quantum algorithms for disordered physics</em>.
        <a href="https://doi.org/10.1103/PhysRevA.101.032325" target="_blank">Phys.Rev A101 (2020) no.3, 032325</a>.
</footer>
    </section>

    <section id="sim-data">
      <header><h1>We can run this!</h1></header>
      <div style="text-align: center;">IBM and Rigetti both provide time on small quantum computers.</div>
      <div style="text-align: center;">
        <img src="anderson-demo.png" style="width:90vh"/>
      </div>
      <p>Demonstrated on a \(4\)-site lattice. The average distance \(D\) grows with time, as the particle diffuses through the lattice.
      <footer>
        Andrei Alexandru, Paulo F. Bedaque, Scott Lawrence.
        <em>Quantum algorithms for disordered physics</em>.
        <a href="https://doi.org/10.1103/PhysRevA.101.032325" target="_blank">Phys.Rev A101 (2020) no.3, 032325</a>.
      </footer>
    </section>-->

    <!-- LATTICE GAUGE THEORY -->
    <section id="lattice-intro">
      <header><h1>Field theories, the finite way</h1></header>
      <div class="flex-horiz">
        <img src="lattice-points.svg" style="width: auto;height:30vh;"/>
      </div>
      <p> Each lattice site has a degree of freedom with Hilbert space \(\mathcal H_1\). The whole system has Hilbert space
      $$
      \mathcal H = \mathcal H_1 \otimes \mathcal H_1 \otimes \cdots
      $$
      <p>Some Hamiltonian \(H\) couples the different lattice sites. For a spin system, we might have
      $$
      H = \sum_{\langle i j\rangle} \sigma_z(i) \sigma_z(j) + \sum_i \sigma_x(i)
      $$
      <p>When correlations are large, the lattice structure is irrelevant, hence "field theory".
      <footer></footer>
    </section>

    <section id="qm-group">
      <header><h1>Quantum mechanics on a group</h1></header>
      <p>The Hamiltonian of a free particle moving on \(G = SU(3)\):
      $$
      H = -\nabla_\ell^2
      $$
      <p>Hilbert space is \(\mathbb C G\), the space of complex functions on \(G\).
      <div class="flex-horiz">
$$
\left|\left(
\begin{matrix}
1 &amp; 0 &amp; 0\\
0 &amp; 1 &amp; 0\\
0 &amp; 0 &amp; 1\\
\end{matrix}
\right)\right&gt;,
\left|\left(
\begin{matrix}
0 &amp; 1 &amp; 0\\
0 &amp; 0 &amp; i\\
-i &amp; 0 &amp; 0\\
\end{matrix}
\right)\right&gt;,
\left|\left(
\begin{matrix}
0 &amp; 0 &amp; 1\\
\frac{1}{\sqrt{2}} &amp; \frac{1}{\sqrt{2}} &amp; 0\\
\frac{1}{\sqrt{2}} &amp; \frac{-1}{\sqrt{2}} &amp; 0\\
\end{matrix}
\right)\right&gt;
$$
      </div>
      <p>We can work with momentum eigenstates instead.
      <div class="flex-horiz">
        <div style="text-align: right;"><img src="spherical-harmonics.png" style="width: 70vh" /><div style="font-size:1.3vh">wikimedia</div>
        </div>
      </div>
      <footer></footer>
    </section>

    <section id="lattice-gauge">
      <header><h1>Gauge theories</h1></header>
      <div class="flex-horiz">
        <img src="lattice3.svg" style="width: auto;height:40vh;"/>
      </div>
      $$
      H = -\beta_K\sum_\ell \nabla_\ell^2 + \beta_P\sum_P \mathrm{Re\,Tr\,}P
      $$
      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>General Methods for Digital Quantum Simulation of Gauge Theories</em>.
<a href="https://doi.org/10.1103/PhysRevD.100.034518">Phys.Rev. D100 (2019) no.3, 034518</a>.
      </footer>
    </section>

    <section id="gauge-symmetry">
      <header><h1>Gauge symmetry</h1></header>
      <div class="flex-horiz">
        <img src="lattice4.svg" style="width:40vh;" />
        <div>
          $$
U_{ij} \mapsto V_j U_{ij} V_i^\dagger
$$
<br/>
$$
\begin{align}
&amp;\mathrm{Tr\,} U_{14}^\dagger U_{45}^\dagger U_{25} U_{12}\\
\mapsto&amp; \mathrm{Tr\,}U_{14}^\dagger U_{45}^\dagger U_{25} V^\dagger_2 V_2 U_{12}
\end{align}
$$
        </div>
      </div>
      <p>The Hamiltonian is gauge-invariant.
      $$
      H = -\beta_K\sum_\ell \nabla_\ell^2 + \beta_P\sum_P \mathrm{Re\,Tr\,}P
      $$
      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>General Methods for Digital Quantum Simulation of Gauge Theories</em>.
<a href="https://doi.org/10.1103/PhysRevD.100.034518">Phys.Rev. D100 (2019) no.3, 034518</a>.
      </footer>
    </section>

    <section id="gauge-hilbert-space">
      <header><h1>Hilbert space</h1></header>
        <div class="flex-horiz">
          <img src="lattice2.svg" style="width:30vh;"/>
          $$
          \mathcal H = \mathbb C G \otimes \mathbb C G \otimes \cdots
          $$
        </div>
        <div></div>
        <div style="text-align: center; font-size: 4vh; font-weight: bold; color: blue">
          Only gauge-invariant states are physical.
        </div>
        <div class="flex-horiz">
          $$\color{red}|U_{12}\rangle $$
          $$\color{green}
          \int \mathrm{d} V_1 \mathrm{d} V_2
          |V^\dagger_{2} U_{12} V_1\rangle
          $$
        </div>
        <p>Here's a projection operator:
        $$
        P |U_{12}\cdots\rangle = 
        \int(\mathrm{d}V_1 \mathrm{d}V_2\cdots)|(V^\dagger_2 U_{12} V_1)\cdots\rangle
        $$
      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>General Methods for Digital Quantum Simulation of Gauge Theories</em>.
<a href="https://doi.org/10.1103/PhysRevD.100.034518">Phys.Rev. D100 (2019) no.3, 034518</a>.
      </footer>
    </section>

    <section id="ex-z2">
      <header><h1>Example: \(\mathbb Z_2\) gauge theory</h1></header>
      <div class="flex-horiz">
        <img src="plaquette2.svg" style="width: 50vh" />
      </div>
      $$
      H = \sigma_x(a) + \sigma_x(b) + \sigma_z(a) \sigma_z(b)
      $$
      <p>Four states, naively: \(|00\rangle\), \(|01\rangle\), \(|10\rangle\), \(|11\rangle\). But these are not gauge-invariant!
      <div></div>
      <p>Gauge transformation takes \(00 \leftrightarrow 11\) and \(01 \leftrightarrow 10\).
      $$
      \color{green}
|00\rangle + |11\rangle
\;\text{ and }\;
|01\rangle + |10\rangle
      $$
      <footer></footer>
    </section>

    <section id="sim-field">
      <header><h1>Simulating a field theory: general principles</h1></header>
      <div class="flex-horiz">
        <img src="lattice-points.svg" style="width: auto;height:30vh;"/>
      </div>
      <p>We need a mapping between the Hilbert spaces. Locality is nice.
      $$ \mathcal H_1 \leftrightarrow \mathcal H_1^{\mathrm{QC}} $$
      <p> This induces a "nice" map \(\mathcal H \leftrightarrow \mathcal H^{\mathrm{QC}}\)
      <p>Implement \(e^{-i H t}\) via Suzuki-Trotter
      <footer></footer>
    </section>

    <section id="sim-gauge-unitary">
      <header><h1>Time evolution</h1></header>
      $$
      H = {\color{red}\overbrace{\sum_L \nabla^2_L}^{H_K}} + {\color{blue}\overbrace{\sum_P \mathrm{Re\,Tr\,} P}^{H_V}}
      $$
      <div class="flex-horiz">
        <div>
          <div style="text-align: center; font-weight: bold; color: red;">Kinetic</div>

          <p>One link only

  <p>Diagonal in Fourier space

          <p style="color: green">Mutually commuting terms
        </div>
        <div>
          <div style="text-align: center; font-weight: bold; color: blue;">Potential</div>
          <p>Four links

          <p>Diagonal (in our basis)

          <p style="color: green">Mutually commuting terms
        </div>
      </div>

\[
e^{-i H t} \approx
\left[{\color{red}\left(
e^{-i \nabla_1^2 \epsilon}
e^{-i \nabla_2^2 \epsilon}
\cdots
\right)}
{\color{blue}\left(
e^{-i \epsilon \mathrm{Re\,Tr\,} P_1}
e^{-i \epsilon \mathrm{Re\,Tr\,} P_2} \cdots
\right)}
\right]^{t/\epsilon}
\]


<footer></footer>
    </section>

    <section id="sim-ex-z2">
      <header><h1>Example: \(\mathbb Z_2\) gauge theory (again)</h1></header>
      <div class="flex-horiz">
        <img src="plaquette2.svg" style="width: 50vh" />
      </div>
      $$
      H = \sigma_x(a) + \sigma_x(b) + \sigma_z(a) \sigma_z(b)
      $$
      <div class="flex-horiz">
        <img src="z2-evolution.png" style="width: 50vh;" />
      </div>
      <footer></footer>
    </section>

    <section id="sim-valentiner">
      <header><h1>Valentiner gauge theory</h1></header>
      <p>We'd like to simulate \(SU(3)\), but \(\mathbb C SU(3)\) is infinite-dimensional.
      <div class="flex-horiz">
             <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
        <img src="spin.svg" style="width: 14vh"/>
 </div>
      \[ \color{red}2^Q &lt; \infty \]
      <div class="flex-horiz"><strong>We can approximate \(SU(3)\) by a finite subgroup.</strong></div>
      \[ S(1080) &lt; SU(3) \]
      <footer>
NuQS Collaboration (Andrei Alexandru, Paulo F. Bedaque, Siddhartha Harmalkar, Henry Lamm, Scott Lawrence, Neill C. Warrington).
<em>Gluon Field Digitization for Quantum Computers</em>.
<a href="https://doi.org/10.1103/PhysRevD.100.114501">Phys.Rev. D100 (2019) no.11, 114501</a>.
      </footer>
    </section>

    <section id="val-approx">
      <header><h1>Is this approximation any good?</h1></header>
      <p>After adding an extra term to the action...
      $$
      S=-\sum_p \left(\frac{\beta_0}{3}\mathrm{Re\,Tr\,} U_p +\beta_1\mathrm{Re\,Tr\,} U_p^2\right)
      $$
      <p>Construct a dimensionless quantity from: Wilson flow, critical temperature
      <div class="flex-horiz">
        <img src="tc_v_t02.png" style="width: 80vh" />
      </div>
      <p>Particle masses not yet measured...
      <div class="flex-horiz"><strong style="color:blue; font-size: 5vh">Maybe</strong></div>
      <footer>
NuQS Collaboration (Andrei Alexandru, Paulo F. Bedaque, Siddhartha Harmalkar, Henry Lamm, Scott Lawrence, Neill C. Warrington).
<em>Gluon Field Digitization for Quantum Computers</em>.
<a href="https://doi.org/10.1103/PhysRevD.100.114501">Phys.Rev. D100 (2019) no.11, 114501</a>.
      </footer>
    </section>

    <section id="sim-space">
      <header><h1>Gauge invariance</h1></header>
      <p>The physical Hilbert space is <em>not</em> \(\mathbb C S(1080) \otimes \mathbb C S(1080) \otimes\cdots\).
      <p>But, the Hilbert space on the quantum computer is isomorphic to that space!
      <div class="flex-horiz"><strong style="color: blue">Time evolution is gauge-invariant.</strong></div>
      $$ [P, H] = 0 $$
      <p>If we start in a gauge-invariant state, we stay gauge-invariant.
      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>General Methods for Digital Quantum Simulation of Gauge Theories</em>.
<a href="https://doi.org/10.1103/PhysRevD.100.034518">Phys.Rev. D100 (2019) no.3, 034518</a>.
      </footer>
    </section>

    <!-- PHYSICS -->
    <section id="ht-intro">
      <header><h1>The hadronic tensor</h1></header>
    \[
W^{\mu\nu}(q)
=
\int {\mathrm d}x\;e^{iqx}
\left&lt;e^{i H x^0}J^\mu(\vec x) e^{-i H x^0}J^\nu(\vec 0)\right&gt;_{\mathrm{proton}}
\]
<p>The hadronic tensor captures nonperturbative (in QCD coupling) information about the proton. For electron-proton scattering, to leading order in \(\alpha_{\mathrm{QED}}\):
\[
\frac{{\mathrm d}^2\sigma}{\mathrm{d} x\;\mathrm{d} y} = \frac{\alpha^2 y}{Q^4}  L_{\mu\nu} W^{\mu\nu}
\]
  <footer></footer>
    </section>

    <section id="state-prep">
      <header><h1>Preparing an interesting state</h1></header>
      <p>Naive state preparation: couple to a heat bath and cool the system. <strong style="color:red">Expensive!</strong>
      <p>Alternative: <strong>adiabatic state preparation</strong>
      <p>Other proposals:
      <ul>
        <li>Tensor networks</li>
        <li>Spectral comb (<code>arXiv:1709.08250</code>)</li>
        <li>Hybrid state preparation (PhysRevLett 121 170501, <code>1908.07051</code>)</li>
      </ul>
      <p>Hard to analyze, impossible to test
      <footer></footer>
    </section>

    <section id="adiabatic">
      <header><h1>Adiabatic theorem</h1></header>
      <div></div>
      <p>Take a time-varying (slowly) Hamiltonian \(H(t)\).

      <p>Prepare an eigenstate of \(H(0)\), with a gap of \(\Delta\).

      <p>When \(\dot H / \Delta^2 \ll 0\), time-evolution will keep us in the eigenstate.

      <div class="flex-horiz">
        <p>Time needed to prepare ground state: \(\Delta^{-2}\)
      </div>
      <div></div>
      <div></div>
      <footer></footer>
    </section>
    
    <section id="ht-prep">
      <header><h1>Making a proton</h1></header>
      <p>Restrict to a certain sector of Hilbert space:
      <ul>
        <li>Gauge-invariant states</li>
        <li>Zero total momentum</li>
        <li>Baryon number 1</li>
      </ul>
      <hr>
      <div class="flex-horiz">
        <img src="adiabatic.svg" style="width: 80vh" />
      </div>
      <div class="flex-horiz">
        <div style="color:blue">
          <ul>
            <li>Free fermions and glue (massive)</li>
            <li>Ground state exactly prepared</li>
            <li>Small gap (\(O(\frac 1 V)\))</li>
          </ul>
        </div><div style="color:green">
          <ul>
            <li>Hadrons</li>
            <li>Large gap (\(m_\pi\))</li>
          </ul>
        </div>
      </div>
      <div style="text-align: center">
      Total circuit size: \(O(V^3)\)
      </div>

      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>Parton physics on a quantum computer</em>
<a href="https://doi.org/10.1103/PhysRevResearch.2.013272">Phys.Rev.Res. 2 (2020) no.1, 013272</a>.
      </footer>
    </section>

    <section id="obtaining-ht">
      <header><h1>Measuring the hadronic tensor</h1></header>
    \[
W^{\mu\nu}(q)
=
\int {\mathrm d}x\;e^{iqx}
\left&lt;e^{i H x^0}J^\mu(\vec x) e^{-i H x^0}J^\nu(\vec 0)\right&gt;_{\mathrm{proton}}
\]
<p>How do we measure \(e^{i H t}J_0(x)e^{-i H t} J_0(0)\)?
\[
H(t) = H_0 + \epsilon \delta(t) J^\nu(\vec 0)
\]
<p>Linear response: differentiate with respect to \(\epsilon\)
\[
\frac{\partial}{\partial \epsilon} \langle e^{i H t} J^\mu(\vec x) e^{-i H t}\rangle
\]
      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>Parton physics on a quantum computer</em>
<a href="https://doi.org/10.1103/PhysRevResearch.2.013272">Phys.Rev.Res. 2 (2020) no.1, 013272</a>.
      </footer>
    </section>

    <section id="drift">
      <header><h1> One type of noise</h1></header>
      <div class="flex-horiz"><strong><em style="color:green">Coherent</em> noise that <span style="color:red">violates gauge invariance</span></strong></div>
      <div class="flex-horiz">
        <img src="plaquette2.svg" style="width: 30vh" />
      </div>
      <div class="flex-horiz">
      $$
      \color{green}
|0_+\rangle = |00\rangle + |11\rangle
$$
$$
\color{red}
|0_-\rangle = |00\rangle - |11\rangle
      $$
      </div>
      $$
      U_{\mathrm{drift}} = \sqrt{1-\epsilon^2}I + \epsilon |0_+\rangle\langle 0_-|- \epsilon |0_-\rangle\langle 0_+|
      $$
      <p> When performing time evolution, we would like \( e^{-i H \Delta t} e^{-i H \Delta t} \cdots \), but instead we get:
      $$
      \color{blue}e^{-i H \Delta t}
      \color{red} U_{\mathrm{drift}}
      \color{blue}e^{-i H \Delta t}
      \color{red} U_{\mathrm{drift}}
      \color{blue}e^{-i H \Delta t}
      \color{red} U_{\mathrm{drift}}
      \cdots
      $$
      <div class="flex-horiz"><strong>Most states are unphysical.</strong></div>
      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>Suppressing Coherent Gauge Drift in Quantum Simulations</em>.
<a href="https://arxiv.org/abs/2005.12688">arXiv:2005.12688</a> (2020).
      </footer>
    </section>

    <section id="drift-hamiltonian">
      <header><h1>Changing the Hamiltonian</h1></header>
      <p>Suppress unphysical states with an energy penalty.
      <p>Define \( H_{\mathrm{gauss}} \) by:
      <div class="flex-horiz"><span></span>
      $$
      H_{\mathrm{gauss}} |\Psi\rangle = 0
      $$
      <span>for physical states, and</span><span></span></div>
      <div class="flex-horiz"><span></span>
        $$ H_{\mathrm{gauss}} |u\rangle = |u\rangle $$
        <span> for unphysical states.</span><span></span>
      </div>
      <p>and add \( H_{\mathrm{gauss}} \) to the Hamiltonian used for evolution.
      <p>Paths which pass through the unphysical subspace will pick up phase cancellations and be supressed.
      <div class="flex-horiz"><strong>But how can we implement \( H_{\mathrm{gauss}} \)?</strong></div>
      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>Suppressing Coherent Gauge Drift in Quantum Simulations</em>.
<a href="https://arxiv.org/abs/2005.12688">arXiv:2005.12688</a> (2020).
      </footer>
    </section>

    <section id="drift-random">
      <header><h1>Random gauge transformations</h1></header>
      <div class="flex-horiz"><strong>But how can we implement \( H_{\mathrm{gauss}} \)?</strong></div>
      <p>We don't need to! We only need \( U_{\mathrm{gauss}} \sim e^{-i H_{\mathrm{gauss}} t} \)
      <p>At long times (equivalently, large energies), this gives each unphysical state a random phase. Approximate by <strong>performing a random gauge transformation</strong>.
      $$
      e^{-i H \Delta t}
      \phi(g_0)
      e^{-i H \Delta t}
      \phi(g_1)
      e^{-i H \Delta t}
      \cdots
      $$
      <p>Each \(\phi(g)\) blesses an unphysical state with a random phase.
      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>Suppressing Coherent Gauge Drift in Quantum Simulations</em>.
<a href="https://arxiv.org/abs/2005.12688">arXiv:2005.12688</a> (2020).
      </footer>
    </section>

    <section id="drift-demo">
      <header><h1>Random gauge transformations: Demonstration</h1></header>
      <div class="flex-horiz">
        <img src="plaquette2.svg" style="width: 30vh" />
        <span>With gauge group \( D_3 \)</span>
      </div>
      <div class="flex-horiz">
        <img src="d3.png" />
      </div>
      <footer>
NuQS Collaboration (Henry Lamm, Scott Lawrence, Yukari Yamauchi).
<em>Suppressing Coherent Gauge Drift in Quantum Simulations</em>.
<a href="https://arxiv.org/abs/2005.12688">arXiv:2005.12688</a> (2020).
      </footer>
    </section>

    <!-- CLOSING -->
    <section id="closing">
      <header></header>
      <div style="text-align: center;font-size: 6vh;">Thanks for watching!</div>
      <div style="text-align: center;font-size: 5vh;">Questions?</div>
      <footer></footer>
    </section>

    <div id="present-control">
      <div id="position"><span id="pos-cur"></span>/<span id="pos-len"></span></div>
      <a href='javascript:prev()'>&lt;</a>
      <a href='javascript:next()'>&gt;</a>
    </div>
  </body>
</html>
