function sign_init() {
	var pathCanvas = document.querySelector("#sign-path");
	var graphCanvas = document.querySelector("#sign-graph");
	pathCanvas.height = pathCanvas.clientHeight;
	pathCanvas.width = pathCanvas.clientWidth;
	graphCanvas.height = graphCanvas.clientHeight;
	graphCanvas.width = graphCanvas.clientWidth;

	var pathCtx = pathCanvas.getContext('2d');
	var graphCtx = graphCanvas.getContext('2d');
	var pW = pathCanvas.width;
	var pH = pathCanvas.height;
	var gW = graphCanvas.width;
	var gH = graphCanvas.height;

	var mag = 0.;
	var hist = [];
	var draw = function () {
		// Draw the path
		pathCtx.fillRect(0,0,pW,pH);
		pathCtx.clearRect(0,0,pW,pH);
		pathCtx.beginPath();
		pathCtx.arc(pW/10,pH/2,5.,0.,Math.PI*2,false);
		pathCtx.fill();
		pathCtx.beginPath();
		pathCtx.arc(9*pW/10,pH/2,5.,0.,Math.PI*2,false);
		pathCtx.fill();
		pathCtx.lineWidth = 3;
		pathCtx.beginPath();
		pathCtx.moveTo(pW/10,pH/2);
		for (var x = pW/10; x < 9*pW/10; x+=1) {
			var t = (x-pW/10)/(8*pW/10);
			pathCtx.lineTo(x,pH/2+mag*Math.sin(2*Math.PI*t));
		}
		pathCtx.stroke();
		pathCtx.closePath();

		// Compute the action.
		var S = 0.;
		for (var x = pW/10; x < 9*pW/10; x+=1) {
			var t = (x-pW/10)/(8*pW/10);
			var deriv = mag*Math.cos(2*Math.PI*t)/100;
			S += Math.pow(deriv,2)/2;
		}
		hist.push(Math.cos(S));
		
		// Update the graph.
		graphCtx.clearRect(0,0,gW,gH);
		graphCtx.beginPath();
		graphCtx.lineWidth = 2;
		graphCtx.moveTo(3,gH/2);
		graphCtx.lineTo(gW-3,gH/2);
		graphCtx.moveTo(gW-3,gH-3);
		graphCtx.lineTo(gW-3,3);
		graphCtx.stroke();
		graphCtx.closePath();
		graphCtx.beginPath();
		var mid = (gH-6)/2
		graphCtx.moveTo(gW-3,3 + mid);
		for (var i = 0; i < hist.length; i++) {
			var j = hist.length - i - 1;
			graphCtx.lineTo(gW-3-i, 3+mid+0.8*mid*hist[j]);
		}
		graphCtx.stroke();
		graphCtx.closePath();
	};
	draw();

	window.onmousemove = function (ev) {
		if (ev.buttons == 1) {
			mag += ev.movementY/5;
			draw();
		}
	}
}

function sign_cleanup() {
	window.onmousemove = undefined;
}
