\documentclass[]{beamer}

\usepackage{amsmath}
\usepackage{slashed}
\usepackage{tikz}
\usepackage{multicol}
\usepackage{comment}
\usepackage{mathtools}
\usepackage{cancel}

\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{textcomp}

%\setlength{\parskip}{1em}

% Colors
\definecolor{red}{rgb}{0.8,0,0}
\definecolor{green}{rgb}{0.0,0.5,0}
\definecolor{blue}{rgb}{0.2,0.2,1.0}

% Math declarations
\let\Im\relax
\let\Re\relax
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Re}{Re}
\renewcommand{\d}{\mathrm d}

% Maintenance macros
\newcommand{\todo}{\colorbox{pink}{TODO}}

\usetheme[sectionpage=none]{metropolis}
\usecolortheme{seahorse}

\setbeamertemplate{footline}[frame number]

\setbeamercolor{progress bar}{fg=gray,bg=black}

\begin{document}

\title{Probing the Hydrodynamics of Strongly Coupled QFTs}
\author{\textbf{Scott Lawrence}\\
\small{\texttt{arXiv:2206.04765} with Romatschke}\\
\small{\texttt{arXiv:2104.02024} with Cohen, Lamm, Yamauchi}\\
\small{\texttt{arXiv:2111.08158}}}
\institute{Brookhaven National Laboratory}
%\institute{University of Colorado, Boulder}
%\institute{\includegraphics[width=2.5in]{umd}}
\date{13 October 2022}

\bgroup
\setbeamertemplate{footline}{}
\begin{frame}[noframenumbering]
	\titlepage
	\begin{tikzpicture}[overlay, remember picture]
		\node[anchor=center] at (7.5,2.5) {\includegraphics[height=0.3in]{cuboulder}};
	\end{tikzpicture}
\end{frame}
\egroup

\begin{frame}{The universe has not yet equilibrated}
\begin{tikzpicture}[overlay, remember picture]
	\node[anchor=center] at (5.5,3.5) {Heat death of the universe is not expected for $\sim 10^{100}$ more years.};
	\node[anchor=center] at (8.5,0.5) {\includegraphics[height=1in]{hic}};
	\node[anchor=center] at (2.5,0.5) {\includegraphics[height=2in]{neutron-star}};
	\node[anchor=center] at (5.5,-3.0) {\color{red}For now: need nonperturbative methods for nonequilibrium physics.};
\end{tikzpicture}

\end{frame}

\begin{frame}{Hydrodynamics}
Finite-temperature quantum matter ({\color{red}hard to simulate!}), when ``zoomed out'', is described by \emph{classical} hydrodynamics ({\color{green}``easy''}).

Navier-Stokes:
\[
\rho \frac{\d u_i}{\d t} + \partial_i p = {\color{blue}\eta} \left(\frac 1 3 \partial_i \partial_j u_j + \partial_j^2 u_i \right) + {\color{blue}\zeta}\cdots
\]
More systematic (and relativistic): $\partial_\mu T^{\mu\nu} = 0$, expand $T^{\mu\nu}$ in $\nabla$:
\[\scriptstyle
T^{\mu\nu} = T^{\mu\nu}_{(0)} - \underbrace{\scriptstyle2 {\color{blue}\eta} \nabla^{\langle \mu} u^{\nu \rangle} - {\color{blue}\zeta\Delta^{\mu\nu}\nabla^\perp_\lambda u^\lambda}}_{T^{\mu\nu}_{(1)}}
\scriptscriptstyle
+ 
\underbrace{\scriptscriptstyle{\color{blue}\kappa}\left[R^{\langle i j \rangle} - 2 R^{t \langle i j \rangle t}\right]
 + \cdots}_{T^{\mu\nu}_{(2)}} + \cdots
\]
\centerline{\textbf{Gradient expansion}: long distances, long times}
\centerline{\textbf{Transport coefficients}: LECs through which quantum effects can appear}
\end{frame}

\begin{frame}{Low-order transport}
\color{green}
\textbf{Sound waves}
\[
\int dx\, \sin kx\, \langle T^{00}(x,t) T^{00}(0,0) \rangle
\sim \exp\left[i c_s k t - \left(\frac{\zeta + \frac{2 (d-1)}{d}\eta}{\epsilon + P}\right)k^2 t\right]
\]
\color{blue}
\textbf{Shear waves}
\[
\int dx\, \sin kx\, \langle T^{01}(x,t) T^{01}(0,0) \rangle
\sim e^{-\frac{\eta}{\epsilon + P} k^2 t}
\]
\color{red}
\textbf{``Shear channel''}
\[
\langle T^{12}(\omega,k) T^{12}(\omega,k) \rangle
= P - i \eta\omega + O(\omega^2) + O(k)
\]
% Show exponential decays
\end{frame}

\begin{frame}{Air}
\small
Paradox: $\eta_{\mathrm{atm}} \sim 10^{-5}\frac{\mathrm{kg}}{\mathrm{m}\,\mathrm{s}}$ results in a sound attenuation time on the order of days, but everyday experience says it should be seconds.

\centering
\includegraphics[width=3in]{attenuation-air}

\footnotesize
(Figure from E.~M.~Viggen's thesis.)
\end{frame}

\begin{frame}{Shear viscosity}
% Intuition for shear viscosity
Two equivalent phrasings of what $\eta$ represents:
\centerline{{\color{green}Transport of $x$-momentum along $\hat z$} OR \color{blue}decay of shear waves}

\centering
\includegraphics[width=2.8in]{wave}
\end{frame}

\begin{frame}{Shear waves in a free theory}
Impart some momentum at $x=0$. After time $t$, what is the amplitude of the shear wave?
\[\color{blue}
C(t) \sim \int d^3 v \underbrace{\rho(v)}_{\sim e^{-v^2}} \cos(k v_z t)
\sim e^{-t^2}
\]
This decay is \emph{gaussian}, faster than any exponential. $\eta_{\mathrm{free}} = \infty$.

Physically: the transport of momentum (carried by individual particles) {\color{green}is entirely unobstructed}.

\vspace{0.5em}
\hrule
\vspace{0.5em}

Another example: in a rigid body, $\eta = \infty$. (Proof: pick up a pen.)
\end{frame}

\begin{frame}{A nontrivial lower bound on transport?}
In a free theory, particles are free, transport momentum efficiently. Sound waves decay super-exponentially.

In a rigid body, particles can't move at all. Phonons are free, and don't decay, so they transport momentum efficiently.

\begin{center}\textbf{\color{blue}In a rigid body, phonons are efficient transporters \emph{because} particles can't move.}
\end{center}

Blocking one mechanism of momentum transport opens up another one (by allowing sound waves to propagate without decaying).

A more careful version of this argument is in Kovtun, Moore, Romatschke, ``The Stickiness of Sound''.
\end{frame}

\begin{frame}{Status of the KSS conjecture}
\textbf{KSS Conjecture}: $\frac\eta s \ge \frac 1 {4\pi}$ in ``all'' theories.

Counterexample from T. Cohen (\texttt{arXiv:0702136}) involves a large number of {\color{red} weakly interacting} massive species.

In this family of models $\frac \eta s$ is made arbitrarily small, but shear waves still decay quickly.

\textbf{Moral}: bound the decay constant, not $\frac\eta s$!

\hrule
See e.g. \texttt{arXiv:2111.08158} (Lawrence) and \texttt{arXiv:2005.06482} (Baggioli and Li)
\end{frame}

\begin{frame}{Large-$N_f$ expansions}
Nonrelativistic fermions in three dimensions:
\tiny
\[
S =
\int_0^\beta \!d\tau \int \! d^3{\bf x} \left[\sum_f \psi^\dagger_{s,f}({\bf x}) \left(\partial_\tau-\frac{\nabla^2}{2m}-\mu\right)\psi_{s,f}({\bf x})+
   \sum_{f,f'}\frac{4 \pi a_s}{mN} \psi^\dagger_{\uparrow,f} ({\bf x})\psi^\dagger_{\downarrow,f'} ({\bf x})\psi_{\downarrow,f} ({\bf x})\psi_{\uparrow,f'} ({\bf x})\right]
\]
\centerline{(The limit $a_s \rightarrow -\infty$ is the unitary Fermi gas.)}
\normalsize
To expand in powers of $N^{-1}$, introduce an auxiliary field to make $N$ a parameter.
\small
\[
S_{\mathrm{eff}} =-N \left[\log\det\left(\partial_\tau-\sigma_z \frac{\nabla^2}{2m}-\sigma_z \mu + i\zeta^* \sigma_--i\zeta \sigma_+\right) -\int\frac{m \zeta \zeta^*}{4\pi a_s}\right]
\]
\color{green}Evaluate the path integral $Z = \int \mathcal D \zeta e^{-S_{\mathrm{eff}}}$ with a saddle-point expansion.
\end{frame}

\begin{frame}{Thermodynamic transport}
Not all LECs in the hydrodynamic expansion are specific to out-of-equilibrium physics.
\begin{itemize}
\item Pressure
\item Gravitational wave-to-matter coupling ($\kappa$)
\end{itemize}
These appear when the spacetime metric undergoes a time-independent perturbation.

Equivalently, these are detectable from fluctuations in thermodynamic equilibrium.
\end{frame}

\begin{frame}{Gravitational wave-to-matter coupling of the unitary Fermi gas}
\footnotesize
\[
T^{ij} \supset \kappa \left[R^{\langle i j \rangle} - 2 R^{t \langle i j \rangle t}\right]
\]
{\color{blue}Thermodynamic transport can be seen from \emph{Euclidean} correlators:}
\[
\kappa = \left.\frac{\partial^2}{\partial k^2} \langle T^{12}T^{12}\rangle(\omega = 0, k)\right|_{k=0}
\]
{\color{blue}For nonrelativistic fermions, the stress-energy tensor is:}
\tiny
\[
T^{12}
=\frac{1}{4m}\left[\partial_1 \Psi^\dagger \sigma_z \partial_2 \Psi + \partial_2 \Psi^\dagger \sigma_z \partial_1 \Psi
- \partial_1 \partial_2 \Psi^\dagger \sigma_z \Psi- \Psi^\dagger \sigma_z \partial_1 \partial_2 \Psi
  \right]-\frac{i s}{4m}\partial_k \Sigma_k
\]
\[
\text{where }\Sigma_3=\Psi \sigma_x (\partial_1-i\partial_2) \Psi+\Psi^\dagger \sigma_x (\partial_1+i \partial_2) \Psi^\dagger
\]
\footnotesize
{\color{blue}Evaluated at the saddle point:}
\tiny
\[\scriptscriptstyle
C(k) =
-\frac{2}{m^2} \int\! \frac{d^4 p}{(2\pi)^4} \frac{{\bf p}_1^2 {\bf p}_2^2 \left[(\epsilon_{\bf p}-\mu)(\epsilon_{\bf k+p}-\mu)-\omega^2-\Delta^2\right]}{\left[(\epsilon_{\bf p}-\mu)^2+\omega^2+\Delta^2\right]\left[(\epsilon_{\bf p+k}-\mu)^2+\omega^2+\Delta^2\right]}+
\frac{s^2 {\bf k}^2}{2 m^2}\int\!\frac{d^4 p}{(2\pi)^4}  {\bf p}_1^2{\rm tr} \left[\sigma_x G(\omega,{\bf p})\sigma_x  G(-\omega,-{\bf p})\right]
\]
\small
{\color{blue}Integrating:}
\[
\lim_{a_s \rightarrow -\infty} = \frac{(2 m \mu)^{\frac{3}{2}}}{3\pi^2 m} \frac{1}{\xi^{\frac{3}{2}}}\frac N {12} = \frac{n}{12m}
\]
\end{frame}

\begin{frame}{Viscosity from molecular dynamics}
From an equilibrated MD simulation, compute a time-series:
\[
f(T) = \sum_n p_x(T) \sin k_z x_z(T)
\]
Plot the autocorrelation and fit the decay to $f(T) \sim e^{-\frac{\eta k^2}{\rho} t}$:

\includegraphics[width=0.48\linewidth]{md_left}\hfil
\includegraphics[width=0.48\linewidth]{md_right}

\small{
(Inter-particle potential $V(r) \sim e^{-2r}$)
}
\end{frame}

\begin{frame}{Viscosity from a quantum computer}
\centerline{The best model of a cat is another cat. (\emph{Norbert Wiener})}
Quantum simulations are \emph{conceptually} simple.
\begin{itemize}
\item Physical Hilbert space $\mathcal H_P$; qubit Hilbert space $\mathcal H_C \approx \mathbb C^{2Q}$
\item Define an injective map $\mathcal H_P \rightarrow \mathcal H_C$
\item Decompose operators $H$ and $\mathcal O$ in 1- and 2-Pauli terms
\item Time-evolve, possibly with $H = H(t)$, and then measure!
\end{itemize}

Either measure $T^{01}$ repeatedly and look at autocorrelation (best on large systems), or explicitly measure $\langle T^{01} T^{01} \rangle$ via linear response (better on small systems).
\centerline{\small See \texttt{arXiv:2104.02024} (Cohen, Lamm, Lawrence, Yamauchi) for details.}
\end{frame}

\begin{frame}{Real-time path integrals}
Let's derive a (lattice) path integral for time-separated correlators.
\[
\langle \mathcal O(t) \mathcal O(0) \rangle
\propto \Tr (e^{-\delta H})^{\beta/\delta}
{\color{red}(e^{i H \delta})^{t/\delta}}
\mathcal O
{\color{red}(e^{-i H \delta})^{t/\delta}}
\mathcal O
\]
\[
\propto \int \mathcal D\phi
\overbrace{\langle \phi| e^{-\delta H}|\phi' \rangle
\cdots
\underbrace{{\color{red}\langle \phi'| e^{i H t}|\phi'' \rangle}
\cdots
{\color{red}\langle \phi''| e^{-i H t}|\phi''' \rangle}}_{\text{Complex!}}}^{p(\phi) \in \mathbb C}
\mathcal O(\phi'') \mathcal O(\phi''')
\]
The ``probability distribution'' is complex!

Moreover, if you replace the complex $p(\phi)$ with $|p(\phi)|$, almost all of the weight cancels. ($\int |p| \gg \int p$)
\vspace{.5em}
\hrule
\vspace{.5em}
(One can also study real-time correlators by analytic continuation. I have nothing to say about this here.)
\end{frame}

\begin{frame}{Cauchy's Theorem}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\center
		\includegraphics[scale=0.7]{cauchy1d}
	\end{column}
	\begin{column}{0.5\textwidth}
		For any holomorphic function $f(z)$:
		\[
			0 = \int_{\partial\Omega} f \;\d z
		\]
	\end{column}
\end{columns}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\[\int_{\gamma_1} f\;\d z = \int_{\gamma_2} f\;\d z\]
		If we can continuously deform $\gamma_1$ to $\gamma_2$, ``tracing out'' $\Omega$.
	\end{column}
	\begin{column}{0.5\textwidth}
		\center
		\includegraphics[scale=0.7]{cauchy1d2}
	\end{column}
\end{columns}
\centerline{\textbf{\color{blue}Physics is unchanged}, but \color{green}fluctuations can be removed ($\int |p(\phi)|$ changes)}
\end{frame}

\begin{frame}{Stokes' Theorem}
\[
\int \mathcal D \phi\, e^{-S(\phi)} = \int e^{-S(\phi)} + \nabla \cdot v(\phi)
\]
{\small This is a strict generalization of the Cauchy's theorem based methods.}
\centerline{\color{green}\textbf{Any} sign problem can be removed by some appropriate $v$.}

The vector field $v$ is responsible for representing the ``pure fluctuation'' part of the integrand.
\vspace{1em}

\centering
\includegraphics[width=1in]{dall-e-1}
\end{frame}

\begin{frame}{The transverse Ising model}
\[\color{blue}
H = - \mu \sum_r \sigma_x(r) - \sum_{\langle r r' \rangle} \sigma_z(r) \sigma_z(r')
\]
In one dimension, this is (dual to via Jordan-Wigner) a theory of free fermions. Tuning $\mu \rightarrow \mu_c$ makes the fermions massless. This is a free CFT.

In two dimensions, this theory is interacting. Tuning to $\mu_c \approx 3.044$ yields the $O(1)$ (Ising) CFT; also accessible from scalar field theory.

Thermodynamics and RG flows around the $O(N)$ models are very well understood (in $2+1$ dimensions):
\begin{itemize}
\item Bootstrap
\item Monte carlo
\item $\epsilon$ expansion
\end{itemize}
\end{frame}

\begin{frame}{Simulating the transverse Ising model}
\begin{columns}
\begin{column}{0.49\textwidth}
\begin{center}
\color{green}\textbf{Ising spin $\leftrightarrow$ qubit}
\end{center}
On a universal quantum computer, time-evolve via Suzuki-Trotter:
\[\color{blue}
e^{-i (A+B) \epsilon}
\approx
e^{-i A \epsilon}e^{-i B \epsilon}
\]
TIM is the first model with nontrivial hydrodynamics likely to be accessed.
\end{column}
\begin{column}{0.49\textwidth}
\centerline{\color{red}\textbf{Exponential-cost classical algorithm}}
Start with a Haar-random $|\Psi\rangle$.

Apply $e^{-\frac \beta 2 H}$ to obtain a thermal state. (Agrees with canonical ensemble in the large-volume limit.)

Apply operator $\mathcal O$.

{\color{blue}Apply $(1 - i \epsilon H)^{\frac t \epsilon}$ to time-evolve.}

Apply $\mathcal O$ again to get expectation value.
\end{column}
\end{columns}
\vspace{0.8em}
In both cases, evaluate $\langle T^{00}(t,x)T^{00}(0,0)\rangle$ and observe the decay.
\end{frame}

\begin{frame}{Sound attenuation in the transverse Ising model}
\centerline{\color{red}\textbf{In progress!}}
% plots
\centerline{
\includegraphics[width=2.3in]{ising_2_2.png}
\includegraphics[width=2.3in]{ising_3_3.png}
}
\centerline{
\includegraphics[width=2.3in]{ising_7_2.png}
\includegraphics[width=2.3in]{ising_7_2_hot.png}
}
\end{frame}

\begin{frame}{Large-N calculation in the $O(N)$ CFT}
From \texttt{arXiv:2104.06435} (Romatschke):
\[
\frac 1 N \left(\frac{\eta T}{\epsilon + P}\right)_{O(N)} = 0.42(1) + O(N^{-1})
\]
Recklessly extrapolating gives an Ising shear viscosity of
\[
4 \pi \frac{\eta T}{\epsilon + P} = 5.28(12)
\text.
\]
\centerline{\color{red}Do other methods agree?}
\end{frame}

\begin{frame}{The Shape of Things To Come}
\begin{itemize}
\item Most analytic methods are limited to narrow regimes. (Weak coupling; large N; holographic theories.)
\item Scalable quantum computers will come online \emph{eventually}; will they come before efficient classical methods are available?
\item Machine learning for sign problems
\item Bootstrap methods
\end{itemize}
\centerline{\color{blue}How slow can transport be?}
\end{frame}

\begin{comment}
% Disable frame numbering.
\setbeamertemplate{footline}{}
\makeatletter
\beamer@noframenumberingtrue
\undef\beamer@noframenumberingfalse
\def\beamer@noframenumberingfalse{}
\makeatother

\bgroup
\setbeamercolor{background canvas}{bg=black}
\setbeamertemplate{navigation symbols}{}
\begin{frame}[plain]
\end{frame}
\egroup

\begin{frame}
	\frametitle{Outline}
	%\begin{multicols}{2}
		\tableofcontents
		%\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extra slides
\end{comment}

\end{document}
