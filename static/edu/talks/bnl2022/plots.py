#!/usr/bin/env python

import numpy as np
import matplotlib
from matplotlib import colors
import matplotlib.pyplot as plt

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

def read_data(f, dtype=np.float64):
    return np.array([[dtype(x) for x in l.split()] for l in f.readlines()])

with open('exact_2_2_0.5.dat') as f:
    data_2_2_hot = read_data(f)
with open('exact_2_2_0.25.dat') as f:
    data_2_2_cold = read_data(f)

with open('exact_3_3_0.5.dat') as f:
    data_3_3_hot = read_data(f)
with open('exact_3_3_0.25.dat') as f:
    data_3_3_cold = read_data(f)

with open('mc_7_2.dat') as f:
    data_7_2 = read_data(f)
with open('mc_7_2_hot.dat') as f:
    data_7_2_hot = read_data(f)

plt.figure(figsize=(5.5, 3.6))
plt.plot(data_2_2_hot[:,0], data_2_2_hot[:,1], label='$T=0.5$')
plt.plot(data_2_2_cold[:,0], data_2_2_cold[:,1], label='$T=0.25$')
plt.xlabel('$t$')
plt.ylabel('$C(k=\\frac{2\\pi}{L},t)$')
plt.legend(loc='upper right')
plt.title('$2 \\times 2$')
plt.tight_layout()
plt.savefig('ising_2_2.png')

plt.figure(figsize=(5.5, 3.6))
plt.plot(data_3_3_hot[:,0], data_3_3_hot[:,1], label='$T=0.5$')
plt.plot(data_3_3_cold[:,0], data_3_3_cold[:,1], label='$T=0.25$')
plt.xlabel('$t$')
plt.ylabel('$C(k=\\frac{2\\pi}{L},t)$')
plt.legend(loc='upper right')
plt.title('$3 \\times 3$')
plt.tight_layout()
plt.savefig('ising_3_3.png')

plt.figure(figsize=(5.5, 3.6))
plt.errorbar(data_7_2[:,0], data_7_2[:,1], data_7_2[:,3], fmt='.')
plt.xlabel('$t$')
plt.ylabel('$C(k=\\frac{2\\pi}{L},t)$')
plt.title('$7 \\times 2$ ($T = 0.5$)')
plt.tight_layout()
plt.savefig('ising_7_2.png')

plt.figure(figsize=(5.5, 3.6))
plt.errorbar(data_7_2_hot[:,0], data_7_2_hot[:,1], data_7_2_hot[:,3], fmt='.')
plt.xlabel('$t$')
plt.ylabel('$C(k=\\frac{2\\pi}{L},t)$')
plt.title('$7 \\times 2$ ($T = 2.$)')
plt.tight_layout()
plt.savefig('ising_7_2_hot.png')


