\documentclass[]{beamer}

\usepackage{amsmath}
\usepackage{slashed}
\usepackage{tikz}
\usepackage{multicol}
\usepackage{comment}
\usepackage{mathtools}
\usepackage{cancel}
\usepackage{soul}

\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{textcomp}

%\setlength{\parskip}{1em}

% Colors
\definecolor{red}{rgb}{0.8,0,0}
\definecolor{green}{rgb}{0.0,0.5,0}
\definecolor{blue}{rgb}{0.2,0.2,1.0}

% Math declarations
\let\Im\relax
\let\Re\relax
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Im}{Im}
\DeclareMathOperator{\Re}{Re}
\renewcommand{\d}{\mathrm d}

% Maintenance macros
\newcommand{\todo}{\colorbox{pink}{\textsc{TODO}}}

\usetheme[sectionpage=none]{Singapore}
\usefonttheme{structuresmallcapsserif}

\setbeamertemplate{footline}[frame number]
\setbeamertemplate{navigation symbols}{}

%\setbeamercolor{progress bar}{fg=gray,bg=black}

\begin{document}
\setlength{\parskip}{0.6\baselineskip}

\title{Machine learning approaches for\\ sign and signal-to-noise problems}
\author{\textbf{Scott Lawrence}}
\institute{Santa Fe}
\date{10 August 2023}

\bgroup
\setbeamertemplate{footline}{}
\begin{frame}[noframenumbering]
	\titlepage
	\begin{tikzpicture}[overlay, remember picture]
		\node[anchor=center] at (2.5,1.0) {\includegraphics[height=0.3in]{cuboulder}};
		\node[anchor=center] at (9.0,1.0) {\includegraphics[height=1.7in]{ml}};
	\end{tikzpicture}
\end{frame}
\egroup

\begin{frame}{The scope of this talk}
Algorithms \emph{\color{blue}in the usual framework of lattice field theory}:
\begin{enumerate}
\item Write down a path integral
\item Perform Monte Carlo sampling
\item Look at some expectation value
\end{enumerate}
The machine learning method may alter \textbf{\color{blue}any} of these three steps.
\vspace{0.6em}
\hrule
\vspace{0.6em}
{\color{green}Many more exotic algorithms are possible!}

In fact, the worse your sign problem is, the more you should care about those algorithms, relative to those in this talk. (More on this later.)
\end{frame}

\begin{frame}{Examples of "sign problems"}
\textbf{\color{green}Length scaling}:
\begin{itemize}
\item Most correlators (famously antiproton-proton)
\item Static quark potential
\end{itemize}
\textbf{\color{blue}Spacetime volume scaling}:
\begin{itemize}
\item Complex couplings
\item Finite fermion density
\end{itemize}
\textbf{\color{red}Lattice volume scaling}:
\begin{itemize}
\item Schwinger-Keldysh (?)
\end{itemize}
\end{frame}

\begin{frame}{Cauchy's theorem}
\textbf{Stokes' theorem:}
\[
\int_{\partial \Omega} \omega = \int_\Omega d \omega
\]
When $f(z)$ is holomorphic, $d(f dz) = - \frac{\partial f}{\partial \bar z} dz \wedge d\bar z = 0$.

So, when $\gamma$ and $\gamma'$ differ by a boundary $\partial\Omega = \gamma - \gamma'$:
\[
\int_\gamma f(z) dz = \int_{\gamma'} f(z) dz
\]
We use $f = e^{-S}$ or $f = e^{-S} \mathcal O$.

\centering
\includegraphics[width=1.8in]{cauchy}
\end{frame}

\begin{frame}{Contour deformations for sign problems}
\centerline{\emph{\color{red}This discussion is entirely ahistorical!}}
We want to compute
\[
\langle \mathcal O \rangle = \frac{\int e^{-S} \mathcal O}{\int e^{-S}}
\]
But $e^{-S}$ isn't (proportional to) a probability distribution. {\color{blue}Reweight}:
\[
\langle \mathcal O \rangle = \frac{\Big\langle e^{-i \Im S} \mathcal O \Big\rangle_Q}{\Big\langle e^{-i \Im S}\Big\rangle_Q}
\]
The denominator is the ``average sign'' $\langle\sigma\rangle$---and because of the $\Im S$, it's not holomorphic. \textbf{\color{blue}The performance of the algorithm depends on the choice of contour.}
\centerline{\emph{\color{green}Similar reasoning holds for signal-to-noise problems.}}
\end{frame}

\begin{frame}{Machine learning of contour deformations}

{\small Developed 2017-2018:\\
\hspace{1.5em}\texttt{arXiv:1705.05605} (Mori, Kashiwa, Ohnishi)\\
\hspace{1.5em}\texttt{arXiv:1804.00697} (Alexandru, Bedaque, SL, Lamm)}

Define a smooth family of contours, parameterized by $\lambda$.

We want to maximize $\langle \sigma \rangle \equiv \frac{Z}{Z_Q}$---and $Z$ won't change. So {\color{blue}minimize $Z_Q(\lambda)$}.

{\color{red} Measuring $\langle \sigma \rangle$ is hard.}

{\color{green} But, $\nabla_\lambda \log Z_Q$ is just a quenched expectation value:
\[
\nabla_\lambda \log Z_Q = - \frac 1 {Z_Q} \int \left(\nabla_\lambda \Re S\right) e^{-\Re S}
\]}

So, measure $\nabla_\lambda \log Z_Q$ and perform gradient descent directly.
\end{frame}

\begin{frame}{Contour deformations in practice}

\centerline{\includegraphics[width=3in]{thirring}}
\centerline{\scriptsize{arXiv:1808.09799} (Alexandru, Bedaque, Lamm, SL, Warrington)}
\begin{columns}
\begin{column}{0.49\textwidth}
\centering
\centerline{\includegraphics[width=0.65\linewidth]{kroyter}}
{\scriptsize \texttt{arXiv:1805.04941} (Bursa, Kroyter)}
\end{column}
\begin{column}{0.49\textwidth}
\includegraphics[width=0.85\linewidth]{cs}
\scriptsize{\texttt{arXiv:2205.12303}\\\hspace{1em} (SL, Oh, Yamauchi)}
\end{column}
\vspace{-1em}
\end{columns}

\vspace{8pt}
\centerline{\color{red}\textbf{The dirt}: most (all?) useful contour deformations are few-parameter}
\end{frame}

\begin{frame}{The connection to normalizing flows}
\textbf{Normalizing flow}: A map $x \mapsto z(x)$ such that the induced probability distribution is $p(z) = e^{-S(z)}$
\[
p(z) dz = e^{-x^2 / 2} dx
\]

{\color{blue} \footnotesize Extensively applied in lattice field theory. \texttt{arXiv:1904.12072, 2003.06413, 2008.05456, 1910.13496, 2007.07115}}...

What happens when $z(x)$ is complex? It parameterizes a contour!

\centerline{\color{green}This contour has no sign problem.}

Argument from analytic continuation: \textbf{\color{blue}for actions sufficiently close to real, perfect contours exist}. See \texttt{arXiv:2101.05755} (SL, Yamauchi).
\end{frame}

\begin{frame}{More about contour deformations}
\color{green}
Sometimes, ``perfect'' contours exist. On these, $e^{-S} dz$ never changes phase. \color{red}\textbf{Sometimes not}.

\vspace{0.5em}\pause\hrule\vspace{.5em}

\color{blue}
On a ``locally perfect'' contour $e^{-S} dz$ only changes phase where $e^{-S} = 0$.

\vspace{0.5em}\pause\hrule\vspace{.5em}

\color{red}
Lefschetz thimbles are locally perfect contours in the limit $\hbar \rightarrow 0$.

\vspace{0.5em}\pause\hrule\vspace{.5em}

\color{black}
``Locally perfect'' contours are also ``locally optimal''. This should not be obvious. Note the different meaning of `local'!
\end{frame}

\begin{frame}{Disadvantages of contour deformations}
\[
\int e^{-S[z(x)]} \bigg| {\color{red}\det \frac{\partial z}{\partial x} \bigg|}
\]
The determinant is cubic-ish {\color{blue}in the number of degrees of freedom}.

\pause
\vspace{0.8em}
\hrule
\vspace{0.5em}
\emph{Best possible contour} has $\frac Z {Z_Q} = \langle \sigma \rangle < 1$ (often $\ll 1$).

\textbf{Proof strategy}: we want to establish a contour independent lower bound on $Z_Q$. Find a differential form $\alpha$ such that:
\begin{itemize}
\item $|\alpha| \le |e^{-S} dz|$.
\item $\alpha$ is closed ($d \alpha = 0$)
\end{itemize}
Then $Z_Q = \int_\gamma |e^{-S} dz| \ge \int_\gamma |\alpha| \ge \big|\int\alpha\big|$.

\vspace{1.1em}
\centerline{\color{green}\texttt{arXiv:2308.xxxxx} (SL, Yamauchi)}
\vspace{0.5em}

\centerline{General expectation: {\color{red}exponential residual sign problem}}
\end{frame}

\begin{frame}{Control variates}
Let $f$ be some observable obeying $\langle f \rangle = 0$. Then $\langle \mathcal O - f \rangle = \langle \mathcal O \rangle$.

But, the variance might be different. {\color{green}Find $f$ such that $\langle \mathcal O f \rangle$ is large.}

To guarantee that $f$ has vanishing EV, construct it as $f e^{-S} = \partial g$.

\centerline{\color{blue}\textbf{Open}: are other constructions of CVs useful?}
\vspace{.8em}

\hrule

A sign problem is two signal-to-noise problems:
\[
\langle \mathcal O \rangle = \frac{\langle {\color{red}e^{-i \Im S}} \mathcal O\rangle_Q}{\langle {\color{red}e^{-i \Im S}}\rangle_Q} = \frac{\text{\color{red}noisy}}{\text{\color{red}noisy}}
\]
{\color{blue}\textbf{Open problem}: can we use the same CV for both?}

\centerline{\scriptsize See \texttt{arXiv:2009.10901} (SL) and \texttt{arXiv:2212.14606} (SL, Yamauchi) for early attempts.}
\end{frame}

\begin{frame}{Trivia about control variates}

{\color{green}
Every sign problem can be exactly removed by some CV.
\[
\int e^{-S} \longrightarrow \int \left[e^{-S} - \frac{\int e^{-S}}{\int 1}\right]
\]
}

\vspace{0.3em}\pause\hrule\vspace{.3em}

{\color{red}Perfect CVs are ``rare'': they occupy an exponentially (V) small region of the space of all possible functions. $f = e^{-S} + O(e^{-V})$}

\vspace{0.3em}\pause\hrule\vspace{.3em}

\color{blue}Every function that integrates to zero can be written as a total divergence. (We are trying to write an approximate antiderivative of the Boltzmann factor.)


\end{frame}

\begin{frame}{Lattice Schwinger-Dyson relations}
Integrals of total derivatives vanish\footnote{Terms and conditions apply.}:
\[
0 = \int \partial \left(e^{-S} f\right)
\]
So, for any first-order derivative with respect to the fields:
\[
\langle \partial f \rangle = \langle f \partial S\rangle
\]
\hrule
{\color{blue}Example: $\phi^4$ field theory.} Take $f = \phi_y$ and $\partial = \frac{\partial}{\partial \phi_x}$.
\[
0 = \langle F_{x,y} \rangle = \delta_{x,y} - \Big\langle \phi_y \frac{\partial S(\phi)}{\partial \phi_x}\Big\rangle
\]
After translational invariance, we have a $V$-dimensional vector space of vanishing observables.
\end{frame}

\begin{frame}{CVs for Scalar Field Theory}
On a $V$-site lattice, using translational invariance, there are $V$ first-order S-D relations.
Minimize the standard deviation:
\[\color{blue}
E(c) = \langle \tilde{\mathcal O}^2 \rangle
= c^T M c + 2 v^T c
\]
This is just solving a linear system:
\[
\color{green}
c = M^{-1} v \text{ with } M_{ij} = \langle F_i F_j \rangle \text{ and } v_i = \langle \mathcal O F_i\rangle
\]
\vspace{-.5em}
\begin{columns}
\begin{column}{0.65\textwidth}
\centerline{
\includegraphics[width=2.6in]{small}}
\end{column}
\begin{column}{0.34\textwidth}
$8\times 8$ lattice, with\\
$m^2 = 0.1$ and $\lambda = 0.5$\\
Interaction is $\frac{\lambda}{24} \phi^4$.
\end{column}
\end{columns}
\centerline{\texttt{arXiv:2307.14950} (Tanmoy Bhattacharya, SL, Jun-Sik Yoo)}
\end{frame}

\begin{frame}{$L^1$ Regularization}
To avoid overfitting, demand that the vector of coefficients be \textbf{\color{blue}sparse} (in momentum space).
\[
E_\mu(c) = E_0(c) + \mu \sum_i |c_i|
\]
\vspace{-.5em}
\centering{
\includegraphics[width=2.4in]{sparse}
}
\centerline{$50 \times 50$ lattice with $m^2 = \lambda = 0.1$}
\[
    S = \sum_{\langle r ,r'\rangle} \frac {\left(\phi(r) - \phi(r')\right)^2}{2}
    + \sum_r \left[\frac{m^2}{2} \phi(r)^2 + \frac{\lambda}{24!} \phi(r)^4\right]
\]
\end{frame}

\begin{frame}{A question of asymptotics}
\begin{columns}
\begin{column}{0.49\textwidth}
\centerline{\textsc{\color{blue}Proton correlator}}

Signal falls as $e^{-m_p}$.

Noise falls as $e^{-3 m_\pi}$.

\[
\text{S/N} \sim
e^{-(500\,\text{MeV})L}
\]

At times of $4\,\text{fm}$ the ratio falls by $\sim 5 \times 10^{-5}$.
\end{column}
\pause
\vrule\hspace{0.1em}
\begin{column}{0.49\textwidth}
\centerline{\textsc{\color{blue}Finite-density QCD}}
Average phase scales as
\[
\langle \sigma \rangle \sim e^{-(m_\pi L)^4}
\]

With $L = 4\,\text{fm}$, the phase falls by {\color{red}$\sim 10^{-23}$}.

(This $L$ is likely not sufficient!)
\end{column}
\end{columns}
\vspace{1em}
\color{green}``Stacking tricks'' likely works for many signal-to-noise problems.

\color{red}\textbf{Not so for sign problems in $3$ and $4$ dimensions.}
\end{frame}

\begin{frame}{Consequences for algorithm design}
Suppose the time taken by the algorithm is $O\big((mL)^\alpha e^{(m L)^4}\big)$.
\[
\alpha = 3 \text{ and } mL = 3
\]
We have the opportunity to change $\alpha$ to 0, while raising $m$ by $1\%$. \textbf{\color{blue}Should we?}
\pause
\vspace{1.5em}

\begin{columns}
\begin{column}{0.49\textwidth}
\centerline{\textsc{\color{green}Before}}
\[(mL)^3 \sim 27\]
\[
e^{(mL)^4} \sim 1.5 \times 10^{35}
\]
\[\text{cost} \sim 3.5 \times 10^{36}\]
\end{column}
\vrule
\begin{column}{0.49\textwidth}
\centerline{\textsc{\color{red}After}}
\[(mL)^0 \sim 1\]
\[
e^{(1 + 4 \times 10^{-2}) (mL)^4} \sim 4 \times 10^{36}
\]
\[\text{cost} \sim 4 \times 10^{36}\]
\end{column}
\end{columns}
\vspace{1em}

\centerline{\large\textbf{\color{red}This is not a good trade}}
\end{frame}

\begin{frame}{Lee-Yang Zeros}
\[
S = -J \sum_{\langle x,y\rangle} s_x s_y - \color{red}h \sum_x s_x
\]
At complex $h$, we have a sign problem.

The partition function $Z(h)$ is a polynomial in the {\color{blue}fugacity} $z = e^{h}$. It is completely characterized by its zeros.\\
{\footnotesize \color{blue} Yang, Lee. \textit{Statistical Theory of Equations of State and Phase Transitions}}

All the zeros occur on the imaginary-$h$ axis.

We can find these zeros by measuring
\[
\langle e^{i h \sum s} \rangle_Q \equiv
\frac{\sum_s e^{J \sum s s + h \sum s}}{\sum_s e^{J \sum s s}}
\propto
Z(h)
\]
(Again, at {\color{red}pure imaginary} $h$).

\centerline{\textbf{A signal-to-noise problem with exponential scaling in volume.}}
\end{frame}

\begin{frame}{eXtreme learning machines}
Popularized around 2006; see Huang, Zhu, and Siew (2006).

\centerline{\emph{``Neural networks for the pathologically lazy''}}

Initialize a large neural network with $N$ outputs. 

\emph{Only train the last layer}. Training is now fast and deterministic (inverting a matrix).

To make up for the lack of tuning on interior layers, we have a much wider network.

\vspace{0.5em}
\hrule
\vspace{0.5em}
\centerline{\textsc{\color{blue}From ELMs to CVs}}
Each output of the network is a function $g_i$. Define discrete derivative $\Delta f = f(1) - f(-1)$.

Differentiate the $g_i$ with respect to a single site, to obtain $f_i = \Delta_0 g_i$.

Average over all translations to obtain the basis $F_i$.
\end{frame}

\begin{frame}{Comparison with transfer matrix}
On an $8 \times 8$ lattice, the Ising model can be solved exactly via the transfer matrix (even at complex magnetic field).

We simulate at $J=0.4$, a little hotter than the critical $J_c \sim 0.44$.


\begin{columns}
\begin{column}{0.34\textwidth}
\parskip 0.4em
\scriptsize{
One nonlinear (CELU) layer

Width is 640

Inputs are designed based on the high-temperature expansion.

$10^4$ samples
}
\end{column}
\begin{column}{0.64\textwidth}
\centering
\includegraphics[width=2.5in]{check}
\end{column}
\end{columns}

\centerline{\texttt{arXiv:2308.xxxxx} (SL, Yamauchi)}
\end{frame}

\begin{frame}{Roads \st{not} taken, but not discussed}
\textbf{\color{blue}Neural network quantum states}
\begin{itemize}
\item \texttt{arXiv:1606.02318} (Carleo and Troyer)
\item \texttt{arXiv:1912.08831} (Gutierrez and Mendl)
\end{itemize}

\textbf{\color{green}Semidefinite programming}
\begin{itemize}
\item \texttt{arXiv:1106.4966} (Barthel and Hubener)
\item \texttt{arXiv:2211.08874} (SL)
\end{itemize}

And many others...
\end{frame}
\end{document}
