#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-1,1,100)

plt.figure(figsize=(4,3))
plt.vlines([-1,1],-2,2,color='red')
plt.plot(x,x)
plt.fill_between(x,x,2,facecolor='#bbbbff')
plt.ylabel('$\\langle H \\rangle$')
plt.xlabel('$\\langle \\sigma_x \\rangle$')
plt.tight_layout()
plt.savefig('vis.png', dpi=300, transparent=True)
