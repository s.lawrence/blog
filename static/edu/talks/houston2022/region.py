#!/usr/bin/env python

# Based on arXiv:2108.08757

import numpy as np

# Hamiltonian parameters
g = 0.5
h = 0.5

import matplotlib.pyplot as plt
plt.figure(figsize=(4,3))
for N in [4,6]:

    def ok(H,x2):
        # Generate moments
        x4 = (H - 2 * g * x2) / (3*h)

        e = np.zeros(2*N)
        e[0] = 1
        e[2] = x2
        e[4] = x4
        for m in range(3,2*N-3):
            e[m+3] = (1/4*m*(m-1)*(m-2)*e[m-3] + 2*m*H*e[m-1] - (2*g+2*m*g)*e[m+1]) / (4*h + 2*m*h)

        M = np.zeros((N,N))
        for i in range(N):
            for j in range(N):
                M[i,j] = e[i+j]

        vals, _ = np.linalg.eig(M)
        if np.all(vals >= 0):
            return 1
        else:
            return 0

    #H = np.linspace(0,12,500)
    #X2 = np.linspace(0,3,500)
    H = np.linspace(0,2,500)
    X2 = np.linspace(0,1,500)
    H,X2 = np.meshgrid(H,X2)

    plt.contour(H,X2,np.vectorize(ok)(H,X2),levels=[0.5])

plt.xlabel('E')
plt.ylabel('$\\langle x^2 \\rangle$')
plt.tight_layout()
plt.savefig('region.png', dpi=300, transparent=True)
#plt.show()

