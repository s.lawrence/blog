from matplotlib.pyplot import *
from numpy import *

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

figure(figsize=(3.0,2.0), dpi=450)
x = linspace(-5,5,1000)
plot(x, 0.*x)
plot(x, 0.*x+2)
xlabel('$\mathrm{Re}\; x$')
ylabel('$\mathrm{Im}\; x$')
xlim([-4,4])
ylim([-0.5,3.5])
tight_layout()
savefig('gaussian-contour.png', transparent=True)
