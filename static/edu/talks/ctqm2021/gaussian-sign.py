from matplotlib.pyplot import *
from numpy import *

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

figure(figsize=(3.0,2.0), dpi=450)
x = linspace(-5,5,1000)
plot(x, exp(-x**2 - 2j * 2 * x))
plot(x, exp(-x**2)*exp(-4))
xlabel('$x$')
ylabel('$e^{-S(x)}$')
xlim([-4,4])
tight_layout()
savefig('gaussian-sign.png', transparent=True)
