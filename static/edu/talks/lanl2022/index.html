<!DOCTYPE html>
<html>
  <head>
    <title>
      Expanding the Applicability of Lattice Methods
    </title>
    <link rel="stylesheet" href="present.css" />
    <script src='present.js'></script>
    <link rel="stylesheet" media="print" href="print.css" />
    <script async src='/mathjax/tex-svg-full.js'></script>
  </head>
  <body>
    <noscript>
      <div style="font-size: 30pt;">Javascript really is required, sorry</div>
    </noscript>

    <!-- TITLE -->
    <header id="title">
      <h1>Expanding the Applicability of Lattice Methods</h1>
      <div><img src="title.svg" style="width: 50vh; text-align: center;"/></div>
      <div class="author">Scott Lawrence</div>
      <div>February 18, 2022</div>
      <div>
        <img src="boulder.png" style="width: 35vh; padding: 0 1em" />
      </div>
      <div></div>
    </header>

    <section id="lattice">
      <header><h1>Lattice Field Theory, Briefly</h1></header>
      <p>To calculate <span style="color:green">\(\langle \mathcal O \rangle \equiv \frac 1 Z \mathrm{Tr}\; e^{-\beta H} \mathcal O\)</span>:
      \[
      \mathrm{Tr}\; e^{-\beta H} = \mathrm {Tr}\; \big(e^{-\delta H}\big)^{\beta/\delta} =
      \sum_{\psi_1,\psi_2,\ldots} 
      \langle \psi_1 | e^{-\delta H} | \psi_2 \rangle \langle \psi_2 | e^{-\delta H} \cdots | \psi_1 \rangle
      \]
      <p>To evaluate \(\langle \psi' | e^{-\delta H} | \psi\rangle\), use \(\delta \ll 1\):
      <div class="flex-horiz">
        \[
        e^{-\delta H} = 1 - \delta H + \cdots
        \]
        \[
        e^{-\delta (K + V)} \approx e^{-\delta V / 2} e^{-\delta K} e^{-\delta V / 2}
        \]
      </div>
      <p>Lo! <span style="color:blue;font-weight:bold">Feynman's path integral:</span>
      \[
      \langle \mathcal O \rangle \propto \int \mathcal D \psi \;e^{- S[\phi]}
      \]
      <hr>
      <p>Evaluate with Monte Carlo... unless \(\langle \cdot | e^{-\delta H} | \cdot \rangle \) is not positive!
      <div class="flex-horiz" style="color:red; font-weight: bold">Sign Problem</div>
      <footer></footer>
    </section>

    <section id="real-time">
      <header><h1>Real-Time Linear Response</h1></header>
      <p>Finite-temperature QCD is well described in the IR by (relativistic) hydrodynamics:
      \[

          \rho \left(\frac{\partial u_i}{\partial t} + u_j \nabla_j u_i\right) + \nabla p
              =
              {\color{blue}\eta \partial_j \left(\partial_i u_j + \partial_j u_i\right)}
                      +
                          \left(\zeta- \frac 2 3 \eta\right) \partial_i \partial_j u_j
      \]
      <p>Shear waves decay with rate \(\propto \eta\):
      \[
      \langle \hat T_{01}(k,t)^\dagger\; \hat T_{01} (k,0)\rangle \sim e^{-\frac{\eta}{\rho} k^2 t}
      \]
      <p>Extracting the shear visocsity requires evaluating time-separated observables:
      \[
      \langle \mathcal O(t) \mathcal O(0) \rangle \propto {\mathrm{Tr}\;} e^{-\beta H} {\color{red}e^{i H t}} \mathcal O {\color{red}e^{-i H t}} \mathcal O
      \]
      <p>Trotterizing the time-evolution operator induces a sign problem:
      \[
      \langle \psi' | e^{-i \delta H} | \psi \rangle  \not \in \mathbb R
      \]
      <footer></footer>
    </section>

    <section id="finite-density">
      <header><h1>Finite Density Fermions</h1></header>
      <div class="flex-horiz" style="margin: 2vh">
        <div>
          <p>Monte Carlo (i.e. lattice QCD) methods are powerful, but fail for relativistic theories at finite fermion (proton - antiproton) density.
          <p>First noted in 1990. Not well-understood in general.
          <p>Makes it difficult to study:
          <ul>
            <li>Superconductors (and other strongly interacting electrons)</li>
            <li>Large nuclei</li>
            <li><b>Dense nuclear matter (neutron stars)</b></li>
          </ul>
        </div>
        <div style="border-left: 1px solid #222; padding-left: 1vh">
          <div class="flex-horiz"><img src="neutron-star.jpg" style="width: 25vh; padding: 2vh"/></div>
          <div class="flex-horiz"><img src="mr.jpg" style="width: 60vh"/></div>
        </div>
      </div>
      <footer></footer>
    </section>

    <section id="quantum-computing">
      <header><h1>The Elephant in the Room</h1></header>
      <div class="flex-horiz">
        <img src="qcpaper1.png" width="40%" />
        <img src="qcpaperglue.png" width="40%" />
      </div>
      <div class="flex-horiz">
        <img src="qctrailhead.png" width="40%" />
        <img src="qctransport.png" width="40%" />
      </div>
      <div class="flex-horiz">
        <img src="qcparton.png" width="40%" />
        <img src="qcdecay.png" width="40%" />
      </div>
      <hr>
      <p style="text-align: center; color: green">Solves the real-time sign problem.</p>
      <p style="text-align: center; color: red">Solves the fermion sign problem <b>if equilibration is fast</b>.</p>
      <footer></footer>
    </section>

    <section id="part-1">
      <div></div>
      <h1 style="text-align: center;">Part I: Contour Deformations</h1>
      <div></div>
    </section>

    <section id="gaussian-example">
      <header><h1>The Easiest Sign Problem</h1></header>
      <div class="flex-horiz" style="font-size: 80%">
      \[
      Z = \int d x\; e^{-x^2 - {\color{red}2 i \alpha x}}
      \]
      \[\color{blue}
      Z_Q = \int d x \; e^{-x^2}
      \]
      </div>
      <div class="flex-horiz"><div style="width: 70vh; padding: 2vh 0">
      <p>We must sample with respect to the <span style="color:blue">quenched</span> Boltzmann factor. Observables are computed via
      \[
      \langle \mathcal O \rangle = \frac{\langle\mathcal O e^{-i S_I} \rangle_Q}{\langle e^{-i S_I}\rangle_Q}
      \]
        </div><div>
      <p>The sign problem is measured by
      \[
      \langle \sigma\rangle \equiv \frac Z {Z_Q} \sim e^{-\alpha^2}
      \]</div></div>
      <hr>
      <div class="flex-horiz">
      <img src="gaussian-sign.png" style="width: 45vh"/>
      <img src="gaussian-contour.png" style="width: 45vh"/>
      </div>
      <footer></footer>
    </section>

    <section id="contour-deformations">
      <header><h1>Contour Integrals for the Sign Problem</h1></header>
      <div class="flex-horiz">
        <div style="margin: 0 5vh">
          <p>The Boltzmann factor \(e^{-S}\) is complex. Sample with \(e^{-S_R}\) and reweight.
          \[
          \langle\sigma\rangle = \frac{\int e^{-S}}{\int |e^{-S}|}
          \]
        </div>
        <div style="margin: 0 5vh">
          <img src="cauchy1d2.svg" style="width:40vh"/>
        </div>
      </div>
      <div class="flex-horiz">
        <span style="font-weight: bold; color:green; margin: 0 5vh">
          Theorem: the integral of a holomorphic function is unchanged by contour deformation.
        </span>
      </div>
      <footer></footer>
    </section>

    <section id="fermion-contours">
      <header><h1>Early Success: Thirring Model</h1></header>
      <div class="flex-horiz">
      <img src="thirring.png" width="80%" />
      </div>
      <hr>
      <p style="text-align: center; color: blue; font-weight: bold">Key Technology: Gradient Descent</p>
      <p>Minimize \(Z_Q \equiv \int |e^{-S}|\).
      <p style="color:green">This is possible: \(\partial \log Z_Q\) is a sign-free observable.
      <footer>See arXiv:1804.00697</footer>
    </section>

    <section id="do-manifolds-exist">
      <header><h1>Do Perfect Manifolds Exist?</h1></header>
      <p><b>Wanted</b>: a manifold such that
      \[
      \left|\int e^{-S} \;d z\right|=\int \left|e^{-S} \;dz\right|
      \]
      <div class="flex-horiz"><span style="font-weight: bold; color: blue">This does not automatically solve the sign problem!</span></div>
      <ul>
        <li>Perfect manifolds might be hard to find</li>
        <li>Hard to sample from</li>
        <li>Observables may have signal-to-noise problem</li>
      </ul>
      <footer></footer>
    </section>

    <section id="example-polynomial">
      <header><h1>Example: One-Dimensional Integrals</h1></header>
      \[
      Z = \int dz\;e^{-z^2 - \lambda e^{i\theta} z^4}
      \]
      <div class="flex-horiz">
        <img style="width:80vh" src="manifolds1d.png" />
      </div>
      <footer></footer>
    </section>

    <section id="example-thimbles">
      <header><h1>Example: One-Dimensional Integrals</h1></header>
      \[
      Z = \int dz\;e^{z^2 - e^{i} z^4 - i z^3}
      \]
      <div class="flex-horiz">
        <img style="width:80vh" src="thimbles1d.png" />
      </div>
      <footer></footer>
    </section>

    <section id="example-cos">
      <header><h1>Example With No Perfect Manifold</h1></header>
        \[ Z = \int (\cos \theta + \epsilon) \;d \theta\]
      <div class="flex-horiz">
        <div>
          <p> \[Z = 2 \pi \epsilon\]
          <p> Quenched partition function is \(O(1)\)
        </div>
        <div>
          <img src="cos-example.png" style="width:70vh" />
        </div>
      </div>
      <hr>
      <div class="flex-horiz">
        <span style="margin: 0 18vh; color:red; font-weight:bold">Similar methods reveal that no manifold exists for the mean-field Thirring model.</span>
      </div>
      <footer></footer>
    </section>

    <section id="following">
      <header><h1>Following Perfect Manifolds</h1></header>
      <div class="flex-horiz">
        <span style="font-weight: bold; color: green">
          Locally perfect manifolds always exist.
        </span>
      </div>
      <div class="flex-horiz">
        <img src="perfects.svg" />
      </div>
      <div class="flex-horiz">
        <span style="font-weight: bold; color: blue; margin: 0 5vh">
          Conjecture: Locally perfect manifolds exist, and depend smoothly on parameters in the action.
        </span>
      </div>
      <p>If that's true, how can a global sign problem be created?
      <footer>See 2101.05755</footer>
    </section>

    <section id="following-cos-failure">
      <header><h1>Creation of a Global Sign Problem</h1></header>
      <p>\[ \int \cos\theta + \epsilon \]
      <p>What happens as we vary \(\epsilon\)?
      <p>A global sign problem is created as we cross \(\epsilon_c = 1\).
      <div class="flex-horiz"><img src="global-creation.svg" style="width: 120vh"/></div>
      <div class="flex-horiz">
        <span style="color: blue; font-weight: bold">Global sign problem is created by the motion of the zeros</span>
      </div>
      <footer></footer>
    </section>

    <section id="bosonic-zeros">
      <header><h1>Singularities of a Bosonic Action</h1></header>
      <div class="flex-horiz">
        <p style="font-weight: bold; color:blue">There aren't very many...</p>
      </div>
      \[
      S = \sum_{i j} x_i M_{ij} x_j + \sum_i \Lambda_i x_i^4
      \]
      <p>The only singularity is at infinity, and it never moves.
      <div class="flex-horiz">
        <span style="font-weight: bold; color:green">No opportunity for a global sign problem!</span>
      </div>
      <footer></footer>
    </section>

    <section id="normalizing">
      <header><h1>Another Perspective: Normalizing Flows</h1></header>
      <span style="text-align: center"><img src="kanwar.png" style="width:65vh" /></span>
      <div class="flex-horiz" style="margin: 2vh; font-size: 80%">
        <div>
          <p>Goal: sample from \(p(z)\)
          <p>A <strong>normalizing flow</strong> is a map \(\phi\)
          \[
          \int_{-\infty}^x e^{-x^2} dx = \int_{-\infty}^{\phi(x)} p(z)\, dz
          \]
          <p style="color: green">Sample from the Gaussian, then apply \(\phi\).
          <p>(Any easily sampled distribution can replace the Gaussian.)
        </div>
        <div style="height: 40vh; border: 1px solid #888888; margin: 2vh"></div>
        <div>
          <p style="color: red; font-weight: bold">What if \(\phi(x)\) is complex?
          <p>Complex normalizing flow \(\Rightarrow\) contour deformation
        </div>
      </div>
      <div class="flex-horiz">
        <span style="font-weight: bold; color: blue; font-size: 90%; padding: 3vh 0">A complex normalizing flow gives a locally perfect contour deformation</span>
      </div>
      <footer></footer>
    </section>

    <section id="analytic-continuation">
      <header><h1>Analytic Continuation of Normalizing Flows</h1></header>
      <p>Here's a normalizing flow for scalar field theory:
      <div class="flex-horiz">
        <img src="sft-nf.png" />
      </div>
      <p>What if \(M,\Lambda\) are complex? Doesn't matter; still a good normalizing flow.
      <div class="flex-horiz">
        <span style="font-weight: bold; color: green">Normalizing flows are analytic in action parameters</span>
      </div>
      <hr />
      <div>
      <div class="flex-horiz">
        <span style="font-weight: bold; color: blue; font-size: 110%; padding: 3vh 0">To summarize:</span>
      </div>
      <ul>
        <li>Normalizing flows always exist for real distributions (theorem)</li>
        <li>These can be analytically continued for complex parameters (ideology)</li>
        <li>Complex normalizing flow gives locally perfect manifold</li>
      </ul>
      </div>
      <footer></footer>
    </section>

    <!--
    <section id="searching">
      <header><h1>Searching For Manifolds</h1></header>
      <p>Evaluating \(\langle \sigma \rangle\) itself is hard. The sign problem manifests as a signal-to-noise problem.
      <p>But, we don't need to! We just need to minimize \(Z_Q\), and
      \[\color{green}
      \frac{d}{d t}\lambda = - \frac{\partial}{\partial \lambda} \log Z_Q
      \]
      has the form of a quenched (i.e., sign-free) observable!
      <hr>
      <p>Training normalizing flows is more difficult &mdash; technical details need to be worked out (and it may not be practical in the complex case).
      <footer>See: 1804.00697, 1808.09799, 1810.06529</footer>
    </section>
    -->


    <section id="part-2">
      <div></div>
      <h1 style="text-align: center;">Part II: Semidefinite Programs</h1>
      <div></div>
    </section>

    <section id="harmonic-oscillator">
      <header><h1>Example: Harmonic Oscillator</h1></header>
      \[
      \hat H = \frac{1}{2} \hat x^2 + \frac{1}{2} \hat p^2
      \]
      <p>Remember: \({\color{green}\langle \Psi|\color{blue}|\Psi\rangle}\ge 0\)
      <p>Of course \({\color{green}\langle 0 | x \color{blue}x | 0 \rangle} \ge 0\) and \({\color{green}\langle 0 | p \color{blue}p | 0 \rangle} \ge 0\). So: \(\langle \hat H \rangle \ge 0\).
      <hr>
      <p><b>We can do better</b>, by using the uncertainty principle: \([x,p] = i\hbar\).
      <div class="flex-horiz">
        <div style="border-right: 1px solid black; padding: 5vh">
          <p>Take \(|\Psi\rangle = (\hat x+i\hat p)|0\rangle\).
          \[
          \langle  (\hat x-i\hat p) (\hat x+i\hat p) \rangle
          =
          \langle \hat x^2 \rangle
          +i\langle[\hat x,\hat p]\rangle
          +\langle \hat p^2 \rangle
          \ge 0
          \]
        </div>
        <div style="padding: 5vh; color: green">
          <p>This is the classic proof that \(\langle \hat H \rangle \ge \frac \hbar 2\) for the harmonic oscillator.
        </div>
      </div>
      <footer></footer>
    </section>

    <section id="positive-semidefinite">
      <header><h1>Being More Systematic</h1></header>
      <p style="text-align:center; color:blue">Let's move from states to operators!</p>
      \[\color{green}
      \langle \mathcal O^\dagger \mathcal O \rangle \ge 0
      \]
      <p>Consider a basis of operators \(\mathcal O_1,\ldots\mathcal O_N\). We can build a matrix
      \[
      M = \left(\begin{matrix}
      \langle \mathcal O_1^\dagger \mathcal O_1 \rangle & \langle \mathcal O_1^\dagger \mathcal O_2\rangle & \cdots \\
      \langle \mathcal O_2^\dagger \mathcal O_1 \rangle & \langle \mathcal O_2^\dagger \mathcal O_2\rangle & \cdots \\
      \vdots & \vdots & \ddots
      \end{matrix}\right)
      \]
      <div style="text-align:center; color:green; font-weight: bold"><p>The matrix \(M\) is positive semi-definite!</p></div>
      <p><em>Proof</em>: for any vector \(v\), consider the operator \(\mathcal O = \sum_i v_i \mathcal O_i\).
      \[
      v^\dagger M v = \langle (\vec v \cdot \mathcal {\vec O})^\dagger (\vec v \cdot \mathcal {\vec O}) \rangle = \langle \mathcal O^\dagger \mathcal O \rangle \ge 0
      \]
      <div style="text-align:center; color:blue"><p>With a complete set of operators, this sums up the <b>entire</b> positivity axiom.</p></div>
      <footer></footer>
    </section>

    <section id="semidefinite-programs">
      <header><h1>Semidefinite Programs</h1></header>
      <div style="text-align: center">This is a specific case of a <b>semi-definite program</b>.</div>
      <p>Let \(C\) be an \(N \times N\) matrix. We wish to find a positive semi-definite matrix \(X\)
      \[
      \text{minimizing }\;\mathrm{Tr}\;CX
      \]
      subject to linear constraints on the matrix elements of \(X\).
      <hr>
      <p>In our context, \(C\) specifies the Hamiltonian, and the linear constraints follow from the commutation relations.

      <p style="text-align:center"><em>Example</em>: \(\langle xp\rangle - \langle px\rangle = i\hbar\)
      <footer></footer>
    </section>

    <section id="conformal-bootstrap">
      <header><h1>Conformal Bootstrap</h1></header>
      <div class="flex-horiz">
      <ul>
        <li>Conformal invariance</li>
        <li>Crossing symmetry</li>
        <li><b>Unitarity</b> \(\Rightarrow\) Radially quantized states have positive norm</li>
        <li>Internal symmetries (\(\mathbb Z_2\))</li>
      </ul>
      <img src="crossing.png" style="width: 70vh"/>
      </div>
      <div style="text-align: center"><img src="conformal-bootstrap.png" style="width:75vh"/></div>
      <footer>See Kos et al: <tt>arXiv:1603.04436</tt></footer>
    </section>

    <section id="matrix-models">
      <header><h1>Matrix Models</h1></header>
      <p>Let \(X,P\) be \(N\times N\) Hermitian matrices of operators, obeying
      \[
      [P_{ij},X_{kl}] = -i \delta_{ik} \delta{jl}
      \]
      <p>Some Hamiltonians
      \[
      \color{green}
      H_{\mathrm{easy}} = \mathrm{Tr}\; P^2 + \mathrm{Tr}\;X^2 + \frac{g}{N}\mathrm{Tr}\;X^4
      \]
      \[\color{red}
      H_{\mathrm{harder}} = \mathrm{Tr}\; P_X^2 + \mathrm{Tr}\; P_Y^2 + m^2 \mathrm{Tr}\;(X^2 + Y^2) - \mathrm{Tr}\;g^2[X,Y]^2
      \]
      <hr>
      <p>Matrix models are quantum mechanical and consistent.
      <p><b>Conjecture</b>: Larger matrix models (e.g. BFSS) exhibit emergent spacetime.
      <footer>See Han, Hartnoll, Kruthoff. Phys. Rev. Lett. 125, 041601.</footer>
    </section>

    <section id="sdp-matrix-models">
      <header><h1>Bootstrapping Matrix Models</h1></header>
      \[
      \color{green}
      H_{\mathrm{easy}} = \mathrm{Tr}\; P^2 + \mathrm{Tr}\;X^2 + \frac{g}{N}\mathrm{Tr}\;X^4
      \]
      <div class="flex-horiz">
      \[
      \langle \mathrm{Tr} \left(\begin{matrix}
      1 & X^2 & 0 & 0\\
      X^2 & X^4 & 0 & 0\\
      0 & 0 & X^2 & XP\\
      0 & 0 & PX & P^2\\
      \end{matrix}\right)\rangle
      \succeq 0
      \]
        <img src="han.png" width="45%" />
      </div>
      <div style="font-size:80%">
      \[\color{red}
      H_{\mathrm{harder}} = \mathrm{Tr}\; P_X^2 + \mathrm{Tr}\; P_Y^2 + m^2 \mathrm{Tr}\;(X^2 + Y^2) - \mathrm{Tr}\;g^2[X,Y]^2
      \]
      <div class="flex-horiz">
        <img src="han2.png" width="37%" />
      </div>
      </div>
      <footer>From Han, Hartnoll, Kruthoff. Phys. Rev. Lett. 125, 041601.</footer>
    </section>

    <section id="fermion-bootstrap">
      <header><h1>Bootstrapping Fermions</h1></header>
      <p>The lattice Thirring model (staggered fermions):
      <div style="font-size:70%">
      \[
      H = \sum_x (-1)^x m \chi^\dagger(x) \chi(x) + {\color{red} \mu \chi^\dagger(x) \chi(x)} + (-1)^x\frac{\chi^\dagger(x) \chi(x+1) + \mathrm{h.c.}}{2}
      +g^2 \chi(x) \chi^\dagger(x)\chi^\dagger(x+1)\chi(x+1)
      \]
      \[
      \text{ where }\;
      \{\chi(x),\chi(y)\} = 0
      \text{ and }
      \{\chi(x)^\dagger,\chi(y)\} = \delta_{xy}
      \]
      </div>
      <p>At finite chemical potential, lattice Monte Carlo exhibits the <span style="color:red">sign problem</span>.
      <div class="flex-horiz">
        <img src="fermion-bootstrap.png" width="40%"/>
        <ul>
          <li>Operators: all \(\chi(x)\) and \(\chi^\dagger(x)\)
          <li>\(L = 4\)
          <li>\(m = 0.5\)
          <li>\(g^2 = 0.1\)
        </ul>
      </div>
      <p style="text-align: center; color: green; font-weight: bold;">No evidence of a sign problem</p>
      <footer></footer>
    </section>

    <section id="future">
      <header><h1>Where Now?</h1></header>
      <div class="flex-horiz">
        <div>
          <div style="text-align:center"><b>Contour deformations</b></div>
          <ul>
            <li style="color:green">Deep neural network ansatze for contours (ongoing)
            <li style="color:red">Can we prove fermionic models lack globally perfect contours?
          </ul>
        </div>
        <div style="height: 70vh; border: 1px solid black"></div>
        <div>
          <div style="text-align:center"><b>Bootstrap</b></div>
          <ul>
            <li>Continuum limit
            <li>Expand to gauge theories
            <li>Technical advances needed to add more operators to the basis
            <li>Thermal physics
          </ul>
        </div>
      </div>
      <footer></footer>
    </section>

    <!-- CLOSING -->
    <section id="closing">
      <header></header>
      <div style="text-align: center;font-size: 5vh;">FIN</div>
      <footer></footer>
    </section>

    <div id="present-control">
      <div id="position"><span id="pos-cur"></span>/<span id="pos-len"></span></div>
      <a href='javascript:prev()'>&lt;</a>
      <a href='javascript:next()'>&gt;</a>
    </div>
  </body>
</html>
