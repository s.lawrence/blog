<!DOCTYPE html>
<html>
  <head>
    <title>
      Convexity and Quantum Physics
    </title>
    <link rel="stylesheet" href="present.css" />
    <link rel="stylesheet" media="print" href="print.css" />
    <script src='present.js'></script>
    <script src='vis.js'></script>
    <script async src='/mathjax/tex-svg-full.js'></script>
    <!--<script src="plotly-2.9.0.min.js"></script>-->
  </head>
  <body>
    <noscript>
      <div style="font-size: 30pt;">Javascript really is required, sorry</div>
    </noscript>

    <!-- TITLE -->
    <header id="title">
      <h1>Convexity and Quantum Physics</h1>
      <div>
        <img src="title.svg" style="height: 40vh; text-align: center;"/>
        <img src="cern.jpeg" style="width: 36vh"/>
      </div>
      <div class="author">Scott Lawrence</div>
      <div style="font-size: 18pt">Based on <tt>arXiv:2111.13007</tt> and <tt>arXiv:2211.08874</tt></div>
      <div>16 December 2022</div>
      <div>
        <img src="boulder.png" style="width: 35vh; padding: 0 1em 0.6em 1em" />
        <!--<img src="uiowa.png" style="width: 25vh; padding: 0em 1em 0em 1em" />-->
      </div>
      <div></div>
    </header>

    <section id="axioms">
      <header><h1>Ingredients of Quantum Mechanics</h1></header>
      <ul>
        <li>Vector space of states (\(|\psi\rangle\))</li>
        <li><span style="color:blue"><b>Positive-semidefinite</b> inner product: \(\langle \psi | \psi \rangle \ge 0\)</span></li>
        <li>Hermitian observables</li>
        <li>Hamiltonian (Hermitian generator of time evolution)</li>
      \[
      U(t) = e^{-i H t}
      \]
        <li><span style="color:green"><b>Ground state</b> \(|\Omega\rangle\) minimizing \(\langle \Omega | H| \Omega\rangle\)</span></li>
      </ul>
      <hr>
      <div class="flex-horiz">
        <span><b>Questions we can ask</b></span>
      </div>
      <ul>
        <li><b>Ground state properties</b></li>
        <li>Thermal properties</li>
        <li>Time-dependent behavior (near or far from equilibrium)</li>
      </ul>
      <footer></footer>
    </section>

    <section id="harmonic-oscillator">
      <header><h1>Example: Harmonic Oscillator</h1></header>
      \[
      \hat H = \frac{1}{2} \hat x^2 + \frac{1}{2} \hat p^2
      \]
      <p>Remember: \({\color{green}\langle \Psi|\color{blue}|\Psi\rangle}\ge 0\)
      <p>Of course \({\color{green}\langle 0 | x \color{blue}x | 0 \rangle} \ge 0\) and \({\color{green}\langle 0 | p \color{blue}p | 0 \rangle} \ge 0\). So: \(\langle \hat H \rangle \ge 0\).
      <hr>
      <p><b>We can do better</b>, by using the uncertainty principle: \([x,p] = i\hbar\).
      <div class="flex-horiz">
        <div style="border-right: 1px solid black; padding: 5vh">
          <p>Take \(|\Psi\rangle = (\hat x+i\hat p)|0\rangle\).
          \[
          \langle  (\hat x-i\hat p) (\hat x+i\hat p) \rangle
          =
          \langle \hat x^2 \rangle
          +{\color{red}i\langle[\hat x,\hat p]\rangle}
          +\langle \hat p^2 \rangle
          \ge 0
          \]
        </div>
        <div style="padding: 5vh; color: green">
          <p>This is the classic proof that \(\langle \hat H \rangle \ge \frac \hbar 2\) for the harmonic oscillator.
        </div>
      </div>
      <footer></footer>
    </section>

    <section id="one-spin">
      <header><h1>Example: A Spin</h1></header>
      <div class="flex-horiz">
      \[
      H = - \mu \sigma_x
      \]
        <img src="spin.svg" />
      </div>
      <p>Define \(|\Psi\rangle = (1 - \sigma_x)|0\rangle\)
      \[
      {\color{red}\langle \Psi | \Psi\rangle \ge 0}
      \Longrightarrow
      {\color{blue}\langle 2 - 2\sigma_x \rangle \ge 0}
      \Longrightarrow
      {\color{green}- \langle \sigma_x \rangle \ge -1}
      \]
      <p>So \(\langle H\rangle \ge -\mu\) (as expected!)
      <div style="font-weight: bold; color:red; text-align: center">
        <p>Both bounds happen to be <em>tight</em>. This won't always happen.</p>
      </div>
      <footer></footer>
    </section>

    <section id="positive-semidefinite">
      <header><h1>Being More Systematic</h1></header>
      <p style="text-align:center; color:blue">Let's move from states to operators!</p>
      \[\color{green}
      \langle \mathcal O^\dagger \mathcal O \rangle \ge 0
      \]
      <p>Consider a basis of operators \(\mathcal O_1,\ldots\mathcal O_N\). We can build a matrix
      \[
      M = \left(\begin{matrix}
      \langle \mathcal O_1^\dagger \mathcal O_1 \rangle & \langle \mathcal O_1^\dagger \mathcal O_2\rangle & \cdots \\
      \langle \mathcal O_2^\dagger \mathcal O_1 \rangle & \langle \mathcal O_2^\dagger \mathcal O_2\rangle & \cdots \\
      \vdots & \vdots & \ddots
      \end{matrix}\right)
      \]
      <div style="text-align:center; color:green; font-weight: bold"><p>The matrix \(M\) is positive semi-definite!</p></div>
      <p><em>Proof</em>: for any vector \(v\), consider the operator \(\mathcal O = \sum_i v_i \mathcal O_i\).
      \[
      v^\dagger M v = \langle (\vec v \cdot \mathcal {\vec O})^\dagger (\vec v \cdot \mathcal {\vec O}) \rangle = \langle \mathcal O^\dagger \mathcal O \rangle \ge 0
      \]
      <div style="text-align:center; color:blue"><p>With a complete set of operators, this sums up the <b>entire</b> positivity axiom.</p></div>
      <footer></footer>
    </section>

    <section id="semidefinite-programs">
      <header><h1>Semidefinite Programs</h1></header>
      <div style="text-align: center">This is a specific case of a <b>semi-definite program</b>.</div>
      <p>Let \(C\) be an \(N \times N\) matrix. We wish to find a positive semi-definite matrix \(X\)
      \[
      \text{minimizing }\;\mathrm{Tr}\;CX
      \]
      subject to <span style="color: blue">linear constraints on the matrix elements of \(X\).</span>
      <hr>
      <p>In our context, \(C\) specifies the Hamiltonian, and the linear constraints follow from the commutation relations.

      <p style="text-align:center;color:blue"><em>Example</em>: \(\langle xp\rangle - \langle px\rangle = i\hbar\)
      <footer></footer>
    </section>

    <section id="one-spin-again">
      <header><h1>One Spin, Again</h1></header>
      <p>Here's a basis of operators: \(\{1, \sigma_x, \sigma_y, \sigma_z\}\). (In fact this basis is <b>complete</b>.)</p>
      \[\color{green}
      \left(\begin{matrix}
      1 & \langle \sigma_x \rangle  & \langle \sigma_y \rangle & \langle \sigma_z \rangle\\
      \langle \sigma_x \rangle & 1 & i \langle \sigma_z \rangle & -i \langle \sigma_y \rangle\\
      \langle \sigma_y \rangle & -i \langle \sigma_z \rangle & 1 & i\langle \sigma_x \rangle\\
      \langle \sigma_z \rangle & i\langle \sigma_y \rangle & -i \langle \sigma_x \rangle & 1
      \end{matrix}\right)\succeq 0
      \]
      <p>Note that <em>any</em> assignment of expectation values obeying the above constraint corresponds to some density matrix \(\rho\).
      <p>For \(H = -\mu \sigma_x\), only the upper-left \(2\times 2\) minor is needed.
      \[
      \left|\begin{matrix}
      1 & \langle \sigma_x \rangle\\
      \langle \sigma_x \rangle & 1
      \end{matrix}\right|\ge 0
      \Longrightarrow
      1 - \langle \sigma_x \rangle^2 \ge 0
      \]
      <footer></footer>
    </section>

    <section id="visualizing">
      <header><h1>Visualizing The One-Spin SDP</h1></header>
      <div class="flex-horiz">
        \[
        \left(\begin{matrix}
        1 & \langle \sigma_x \rangle\\
        \langle \sigma_x \rangle & 1
        \end{matrix}\right)\succeq 0
        \]
        <img src="vis.png" style="width: 80vh" />
      <!--
      <div id="spin-vis" style="width: 40vh; height: 20vh"></div>
      -->
      </div>
      <script>
      </script>
      <footer></footer>
    </section>

    <section id="ho-again">
      <header><h1>Harmonic Oscillator, Again</h1></header>
      <p>We can't use a complete basis. Let's start with \(\{1,x,p\}\).
      \[\color{green}
      \left(\begin{matrix}
      1 & \langle x\rangle & \langle p \rangle\\
      \langle x \rangle & \langle x^2 \rangle & \langle xp \rangle\\
      \langle p \rangle & \langle xp \rangle + i\hbar & \langle p^2 \rangle
      \end{matrix}\right)\succeq 0
      \]
      <p>This is just one small part of the "full" SDP, but it represents several important constraints!
      <p>Using the lower \(2\times 2\) minor:
      \[
      \left|\begin{matrix}
      \langle x^2 \rangle & \langle xp \rangle\\
      \langle xp \rangle + i\hbar & \langle p^2 \rangle
      \end{matrix}\right|
      =
      \langle x^2 \rangle \langle p^2\rangle - \langle xp\rangle^2 - i \hbar \langle xp \rangle
      \ge 0
      \Longrightarrow
      \langle x^2\rangle \langle p^2\rangle \ge \hbar
      \]
      <p style="color: blue; font-weight: bold; text-align: center">Adding more operators can only make the bound stricter.
      <footer></footer>
    </section>    <section id="sum-of-squares">
      <header><h1>Noncommutative Sum-of-Squares</h1></header>
      <div class="flex-horiz">
        <div>
        \[ H = -\mu \sigma_x = \color{green}\frac 1 2 (1+\sigma_x)(1+\sigma_x) - 1\]
        <p>From which we see that \(\color{blue}\langle H \rangle \ge -1\)
        </div>
        <div style="height: 25vh; border: 1px solid black"></div>
        <div>
        \[ H = \frac 1 2 x^2 + \frac 1 2 p^2 = \color{green}a^\dagger a + \frac 1 2\]
        <p>Similarly, \(\color{blue}\langle H \rangle \ge \frac 1 2\)
        </div>
      </div>
      <hr>
      <p>In general, if we can write
      \[
      H = C + \mathcal O_1^\dagger \mathcal O_1 + \mathcal O_2^\dagger \mathcal O_2 + \cdots
      \]
      <p>then the bound \(\color{blue}\langle H \rangle \ge C\) follows immediately.
      <div class="flex-horiz">
        <span style="font-weight: bold; color: #885500">
          This is equivalent to the semi-definite program formulation.
        </span>
      </div>
      <footer></footer>
    </section>

    <section id="recap">
      <header><h1>Summary</h1></header>
      <ul>
        <li>The space of permitted expectation values is <em>convex</em></li>
        <li>The ground state sits at the boundary (the lowest possible value of \(\langle H \rangle\))</li>
        <li>Any sum-of-squares representation yields a rigorous lower bound</li>
        <li>Equivalently, solving a truncated SDP yields a rigorous lower bound</li>
      </ul>
      <hr>
      <div class="flex-horiz"><span style="font-weight: bold">Coming up:</span></div>
      <p style="color: blue">Quantum information
      <p style="color: green">Conformal bootstrap: a history lesson
      <p style="color: red">Computational applications (matrix models, lattice field theories)
      <footer></footer>
    </section>

    <section id="lattice-field-theory">
      <header><h1>Lattice Field Theory</h1></header>
      <div class="flex-horiz">
        <img src="lattice-points.svg" style="width: auto;height:20vh;"/>
      </div>
      <p> Each lattice site has a degree of freedom with Hilbert space \(\mathcal H_1\). The whole system has Hilbert space
      $$
      \mathcal H = \mathcal H_1 \otimes \mathcal H_1 \otimes \cdots
      $$
      <p>Some Hamiltonian \(H\) couples the different lattice sites. For a spin system, we might have
      $$
      H = \sum_{\langle i j\rangle} \sigma_z(i) \sigma_z(j) + \sum_i \sigma_x(i)
      $$
      <p style="color: blue">When correlations are large, the lattice structure is irrelevant, hence "field theory".
      <footer></footer>
    </section>

    <section id="bootstrap-lattice">
      <header><h1>The Bootstrap on the Lattice</h1></header>
      \[
      H = \sum_x \left[\frac 1 2 \pi(x)^2 + \frac 1 2 (\phi(x) - \phi(x-1))^2 + \frac{m^2}{2} \phi(x)^2 + \lambda \phi(x)^4\right]
      \]
      <div class="flex-horiz" style="padding: 2vh">
        <div>
        <p>With \(m = 0.2\)</p>
        <p>Operators:
        <ul>
          <li>\(1,\pi(0)\)</li>
            <li>\(\phi(0),\phi(1),\ldots,\phi(L)\)</li>
            <li>\(\phi(0)^2,\phi(0)^3,\ldots,\phi(0)^N\)</li>
        </ul>
        </div>
        <img src="scalar-energy.png" width="45%"/>
      </div>
      <div class="flex-horiz"><p style="color:green">In the infinite-volume limit!</p></div>
      <footer>See S.L., <tt>arXiv:2111.13007</tt></footer>
    </section>

    <section id="finite-density">
      <header><h1>A Hard Problem: Finite Density Fermions</h1></header>
      <div class="flex-horiz" style="margin: 2vh">
        <div>
          <p>Monte Carlo (i.e. lattice QCD) methods are powerful, but fail for relativistic theories at finite fermion (proton - antiproton) density.
          <p>First noted in 1990. Not well-understood in general.
          <p>Makes it difficult to study:
          <ul>
            <li>Superconductors (and other strongly interacting electrons)</li>
            <li>Large nuclei</li>
            <li><b>Dense nuclear matter (neutron stars)</b></li>
          </ul>
        </div>
        <div style="border-left: 1px solid #222; padding-left: 1vh">
          <div class="flex-horiz"><img src="neutron-star.jpg" style="width: 25vh; padding: 2vh"/></div>
          <div class="flex-horiz"><img src="mr.jpg" style="width: 60vh"/></div>
        </div>
      </div>
      <footer></footer>
    </section>

    <section id="fermion-bootstrap">
      <header><h1>Bootstrapping Fermions</h1></header>
      <p>The lattice Thirring model (staggered fermions):
      <div style="font-size:70%">
      \[
      H = \sum_x (-1)^x m \chi^\dagger(x) \chi(x) + {\color{red} \mu \chi^\dagger(x) \chi(x)} + (-1)^x\frac{\chi^\dagger(x) \chi(x+1) + \mathrm{h.c.}}{2}
      +g^2 \chi(x) \chi^\dagger(x)\chi^\dagger(x+1)\chi(x+1)
      \]
      \[
      \text{ where }\;
      \{\chi(x),\chi(y)\} = 0
      \text{ and }
      \{\chi(x)^\dagger,\chi(y)\} = \delta_{xy}
      \]
      </div>
      <p>At finite chemical potential, lattice Monte Carlo exhibits the <span style="color:red">sign problem</span>.
      <div class="flex-horiz">
        <img src="fermion-figures/figure2-left.png" width="40%"/>
        <ul>
          <li>\(L = 10\)
          <li>\(m = 0.05\)
          <li>\(g^2 = 0.5\)
          <li>(Operators on next slide)
        </ul>
      </div>
      <p style="text-align: center; color: green; font-weight: bold;">No evidence of a sign problem</p>
      <footer>S.L., <tt>arXiv:2211.08874</tt></footer>
    </section>

    <section id="fermion-operators">
      <header><h1>Fermion Operators</h1></header>
      <p>Four operator bases are used; strictly increasing:
      <ul>
        <li>H0: Only \(c^\dagger\), \(c\), and \(c^\dagger c\) at every site. (The minimum necessary!)</li>
        <li style="color:red">H1: Add \(c^\dagger(x) c(x+1)\) (and complex conjugate).</li>
        <li style="color:blue">H2: Add \(c^\dagger(x) c(x) c(x+1) c^\dagger(x+1)\).</li>
        <li style="color:green">C1: Add \(c^\dagger(x) c(x+1) c^\dagger(x+2) c(x+2)\).</li>
      </ul>
      <hr/>
      <p>"Greedy algorithm" for adding more (described in <tt>arXiv:2111.13007</tt>, SL):
      <ul>
        <li>List all operators that could potentially improve the bound</li>
        <li>Test adding each one</li>
        <li>Permanently add the one that increases the bound by the largest amount</li>
      </ul>
      <footer></footer>
    </section>

    <section id="fermion-bootstrap-2">
      <header><h1>More Bootstrapped Fermions</h1></header>
      <p>Again on a 10-site lattice:
      <div class="flex-horiz">
        <img src="fermion-figures/figure2-left.png" width="40%"/>
        <img src="fermion-figures/figure2-right.png" width="40%"/>
      </div>
      <p>And then at "infinite" volume:
      <div class="flex-horiz">
        <img src="fermion-figures/figure5-left.png" width="40%"/>
        <img src="fermion-figures/figure5-right.png" width="40%"/>
      </div>
      <footer></footer>
    </section>

    <section id="fermion-bootstrap-infinite">
      <header><h1>Bootstrapping at Infinite Volume</h1></header>
      <div class="flex-horiz">
        <div style="width: 70vh">
      <p>Minimize <span style="color:blue">hamiltonian density</span> \(\hat h(0)\)
      <p>Include operators based at \(x \in [-L,L+1]\).
      <p>Assert <span style="color:red">translational invariance</span>: \(\langle \mathcal O(x)\rangle = \langle \mathcal O(x+2r) \rangle\)
      <p>The Hilbert space is that of the infinite-volume system.
        </div>
      <img src="fermion-figures/figure4.png" width="45%"/>
      </div>
      <footer></footer>
    </section>

    <section id="open-problems">
      <header><h1>Open Problems</h1></header>
      <div class="flex-horiz">
        <div>
          <div style="text-align:center"><b>Direct extensions</b></div>
        <ul style="padding-left: 2em">
          <li>QM fermions</li>
          <li>Continuum limit</li>
          <li>Finite temperature</li>
          <li>Time-separated correlation functions</li>
        </ul>
        </div>
      <div style="height: 70vh; border: 1px solid black"></div>
      <ul style="padding-left: 2em">
        <li style="color:red"><b>Better tools</b>. Faster, easier to use.</li>
        <li style="color:blue">Understand scaling:
          <ul>
            <li>For QM bootstrap, getting \(d\) digits takes polynomial time, empirically. Why?</li>
            <li>For field theories, it's unknown</li>
            <li>Why is the conformal bootstrap so successful?</li>
          </ul>
        <li style="color:green">Lattice QCD</li>
        <li>Momentum space?</li>
        <li>Euclidean path integrals (see <tt>arXiv:2206.12538</tt>; Cho et al) </li>
      </ul>
      </div>
      <footer></footer>
    </section>

    <!-- CLOSING -->
    <section id="closing">
      <header></header>
      <div style="text-align: center;font-size: 5vh;">FIN</div>
      <footer></footer>
    </section>

    <div id="present-control">
      <div id="position"><span id="pos-cur"></span>/<span id="pos-len"></span></div>
      <a href='javascript:prev()'>&lt;</a>
      <a href='javascript:next()'>&gt;</a>
    </div>
  </body>
</html>
