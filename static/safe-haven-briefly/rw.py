#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np

T = 1000

def sample(frac, bias):
    x = 100
    xs = []
    ts = []
    for t in range(T+1):
        ts.append(t)
        xs.append(x)
        if np.random.uniform() < 0.5:
            x *= (1+bias*frac)
        else:
            x *= (1-frac)
    return ts, xs

plt.figure(figsize=(6,4), dpi=300)
plt.plot(range(T+1), [100]*(T+1), color='black')
for _ in range(10):
    plt.plot(*sample(2e-3, 1.2), color='red')
for _ in range(10):
    plt.plot(*sample(1e-2, 1.2), color='blue')
plt.ylabel('Account balance')
plt.xlabel('Investment round')
plt.xlim([0,T])
plt.tight_layout()
plt.savefig('mc.png', transparent=True)
plt.show()
