
// TODO perspective
// TODO fog

//const Nboids = 100;
const Nboids = 10;

const vsSource = `
attribute vec4 vp;

varying lowp vec4 color;

uniform vec4 uPosition;
uniform vec4 uVelocity;
uniform mat4 uMVM;
uniform mat4 uPM;
uniform mat4 uAM;

uniform bool uPred;

void main() {
	vec4 dir = normalize(uVelocity);
	vec4 perp1 = vec4(1,0,0,0);
	vec4 perp2 = vec4(0,1,0,0);
	perp1 -= dot(perp1, dir)*dir;
	perp1 = normalize(perp1);
	perp2 -= dot(perp2, dir)*dir;
	perp2 = normalize(perp2);
	perp2 -= dot(perp2, perp1)*perp1;
	perp2 = normalize(perp2);
	mat4 rot = mat4(perp1, perp2, dir, vec4(0,0,0,1));
	gl_Position = uAM * uPM * uMVM * (rot*vp + uPosition);
	if (uPred) {
		color = vec4(0.4 + 1.5*vp.z, 0.1 + 1.*vp.z, 0.1 + 1.*vp.z, 1.);
	} else {
		color = vec4(0.2 + 2.*vp.z, 0.2 + 2.*vp.z, 0.2 + 2.*vp.z, 1.);
	}
}
`;

const fsSource = `
varying lowp vec4 color;

void main() {
	//gl_FragColor = vec4(1., 1., 1., 1.);
	gl_FragColor = color;
}
`;

function norm3(v) {
	return Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

function matmul(a, b) {
	var c = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	for (var i = 0; i < 4; i++)
		for (var j = 0; j < 4; j++)
			for (var k = 0; k < 4; k++)
				c[i*4+j] += a[i*4+k]*b[k*4+j];
	return c;
}

function start() {
	const canvas = document.querySelector("#glCanvas");
	const gl = canvas.getContext("webgl", {antialias: true});
	var aspect = 1.;

	window.onresize = function () {
		canvas.height = gl.canvas.clientHeight;
		canvas.width = gl.canvas.clientWidth;
		aspect = canvas.width / canvas.height;
		gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
	}
	window.onresize();

	gl.loadShader = function (type, src) {
		const shader = gl.createShader(type);
		gl.shaderSource(shader, src);
		gl.compileShader(shader);
		return shader;
	}

	/* Initialize shaders */
	const vertexShader = gl.loadShader(gl.VERTEX_SHADER, vsSource);
	const fragmentShader = gl.loadShader(gl.FRAGMENT_SHADER, fsSource);
	const shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);
	gl.useProgram(shaderProgram);
	const vpPos = gl.getAttribLocation(shaderProgram, 'vp');

	const uPosition = gl.getUniformLocation(shaderProgram, 'uPosition');
	const uVelocity = gl.getUniformLocation(shaderProgram, 'uVelocity');
	const uAM = gl.getUniformLocation(shaderProgram, 'uAM');
	const uPM = gl.getUniformLocation(shaderProgram, 'uPM');
	const uMVM = gl.getUniformLocation(shaderProgram, 'uMVM');
	const uPred = gl.getUniformLocation(shaderProgram, 'uPred');

	//gl.enable(gl.BLEND);
	gl.enable(gl.DEPTH_TEST);

	gl.clearColor(0., 0., 0., 1.);
	gl.clearDepth(1.0);

	// TODO make cone
	const boidBuffer = gl.createBuffer();
	/*
	const boidGeometry = new Float32Array([
		0., 0., 2.,   1., 0., 0.,    0., 1., 0.,
		0., 0., 2.,   0., 1., 0.,    -1., 0., 0.,
		0., 0., 2.,   -1., 0., 0.,    0., -1., 0.,
		0., 0., 2.,   0., -1., 0.,    1., 0., 0.,
		0., 0., -0.3,   1., 0., 0.,    0., 1., 0.,
		0., 0., -0.3,   0., 1., 0.,    -1., 0., 0.,
		0., 0., -0.3,   -1., 0., 0.,    0., -1., 0.,
		0., 0., -0.3,   0., -1., 0.,    1., 0., 0.,
	]).map(x => x*0.07);*/
	const boidResolution = 50;
	const boidGeometry = new Float32Array(boidResolution * 2 * 3 * 3);
	for (var i = 0; i < boidResolution; i++) {
		theta = 2*Math.PI * i / (boidResolution);
		thetap = 2*Math.PI * (i+1) / (boidResolution);
		// Upper half
		boidGeometry[i*2*3*3 + 0] = 0.;
		boidGeometry[i*2*3*3 + 1] = 0.;
		boidGeometry[i*2*3*3 + 2] = 2.;
		boidGeometry[i*2*3*3 + 3] = Math.cos(theta);
		boidGeometry[i*2*3*3 + 4] = Math.sin(theta);
		boidGeometry[i*2*3*3 + 5] = 0.;
		boidGeometry[i*2*3*3 + 6] = Math.cos(thetap);
		boidGeometry[i*2*3*3 + 7] = Math.sin(thetap);
		boidGeometry[i*2*3*3 + 8] = 0.;

		// Lower half
		boidGeometry[i*2*3*3 + 0 + 9] = 0.;
		boidGeometry[i*2*3*3 + 1 + 9] = 0.;
		boidGeometry[i*2*3*3 + 2 + 9] = -0.4;
		boidGeometry[i*2*3*3 + 3 + 9] = Math.cos(theta);
		boidGeometry[i*2*3*3 + 4 + 9] = Math.sin(theta);
		boidGeometry[i*2*3*3 + 5 + 9] = 0.;
		boidGeometry[i*2*3*3 + 6 + 9] = Math.cos(thetap);
		boidGeometry[i*2*3*3 + 7 + 9] = Math.sin(thetap);
		boidGeometry[i*2*3*3 + 8 + 9] = 0.;

	}
	gl.bindBuffer(gl.ARRAY_BUFFER, boidBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, boidGeometry.map(x => x*0.07), gl.STATIC_DRAW);

	const predBuffer = gl.createBuffer();
	const c2 = Math.cos(2*Math.PI/3);
	const s2 = Math.sin(2*Math.PI/3);
	const c4 = Math.cos(4*Math.PI/3);
	const s4 = Math.sin(4*Math.PI/3);
	const predGeometry = new Float32Array([
		1., 0., 0.,   c2, s2, 0.,   0., 0., 2.,
		c2, s2, 0.,   c4, s4, 0.,   0., 0., 2.,
		c4, s4, 0.,   1., 0., 0.,   0., 0., 2.,
		1., 0., 0.,   c2, s2, 0.,   c4, s4, 0.,
	]).map(x => x*0.1);
	gl.bindBuffer(gl.ARRAY_BUFFER, predBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, predGeometry, gl.STATIC_DRAW);

	var proj = [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1];

	window.onmousemove = function (ev) {
		if (ev.buttons == 1) {
			const ya = ev.movementX / 100;
			const xa = -ev.movementY / 100;
			const rotx = [1,0,0,0, 0,Math.cos(xa),Math.sin(xa),0, 0,-Math.sin(xa),Math.cos(xa),0, 0,0,0,1];
			const roty = [Math.cos(ya),0,Math.sin(ya),0, 0,1,0,0, -Math.sin(ya),0,Math.cos(ya),0, 0,0,0,1];
			const rot = matmul(rotx, roty);
			proj = matmul(proj, rot)
		}
	}

	window.ontouchstart = function (ev) {
		var px = ev.touches[0].clientX;
		var py = ev.touches[0].clientY;
		window.ontouchmove = function (ev) {
			var mx = ev.touches[0].clientX - px;
			var my = ev.touches[0].clientY - py;

			const ya = mx / 100;
			const xa = -my / 100;
			const rotx = [1,0,0,0, 0,Math.cos(xa),Math.sin(xa),0, 0,-Math.sin(xa),Math.cos(xa),0, 0,0,0,1];
			const roty = [Math.cos(ya),0,Math.sin(ya),0, 0,1,0,0, -Math.sin(ya),0,Math.cos(ya),0, 0,0,0,1];
			const rot = matmul(rotx, roty);
			proj = matmul(proj, rot)

			px = ev.touches[0].clientX;
			py = ev.touches[0].clientY;
		}
		window.ontouchcancel = window.ontouchend = function (ev) {
			window.ontouchmove = window.ontouchcancel = window.ontouchend = null;
		}
	};

	// TODO later, we'll just interpret a drag as an instruction to turn
	// the predator. The rotation of the view is just along for the ride.


	/* Initialize the boids. */
	const pos = new Array(Nboids);
	const vel = new Array(Nboids);
	for (var i = 0; i < Nboids; i++) {
		pos[i] = new Array(3);
		for (var j = 0; j < 3; j++)
			pos[i][j] = Math.random()*2-1;
		vel[i] = new Array(3);
		for (var j = 0; j < 3; j++)
			vel[i][j] = (Math.random()*2-1)/10;
	}

	/* Initialize the predatoid. */
	const predPos = [0., 0., -2.5];
	const predVel = [0., 0., 1.];


	const update = function(dt) {
		for (var i = 0; i < Nboids; i++) {
			pos[i][0] += dt*vel[i][0];
			pos[i][1] += dt*vel[i][1];
			pos[i][2] += dt*vel[i][2];

			for (var j = 0; j < Nboids; j++) {
				if (j == i) continue;
				const offset = new Float32Array(3);
				for (var k = 0; k < 3; k++)
					offset[k] = pos[j][k] - pos[i][k];
				const dist = norm3(offset);
				// Avoid other boids.
				if (dist < 0.4) {
					for (var k = 0; k < 3; k++)
						vel[i][k] -= dt * offset[k] * 0.2;
				}
				
				// Align with other boids.
				if (dist < 1.) {
					var a = Math.pow(0.98, dt);
					for (var k = 0; k < 3; k++) {
						vel[i][k] = a * vel[i][k] + (1-a) * vel[j][k];
					}
				}

				// Maintain a reasonable speed
				const speed = norm3(vel[i]);
				const acc = Math.pow(0.4/speed, dt);
				for (var k = 0; k < 3; k++) {
					vel[i][k] *= acc;
				}
			}

			// Avoid walls.
			var radius = 2.;
			var margin = 0.05;
			var mvel = norm3(vel[i]);
			var mpos = norm3(pos[i]);
			if (mpos > radius - margin) {
				for (var j = 0; j < 3; j++)
					vel[i][j] -= dt * mvel * pos[i][j] * 0.2;
			}
		}
	}

	var last_ms = 0;
	const render = function (ms) {
		update((ms - last_ms)/1000);
		requestAnimationFrame(render);
		last_ms = ms;

		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		// Draw boids.
		gl.uniform1i(uPred, 0);
		gl.uniform4fv(uPosition, [0,0,0,0]);
		gl.uniform4fv(uVelocity, [0,0,0,0]);
		gl.uniformMatrix4fv(uAM, false, [1/aspect*0.4,0,0,0,0,0.4,0,0,0,0,0.1,0,0,0,0,1]);
		gl.uniformMatrix4fv(uPM, false, proj);
		gl.uniformMatrix4fv(uMVM, false, [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]);

		gl.bindBuffer(gl.ARRAY_BUFFER, boidBuffer);
		gl.vertexAttribPointer(vpPos, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(vpPos);
	
		for (var i = 0; i < Nboids; i++) {
			gl.uniform4fv(uPosition, [pos[i][0],pos[i][1],pos[i][2],0]);
			gl.uniform4fv(uVelocity, [vel[i][0],vel[i][1],vel[i][2],0]);
			gl.drawArrays(gl.TRIANGLES, 0, boidResolution*2*3);
		}

		// Draw predatoid.
		gl.uniform1i(uPred, 1);
		gl.uniform4f(uPosition, predPos[0], predPos[1], predPos[2], 0.);
		gl.uniform4f(uVelocity, predVel[0], predVel[1], predVel[2], 0.);
		
		gl.bindBuffer(gl.ARRAY_BUFFER, predBuffer);
		gl.vertexAttribPointer(vpPos, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(vpPos);

		gl.drawArrays(gl.TRIANGLES, 0, 12);
	}
	requestAnimationFrame(render);
}
